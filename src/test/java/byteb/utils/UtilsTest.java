package byteb.utils;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testTwoDimensionalArrayFromString() {
        String str = "[[0,1],[1,2],[2,3],[1,3],[1,4]]";
        var arr = Utils.twoDimensionalArrayFromString(str);
        assertEquals(arr, new int[][]{{0, 1}, {1, 2}, {2, 3}, {1, 3}, {1, 4}});
    }
}