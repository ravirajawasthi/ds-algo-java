package byteb.nptel.week4;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class DSUImplTest {
    @Test
    public void DSUTest() {
        var dsu = new DSUImpl.DSU(10);
        assertEquals(dsu.findParent(10), 10);

        dsu.union(1, 2);
        dsu.union(5, 6);
        dsu.union(9, 10);

        assertEquals(dsu.findParent(10), dsu.findParent(9));
        assertEquals(dsu.findParent(5), dsu.findParent(6));
        assertEquals(dsu.findParent(1), dsu.findParent(2));

        dsu.union(5, 1);

        assertEquals(dsu.findParent(2), dsu.findParent(6));
        assertEquals(dsu.findParent(1), dsu.findParent(6));
        assertEquals(dsu.findParent(1), dsu.findParent(5));

        dsu.union(2, 9);

        assertEquals(dsu.findParent(1), dsu.findParent(10));
        assertEquals(dsu.findParent(6), dsu.findParent(9));


    }
}