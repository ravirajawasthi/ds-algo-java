package byteb.nptel.week4;

import org.testng.annotations.Test;

public class DSUMinMaxImplTest {

    @Test
    void stressTest() {
        var dsu = new DSUMinMaxImpl.DSU(100_000_000);
        var time = System.currentTimeMillis();
        for (int i = 2; i <= 100_000_000; i++) {
            dsu.union(1, i);
        }
        var execTime = System.currentTimeMillis();
        System.out.println((execTime - time));
        System.out.println(dsu.getNodeInfo(dsu.findParent(4)));
    }

}