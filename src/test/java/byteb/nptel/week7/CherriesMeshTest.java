package byteb.nptel.week7;

import org.testng.annotations.Test;

import java.io.*;
import java.util.Scanner;

import static org.testng.Assert.assertEquals;

public class CherriesMeshTest {

    @Test
    public void set1Test() throws FileNotFoundException {
        FileInputStream is = new FileInputStream("src/test/resources/testdata/cherriesmesh/set1/in");
        File expectedOutputFile = new File("src/test/resources/testdata/cherriesmesh/set1/out");
        String expectedOutputString = new Scanner(expectedOutputFile).useDelimiter("\\Z").next();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        CherriesMesh.InputReader in = new CherriesMesh.InputReader(is);
        PrintWriter out = new PrintWriter(output);

        new CherriesMesh.CherriesMeshTask().solve(in, out);
        out.flush();


        String actualOutputString = output.toString().trim();
        assertEquals(actualOutputString, expectedOutputString);
    }

    @Test
    public void set2Test() throws FileNotFoundException {
        FileInputStream is = new FileInputStream("src/test/resources/testdata/cherriesmesh/set2/in");
        File expectedOutputFile = new File("src/test/resources/testdata/cherriesmesh/set2/out");
        String expectedOutputString = new Scanner(expectedOutputFile).useDelimiter("\\Z").next();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        CherriesMesh.InputReader in = new CherriesMesh.InputReader(is);
        PrintWriter out = new PrintWriter(output);

        new CherriesMesh.CherriesMeshTask().solve(in, out);
        out.flush();


        String actualOutputString = output.toString().trim();
        assertEquals(actualOutputString, expectedOutputString);
    }

}