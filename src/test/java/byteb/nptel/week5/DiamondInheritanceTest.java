package byteb.nptel.week5;

import org.testng.annotations.Test;

import java.io.*;
import java.util.Scanner;

import static org.testng.Assert.assertEquals;

public class DiamondInheritanceTest {

    @Test
    public void allGCACases() throws FileNotFoundException {
        FileInputStream is = new FileInputStream("src/test/resources/testdata/diamondinheritance.in");
        File expectedOutputFile = new File("src/test/resources/testdata/diamondinheritance.out");
        String expectedOutputString = new Scanner(expectedOutputFile).useDelimiter("\\Z").next();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        DiamondInheritance.InputReader in = new DiamondInheritance.InputReader(is);
        PrintWriter out = new PrintWriter(output);

        new DiamondInheritance.DiamondInheritanceTask().solve(in, out);
        out.flush();


        String actualOutputString = output.toString().trim();
        assertEquals(actualOutputString, expectedOutputString);


    }
}