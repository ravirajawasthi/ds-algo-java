package byteb.nptel.week3;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.testng.Assert.assertEquals;

public class PancakeFlipperTest {

    private static final String TEST_TILE = "pancakeflipper";

    @Test(dataProvider = "testData")
    public void testOversizePancakeFlipper(String s, int size, String expected) {
        String actual = PancakeFlipper.oversizePancakeFlipper(s, size);
        assertEquals(actual, expected);
    }

    @Test(dataProvider = "testDataAssignment")
    public void testOversizePancakeFlipperAssignment(String s, int size, String expected) {
        String actual = PancakeFlipper.oversizePancakeFlipper(s, size);
        assertEquals(actual, expected);
    }


    @DataProvider(name = "testData", parallel = true)
    Object[][] getTestData() throws FileNotFoundException {
        File inputFile = new File("src/test/resources/testdata/" + TEST_TILE + ".in");
        File outputFile = new File("src/test/resources/testdata/" + TEST_TILE + ".out");

        Scanner inputScanner = new Scanner(inputFile);
        Scanner outputScanner = new Scanner(outputFile);

        int t = inputScanner.nextInt();
        inputScanner.nextLine();//To remove \n after next int

        var data = new Object[t][3];
        for (int i = 0; i < t; i++) {
            var inputString = inputScanner.nextLine().split(" ");
            data[i] = new Object[]{inputString[0], Integer.valueOf(inputString[1]), String.valueOf(outputScanner.nextLine())};
        }

        return data;

    }


    @DataProvider(name = "testDataAssignment")
    Object[][] getTestDataAssignment() throws FileNotFoundException {
        return new Object[][]{{
                "+++---+++", 4, "IMPOSSIBLE"},
                {"+-+-+-+-++++", 4, "4"},
                {"----++++----", 4, "2"},
                {"-+-+-+-+----", 4, "5"},
                {"-+-+-++-+---", 4, "IMPOSSIBLE"},
        };


    }
}