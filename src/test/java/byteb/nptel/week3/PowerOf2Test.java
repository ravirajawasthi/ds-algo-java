package byteb.nptel.week3;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.testng.Assert.assertEquals;

public class PowerOf2Test {

    private static final String TEST_FILE_NAME = "powerof2";

    @Test(dataProvider = "dataProvider")
    public void testCreatePowerOf2Recursive(String n, int expected, int worstAnswer) {
        int actual = PowerOf2.createPowerOf2Recursive(n, 0, worstAnswer);
        assertEquals(actual, expected);
    }

    @Test
    public void testCreatePowerOf2RecursiveSpecificCases() {
        int actual = PowerOf2.createPowerOf2Recursive("401", 0, 4);
        assertEquals(actual, 2);
    }

    @Test(dataProvider = "dataProvider")
    public void testCreatePowerOf2Greedy(String n, int expected, int wrongAns) {
        int actual = PowerOf2.convertPowerOf2(n);
        assertEquals(actual, expected);
    }


    @DataProvider(name = "dataProvider", parallel = true)
    private Object[][] getData() throws FileNotFoundException {
        File input = new File("src/test/resources/testdata/" + TEST_FILE_NAME + ".in");
        File output = new File("src/test/resources/testdata/" + TEST_FILE_NAME + ".out");

        Scanner inputScanner = new Scanner(input);
        Scanner outputScanner = new Scanner(output);

        int t = inputScanner.nextInt();

        Object[][] data = new Object[t][3];

        int index = 0;
        while (t-- > 0) {
            String n = String.valueOf(inputScanner.nextInt());
            data[index++] = new Object[]{n, outputScanner.nextInt(), n.length() + 1};
        }

        return data;

    }
}