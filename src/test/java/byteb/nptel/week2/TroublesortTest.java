package byteb.nptel.week2;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.testng.Assert.*;

public class TroublesortTest {

    @Test(dataProvider = "testData")
    public void testTroubleSort(String expected, int n, int[] arr) {
        String actual = Troublesort.TroubleSort(n, arr);
        assertEquals(actual, expected);
    }


    @Test
    public void swapsCount() {
        int[] arr = new int[]{7, 8, 5, 6, 3, 4, 1, 2} ;
        int n = arr.length;
        String actual = Troublesort.TroubleSort(n, arr);
        assertEquals(actual, "OK");
    }

    @DataProvider(name = "testData", parallel = true)
    private Object[][] getTestData() throws FileNotFoundException {
        File input = new File("src/test/resources/testdata/troublesort.in");
        File output = new File("src/test/resources/testdata/troublesort.out");

        Scanner inputScanner = new Scanner(input);
        Scanner outputScanner = new Scanner(output);

        int t = inputScanner.nextInt();
        inputScanner.nextLine();
        Object[][] data = new Object[t][3];

        for (int i = 0; i < t; i++) {
            data[i] = new Object[]{
                    outputScanner.nextLine(),
                    Integer.valueOf(inputScanner.nextLine()),
                    Utils.createArrayFromString(inputScanner.nextLine())
            };
        }
        return data;

    }

}