package byteb.nptel.week1;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.testng.AssertJUnit.assertEquals;

public class EngineerReverseSortTest {

    @Test(dataProvider = "testData")
    public void testEngineerReverseSort(int size, int cost, String ansArr) {
        var arr = EngineerReverseSort.engineerReverseSort(cost, size);
        if (arr.equals("IMPOSSIBLE")) {
            assertEquals(arr, ansArr);
        } else {
            var actualArr = Utils.createArrayFromString(arr);
            var expectedArr = Utils.createArrayFromString(ansArr);
            assertEquals(ReverseSort.reverseSort(actualArr), ReverseSort.reverseSort(expectedArr));
        }
    }


    @Test(dataProvider = "edgeCaseData")
    public void testEngineerReverseSortEdgeCase(int size, int cost) {
        var arr = EngineerReverseSort.engineerReverseSort(cost, size);
        var intArr = Utils.createArrayFromString(arr);
        assertEquals(ReverseSort.reverseSort(intArr), cost);
    }


    @DataProvider(name = "testData", parallel = true)
    public static Object[][] getTestData() throws FileNotFoundException {
        File input = new File("src/test/resources/testdata/engineeringreversesort.in");
        File output = new File("src/test/resources/testdata/engineeringreversesort.out");
        Scanner sc = new Scanner(input);
        Scanner op = new Scanner(output);
        int n = sc.nextInt();
        var inputArr = new Object[n][2];

        for (int i = 0; i < n; i++) {
            //[0] is size
            //[1] is cost

            inputArr[i] = new Object[]{
                    sc.nextInt(), sc.nextInt(), op.nextLine()
            };
        }

        return inputArr;
    }

    @DataProvider(name = "edgeCaseData", parallel = true)
    public static Object[][] getEdgeCaseTestData() {
        return new Object[][]{
                {5, 4},
                {4, 3},
                {7, 6},
                {6, 5},
                {3, 2},
        };
    }
}