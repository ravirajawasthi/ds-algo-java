package byteb.nptel.week1;

import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.testng.Assert.*;

public class ReverseSortTest {

    @Test
    public void testReverseSort() throws FileNotFoundException {
        String PROBLEM_DATA = "src/test/resources/testdata/reversesort";
        File inputFile = new File(PROBLEM_DATA + ".in");
        File outputFile = new File(PROBLEM_DATA + ".out");

        Scanner inputScanner = new Scanner(inputFile);
        Scanner outputScanner = new Scanner(outputFile);

        int t = inputScanner.nextInt();
        for (int c = 1; c < t + 1; c++) {
            int n = inputScanner.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = inputScanner.nextInt();
            }
            String actual = "Case #" + c + ": " + ReverseSort.reverseSort(arr);
            String expected = outputScanner.nextLine();
            assertEquals(actual, expected);
        }
    }


}