package byteb.stacks.and.queues;

import org.testng.annotations.Test;

import java.util.stream.IntStream;

import static org.testng.Assert.assertEquals;

public class QueueUsingStacksTest {

    @Test
    void correctnessTest() {
        var customQueue = new QueueUsingStacks();
        IntStream.range(0, 1_000_000).forEach(customQueue::push);
        IntStream.range(0, 1_000_000).forEach(n -> assertEquals(n, customQueue.pop()));
    }

}