package byteb.stacks.and.queues.stack.array.impl;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class MinStackTest {

    //    ["MinStack","push","push","push","getMin","pop","top","getMin"]
    //            [[],[-2],[0],[-3],[],[],[],[]]

    @Test
    void correctnessTest() {
        var minStack = new MinStack();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        minStack.getMin();
        minStack.pop();
        assertEquals(minStack.top(), 0);
        assertEquals(minStack.getMin(), -2);

    }

}