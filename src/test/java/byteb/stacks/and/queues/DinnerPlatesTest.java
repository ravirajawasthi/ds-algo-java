package byteb.stacks.and.queues;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class DinnerPlatesTest {

    @Test
    public void correctnessTest() {
        var dinnerplates = new DinnerPlates(2);
        dinnerplates.push(1);
        dinnerplates.push(2);
        dinnerplates.push(3);
        dinnerplates.push(4);
        dinnerplates.push(5);

        dinnerplates.popAtStack(0);

        dinnerplates.push(20);
        dinnerplates.push(21);

        dinnerplates.popAtStack(0);
        dinnerplates.popAtStack(2);

        assertEquals(dinnerplates.pop(), 5);
        assertEquals(dinnerplates.pop(), 4);
        assertEquals(dinnerplates.pop(), 3);
        assertEquals(dinnerplates.pop(), 1);
        assertEquals(dinnerplates.pop(), -1);

    }

    @Test
    public void correctnessTest2() {
        var dinnerplates = new DinnerPlates(1);
        dinnerplates.push(1);
        dinnerplates.push(2);
        dinnerplates.push(3);

        dinnerplates.popAtStack(1);

        assertEquals(dinnerplates.pop(), 3);
        assertEquals(dinnerplates.pop(), 1);
    }

    @Test
    public void correctnessTest3() {
        var dinnerplates = new DinnerPlates(2);
        dinnerplates.push(1);
        dinnerplates.push(2);
        dinnerplates.push(3);
        dinnerplates.push(4);
        dinnerplates.push(7);

        dinnerplates.popAtStack(8);

        dinnerplates.push(20);
        dinnerplates.push(21);

        dinnerplates.popAtStack(0);
        dinnerplates.popAtStack(2);

        dinnerplates.pop();
        dinnerplates.pop();
        dinnerplates.pop();
        dinnerplates.pop();
        dinnerplates.pop();
    }

    @Test
    void correctnessTest4() {
        var dinnerplates = new DinnerPlates(2);

        dinnerplates.push(1);
        dinnerplates.push(2);
        dinnerplates.push(3);
        dinnerplates.push(4);

        assertEquals(dinnerplates.pop(), 4);

        dinnerplates.push(4);

        assertEquals(dinnerplates.popAtStack(1), 4);

    }

}