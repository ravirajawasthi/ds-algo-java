package byteb.trees;

import byteb.trees.model.TreeNode;
import byteb.utils.Utils;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.testng.Assert.*;

public class FlattenBinaryTreeTest {

    @Test
    public void testFlatten() {
        int[] tree = new int[]{1, 2, 5, 3, 4, -1, 6};
        TreeNode root = Utils.createABtFromArrayTreeNode(tree);
        FlattenBinaryTree.flatten(root);
        assertEquals(Arrays.stream(new int[]{1, 2, 3, 4, 5, 6}).boxed().collect(Collectors.toList()), MorrisTraversal.inorder(root));
    }
}