package byteb.trees;

import byteb.trees.model.TreeNode;
import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class MorrisTraversalTest {

    @Test
    public void inorderTest() {
        TreeNode root = Utils.createABtFromArrayTreeNode(new int[] { 1, -1, 2, 3 });
        assertEquals(List.of(1, 3, 2), MorrisTraversal.inorder(root));
    }

    @Test(dataProvider = "preorderDataProvider")
    public void preorderTest(int[] tree, int[] expected) {
        TreeNode root = Utils.createABtFromArrayTreeNode(tree);
        assertEquals(Arrays.stream(expected).boxed().collect(Collectors.toList()), MorrisTraversal.preorder(root));
    }

    @DataProvider(name = "preorderDataProvider")
    public Object[][] preorderData() {
        return new int[][][] {
                { new int[] { 1, 2, 3, -1, -1, -1, 6, -1, -1 }, new int[] { 1, 2, 3, 6 } },
                { new int[] { 1, 2, 3, -1, -1, -1, -1 }, new int[] { 1, 2, 3 } },
        };
    }

}