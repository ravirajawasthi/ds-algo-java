package byteb.trees;

import byteb.trees.model.BinaryTreeNode;
import byteb.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class BinaryTreeToDoublyLinkedListTest {

    @Test
    public void testBTtoDLL() {
        BinaryTreeNode<Integer> root = Utils.createABtFromArrayBinaryTreeNode("3 1 5 -1 2 -1 -1 -1 -1");
        List<Integer> actual = MorrisTraversal.inorder(BinaryTreeToDoublyLinkedList.BTtoDLL(root));
        List<Integer> expected = Utils.createListFromString("1 2 3 5");
        assertEquals(actual, expected, StringUtils.join(actual, ","));
    }
}