package byteb.dsu;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class WaterDistributionToAVillageTest {

    @Test
    public void testMinCostToSupplyWater() {
        var edges = Utils.twoDimensionalArrayFromString("[[1,2,1],[2,3,1]]");
        var wells = new int[]{1, 2, 2};
        int n = 3;
        int expected = 3;
        int actual = new WaterDistributionToAVillage().minCostToSupplyWater(n, wells, edges);
        assertEquals(actual, expected);

    }
}