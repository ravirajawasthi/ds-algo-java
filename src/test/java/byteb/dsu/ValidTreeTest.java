package byteb.dsu;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ValidTreeTest {

    @Test(dataProvider = "testData")
    public void testValidTree(int[][] edges, int n, boolean expected) {
        boolean actual = new ValidTree().validTree(n, edges);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData", parallel = true)
    private Object[][] getTestData() {
        return new Object[][]{
                {new int[][]{{0, 1}, {2, 3}}, 4, false},
                {new int[][]{{0, 1}, {0, 2}, {0, 3}, {1, 4}}, 5, true},


        };

    }
}

