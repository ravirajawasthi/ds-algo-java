package byteb.dsu;

import org.testng.annotations.Test;

import java.text.DecimalFormat;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class EvaluateDivisionTest {

    @Test
    public void testCalcEquation() {
        List<List<String>> gg = List.of(List.of("a", "b"), List.of("b", "c"));
        List<List<String>> queries = List.of(List.of("a", "c"), List.of("b", "a"), List.of("a", "e"), List.of("a", "a"), List.of("x", "x"));
        double[] vals = new double[]{2d, 3d};
        var actual = new EvaluateDivision().calcEquation(gg, vals, queries);
        assertEquals(actual, new double[]{6.00000, 0.50000, -1.00000, 1.00000, -1.00000});
    }


    @Test
    public void testCalcEquation2() {
        List<List<String>> gg = List.of(List.of("a", "b"), List.of("e", "f"), List.of("b", "e"));
        List<List<String>> queries = List.of(List.of("b", "a"), List.of("a", "f"), List.of("f", "f"), List.of("e", "e"), List.of("c", "c"), List.of("a", "c"), List.of("f", "e"));
        double[] vals = new double[]{3.4, 1.4, 2.3};
        var actual = new EvaluateDivision().calcEquation(gg, vals, queries);
        var expected = new double[]{0.29412, 10.94800, 1.00000, 1.00000, -1.00000, -1.00000, 0.71429};
        assertEquals(expected.length, actual.length);
        DecimalFormat df = new DecimalFormat("#.00000");
        for (int i = 0; i < expected.length; i++) {
            assertEquals(df.format(actual[i]), df.format(expected[i]));
        }
    }


    @Test
    public void testCalcEquation3() {
        List<List<String>> gg = List.of(List.of("x1", "x2"), List.of("x2", "x3"), List.of("x1", "x4"), List.of("x2", "x5"));
        List<List<String>> queries = List.of(List.of("x2", "x4"),
                List.of("x1", "x5"),
                List.of("x1", "x3"),
                List.of("x5", "x5"),
                List.of("x5", "x1"),
                List.of("x3", "x4"),
                List.of("x4", "x3"),
                List.of("x6", "x6"),
                List.of("x0", "x0"));
        double[] vals = new double[]{3.0, 0.5, 3.4, 5.6};
        var actual = new EvaluateDivision().calcEquation(gg, vals, queries);
        var expected = new double[]{1.13333,16.80000,1.50000,1.00000,0.05952,2.26667,0.44118,-1.00000,-1.00000};
        assertEquals(expected.length, actual.length);
        DecimalFormat df = new DecimalFormat("#.00000");
        for (int i = 0; i < expected.length; i++) {
            assertEquals(df.format(actual[i]), df.format(expected[i]));
        }
    }
    

}