package byteb.dsu;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SmallestStringWithSwapsTest {

    @Test
    public void testSmallestStringWithSwaps() {
        var actual = new SmallestStringWithSwaps().smallestStringWithSwaps("dcab", Utils.getArrayListFromString("[[0,3],[1,2]]"));
        assertEquals("bacd", actual);
    }
}