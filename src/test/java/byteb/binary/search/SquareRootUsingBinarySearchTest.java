package byteb.binary.search;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SquareRootUsingBinarySearchTest {
    @Test(dataProvider = "testData")
    void testFloorSqrt(long x, long expected) {
        long actual = SquareRootUsingBinarySearch.floorSqrt(x);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    private Object[][] testData() {
        return new Object[][]{
                {25L, 5L},
                {18L, 4L},
                {4L, 2L},
                {5L, 2L},
        };
    }
}
