package byteb.binary.search;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FirstAndLastPositionInSortedArrayTest {

    @Test
    public static void testSearchRange() {
        int[] arr = new int[]{5, 7, 7, 8, 8, 10};
        int target = 8;
        int[] actualRes = FirstAndLastPositionInSortedArray.searchRange(arr, target);
        int[] expected = new int[]{3, 4};
        assertEquals(actualRes, expected);
    }

    @Test
    public static void notFound() {
        int[] arr = new int[]{5, 7, 7, 8, 8, 10};
        int target = 6;
        int[] actualRes = FirstAndLastPositionInSortedArray.searchRange(arr, target);
        int[] expected = new int[]{-1, -1};
        assertEquals(actualRes, expected);
    }

    @Test
    public static void singleElement() {
        int[] arr = new int[]{1};
        int target = 1;
        int[] actualRes = FirstAndLastPositionInSortedArray.searchRange(arr, target);
        int[] expected = new int[]{0, 0};
        assertEquals(actualRes, expected);
    }

}