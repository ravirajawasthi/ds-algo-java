package byteb.binary.search;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class UpperBoundBinarySearchTest {

    @Test
    public void testUpperBound() {

        int[] arr = new int[]{2, 4, 6, 8, 10, 12, 16, 18};
        assertEquals(UpperBoundBinarySearch.upperBound(arr, 18, 8), 8);
        assertEquals(UpperBoundBinarySearch.upperBound(arr, 16, 8), 7);
        assertEquals(UpperBoundBinarySearch.upperBound(arr, 17, 8), 7);
        assertEquals(UpperBoundBinarySearch.upperBound(arr, 15, 8), 6);
        assertEquals(UpperBoundBinarySearch.upperBound(arr, 2, 8), 1);

        arr = Utils.createArrayFromString("1 4 7 8 10");
        assertEquals(UpperBoundBinarySearch.upperBound(arr, 7, 5), 3);
    }
}