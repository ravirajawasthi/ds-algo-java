package byteb.binary.search;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class InsertPositionInSortedArrayTest {

    @Test
    public void testSearchInsert() {

        int[] arr = Utils.createArrayFromString("0 1 2 3 4 5 6 7 8 9");
        assertEquals(InsertPositionInSortedArray.searchInsert(arr, 0), 0);
        assertEquals(InsertPositionInSortedArray.searchInsert(arr, 7), 7);

        arr = Utils.createArrayFromString("1 3 5 6");
        assertEquals(InsertPositionInSortedArray.searchInsert(arr, 2), 1);

    }
}