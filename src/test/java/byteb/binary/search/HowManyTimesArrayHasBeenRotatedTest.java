package byteb.binary.search;

import org.testng.Assert;
import org.testng.annotations.Test;

import byteb.utils.Utils;

public class HowManyTimesArrayHasBeenRotatedTest {
    @Test
    void testFindKRotation() {
        var arr = Utils.createArrayFromString("1 2 3 4 5 6 8");
        int expected = 0;
        int actual = HowManyTimesArrayHasBeenRotated.findKRotation(arr, arr.length);
        Assert.assertEquals(actual, expected);
    }

    @Test
    void testFindKRotationWithRotation() {
        var arr = Utils.createArrayFromString("2 3 4 5 6 8 1");
        int expected = 6;
        int actual = HowManyTimesArrayHasBeenRotated.findKRotation(arr, arr.length);
        Assert.assertEquals(actual, expected);
    }
}
