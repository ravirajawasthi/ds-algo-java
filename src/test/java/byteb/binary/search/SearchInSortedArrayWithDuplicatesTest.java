package byteb.binary.search;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SearchInSortedArrayWithDuplicatesTest {

    @Test
    public void testSearch() {
        int[] arr = Utils.createArrayFromString("9 8 0 1 2 3 3 4 5 6 6 7");
        boolean actual = SearchInSortedArrayWithDuplicates.search(arr, 7);
        assertTrue(actual);
    }

    @Test
    public void testSearchNotPresent() {
        int[] arr = Utils.createArrayFromString("9 8 0 1 2 3 3 4 5 6 6 7");
        boolean actual = SearchInSortedArrayWithDuplicates.search(arr, 52);
        assertFalse(actual);
    }

    @Test
    public void testSearchLeetcode() {
        int[] arr = Utils.createArrayFromString("3 1");
        assertTrue(SearchInSortedArrayWithDuplicates.search(arr, 1));
    }
}