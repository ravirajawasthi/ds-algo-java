package byteb.binary.search;

import byteb.utils.Utils;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BinarySearchTest {

    @Test
    public void binarySearchTest() {
        int[] nums = Utils.createArrayFromString("0 1 2 3 4 5 6 7 8 9 10");

        Assert.assertEquals(BinarySearch.search(nums, 5), 5);
        Assert.assertEquals(BinarySearch.search(nums, 0), 0);
        Assert.assertEquals(BinarySearch.search(nums, 9), 9);
        Assert.assertEquals(BinarySearch.search(nums, 7), 7);
        Assert.assertEquals(BinarySearch.search(nums, -1), -1);
        Assert.assertEquals(BinarySearch.search(nums, 11), -1);

    }

    @Test
    void testSearch() {
        int[] row1 = Utils.createArrayFromString("0 1 2");
        int[] row2 = Utils.createArrayFromString("3 4 5");
        int[] row3 = Utils.createArrayFromString("6 7 8");

        int[][] matrix = new int[][] { row1, row2, row3 };

        Assert.assertTrue(BinarySearch.search(matrix, 0), "success");
        Assert.assertTrue(BinarySearch.search(matrix, 8), "success");
        Assert.assertTrue(BinarySearch.search(matrix, 3), "success");
        Assert.assertTrue(BinarySearch.search(matrix, 6), "success");
        Assert.assertFalse(BinarySearch.search(matrix, 9), "failure");
        Assert.assertFalse(BinarySearch.search(matrix, -1), "failure");

    }

}
