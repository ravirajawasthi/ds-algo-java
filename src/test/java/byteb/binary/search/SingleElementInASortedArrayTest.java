package byteb.binary.search;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import byteb.utils.Utils;

public class SingleElementInASortedArrayTest {
    @Test(dataProvider = "parameterisedTest")
    void testSingleNonDuplicateInStarting(int[] arr, int expected) {
        int actual = SingleElementInASortedArray.singleNonDuplicate(arr);
        Assert.assertEquals(actual, expected);
    }

    @DataProvider(name = "parameterisedTest")
    public static Object[][] arrays() {

        return new Object[][] {
                { Utils.createArrayFromString("1 2 2 3 3 4 4"), 1 },
                { Utils.createArrayFromString("1 1 2 2 3 3 4"), 4 },
                { Utils.createArrayFromString("1 1 2 2 3 4 4"), 3 },
                { Utils.createArrayFromString("1 1 2 3 3 4 4 5 5 6 6"), 2 }
        };
    }

}
