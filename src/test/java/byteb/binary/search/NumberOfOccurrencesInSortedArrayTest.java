package byteb.binary.search;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class NumberOfOccurrencesInSortedArrayTest {

    @Test
    public void testCount() {
        int[] arr = Utils.createArrayFromString("1 2 2 2 3 4 5 6 7");
        int expected = 3;
        int actual = NumberOfOccurrencesInSortedArray.count(arr, arr.length, 2);
        assertEquals(expected, actual);
    }

    @Test
    public void notFound() {
        int[] arr = Utils.createArrayFromString("1 2 2 2 3 4 5 6 7");
        int expected = 0;
        int actual = NumberOfOccurrencesInSortedArray.count(arr, arr.length, 8);
        assertEquals(expected, actual);
    }

    @Test
    public void notFoundLowerBound() {
        int[] arr = Utils.createArrayFromString("1 2 2 2 3 4 5 6 7");
        int expected = 0;
        int actual = NumberOfOccurrencesInSortedArray.count(arr, arr.length, -1);
        assertEquals(expected, actual);
    }
}