package byteb.binary.search;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SearchInARotatedArrayTest {

    @Test
    public void testSearch() {
        int[] arr = Utils.createArrayFromString("9 8 0 1 2 3 4 5 6 7");
        int expected = 1;
        int actual = SearchInARotatedArray.search(arr, 8);
        assertEquals(expected, actual);
    }

    @Test
    public void testSearchNotPreset() {
        int[] arr = Utils.createArrayFromString("9 8 0 1 2 3 4 5 6 7");
        int expected = -1;
        int actual = SearchInARotatedArray.search(arr, 10);
        assertEquals(expected, actual);
    }

    @Test
    public void testSearchPresetButSorted() {
        int[] arr = Utils.createArrayFromString("0 1 2 3 4 5 6 7 8 9");
        int expected = 8;
        int actual = SearchInARotatedArray.search(arr, 8);
        assertEquals(expected, actual);
    }
}