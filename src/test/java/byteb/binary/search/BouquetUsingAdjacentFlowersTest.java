package byteb.binary.search;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BouquetUsingAdjacentFlowersTest {

    @Test(dataProvider = "testData")
    public void testMinDays(int[] arr, int m, int k, int expected) {
        int actual = BouquetUsingAdjacentFlowers.minDays(arr, m, k);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {new int[]{1, 10, 3, 10, 2}, 3, 1, 3},
                {new int[]{1, 10, 3, 10, 2}, 3, 2, -1},
                {new int[]{7, 7, 7, 7, 12, 7, 7}, 2, 3, 12},
        };
    }

}