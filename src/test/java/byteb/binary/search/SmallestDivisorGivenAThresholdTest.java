package byteb.binary.search;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SmallestDivisorGivenAThresholdTest {

    @Test(dataProvider = "testData")
    public void testSmallestDivisor(int[] arr, int threshold, int expected) {
        int actual = SmallestDivisorGivenAThreshold.smallestDivisor(arr, threshold);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {Utils.createArrayFromString("1 2 5 9"), 6, 5},
                {Utils.createArrayFromString("44 22 33 11 1"), 5, 44},

        };
    }
}