package byteb.binary.search;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.testng.Assert.*;

public class KokoEatingBananasTest {

    @Test(dataProvider = "testData", timeOut = 1000L)
    public void testSolve(int[] arr, int h, int expected) {
        var actual = KokoEatingBananas.Solve(arr.length, arr, h);
        assertEquals(actual, expected);

    }

    @DataProvider(name = "testData")
    private Object[][] testData() {
        return new Object[][]{
                {new int[]{3, 6, 7, 11}, 8, 4},
                {new int[]{30, 11, 23, 4, 20}, 5, 30},
        };
    }

    @Test(testName = "overflow test")
    public void overflowTest() throws FileNotFoundException {
        //Creating Scanner
        File file = new File("src/test/resources/testdata/KokoLargeTestCase.txt");
        Scanner sc = new Scanner(file.getAbsoluteFile());

        //Reading input
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        int H = sc.nextInt();
        int expected = sc.nextInt();

        //Getting output
        int actual = KokoEatingBananas.Solve(n, arr, H);

        assertEquals(actual, expected);


    }

}