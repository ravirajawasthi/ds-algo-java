package byteb.binary.search;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LowerBoundBinarySearchTest {

    @Test
    void testFindFloor() {
        long[] arr = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 10 };
        Assert.assertEquals(LowerBoundBinarySearch.findFloor(arr, arr.length, 0), -1);
        Assert.assertEquals(LowerBoundBinarySearch.findFloor(arr, arr.length, 10), 8);
        Assert.assertEquals(LowerBoundBinarySearch.findFloor(arr, arr.length, 9), 7);
        Assert.assertEquals(LowerBoundBinarySearch.findFloor(arr, arr.length, 3), 2);
        Assert.assertEquals(LowerBoundBinarySearch.findFloor(arr, arr.length, 1), 0);
    }
}
