package byteb.binary.search;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class AllocateBooksTest {

    @Test(dataProvider = "testData")
    public void testFindPages(ArrayList<Integer> arr, int students, int expected) {
        int actual = AllocateBooks.findPages(arr, arr.size(), students);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    private Object[][] getTestData() {
        return new Object[][]{
                {Utils.createListFromString("12 34 67 90"), 2, 113},
                {Utils.createListFromString("25 46 28 49 24"), 4, 71}
        };
    }
}