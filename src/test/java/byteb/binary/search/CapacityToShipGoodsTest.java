package byteb.binary.search;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CapacityToShipGoodsTest {

    @Test(dataProvider = "testData")
    public void testShipWithinDays(int[] arr, int days, int expected) {
        int actual = CapacityToShipGoods.shipWithinDays(arr, days);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 5, 15},
                {new int[]{3, 2, 2, 4, 1, 4}, 3, 6},
                {new int[]{1, 2, 3, 1, 1}, 4, 3},
                {new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 10, 10},
                {new int[]{3, 3, 3, 3, 3, 3}, 2, 9},


        };
    }
}