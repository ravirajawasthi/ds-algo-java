package byteb.binary.search;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PeakInArrayTest {

    @Test(dataProvider = "peakTestData")
    public void testFindPeakElement(int[] arr, int expected) {

        int actual = PeakInArray.findPeakElement(arr);
        assertEquals(actual, expected);

    }


    @DataProvider(name = "peakTestData")
    private Object[][] testDate() {
        return new Object[][]{
                {Utils.createArrayFromString("1 2 3 1"), 2},
                {Utils.createArrayFromString("1 2 1 3 5 6 4"), 5},
                {new int[]{3, 4, 3, 2, 1}, 1}
        };
    }
}