package byteb.binary.search;

import org.testng.Assert;
import org.testng.annotations.Test;

import byteb.utils.Utils;

public class MinimunInARotatedSortedArrayTest {
    @Test
    void testFindMin() {
        var arr = Utils.createArrayFromString("5 6 1 2 3 4");
        int actual = MinimunInARotatedSortedArray.findMin(arr);
        int expected = 1;
        Assert.assertEquals(actual, expected);
    }

    @Test
    void testFindMinNonRotated() {
        var arr = Utils.createArrayFromString("2 3 4 7 8 9 10 11 12 14");
        int actual = MinimunInARotatedSortedArray.findMin(arr);
        int expected = 2;
        Assert.assertEquals(actual, expected);
    }
}
