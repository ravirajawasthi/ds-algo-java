package byteb.binary.search;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class NthRootTest {

    @Test(dataProvider = "testData")
    public void testNthRoot(int n, int m, int expected) {
        int actual = NthRoot.findNthRoot(n, m);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    private Object[][] dataProvider(){
        return new Object[][]{
                {2, 9, 3},
                {3, 9, -1},
        };
    }
}