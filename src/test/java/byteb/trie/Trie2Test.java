package byteb.trie;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class Trie2Test {

    @Test
    void trieTest(){
        var trie = new Trie2();
        trie.insert("apple");
        trie.insert("apple");
        trie.insert("apple");
        assertEquals(trie.countWordsEqualTo("apple"), 3);
    }

}