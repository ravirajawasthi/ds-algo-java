package byteb.trie;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TrieTest {

    @Test
    void trieTest() {
        Trie trie = new Trie();
        trie.insert("apple");
        trie.insert("appss");
        trie.insert("zap");
        assertTrue(trie.search("apple"));
        assertFalse(trie.search("applee"));
        assertTrue(trie.search("appss"));
        assertFalse(trie.search("ball"));
        assertTrue(trie.startsWith("app"));
        assertTrue(trie.startsWith("z"));
        assertFalse(trie.startsWith("zapa"));
        assertTrue(trie.startsWith("a"));
    }

}
