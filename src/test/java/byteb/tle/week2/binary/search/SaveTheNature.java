package byteb.tle.week2.binary.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class SaveTheNature {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {

            int t = sc.nextInt();
            while (t-- > 0) {
                int tickets = sc.nextInt();
                int[] ticketPrices = new int[tickets];
                for (int i = 0; i < tickets; i++) {
                    ticketPrices[i] = sc.nextInt();
                }

                int x = sc.nextInt();
                int a = sc.nextInt();

                int y = sc.nextInt();
                int b = sc.nextInt();

                var lcm = lcm(a, b);

                long requiredContribution = sc.nextLong();

                Arrays.sort(ticketPrices);

                int start = 1;
                int end = tickets;
                while (start <= end) {
                    int mid = (start) + (end - start) / 2;
                    if (check(mid, ticketPrices, x, a, y, b, lcm, requiredContribution)) {
                        end = mid - 1;
                    } else {
                        start = mid + 1;
                    }
                }
                out.println(end == tickets ? -1 : start);

            }

        }

        private boolean check(int mid, int[] ticketPrices, int x, int a, int y, int b, long lcm,
                long requiredContribution) {
            // Assuming x > y
            if (x < y) {
                var temp = y;
                y = x;
                x = temp;

                temp = a;
                a = b;
                b = temp;
            }
            double collection = 0;
            int pointer = ticketPrices.length - 1;

            long bumper = mid / lcm;
            long xPrices = mid / a - bumper;
            long yPrices = mid / b - bumper;
            double bumperRatio = (x + y) / 100.0;
            while (bumper-- > 0 && pointer > -1) {
                collection += ticketPrices[pointer--] * (bumperRatio);
                if (collection >= requiredContribution)
                    return true;
            }
            double xRatio = (x) / 100.0;

            while (xPrices-- > 0 && pointer > -1) {
                collection += ticketPrices[pointer--] * (xRatio);
                if (collection >= requiredContribution)
                    return true;

            }
            double yRatio = (y) / 100.0;
            while (yPrices-- > 0 && pointer > -1) {
                collection += ticketPrices[pointer--] * yRatio;
                if (collection >= requiredContribution)
                    return true;

            }
            return false;
        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
