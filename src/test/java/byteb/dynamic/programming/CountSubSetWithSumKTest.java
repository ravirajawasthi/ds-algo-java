package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CountSubSetWithSumKTest {

    @Test(dataProvider = "testData")
    public void testFindWays(int[] arr, int target, int expected) {
        int actual = CountSubSetWithSumK.findWays(arr, target);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {Utils.createArrayFromString("17 12 16"), 13, 0},
                {Utils.createArrayFromString("19 18 12"), 6, 0},
                {Utils.createArrayFromString("1 4 4 5"), 5, 3},
        };
    }
}