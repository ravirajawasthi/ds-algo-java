package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.testng.Assert.*;

public class DominoTriminoTest {

    @Test(dataProvider = "testData")
    public void testNumTilings(int n, int expected) {
        var obj = new DominoTrimino();
        assertEquals(obj.numTilings(n), expected);
    }


    @DataProvider(name = "testData")
    private Object[][] getTestData() throws FileNotFoundException {
        return new Object[][]{
                {3, 5},
                {1, 1}
        };

    }
}