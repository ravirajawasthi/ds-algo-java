package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FrogWithKJumpTest {

    @Test(dataProvider = "testData")
    public void testMinimizeCost(int[] arr, int k, int expected) {
        int actual = FrogWithKJump.minimizeCost(arr.length, k, arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {Utils.createArrayFromString("10 40 30 10"), 2, 40},
                {Utils.createArrayFromString("10 40 50 20 60"), 3, 50},
        };
    }
}