package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class NinjaTrainingTest {

    @Test(dataProvider = "testData")
    public void testNinjaTraining(int[][] arr, int expected) {
        int actual = NinjaTraining.ninjaTraining(arr.length, arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {
                        new int[][]{
                                {1, 2, 5},
                                {3, 1, 1},
                                {3, 3, 3}
                        }, 11
                }
        };
    }


}