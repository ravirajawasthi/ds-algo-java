package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class KInversePairTest {

    @Test(dataProvider = "testData")
    public void testKInversePairs(int n, int k, int expected) {
        var obj = new KInversePair();
        assertEquals(obj.kInversePairs(n, k), expected);
    }

    @DataProvider(name = "testData", parallel = false)
    public Object[][] getTestData() {
        return new Object[][]{
                {1000, 1000, 663677020},
                {15, 4, 2260},
        };
    }

}