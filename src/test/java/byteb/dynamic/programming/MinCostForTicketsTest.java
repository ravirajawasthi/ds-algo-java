package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MinCostForTicketsTest {

    @Test(dataProvider = "testData")
    public void testMincostTickets(int[] days, int[] costs, int expected) {
        int actual = MinCostForTickets.mincostTickets(days, costs);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {new int[]{1, 4, 6, 7, 8, 20}, new int[]{2, 7, 15}, 11},
                {new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 30, 31}, new int[]{2, 7, 15}, 17},
                {new int[]{1, 4, 6, 7, 8, 20}, new int[]{7, 2, 12}, 6},
                {new int[]{1, 2, 3, 4, 6, 8, 9, 10, 13, 14, 16, 17, 19, 21, 24, 26, 27, 28, 29}, new int[]{3, 14, 50}, 50},
        };
    }
}