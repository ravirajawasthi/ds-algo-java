package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MaximalSquareTest {

    @Test(dataProvider = "testData")
    public void testMaximalSquare(char[][] arr, int expected) {
        var actual = new MaximalSquare().maximalSquare(arr);
        assertEquals(actual, expected);

    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {new char[][]{{'1', '0'}}, 1},
                {new char[][]{{'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'}}, 1},
                {new char[][]{{'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1'}}, 1},
                {new char[][]{{'1', '0'}, {'0', '0'}, {'0', '0'}, {'0', '0'}, {'0', '0'}, {'0', '0'}, {'0', '0'}}, 1},
                {new char[][]{{'0', '0'}, {'0', '0'}, {'0', '0'}, {'0', '0'}, {'0', '0'}, {'0', '0'}, {'0', '0'}}, 0},
                {new char[][]{
                        {'1', '1', '1', '1', '1'},
                        {'1', '1', '1', '1', '1'},
                        {'0', '0', '0', '0', '0'},
                        {'1', '1', '1', '1', '1'},
                        {'1', '1', '1', '1', '1'}}, 4}
        };
    }

}