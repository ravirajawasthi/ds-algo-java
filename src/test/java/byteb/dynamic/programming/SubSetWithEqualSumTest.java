package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SubSetWithEqualSumTest {

    @Test(dataProvider = "testData")
    public void testCanPartition(int[] arr, boolean expected) {
        boolean actual = SubSetWithEqualSum.canPartition(arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {Utils.createArrayFromString("1 5 11 5"), true},
                {Utils.createArrayFromString("1 2 3 5"), false},
                {Utils.createArrayFromString("1 2 5"), false}
        };
    }

}