package byteb.dynamic.programming;

import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class WordBreakTest {

    @Test
    public void testWordBreak() {
        assertTrue(new WordBreak().wordBreak("leetcode", List.of("leet", "code")));

        assertTrue(new WordBreak().wordBreak("applepenapple", List.of("apple", "pen")));

        assertFalse(new WordBreak().wordBreak("catsandog", List.of("cats", "dog", "sand", "and", "cat")));

        assertTrue(new WordBreak().wordBreak("aaaaaaa", List.of("aaaa","aaa")));
    }
}