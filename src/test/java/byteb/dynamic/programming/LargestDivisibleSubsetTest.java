package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class LargestDivisibleSubsetTest {

    @Test(dataProvider = "testData")
    public void testLargestDivisibleSubset(int[] arr, List<Integer> expected) {
        var obj = new LargestDivisibleSubset();
        var ans = obj.largestDivisibleSubset(arr);
        assertEquals(ans, expected, ans.toString());
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {new int[]{5, 9, 18, 54, 108, 540, 90, 180, 360, 720}, List.of(9, 18, 90, 180, 360, 720)},
        };
    }

}