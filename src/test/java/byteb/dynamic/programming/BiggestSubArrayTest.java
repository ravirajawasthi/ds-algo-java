package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BiggestSubArrayTest {

    @Test(dataProvider = "testData")
    public void testFindLength(int[] arr1, int[] arr2, int expected) {
        var obj = new BiggestSubArray();
        assertEquals(obj.findLength(arr1, arr2), expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {Utils.createArrayFromString("0 1 1 1 1"), Utils.createArrayFromString("1 0 1 0 1"), 2},
                {Utils.createArrayFromString("0 0 0 0 0 0 0 1 0 0"), Utils.createArrayFromString("0 0 0 0 0 0 1 0 0 0"), 9},
        };
    }
}