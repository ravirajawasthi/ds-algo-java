package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class UniquePathsTest {

    @Test(dataProvider = "testData")
    public void testUniquePaths(int m, int n, int expected) {
        int actual = UniquePaths.uniquePaths(m, n);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {
                        3, 7, 28
                }
        };
    }

}