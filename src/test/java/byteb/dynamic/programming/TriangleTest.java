package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class TriangleTest {

    @Test
    public void testMinimumTotal() {

        var l1 = List.of(-1);
        var l2 = Utils.createListFromString("2 3");
        var l3 = Utils.createListFromString("1 -1 -3");
        var inputList = new ArrayList<>(List.of(l1, l2, l3));

        int actual = Triangle.minimumTotal(inputList);
        assertEquals(actual, -1);
    }

    @Test
    public void testMinimumTotal1() {

        var l1 = List.of(2);
        var l2 = List.of(3, 4);
        var l3 = List.of(6, 5, 7);
        var l4 = List.of(4, 1, 8, 3);
        var inputList = new ArrayList<>(List.of(l1, l2, l3, l4));

        int actual = Triangle.minimumTotal(inputList);
        assertEquals(actual, 11);
    }

}