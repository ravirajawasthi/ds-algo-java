package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MaximumSumCircularSubArrayTest {

    @Test(dataProvider = "testData")
    public void testMaxSubarraySumCircular(int[] arr, int expected) {
        var obj = new MaximumSumCircularSubArray();
        assertEquals(obj.maxSubarraySumCircular(arr), expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][] {
                { new int[] { 5, -3, 5 }, 10 },
                { new int[] { 10, 10, 10, 10, 10, 10, -3, 10, 10, 10, 10 }, 100 }
        };
    }
}