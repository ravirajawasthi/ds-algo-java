package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MinCostClimbingStairsTest {

    @Test(dataProvider = "testData")
    public void testClimbStairs(int[] n, int expected) {
        int actual = MinCostOfStairs.minCostClimbingStairs(n);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {new int[]{10,15,20}, 15},
                {new int[]{1,100,1,1,1,100,1,1,100,1}, 6},
                {new int[]{1,100}, 1},
        };
    }
}