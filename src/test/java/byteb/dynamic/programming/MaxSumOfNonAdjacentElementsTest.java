package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class MaxSumOfNonAdjacentElementsTest {

    @Test(dataProvider = "testData")
    public void testMaximumNonAdjacentSum(ArrayList<Integer> arr, int expected) {
        int actual = MaxSumOfNonAdjacentElements.maximumNonAdjacentSum(arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {Utils.createListFromString("1 2 4"), 5},
                {Utils.createListFromString("2 1 4 9"), 11},
                {Utils.createListFromString("5 6 6"), 11},
                {Utils.createListFromString("9 9 8 2"), 17},

        };
    }

}