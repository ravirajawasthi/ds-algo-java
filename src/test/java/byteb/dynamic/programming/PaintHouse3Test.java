package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PaintHouse3Test {

    @Test(dataProvider = "testData")
    public void testMinCost(int[] houses, int[][] costs, int m, int colors, int target, int expected) {
        var obj = new PaintHouse3();
        assertEquals(obj.minCost(houses, costs, m, colors, target), expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
//                {new int[]{0, 0, 0, 0, 0}, new int[][]{{1, 10}, {10, 1}, {10, 1}, {1, 10}, {5, 1}}, 5, 2, 3, 9},
//                {new int[]{0, 0, 0, 1}, new int[][]{{1, 5}, {4, 1}, {1, 3}, {4, 4}}, 4, 2, 4, 12},
                {new int[]{3, 1, 2, 3}, new int[][]{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 4, 3, 3, -1},
        };
    }
}