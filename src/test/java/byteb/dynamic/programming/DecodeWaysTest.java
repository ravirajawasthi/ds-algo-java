package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DecodeWaysTest {

    @Test(dataProvider = "testData")
    public void testNumDecodings(String s, int expected) {
        int actual = DecodeWays.numDecodings(s);
        assertEquals(actual, expected);
    }


    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {"12", 2},
                {"226", 3},
                {"06", 0},
                {"11445512111111", 102},
                {"114455121111110", 63},
                {"1144551021111110", 39},
                {"114455102111111", 63},
                {"114455102111", 15},
                {"10", 1},
                {"10011", 0},
                {"2611055971756562", 4},
                {"1", 1},

        };
    }
}