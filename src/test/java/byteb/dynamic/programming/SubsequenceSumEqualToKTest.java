package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SubsequenceSumEqualToKTest {

    @Test(dataProvider = "testData")
    public void testSubsetSumToK(int[] arr, int k, boolean expected) {
        boolean actual = SubsequenceSumEqualToK.subsetSumToK(arr.length, k, arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {Utils.createArrayFromString("4 3 2 1"), 5, true},
                {Utils.createArrayFromString("2 5 1 6 7"), 4, false},
                {Utils.createArrayFromString("63 19 8 100 88 87 27 56 27 75"), 132, false},
                {Utils.createArrayFromString("91 67 83 39 45 13 89 43 77 65"), 2, false},
                {Utils.createArrayFromString("8 59 58 79 44 7 65 69 60 51"), 172, true},
                {Utils.createArrayFromString("1 2 5"), 4, false}
        };
    }
}