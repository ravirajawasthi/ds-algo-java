package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class HouseRobberIITest {

    @Test(dataProvider = "testData")
    public void testHouseRobber(int[] arr, long expected) {
        long actual = HouseRobberII.houseRobber(arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {Utils.createArrayFromString("1 5 1 2 6"), 11L},
                {Utils.createArrayFromString("1 3 2 0"), 3L},
                {Utils.createArrayFromString("99"), 99L},
                {Utils.createArrayFromString("2 3 2"), 3L},
        };
    }


}