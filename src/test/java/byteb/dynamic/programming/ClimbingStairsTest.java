package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ClimbingStairsTest {

    @Test(dataProvider = "testData")
    public void testClimbStairs(int n, int expected) {
        int actual = ClimbingStairs.climbStairs(n);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {3, 3},
                {2, 2},
        };
    }
}