package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FrogJumpTest {

    @Test(dataProvider = "testData")
    public void testFrogJump(int[] arr, int n, int expected) {
        int actual = FrogJump.frogJump(n, arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {Utils.createArrayFromString("10 20 30 10"), 4, 20},
                {Utils.createArrayFromString("10 50 10"), 3, 0},
                {Utils.createArrayFromString("7 4 4 2 6 6 3 4"), 8, 7},
                {Utils.createArrayFromString("4 8 3 10 4 4"), 6, 2},
        };
    }
}