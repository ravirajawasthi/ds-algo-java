package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CherryPickup2Test {

    @Test(dataProvider = "testData")
    public void testCherryPickup(int[][] grid, int expected) {
        int actual = CherryPickup2.cherryPickup(grid);
        assertEquals(actual, expected);
    }

    @DataProvider(parallel = true, name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {new int[][]{{3, 1, 1}, {2, 5, 1}, {1, 5, 5}, {2, 1, 1}}, 24},
                {new int[][]{{1, 0, 0, 0, 0, 0, 1}, {2, 0, 0, 0, 0, 3, 0}, {2, 0, 9, 0, 0, 0, 0},
                        {0, 3, 0, 5, 4, 0, 0}, {1, 0, 2, 3, 0, 0, 6}}, 28}
        };

    }
}