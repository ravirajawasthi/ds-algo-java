package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MinimumFallingPathSumTest {

    @Test(dataProvider = "testData")
    public void testMinFallingPathSum(int[][] arr, int expected) {
        int actual = MinimumFallingPathSum.minFallingPathSum(arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {
                        new int[][]{
                                {2, 1, 3},
                                {6, 5, 4},
                                {7, 8, 9}
                        }, 13
                },
                {
                        new int[][]{
                                {-48},

                        }, -48
                }
        };
    }


}