package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CoinChange2Test {


    @Test(dataProvider = "testData")
    public void testClimbStairs(int amount, int[] arr, int expected) {
        var obj = new CoinChange2();
        assertEquals(obj.change(amount, arr), expected);
    }

    @DataProvider(name = "testData")
    public static Object[][] getTestData() {

        return new Object[][]{
                {5, new int[]{1,2,5}, 4},
                {10, new int[]{10}, 1},
        };
    }
}