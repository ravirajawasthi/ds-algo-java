package byteb.dynamic.programming;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MinimumScheduleJobDifficultyTest {

    @Test
    public void testMinDifficulty() {
        assertEquals(new MinimumScheduleJobDifficulty().minDifficulty(Utils.createArrayFromString("6 5 4 3 2 1"), 2), 7);
    }

    @Test
    public void testMinDifficultyNew() {
        assertEquals(new MinimumScheduleJobDifficulty().minDifficulty(Utils.createArrayFromString("11 111 22 222 33 333 44 444"), 6), 843);
    }

    @Test
    public void testMinDifficultyNew2() {
        assertEquals(new MinimumScheduleJobDifficulty().minDifficulty(Utils.createArrayFromString("186 398 479 206 885 423 805 112 925 656 16 932 740 292 671 360"), 4), 1803);
    }
}