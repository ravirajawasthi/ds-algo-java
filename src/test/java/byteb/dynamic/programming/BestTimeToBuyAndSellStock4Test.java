package byteb.dynamic.programming;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BestTimeToBuyAndSellStock4Test {

    @Test
    public void testMaxProfit() {
        assertEquals(new BestTimeToBuyAndSellStock4().maxProfit(2, new int[]{3, 2, 6, 5, 0, 3}), 7);
    }

    @Test
    public void testMaxProfitEdge() {
        assertEquals(new BestTimeToBuyAndSellStock4().maxProfit(2, new int[]{3, 3, 5, 0, 0, 3, 1, 4}), 6);
    }

    @Test
    public void testMaxProfitEdgeBottomUp() {
        assertEquals(new BestTimeToBuyAndSellStock4().maxProfit(2, new int[]{6,1,3,2,4,7}), 7);
    }

}