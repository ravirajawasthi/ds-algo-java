package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MinimunPathSumTest {

    @Test(dataProvider = "testData")
    public void testMinPathSum(int[][] arr, int expected) {
        int actual = MinimunPathSum.minPathSum(arr);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
                {
                        new int[][]{
                                {1, 3, 1},
                                {1, 5, 1},
                                {4, 2, 1}
                        }, 7
                }
        };
    }

}