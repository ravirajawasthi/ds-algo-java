package byteb.dynamic.programming;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CoinChangeTest {

    @Test
    public void testCoinChange() {
        int[] arr = new int[]{
                411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422
        };
        int expected = 24;
        assertEquals(expected, new CoinChange().coinChange(arr, 9864));
    }


    @Test
    public void testCoinChange2() {
        int[] arr = new int[]{
                6,5,2
        };
        int expected = 7;
        assertEquals(expected, new CoinChange().coinChange(arr, 37));
    }

    @Test
    public void testCoinChange3() {
        int[] arr = new int[]{
                96, 6, 8, 7, 1
        };
        int expected = 1;
        assertEquals(expected, new CoinChange().coinChange(arr, 96));
    }
}