package byteb.dynamic.programming;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PaintHouseTest {

    @Test(dataProvider = "testData")
    public void testMinCost(int[][] arr, int expected) {
        var obj = new PaintHouse();
        assertEquals(obj.minCost(arr), expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData() {
        return new Object[][]{
//                {new int[][]{{17, 2, 17}, {16, 16, 5}, {14, 3, 19}}, 10},
//                {new int[][]{{7, 6, 2}}, 2},
                {new int[][]{{3, 5, 3}, {6, 17, 6}, {7, 13, 18}, {9, 10, 18}}, 26},

        };
    }
}