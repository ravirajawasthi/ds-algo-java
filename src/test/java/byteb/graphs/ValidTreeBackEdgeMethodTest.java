package byteb.graphs;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ValidTreeBackEdgeMethodTest {

    @Test
    public void testValidTree() {
        var arr = Utils.twoDimensionalArrayFromString("[[0,1],[1,2],[2,3],[1,3],[1,4]]");
        var arr2 = Utils.twoDimensionalArrayFromString("[[0,1],[0,2],[0,3],[1,4]]");
        var arr3 = Utils.twoDimensionalArrayFromString("[[0,1],[2,3]]");

        assertFalse(new ValidTreeBackEdgeMethod().validTree(5, arr));
        assertTrue(new ValidTreeBackEdgeMethod().validTree(5, arr2));
        assertFalse(new ValidTreeBackEdgeMethod().validTree(3, arr3));
    }
}