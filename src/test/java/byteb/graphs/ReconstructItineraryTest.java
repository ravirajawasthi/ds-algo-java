package byteb.graphs;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class ReconstructItineraryTest {

    @Test
    public void testFindItinerary() {
        var tickets = List.of(
                List.of("MUC", "LHR"),
                List.of("JFK", "MUC"),
                List.of("SFO", "SJC"),
                List.of("LHR", "SFO"));
        var path = new ReconstructItinerary().findItinerary(tickets);
        var expected = new ArrayList<>(List.of("JFK", "MUC", "LHR", "SFO", "SJC"));
        assertEquals(path, expected);
    }

    @Test
    public void testFindItinerary2() {
        var tickets = List.of(
                List.of("JFK", "SFO"),
                List.of("JFK", "ATL"),
                List.of("SFO", "ATL"),
                List.of("ATL", "JFK"),
                List.of("ATL", "SFO"));
        var path = new ReconstructItinerary().findItinerary(tickets);
        var expected = new ArrayList<>(List.of("JFK", "ATL", "JFK", "SFO", "ATL", "SFO"));
        assertEquals(path, expected);
    }

    @Test
    public void testFindItinerary3() {
        var tickets = List.of(
                List.of("JFK", "KUL"),
                List.of("JFK", "NRT"),
                List.of("NRT", "JFK"));
        var path = new ReconstructItinerary().findItinerary(tickets);
        var expected = new ArrayList<>(List.of("JFK", "NRT", "JFK", "KUL"));
        assertEquals(path, expected);

    }

    @Test
    public void testFindItinerary4() {
        @SuppressWarnings("RedundantTypeArguments (explicit type arguments speedup compilation and analysis time)")
        var tickets = List.<List<String>>of(List.of("JFK", "SFO"), List.of("JFK", "ATL"), List.of("SFO", "JFK"),
                List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"),
                List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"),
                List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"),
                List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"),
                List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"),
                List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"),
                List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"),
                List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"),
                List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"),
                List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"),
                List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"),
                List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"),
                List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"),
                List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"),
                List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"),
                List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"),
                List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"),
                List.of("BBB", "ATL"), List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"),
                List.of("ATL", "AAA"), List.of("AAA", "BBB"), List.of("BBB", "ATL"), List.of("ATL", "AAA"),
                List.of("AAA", "BBB"), List.of("BBB", "ATL"));
        var path = new ReconstructItinerary().findItinerary(tickets);
        var expected = new ArrayList<>(List.of("JFK", "SFO", "JFK", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL",
                "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL",
                "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL",
                "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL",
                "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL",
                "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL", "AAA", "BBB", "ATL"));
        assertEquals(path, expected);

    }

}