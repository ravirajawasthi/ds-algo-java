package byteb.graphs;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TheMaze2Test {

    @Test
    public void testShortestDistance() {
        var matrix = Utils.twoDimensionalArrayFromString("[[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]");
        int[] start = new int[]{0, 4};
        int[] target = new int[]{4, 4};

        var actual = new TheMaze2().shortestDistance(matrix, start, target);
        assertEquals(actual, 12);
    }

    @Test
    public void testShortestDistance2() {
        var matrix = Utils.twoDimensionalArrayFromString("[[0,0,0,0,1,0,0],[0,0,1,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,1],[0,1,0,0,0,0,0],[0,0,0,1,0,0,0],[0,0,0,0,0,0,0],[0,0,1,0,0,0,1],[0,0,0,0,1,0,0]]");
        int[] start = new int[]{0, 0};
        int[] target = new int[]{8, 6};

        var actual = new TheMaze2().shortestDistance(matrix, start, target);
        assertEquals(actual, 26);
    }

}