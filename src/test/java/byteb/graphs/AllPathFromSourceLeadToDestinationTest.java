package byteb.graphs;

import byteb.utils.Utils;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AllPathFromSourceLeadToDestinationTest {

    @Test
    public void testLeadsToDestination() {

        var actual = new AllPathFromSourceLeadToDestination().leadsToDestination(3, new int[][]{{0, 1}, {0, 2}}, 0, 2);
        assertFalse(actual);

    }

    @Test
    public void testLeadsToDestination2() {

        var actual = new AllPathFromSourceLeadToDestination().leadsToDestination(4, Utils.twoDimensionalArrayFromString("[[0,1],[0,3],[1,2],[2,1]]"), 0, 3);
        assertFalse(actual);

    }

    @Test
    public void testLeadsToDestination3() {

        var actual = new AllPathFromSourceLeadToDestination().leadsToDestination(4, Utils.twoDimensionalArrayFromString("[[0,1],[0,2],[1,3],[2,3]]"), 0, 3);
        assertTrue(actual);

    }

    @Test
    public void testLeadsToDestinationSelfLoop() {

        var actual = new AllPathFromSourceLeadToDestination().leadsToDestination(3, Utils.twoDimensionalArrayFromString("[[0,1],[1,1],[1,2]]"), 0, 2);
        assertFalse(actual);

    }


    @Test
    public void testLeadsToDestinationSelfLoop2() {

        var actual = new AllPathFromSourceLeadToDestination().leadsToDestination(2, Utils.twoDimensionalArrayFromString("[[0,1],[1,1]]"), 0, 2);
        assertFalse(actual);

    }

    @Test
    public void testLeadsToDestinationSelfLoop3() {

        var actual = new AllPathFromSourceLeadToDestination().leadsToDestination(5, Utils.twoDimensionalArrayFromString("[[0,1],[1,2],[2,3],[3,4]]"), 1, 3);
        assertFalse(actual);

    }



}