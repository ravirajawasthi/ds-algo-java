package byteb.graphs;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class IsGraphBiPartiteTest {

    @Test
    public void testIsBipartite() {
        int[][] graph = new int[][]{{1, 3}, {0, 2}, {1, 3}, {0, 2}};
        assertTrue(IsGraphBiPartite.isBipartite(graph));
    }

    @Test
    public void testIsBipartite2() {
        int[][] graph = new int[][]{{1, 2, 3}, {0, 2}, {0, 1, 3}, {0, 2}};
        assertFalse(IsGraphBiPartite.isBipartite(graph));
    }

    @Test
    public void testIsBipartite3() {
        int[][] graph = new int[][]{
                {}, {2, 4, 6}, {1, 4, 8, 9},
                {7, 8}, {1, 2, 8, 9}, {6, 9},
                {1, 5, 7, 8, 9}, {3, 6, 9},
                {2, 3, 4, 6, 9}, {2, 4, 5, 6, 7, 8}
        };
        assertEquals(false, IsGraphBiPartite.isBipartite(graph));
    }

    @Test
    public void testIsBipartite4() {
        int[][] graph = new int[][]{{}, {3}, {}, {1}, {}};
        assertEquals(IsGraphBiPartite.isBipartite(graph), true);
    }


}