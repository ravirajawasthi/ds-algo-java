package byteb.sandbox;

import byteb.utils.Utils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PartitionIntoSubSetWithMinDiffTest {

    @Test(dataProvider = "testData")
    public void testMinSubsetSumDifference(int[] arr, int n, int expected) {
        int actual = PartitionIntoSubSetWithMinDiff.minSubsetSumDifference(arr, n);
        assertEquals(actual, expected);
    }

    @DataProvider(name = "testData")
    Object[][] getTestData(){
        return new Object[][]{
                {Utils.createArrayFromString("4 1 2 3"), 4, 0},
                {Utils.createArrayFromString("3 5 15 2"), 4, 5}
        };
    }
}