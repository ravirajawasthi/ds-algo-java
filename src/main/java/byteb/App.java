package byteb;


import byteb.strings.and.arrays.builder.StringBuilderBenchmark;
import byteb.strings.and.arrays.map.HashMapBenchmark;

public class App {

    public static void main(String[] args) throws Exception {
        System.out.println("Author : Raviraj Awasthi");
        System.out.println("================StringBuilder test===================================");

        StringBuilderBenchmark.execute();

        System.out.println("================LinkedList HashMap test===================================");

        HashMapBenchmark.execute();
    }
}
