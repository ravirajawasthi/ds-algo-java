package byteb.sandbox;

import java.util.Arrays;

public class SubsetWithSumEqualToK {

    public static boolean canPartition(int[] arr, int n) {
        int sum = Arrays.stream(arr).sum();
        if (sum % 2 != 0) {
            return false;
        } else {
            return canPartitionDP(arr, sum / 2);
        }
    }

    private static boolean canPartitionDP(int[] arr, int k) {
        boolean[] curr = new boolean[k + 1];
        boolean[] prev = new boolean[k + 1];

        curr[0] = prev[0] = true;

        for (int i = 0; i < arr.length; i++) {
            for (int target = 1; target <= k; target++) {
                if (arr[i] > target) {
                    curr[target] = prev[target];
                } else {
                    curr[target] = prev[target] || prev[target - arr[i]];
                }
            }
            prev = curr;
            curr = new boolean[k + 1];
        }

        return prev[k];

    }

}
