package byteb.sandbox;

import java.util.Arrays;

public class PartitionIntoSubSetWithMinDiff {

    public static int minSubsetSumDifference(int[] arr, int n) {
        return minsSubSetDiffUtil(arr, n);
    }

    private static int minsSubSetDiffUtil(int[] arr, int n) {
        int sum = Arrays.stream(arr).sum();
        boolean[] possibleSum = minSubSetSumDifferenceDP(arr, sum);
        int min = Integer.MAX_VALUE;
        for (int i = 1; i <=sum; i++) {
            if (possibleSum[i]) {
                min = Math.min(min, Math.abs(sum - 2 * i));
            }
        }
        return min;
    }

    private static boolean[] minSubSetSumDifferenceDP(int[] arr, int k) {
        boolean[] curr = new boolean[k + 1];
        boolean[] prev = new boolean[k + 1];

        curr[0] = prev[0] = true;

        for (int i = 0; i < arr.length; i++) {
            for (int target = 1; target <= k; target++) {
                if (arr[i] > target) {
                    curr[target] = false;
                } else {
                    curr[target] = prev[target] || prev[target - arr[i]];
                }
            }
            prev = curr;
            curr = new boolean[k + 1];
            curr[0] = true;
        }

        return prev;
    }

}
