package byteb.stacks.and.queues;

import java.util.HashMap;

public class LRUCache {

    private final int capacity;
    private int currsize;

    DoubleListNode forwardNode;
    DoubleListNode endNode;

    HashMap<Integer, DoubleListNode> hashmap;

    LRUCache(int capacity) {
        this.capacity = capacity;
        this.currsize = 0;
        forwardNode = null;
        endNode = null;
        hashmap = new HashMap<>();
    }


    int get(int index) {
        if (!hashmap.containsKey(index)) {
            //Key does not exist
            return -1;
        }

        //The node exists

        //If the node is forward node, nothing needs to be done
        if (forwardNode.index == index) {
            return forwardNode.value;
        }

        var node = hashmap.get(index);
        if (node == endNode) {
            //Need to update end node if node we are accessing is endNode
            endNode = endNode.previous;
            endNode.next = null;
        }

        //Deleting node
        deleteNode(node);

        //Getting new forward node and maintaining double linked list structure
        var next = forwardNode;
        forwardNode = new DoubleListNode(index, node.value, forwardNode);

        //If Forward node has a capacity of 1, then there will be no next
        if (next != null) {
            next.previous = forwardNode;
        }

        //Updating the hashmap, index remains same but new node is created
        hashmap.put(index, forwardNode);

        //the accessed node is moved to front
        return forwardNode.value;
    }

    void put(int index, int value) {

        if (hashmap.containsKey(index)) {
            var node = hashmap.get(index);
            if (node == forwardNode) {
                //If the node to be changes is already forward node then nothing needs to be done
                forwardNode.value = value;
                return;
            } else if (node == endNode) {
                //In 2 cases we have to remove endnode, once we are on capacity and 2nd, then the updated node is endnode
                hashmap.remove(endNode.index);
                endNode = endNode.previous;
                if (endNode != null) {
                    //In case capacity is 1
                    endNode.next = null;
                }
            }

            deleteNode(node);
            //we have the node, so need to remove its entry from hashtable
            hashmap.remove(index);
            currsize--;
        }
        if (capacity == currsize) {
            //Removing the last used entry
            hashmap.remove(endNode.index);
            endNode = endNode.previous;
            if (endNode != null) {
                //In case capacity is 1
                endNode.next = null;
            }
            currsize--;
        }


        var next = forwardNode;
        forwardNode = new DoubleListNode(index, value, forwardNode);
        if (next != null) {
            //This check, in case capacity is only 1
            next.previous = forwardNode;
        }
        hashmap.put(index, forwardNode);
        currsize++;
        if (currsize == 1) {
            //this means its the first put, or the capacity is only 1
            endNode = forwardNode;
        }
    }


    void deleteNode(DoubleListNode node) {
        var prev = node.previous;
        var next = node.next;

        if (prev != null) {
            prev.next = next;
        }

        if (next != null) {
            next.previous = prev;
        }
    }


}
