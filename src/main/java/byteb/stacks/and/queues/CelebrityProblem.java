package byteb.stacks.and.queues;

import java.util.ArrayDeque;

//TODO Not efficient

public class CelebrityProblem {
    public static void main(String[] args) {
        System.out.println("celebrity(int[][]{}) = " + celebrity(new int[][]{{0, 1, 0},
                {0, 0, 0}, {0, 1, 0}}, 3));
    }


    static int celebrity(int[][] M, int n) {

        ArrayDeque<Integer> stack = new ArrayDeque<>();

        for (int i = 0; i < n; i++) {
            stack.push(i);
        }

        while (!stack.isEmpty()) {
            int c1 = stack.pop();
            if (stack.isEmpty()) {
                var goodCol = true;
                for (int i = 0; i < n; i++) {
                    if (i == c1) {
                        continue;
                    } else if (M[i][c1] != 1) {
                        goodCol = false;
                    }
                }
                if (!goodCol) {
                    return -1;
                }

                for (int i = 0; i < n; i++) {
                    if (M[c1][i] == 1) {
                        return -1;
                    }
                }

                return c1;
            }
            var c2 = stack.pop();

            if (M[c1][c2] == 0 && M[c2][c1] == 1) {
                stack.push(c1);
            } else if (M[c2][c1] == 0 && M[c1][c2] == 1) {
                stack.push(c2);
            }

        }
        return -1;

    }
}
