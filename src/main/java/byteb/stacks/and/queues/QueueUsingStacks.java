package byteb.stacks.and.queues;

//https://leetcode.com/problems/implement-queue-using-stacks/

import java.util.Stack;

public class QueueUsingStacks {

    Stack<Integer> stackPush;
    Stack<Integer> stackPop;


    public QueueUsingStacks() {
        stackPush = new Stack<>();
        stackPop = new Stack<>();
    }


    public void push(int x) {
        stackPush.push(x);
    }


    public int pop() {
        this.checkPopStackHealth();
        return stackPop.pop();
    }

    public int peek() {
        this.checkPopStackHealth();
        return stackPop.peek();
    }

    public boolean empty() {
        return stackPop.isEmpty() && stackPush.isEmpty();
    }

    private void checkPopStackHealth() {
        if (stackPop.isEmpty()) {
            while (!stackPush.isEmpty()) {
                stackPop.push(stackPush.pop());
            }
        }
    }
}
