package byteb.stacks.and.queues;

import java.util.Arrays;

//https://leetcode.com/problems/dinner-plate-stacks/

public class DinnerPlates {
    public static final double RESIZE_FACTOR = 2;
    int capacity;

    int noOfStacks;

    MinStack[] stackArray;

    /* These should be ready for every function beforehand */
    int popIndex; //Rightmost not Empty to pushTo
    int pushIndex; //Leftmost empty index to pushto


    public DinnerPlates(int capacity) {
        this.capacity = capacity;
        noOfStacks = 3;
        stackArray = new MinStack[ noOfStacks ];
        for (int i = 0 ; i < noOfStacks ; i++) {
            stackArray[ i ] = new MinStack(capacity);
        }
        popIndex = 0;
        pushIndex = 0;
    }

    public void push(int val) {
        stackArray[ pushIndex ].push(val);
        popIndex = Math.max(popIndex, pushIndex);

        // Current stack is full
        var currStackFull = stackArray[ pushIndex ].isFull();

        //Need to get more stacks
        if (currStackFull && pushIndex == noOfStacks - 1) {
            pushIndex = noOfStacks;
            noOfStacks *= RESIZE_FACTOR;
            stackArray = Arrays.copyOf(stackArray, noOfStacks);
            for (int i = pushIndex ; i < noOfStacks ; i++) {
                stackArray[ i ] = new MinStack(capacity);
            }
        }
        //Now Push to next stack
        else if (currStackFull) {
            for (int i = pushIndex ; i < noOfStacks ; i++) {
                if (!stackArray[ i ].isFull()) {
                    pushIndex = i;
                    return;
                }
            }

            //More stacks required
            pushIndex = noOfStacks;
            noOfStacks *= RESIZE_FACTOR;
            stackArray = Arrays.copyOf(stackArray, noOfStacks);
            for (int i = pushIndex ; i < noOfStacks ; i++) {
                stackArray[ i ] = new MinStack(capacity);
            }
        }
    }

    public int pop() {
        for (int i = popIndex ; i > -1 ; i--) {
            if (!stackArray[ i ].isEmpty()) {
                popIndex = i;

                var returnElement = stackArray[ i ].pop();

                if (stackArray[ pushIndex ].isEmpty() && i + 1 == pushIndex) {
                    pushIndex = i;
                }

                return returnElement;
            }
        }
        return -1;
    }

    public int popAtStack(int index) {
        var insufficientStacks = index > ( noOfStacks - 1 );

        //Getting more stacks
        if (insufficientStacks) {
            var initialSize = noOfStacks;
            while (noOfStacks < index) {
                noOfStacks *= RESIZE_FACTOR;
            }
            stackArray = Arrays.copyOf(stackArray, noOfStacks);
            for (int i = initialSize ; i < noOfStacks ; i++) {
                stackArray[ i ] = new MinStack(capacity);
            }
            return -1;
        }
        var returnElement = stackArray[ index ].pop();
        if (returnElement != -1) {
            pushIndex = index;
        }
        return returnElement;
    }


    static class MinStack {

        int currIndex;
        int[] arr;
        int capacity;

        public MinStack(int capacity) {
            this.capacity = capacity;
            arr = new int[ capacity ];
            currIndex = -1;
        }

        public void push(int val) {
            arr[ ++currIndex ] = val;
        }

        public int pop() {
            var top = this.top();
            if (top != -1) {
                currIndex--;
            }
            return top;

        }

        public int top() {
            if (currIndex == -1) {
                return -1;
            } else {
                return arr[ currIndex ];
            }
        }

        public boolean isFull() {
            return currIndex == capacity - 1;
        }

        public boolean isEmpty() {
            return currIndex == -1;
        }

    }


}


