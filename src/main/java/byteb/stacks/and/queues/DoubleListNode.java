package byteb.stacks.and.queues;

class DoubleListNode {

    DoubleListNode(int index, int value) {
        this.value = value;
        this.index = index;
        next = null;
        this.previous = null;
    }

    DoubleListNode(int index, int data, DoubleListNode next) {
        this.value = data;
        this.index = index;
        this.next = next;
        this.previous = null;
    }

    DoubleListNode(int index, int data, DoubleListNode next, DoubleListNode previous) {
        this.value = data;
        this.index = index;
        this.next = next;
        this.previous = previous;
    }

    public int index;
    public int value;
    public DoubleListNode next;
    public DoubleListNode previous;
}