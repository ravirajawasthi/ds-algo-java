package byteb.stacks.and.queues;

import java.util.*;

//https://practice.geeksforgeeks.org/problems/stock-span-problem-1587115621/1
public class StockSpanProblem {
    public static void main(String[] args) {
        System.out.println("calculateSpan(new int[], 0) = " + Arrays.toString(calculateSpan(new int[]{100, 80, 60, 70, 60, 75, 85}, 7)));
    }

    public static int[] calculateSpan(int[] prices, int n) {
        ArrayDeque<Pair> stack = new ArrayDeque<>();
        int[] stockSpan = new int[n];
        stockSpan[0] = 1;
        stack.push(new Pair(prices[0], 1));
        int num;
        Pair bucket;
        for (int i = 1; i < n; i++) {
            num = prices[i];
            bucket = stack.peek();
            if (bucket == null) {
                stack.push(new Pair(num, 1));
                stockSpan[i] = 1;
            } else if (bucket.left > num) {
                stockSpan[i] = 1;
                stack.push(new Pair(num, 1));
            } else {
                int span = 1;
                while (!stack.isEmpty() && stack.peek().left <= num) {
                    span += stack.pop().right;
                }
                stack.push(new Pair(num, span));
                stockSpan[i] = span;
            }
        }


        return stockSpan;

    }

    private static class Pair {
        Pair(int l, int r) {
            this.left = l;
            this.right = r;
        }

        public int left;
        public int right;
    }
}
