package byteb.stacks.and.queues.stack.array.impl;

import java.util.Arrays;

class MinStack {

    int currIndex;
    int[] arr;
    int currMin;
    int currCapacity;

    public MinStack() {
        currIndex = -1;
        currMin = Integer.MAX_VALUE;
        currCapacity = 100;
        arr = new int[currCapacity];
    }

    public void push(int val) {
        if (currIndex + 1 == currCapacity){
            currCapacity += currCapacity*0.5;
            arr = Arrays.copyOf(arr, currCapacity);
        }
        arr[++currIndex] = val;
        currMin = Math.min(currMin, val);
    }

    public void pop() {
        var top = this.top();
        if (top != null){
            currIndex--;
        }
        if (top == currMin){
            currMin = Integer.MAX_VALUE;
            for (int i = 0; i <= currIndex; i++){
                currMin = Math.min(currMin, arr[i]);
            }
        }
    }

    public Integer top() {
        if (currIndex == -1){
            return null;
        }else{
            return arr[currIndex];
        }
    }

    public Integer getMin() {
        if (currIndex == -1){
            return null;
        }
        return this.currMin;
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(val);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */