package byteb.stacks.and.queues.stack.ll.impl;

public class StackLL<T> {

    private int size;
    private StackNode<T> head;

    //POP
    //PEEK
    //PUSH
    //IS_EMPTY
    //SIZE

    private static class StackNode<T> {
        public T data;
        public StackNode<T> next;
    }


    public StackLL() {
        this.size = 0;
        this.head = null;
    }

    public boolean isEmpty() {
        return this.head == null;
    }

    public int size() {
        return this.size;
    }

    public void push(T data) {
        if (head == null) {
            head = new StackNode<>();
            head.data = data;

        } else {
            var newNode = new StackNode<T>();
            newNode.data = data;
            newNode.next = head;
            head = newNode;
        }
        size++;
    }

    public T peek() {
        if (head == null) {
            return null;
        } else {
            return head.data;
        }
    }

    public T pop() {
        var topElement = this.peek();
        if (topElement != null) {
            head = head.next;
            size--;
        }

        return topElement;
    }


}
