package byteb.graphs;

import java.util.*;

public class TheMaze2 {

    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        HashMap<Pair<Integer, Integer>, Integer> visited = new HashMap<>();
        //PQ is mandatory
        PriorityQueue<Pair<int[], Integer>> queue = new PriorityQueue<>((o1, o2) -> o1.value - o2.value);
        queue.add(new Pair<>(start, 0));
        while (!queue.isEmpty()) {
            var curr = queue.remove();
            int[] coordinate = curr.getKey();
            int dist = curr.getValue();
            if (visited.containsKey(new Pair<>(coordinate[0], coordinate[1]))) continue;
            visited.put(new Pair<>(coordinate[0], coordinate[1]), dist);
            ArrayList<int[]> neighbours = new ArrayList<>();

            //Up neighbour
            int[] upNeighbour = new int[]{coordinate[0], coordinate[1]};
            while (true) {
                upNeighbour[0]--;
                if (upNeighbour[0] < 0 || maze[upNeighbour[0]][upNeighbour[1]] == 1) {
                    upNeighbour[0]++;
                    break;
                }
            }

            if (!Arrays.equals(upNeighbour, coordinate)) neighbours.add(upNeighbour);

            //Down neighbour
            int[] downNeighbour = new int[]{coordinate[0], coordinate[1]};
            while (true) {
                downNeighbour[0]++;
                if (downNeighbour[0] >= maze.length || maze[downNeighbour[0]][downNeighbour[1]] == 1) {
                    downNeighbour[0]--;
                    break;
                }
            }

            if (!Arrays.equals(downNeighbour, coordinate)) neighbours.add(downNeighbour);

            //Left neighbour
            int[] leftNeighbour = new int[]{coordinate[0], coordinate[1]};
            while (true) {
                leftNeighbour[1]--;
                if (leftNeighbour[1] < 0 || maze[leftNeighbour[0]][leftNeighbour[1]] == 1) {
                    leftNeighbour[1]++;
                    break;
                }
            }

            if (!Arrays.equals(leftNeighbour, coordinate)) neighbours.add(leftNeighbour);


            //right neighbour
            int[] rightNeighbour = new int[]{coordinate[0], coordinate[1]};
            while (true) {
                rightNeighbour[1]++;
                if (rightNeighbour[1] >= maze[0].length || maze[rightNeighbour[0]][rightNeighbour[1]] == 1) {
                    rightNeighbour[1]--;
                    break;
                }
            }

            if (!Arrays.equals(rightNeighbour, coordinate)) neighbours.add(rightNeighbour);

            for (var neighbour : neighbours) {
                if (!visited.containsKey(new Pair<>(neighbour[0], neighbour[1]))) {
                    int neighbourDist = Math.abs(coordinate[0] - neighbour[0]) + Math.abs(coordinate[1] - neighbour[1]);
                    queue.add(new Pair<>(neighbour, neighbourDist + dist));
                }
            }


        }

        if (visited.containsKey(new Pair<>(destination[0], destination[1]))) {
            return visited.get(new Pair<>(destination[0], destination[1]));
        }
        return -1;


    }

    public class Pair<K, V> {
        K key;
        V value;

        public Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair<?, ?> pair = (Pair<?, ?>) o;

            if (!key.equals(pair.key)) return false;
            return value.equals(pair.value);
        }

        @Override
        public int hashCode() {
            int result = key.hashCode();
            result = 31 * result + value.hashCode();
            return result;
        }
    }
}
