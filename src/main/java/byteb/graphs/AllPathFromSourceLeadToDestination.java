package byteb.graphs;

//https://leetcode.com/problems/all-paths-from-source-lead-to-destination/

import java.util.ArrayList;

public class AllPathFromSourceLeadToDestination {
    Boolean[] valid;
    ArrayList<ArrayList<Integer>> graph;

    public boolean leadsToDestination(int n, int[][] edges, int source, int destination) {
        graph = new ArrayList<>();

        for (int i = 0; i < n; i++) graph.add(new ArrayList<>());

        for (var edge : edges) {
            int u = edge[0];
            int v = edge[1];
            graph.get(u).add(v);
        }
        valid = new Boolean[n];
        return dfs(source, destination);

    }

    private boolean dfs(int currElement, int destination) {
        if (valid[currElement] != null) {
            return valid[currElement];
        }

        if (graph.get(currElement).isEmpty()) {
            return currElement == destination;
        }

        for (int neighbour : graph.get(currElement)) {
            valid[currElement] = false;
            var res = dfs(neighbour, destination);
            if (!res) return false;
        }
        valid[currElement] = true;
        return true;


    }

}
