package byteb.graphs;

import java.util.*;

public class CloneGraph {
    HashMap<Node, Node> oldNewNodeMapping;

    public Node cloneGraph(Node node) {
        if (Objects.isNull(node)) return null;
        oldNewNodeMapping = new HashMap<>();
        Node returnNode = new Node(node.val, new ArrayList<>());
        for (Node neighbour : node.neighbors) {
            returnNode.neighbors.add(dfs(neighbour));
        }

        for (Node originalNode: oldNewNodeMapping.keySet()){
            System.out.println("ORIGINAL " + originalNode);
            for (Node neighbor: originalNode.neighbors)
                System.out.println(originalNode.val + "->" + neighbor.val + ":" +neighbor);
            System.out.println("----------");
            System.out.println("COPY " + oldNewNodeMapping.get(originalNode));
            for (Node neighbor: oldNewNodeMapping.get(originalNode).neighbors)
                System.out.println(oldNewNodeMapping.get(originalNode).val + "->" + neighbor.val+":"+neighbor);
            System.out.println("===========================================================");

        }
        return returnNode;
    }

    Node dfs(Node node) {
        if (oldNewNodeMapping.containsKey(node))
            return oldNewNodeMapping.get(node);
        Node newNode = new Node(node.val, new ArrayList<>());
        oldNewNodeMapping.put(node, newNode);

        for (Node neighbours : node.neighbors) {
            newNode.neighbors.add(dfs(neighbours));
        }
        return newNode;
    }

    private static class Node {
        public int val;
        public List<Node> neighbors;

        public Node() {
            val = 0;
            neighbors = new ArrayList<Node>();
        }

        public Node(int _val) {
            val = _val;
            neighbors = new ArrayList<Node>();
        }

        public Node(int _val, ArrayList<Node> _neighbors) {
            val = _val;
            neighbors = _neighbors;
        }
    }
}
