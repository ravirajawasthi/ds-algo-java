package byteb.graphs;

import java.util.*;

public class ReconstructItinerary {
    LinkedList<String> path;
    HashMap<String, PriorityQueue<String>> graph;
    int size;

    public List<String> findItinerary(List<List<String>> tickets) {
        size = tickets.size();
        graph = new HashMap<>();

        for (var ticket : tickets) {
            String source = ticket.get(0);
            String destination = ticket.get(1);

            graph.putIfAbsent(source, new PriorityQueue<>());

            graph.get(source).add(destination);
        }

        path = new LinkedList<>();
        dfs("JFK");
        return path;
    }

    private void dfs(String currLocation) {

        if (graph.getOrDefault(currLocation, new PriorityQueue<>()).isEmpty()) {
            path.offerFirst(currLocation);
            return;
        }

        while (!graph.get(currLocation).isEmpty()) {
            String neighbour = graph.get(currLocation).remove();
            dfs(neighbour);

        }
        path.offerFirst(currLocation);

    }

}
