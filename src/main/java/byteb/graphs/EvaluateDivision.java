package byteb.graphs;

import java.util.*;

public class EvaluateDivision {

    public double[] calcEquation(List<List<String>> equations, double[] values, List<List<String>> queries) {
        HashMap<String, HashMap<String, Double>> graph = new HashMap<>();
        for (int i = 0; i < equations.size(); i++) {
            var eq = equations.get(i);
            String u = eq.get(0);
            String v = eq.get(1);
            double w = values[i];
            graph.putIfAbsent(u, new HashMap<>());
            graph.putIfAbsent(v, new HashMap<>());

            graph.get(u).put(v, w);
            graph.get(v).put(u, 1 / w);
        }
        double[] ans = new double[queries.size()];
        for (int i = 0; i < queries.size(); i++) {
            var query = queries.get(i);
            String u = query.get(0);
            String v = query.get(1);

            if (!graph.containsKey(u) || !graph.containsKey(v)) {
                ans[i] = -1;
                continue;
            }

            if (u.equals(v)) {
                ans[i] = 1;
                continue;
            }
            ans[i] = dfs(u, v, graph, new HashSet<>(), 1);


        }
        return ans;

    }

    double dfs(String element, String target, HashMap<String, HashMap<String, Double>> graph, HashSet<String> visited, double currAns) {
        visited.add(element);
        for (var neighbour : graph.get(element).keySet()) {
            if (!visited.contains(neighbour)) {
                if (neighbour.equals(target)) {
                    return graph.get(element).get(neighbour) * currAns;
                }
                var ans = dfs(neighbour, target, graph, visited, currAns * graph.get(element).get(neighbour));
                if (ans != -1) {
                    return ans;
                }
            }
        }
        return -1;


    }

}
