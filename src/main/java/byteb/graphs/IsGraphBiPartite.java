package byteb.graphs;

import java.util.ArrayDeque;
import java.util.Queue;

public class IsGraphBiPartite {

    public static boolean isBipartite(int[][] graph) {
        int n = graph.length;





        boolean[] visited = new boolean[n];
        boolean isBipartite = true;

        for (int i = 0; i < n && isBipartite; i++) {
            if (visited[i]){
                continue;
            }
            char[] color = new char[n];

            Queue<Integer> queue = new ArrayDeque<>();

            queue.add(i);
            color[i] = 'r';


            while (!queue.isEmpty() && isBipartite) {
                int currElement = queue.remove();
                visited[currElement] = true;
                for (int edge : graph[currElement]) {
                    if (color[edge] == '\0') {
                        color[edge] = color[currElement] == 'r' ? 'b' : 'r';
                        queue.add(edge);
                    } else {
                        if (color[edge] == color[currElement]) {
                            isBipartite = false;
                            break;
                        }
                    }

                }
            }


        }
        return isBipartite;


    }


}