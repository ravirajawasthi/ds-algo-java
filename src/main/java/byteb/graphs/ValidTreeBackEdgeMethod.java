package byteb.graphs;

import java.util.ArrayList;
import java.util.HashSet;

public class ValidTreeBackEdgeMethod {

    HashSet<Integer> seen;
    ArrayList<ArrayList<Integer>> graph;


    public boolean validTree(int n, int[][] edges) {
        //Graph Init
        if (edges.length != n - 1) return false;

        graph = new ArrayList<>();
        for (int i = 0; i <= n; i++) {
            graph.add(new ArrayList<>());

        }

        for (var edge : edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }

        seen = new HashSet<>();


        return dfs(-1, 0) && seen.size() == n;


    }

    private boolean dfs(int v, int n) {
        if (seen.contains(n))
            return false;
        seen.add(n);
        for (int i : graph.get(n)) {
            if (i != v) {
                if (!dfs(n, i))
                    return false;
            }
        }
        return true;
    }



}
