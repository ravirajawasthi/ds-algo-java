package byteb.graphs;

import java.util.ArrayList;
import java.util.List;

//https://leetcode.com/problems/all-paths-from-source-to-target/description/

class AllPathsFromSourceToDestination {
    List<List<Integer>> res;
    int[][] graph;

    boolean[] visited;

    public List<List<Integer>> allPathsSourceTarget(int[][] graph) {
        this.graph = graph;
        this.res = new ArrayList<>();

        this.visited = new boolean[graph.length];
        dfs(0, new ArrayList<>(List.of(0)));

        return this.res;
    }

    private void dfs(int currElement, ArrayList<Integer> currPath) {
        if (currElement == this.graph.length - 1) {
            this.res.add(new ArrayList<>(currPath));
            return;
        }
        if (visited[currElement]) return;
        visited[currElement] = true;
        for (int neighbour : graph[currElement]) {
            if (!visited[neighbour]) {
                currPath.add(neighbour);
                dfs(neighbour, currPath);
                currPath.remove(currPath.size() - 1);
            }

        }
        visited[currElement] = false;
    }
}
