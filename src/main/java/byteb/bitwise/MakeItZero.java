package byteb.bitwise;

import java.io.*;
import java.util.StringTokenizer;

public class MakeItZero {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                for (int i = 0; i < n; i++) {
                    sc.nextInt();
                }
                if (n % 2 == 0) {
                    out.println(2);
                    out.println(1 + " " + n);
                    out.println(1 + " " + n);
                } else {
                    out.println(4);
                    out.println(1 + " " + (n - 1));
                    out.println(1 + " " + (n - 1));
                    out.println((n - 1) + " " + n);
                    out.println((n - 1) + " " + n);
                }


            }

            //Solution End
        }

        private String padString(String s) {
            int padding = 7 - s.length();
            return "0".repeat(Math.max(0, padding)) + s;
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
