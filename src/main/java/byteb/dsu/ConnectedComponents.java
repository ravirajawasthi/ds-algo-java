package byteb.dsu;

public class ConnectedComponents {
    public int countComponents(int n, int[][] edges) {
        var dsu = new DSU(n);
        for (var edge : edges) {
            dsu.union(edge[0], edge[1]);
        }

        return dsu.groups;
    }

    private static class DSU {

        final private int[] parent;
        public int groups;

        DSU(int n) {
            groups = n;
            parent = new int[n];
            for (int i = 0; i < n; i++) {
                this.parent[i] = i;
            }
        }

        public int find(int x) {
            if (this.parent[x] == x) {
                return x;
            }
            this.parent[x] = find(this.parent[x]);
            return this.parent[x];
        }

        public void union(int x, int y) {
            int xP = find(x);
            int yP = find(y);
            if (xP != yP) {
                groups--;
                parent[yP] = xP;
            }
        }

    }
}
