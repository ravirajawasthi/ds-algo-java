package byteb.dsu;

import java.util.List;

class SmallestStringWithSwaps {
    public String smallestStringWithSwaps(String s, List<List<Integer>> pairs) {
        //DSU Initialization
        var dsu = new DSU(s.length());

        for (var pair : pairs) {
            dsu.union(pair.get(0), pair.get(1));
        }

        int[][] rootCharArr = new int[s.length()][];
        for (int i = 0; i < s.length(); i++) {
            var parent = dsu.find(i);
            if (rootCharArr[parent] == null) {
                rootCharArr[parent] = new int[26]; //all lower case
            }
            rootCharArr[parent][s.charAt(i) - 'a']++; //This will automatically sort the string for that particular node
        }

        char[] string = new char[s.length()];

        for (int i = 0; i < s.length(); i++) {
            int parent = dsu.find(i);
            if (rootCharArr[parent] == null) continue;
            int p = 0;
            while (p < 26) {
                if (rootCharArr[parent][p] != 0) {
                    string[i] = (char) (p + 'a');
                    rootCharArr[parent][p]--;
                    break;
                }
                p++;
            }
        }

        return new String(string);

    }

    private static class DSU {
        private final int[] parent;
        private final int[] rank;

        DSU(int n) {
            this.parent = new int[n];
            this.rank = new int[n];
            for (int i = 0; i < n; i++) {
                parent[i] = i;
                rank[i] = 1;
            }
        }

        int find(int i) {
            if (i == parent[i]) {
                return i;
            }
            return parent[i] = find(parent[i]);
        }

        void union(int x, int y) {
            int xP = find(x);
            int yP = find(y);
            if (xP != yP) {
                if (rank[xP] > rank[yP]) {
                    this.parent[yP] = xP;
                } else if (rank[yP] > rank[xP]) {
                    this.parent[xP] = yP;
                } else {
                    this.parent[xP] = yP;
                    this.rank[yP]++;
                }
            }
        }

    }

}