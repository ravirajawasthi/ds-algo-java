package byteb.dsu;

//https://leetcode.com/problems/optimize-water-distribution-in-a-village/

import java.util.Arrays;

public class WaterDistributionToAVillage {

    public int minCostToSupplyWater(int n, int[] wells, int[][] pipes) {
        int e = pipes.length;
        pipes = Arrays.copyOf(pipes, e + n);
        for (int i = 1; i <= n; i++) {
            pipes[e + i - 1] = new int[]{i, n + 1, wells[i - 1]};
        }

        Arrays.sort(pipes, (p1, p2) -> p1[2] - p2[2]);

        var dsu = new DSU(n + 1);

        int cost = 0;
        int addedEdges = 0;
        for (var edge : pipes) {
            int x = edge[0];
            int y = edge[1];
            if (dsu.union(x, y)) {
                addedEdges++;
                cost += edge[2];
                if (addedEdges == n) {
                    return cost;
                }
            }

        }
        return cost;


    }

    private class DSU {
        int[] parent;
        int[] rank;

        DSU(int n) {
            parent = new int[n + 1];
            rank = new int[n + 1];
            for (int i = 1; i <= n; i++) {
                parent[i] = i;
                rank[i] = 1;
            }

        }

        int find(int element) {
            if (element == parent[element]) {
                return element;
            }
            return parent[element] = find(parent[element]);
        }

        boolean union(int x, int y) {
            int xP = find(x);
            int yP = find(y);
            if (xP != yP) {
                if (rank[xP] > rank[yP]) {
                    parent[yP] = xP;
                } else if (rank[yP] > rank[xP]) {
                    parent[xP] = yP;
                } else {
                    parent[xP] = yP;
                    rank[yP]++;
                }
                return true;
            }
            return false;

        }
    }

}
