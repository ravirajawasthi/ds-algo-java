package byteb.dsu;

import java.util.HashSet;

public class EfficientDsu {
    public int findCircleNum(int[][] isConnected) {
        int n = isConnected.length;
        var dsu = new DSU(n);
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (isConnected[i][j] == 1) {
                    dsu.union(i, j);
                }
            }
        }
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < n; i++) {
            set.add(dsu.find(i));
        }

        return set.size();
    }

    private static class DSU {

        final private int[] parent;
        final private int[] rank;

        DSU(int n) {
            parent = new int[n];
            rank = new int[n];
            for (int i = 0; i < n; i++) {
                this.parent[i] = i;
                this.rank[i] = 1;
            }
        }

        public int find(int x) {
            if (this.parent[x] == x) {
                return x;
            }
            this.parent[x] = find(this.parent[x]);
            return this.parent[x];
        }

        public void union(int x, int y) {
            int xP = find(x);
            int yP = find(y);
            if (xP != yP) {
                if (rank[xP] > rank[yP]) {
                    parent[yP] = xP;
                } else if (rank[xP] < rank[yP]) {
                    parent[xP] = yP;
                } else {
                    parent[xP] = yP;
                    rank[yP]++;
                }
            }
        }

    }

}
