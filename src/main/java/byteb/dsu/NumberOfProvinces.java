package byteb.dsu;

import java.util.HashSet;

public class NumberOfProvinces {
    public int findCircleNum(int[][] isConnected) {
        int n = isConnected.length;
        var dsu = new QuickUnionPathCompressionDSU(n);
//        var dsu = new DSUQuickUnionFind(n);
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (isConnected[i][j] == 1) {
                    dsu.union(i, j);
                }
            }
        }
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < n; i++) {
            set.add(dsu.find(i));
        }
        //[9, 8, 2, 8, 12, 5, 6, 8, 8, 9, 10, 8, 12, 8, 14] correct
        //[9, 13, 2, 11, 12, 5, 6, 8, 8, 9, 10, 11, 12, 13, 14] incorrect
        return set.size();
    }

    private static class DSUQuickUnionFind {
        private final int[] parent;

        DSUQuickUnionFind(int n) {
            this.parent = new int[n];
            for (int i = 0; i < n; i++) {
                this.parent[i] = i;
            }
        }

        int find(int x) {
            return this.parent[x];
        }

        void union(int x, int y) {
            int xP = this.find(x);
            int yP = this.find(y);
            if (xP != yP) {
                for (int i = 0; i < parent.length; i++) {
                    if (parent[i] == xP) {
                        parent[i] = yP;
                    }
                }
            }
        }

    }

    private static class QuickUnionPathCompressionDSU {
        private final int[] parent;

        QuickUnionPathCompressionDSU(int n) {
            parent = new int[n];
            for(int i = 0; i < n; i++){
                parent[i] = i;
            }
        }

        public int find(int x) {
            if (this.parent[x] == x) {
                return x;
            }
            this.parent[x] = find(parent[x]);
            return this.parent[x];
        }

        public void union(int x, int y) {
            int xP = find(x);
            int yP = find(y);

            if (xP != yP) {
                this.parent[xP] = yP;
            }
        }

    }


}
