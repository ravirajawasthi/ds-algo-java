package byteb.dsu;

import java.util.Arrays;
import java.util.Comparator;

public class EarliestFriends {

    public int earliestAcq(int[][] logs, int n) {
        Arrays.sort(logs, Comparator.comparingInt(l -> l[0]));
        var dsu = new DSU(n);
        for (var log : logs) {
            dsu.union(log[1], log[2]);
            if (dsu.groups == 1) {
                return log[0];
            }
        }
        return -1;
    }

    private static class DSU {

        final private int[] parent;
        public int groups;

        DSU(int n) {
            groups = n;
            parent = new int[n];
            for (int i = 0; i < n; i++) {
                this.parent[i] = i;
            }
        }

        public int find(int x) {
            if (this.parent[x] == x) {
                return x;
            }
            this.parent[x] = find(this.parent[x]);
            return this.parent[x];
        }

        public void union(int x, int y) {
            int xP = find(x);
            int yP = find(y);
            if (xP != yP) {
                groups--;
                parent[yP] = xP;
            }
        }

    }


}
