package byteb.dsu;

public class ValidTree {

    public boolean validTree(int n, int[][] edges) {
        if (edges.length != n-1) return false;
        var dsu = new DSU(n);
        for (int[] edge : edges) {
            if (dsu.union(edge[0], edge[1])) {
                return false;
            }
        }
        return true;

    }
    private static class DSU {

        final private int[] parent;
        final private int[] rank;

        DSU(int n) {
            parent = new int[n];
            rank = new int[n];
            for (int i = 0; i < n; i++) {
                this.parent[i] = i;
                this.rank[i] = 1;
            }
        }

        public int find(int x) {
            if (this.parent[x] == x) {
                return x;
            }
            this.parent[x] = find(this.parent[x]);
            return this.parent[x];
        }

        public boolean union(int x, int y) {
            int xP = find(x);
            int yP = find(y);
            if (xP != yP) {
                if (rank[xP] > rank[yP]) {
                    parent[yP] = xP;
                } else if (rank[xP] < rank[yP]) {
                    parent[xP] = yP;
                } else {
                    parent[xP] = yP;
                    rank[yP]++;
                }
                return false;
            }
            return true;
        }

    }

}
