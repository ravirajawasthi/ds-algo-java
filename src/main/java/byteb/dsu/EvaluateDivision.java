package byteb.dsu;


import byteb.utils.Pair;

import java.util.HashMap;
import java.util.List;

class EvaluateDivision {

    public double[] calcEquation(List<List<String>> equations, double[] values, List<List<String>> queries) {
        HashMap<String, Pair<String, Double>> group = new HashMap<>();
        double[] ans = new double[queries.size()];
        var dsu = new DSU(group);
        for (int i = 0; i < equations.size(); i++) {
            var eq = equations.get(i);
            String up = eq.get(0);
            String down = eq.get(1);
            double q = values[i];
            dsu.union(up, down, q);
        }
        for (int i = 0; i < queries.size(); i++) {
            var query = queries.get(i);
            String x = query.get(0);
            String y = query.get(1);
            if (!group.containsKey(x) || !group.containsKey(y)) {
                ans[i] = -1;
                continue;
            }
            String xP = dsu.find(x);
            String yP = dsu.find(y);
            if (!xP.equals(yP)) {
                ans[i] = -1;
            } else {
                ans[i] = group.get(x).getValue() * 1 / group.get(y).getValue();
            }
        }
        return ans;
    }

    private static class DSU {
        private final HashMap<String, Pair<String, Double>> group;

        DSU(HashMap<String, Pair<String, Double>> group) {
            this.group = group;
        }

        String find(String element) {
            group.putIfAbsent(element, new Pair<>(element, 1d));
            if (group.get(element).getKey().equals(element)) {
                return group.get(element).getKey();
            }
            String parent = find(group.get(element).getKey());
            String immParent = group.get(element).getKey();
            double immVal = group.get(immParent).getValue();
            group.get(element).setValue(immVal * group.get(element).getValue());
            group.get(element).setKey(parent);
            return parent;
        }

        void union(String x, String y, Double quotient) {
            String xP = find(x);
            String yP = find(y);
            if (!xP.equals(yP)) {
                group.get(xP).setKey(yP);
                group.get(xP).setValue(group.get(y).getValue() * quotient / group.get(x).getValue());
            }
        }
    }
}