package byteb.arrays;

//https://leetcode.com/problems/remove-colored-pieces-if-both-neighbors-are-the-same-color/?envType=daily-question&envId=2023-10-02
public class RemovePieceIfBothNeighbourSame {
    public static void main(String[] args) {
        System.out.println(winnerOfGame("AAAAABBBBBBAAAAA"));
    }

    public static boolean winnerOfGame(String colors) {

        int currLongA = 0;
        int currLongB = 0;
        int maxA = 0;
        int maxB = 0;

        boolean currSequence = false; //False means A, True means B

        int p = 0;
        while (p < colors.length()) {
            char c = colors.charAt(p++);
            if (c == 'A') {
                if (currSequence) {
                    currSequence = false;
                    currLongA = 1;
                    currLongB = 0;
                } else {
                    currLongA++;
                    if (currLongA >= 3) {
                        maxA++;
                    }
                }
            } else {
                if (!currSequence) {
                    currSequence = true;
                    currLongB = 1;
                    currLongA = 0;
                } else {
                    currLongB++;
                    if (currLongB >= 3) {
                        maxB++;
                    }
                }

            }
        }

        if (maxA == maxB) {
            return false;
        } else {
            return maxA > maxB;
        }

    }


}
