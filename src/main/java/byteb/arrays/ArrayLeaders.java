package byteb.arrays;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayLeaders {
    public static void main(String[] args) {
        System.out.println("leaders(new int[]{ 16, 17, 4, 3, 5, 2 }, 6) = " + leaders(new int[]{ 16, 17, 4, 3, 5, 2 }, 6))
        ;
    }

    static ArrayList<Integer> leaders(int[] arr, int n) {
        ArrayList<Integer> res = new ArrayList<>();
        int currMax = arr[ n - 1 ];
        for (int i = n - 1 ; i > -1 ; i--) {
            if (arr[ i ] >= currMax) {
                res.add(arr[ i ]);
            }

            if (currMax < arr[ i ]) {
                currMax = arr[ i ];
            }
        }
        Collections.reverse(res);
        return res;

    }

}
