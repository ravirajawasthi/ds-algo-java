package byteb.arrays;

import java.util.ArrayList;

public class CaseSpecificStringSorting {
    public static void main(String[] args) {
        System.out.println("caseSort(\"defRTSersUXI\") = " + caseSort("defRTSersUXI"));
    }

    public static String caseSort(String str) {
        ArrayList<Character> lowerCase = new ArrayList<>();
        ArrayList<Character> upperCase = new ArrayList<>();

        for (char c : str.toCharArray()) {
            if (Character.isLowerCase(c)) {
                lowerCase.add(c);
            } else {
                upperCase.add(c);
            }
        }

        lowerCase.sort(Character::compare);
        upperCase.sort(Character::compare);

        StringBuilder res = new StringBuilder();
        int lcIndex = 0;
        int ucIndex = 0;
        for (int i = 0 ; i < str.length() ; i++) {
            if (Character.isLowerCase(str.charAt(i))) {
                res.append(lowerCase.get(lcIndex));
                lcIndex++;
            } else {
                res.append(upperCase.get(ucIndex));
                ucIndex++;
            }
        }

        return res.toString();
    }
}

