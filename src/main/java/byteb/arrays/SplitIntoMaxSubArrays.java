package byteb.arrays;

public class SplitIntoMaxSubArrays {
    public static void main(String[] args) {
        System.out.println(maxSubarrays(new int[]{5,7,1,3}));
    }

    public static int maxSubarrays(int[] nums) {

        if (nums.length == 1) {
            return 1;
        }
        int allAndAns = nums[0];
        for (int i = 1; i < nums.length; i++) {
            allAndAns = allAndAns & nums[i];
        }

        if (allAndAns == 0) {
            int subarrays = 0;
            int i = 0;
            int currAnd = nums[0];
            while (i < nums.length) {
                currAnd &= nums[i++];
                if (currAnd == 0) {
                    subarrays++;
                    if (i < nums.length)
                        currAnd = nums[i];
                }
            }
            return subarrays;

        } else {
            return 1;
        }

    }

}
