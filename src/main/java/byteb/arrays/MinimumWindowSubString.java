package byteb.arrays;

public class MinimumWindowSubString {


    public String minWindow(String s, String t) {
        int having = 0;
        int required = 0;

        int ansStart = 0;
        int VERYLARGEVAL = 100000007;
        int ansEnd = VERYLARGEVAL;

        int startWindow = 0;
        int endWindow = 0;

        int[] sMap = new int[52];
        updateInMap(sMap, s.charAt(0), 1);
        int[] tMap = getCharMap(t);
        for (int i : tMap) {
            if (i > 0) required++;
        }

        int charIndex = getCharIndex(s.charAt(0));
        if (tMap[charIndex] == sMap[charIndex]) having++;

        while (startWindow < s.length() && endWindow < s.length()) {
            if (having == required) {
                int currAns = ansEnd - ansStart + 1;
                int newAns = endWindow - startWindow + 1;
                if (newAns < currAns) {
                    ansEnd = endWindow;
                    ansStart = startWindow;
                }
                //Gonna remove a char from left now
                int index = getCharIndex(s.charAt(startWindow));
                sMap[index]--;

                if (tMap[index] != 0 && tMap[index] > sMap[index]) {
                    having--;
                }

                startWindow++;
            } else {
                endWindow++;
                if (endWindow >= s.length()) continue;
                int index = getCharIndex(s.charAt(endWindow));
                if (tMap[index] != 0) {
                    if (sMap[index] >= tMap[index]) {
                        sMap[index]++;
                    } else {
                        sMap[index]++;
                        if (sMap[index] >= tMap[index]) having++;
                    }
                }else{
                    sMap[index]++;
                }
            }
        }
        if (having == required) {
            int currAns = ansEnd - ansStart + 1;
            int newAns = endWindow - startWindow + 1;
            if (newAns < currAns) {
                ansEnd = endWindow;
                ansStart = startWindow;
            }
        }

        return ansEnd == VERYLARGEVAL ? "" : s.substring(ansStart, ansEnd + 1);

    }


    private int[] getCharMap(String s) {
        int[] map = new int[52];

        for (char c : s.toCharArray()) {
            map[getCharIndex(c)]++;
        }

        return map;
    }

    private void updateInMap(int[] map, char c, int n) {
        map[getCharIndex(c)] += n;
    }

    private int getCharIndex(char c) {
        if (c >= 97) return c - 71;
        else return c - 65;
    }
}

