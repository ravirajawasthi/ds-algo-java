package byteb.arrays;

public class GoodPair {
    public int numIdenticalPairs(int[] nums) {
        int[] arr = new int[101];

        int counter = 0;

        for (int num : nums) {
            if (arr[num] != 0) {
                counter += arr[num];
            }
            arr[num]++;
        }

        return counter;

    }
}
