package byteb.arrays;

import java.util.ArrayList;
import java.util.HashMap;

public class MakeArrayEmpty {

    public int minOperations(int[] nums) {
        HashMap<Integer, Integer> frequency = new HashMap<>();
        for (int num : nums) {
            frequency.put(num, frequency.getOrDefault(num, 0) + 1);
        }

        HashMap<Integer, ArrayList<Integer>> reverseHashMap = new HashMap<>();
        for (int key : frequency.keySet()) {
            int val = frequency.get(key);
            reverseHashMap.putIfAbsent(val, new ArrayList<>());
            reverseHashMap.get(val).add(key);
        }

        frequency = null;

        ArrayList<Integer> keyList = new ArrayList<>(reverseHashMap.keySet());
        keyList.sort(Integer::compareTo);

        int ans = 0;

        for (int key : keyList) {
            int keyAns = 0;
            if (key < 2) {
                return -1;
            }
            switch (key % 3) {
                case (1): {
                    keyAns = 1 + key / 3;
                    break;
                }
                case (0): {
                    keyAns = key / 3;
                    break;
                }
                case (2): {
                    keyAns = 2 + (key / 3) - 1;
                }
            }

            ans += keyAns * reverseHashMap.get(key).size();

        }

        return ans;

    }


}
