package byteb.arrays;

import java.util.HashMap;

//https://leetcode.com/problems/subarray-sum-equals-k/
public class SubarrayWithGivenSum {
    public static void main(String[] args) {
        System.out.println("subarraySum() = " + subarraySum(new int[]{ 1, 4, 20, 3, 5 }, 33));
    }

    public static int subarraySum(int[] nums, int k) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        hm.put(0, 1); //At the start we zero sum, 1 time.
        int ans = 0;
        int currSum = 0;
        for (int num : nums) {
            currSum += num;
            if (hm.containsKey(currSum - k)) {
                ans += hm.get(currSum - k);
            }
            hm.put(currSum, hm.getOrDefault(currSum, 0) + 1);
        }
        return ans;
    }
}

