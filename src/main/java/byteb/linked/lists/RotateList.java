package byteb.linked.lists;

//LC 61
public class RotateList {
    public static void main(String[] args) {
        var head = new ListNode(1);
        var llPointer = head;
        for (int i = 2 ; i < 6 ; i++) {
            llPointer.next = new ListNode(i);
            llPointer = llPointer.next;
        }
        head = rotateRight(head, 6);
        while (head != null) {
            System.out.print(head.val + " -> ");
            head = head.next;
        }
        System.out.println("(x)");

    }

    public static ListNode rotateRight(ListNode head, int k) {
        int size = 1;
        var llPointer = head;
        ListNode end;

        while (llPointer.next != null) {
            size++;
            llPointer = llPointer.next;
        }
        k = k % size;

        end = llPointer;

        var offsetFromBeginning = size - k;
        int count = 1;

        llPointer = head;

        while (count < offsetFromBeginning) {
            count++;
            llPointer = llPointer.next;
        }
        end.next = head;
        head = llPointer.next;
        llPointer.next = null;
        return head;
    }


}
