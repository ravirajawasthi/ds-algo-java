package byteb.linked.lists;

public class ReverseLinkedListInGroupOfKGFGEdition {
    private static class ReverseLinkedListReturn {
        public ListNode start;
        public ListNode end;
    }

    public static void main(String[] args) {

        ListNode head = new ListNode(1);
        var llPointer = head;

        for (int i = 2 ; i < 6 ; i++) {
            llPointer.next = new ListNode(i);
            llPointer = llPointer.next;
        }

        head = reverseKGroup(head, 3);

        while (head != null) {
            System.out.print(head.val + " -> ");
            head = head.next;
        }
        System.out.println("(x)");

    }

    public static ListNode reverseKGroup(ListNode head, int k) {

        if (head == null || k == 1) {
            return head;
        }
        int length = 0;
        var llPointer = head;
        while (llPointer != null) {
            length++;
            llPointer = llPointer.next;
        }

        int groupCount = length / k;

        llPointer = head;

        ListNode prevGroupStarting = null;
        ListNode nextGroupEnd = null;
        ListNode groupStart = head;
        ListNode groupEnd = null;
        for (int i = 0 ; i < groupCount ; i++) {

            for (int j = 1 ; j < k ; j++) {
                if (j == 1) {
                    groupStart = llPointer;
                }
                llPointer = llPointer.next;
            }
            groupEnd = llPointer;
            nextGroupEnd = llPointer.next;
            llPointer = llPointer.next;
            var resp = reverseLinkedListGroup(nextGroupEnd, groupStart, groupEnd);
            if (prevGroupStarting == null) {
                head = resp.start;
            } else {
                prevGroupStarting.next = resp.start;
            }
            prevGroupStarting = resp.end;
        }
        return head;

    }

    private static ReverseLinkedListReturn reverseLinkedListGroup(ListNode nextGroupEnd, ListNode groupStart,
                                                                  ListNode groupEnd) {
        var llPointer = groupStart;
        ListNode bucket;
        ListNode prev = nextGroupEnd;
        while (llPointer != groupEnd) {
            bucket = llPointer.next;
            llPointer.next = prev;
            prev = llPointer;
            llPointer = bucket;
        }
        llPointer.next = prev;

        var response = new ReverseLinkedListReturn();
        response.end = groupStart;
        response.start = llPointer;

        return response;
    }
}
