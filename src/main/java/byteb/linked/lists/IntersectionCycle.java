package byteb.linked.lists;

//https://leetcode.com/problems/linked-list-cycle-ii/
public class IntersectionCycle {
    public static void main(String[] args) {

    }

    public static ListNode detectCycle(ListNode head) {
        var slowPointer = head;
        var fastPointer = head;

        if (head == null) {
            return null;
        }

        while (true) {
            slowPointer = slowPointer.next;
            fastPointer = fastPointer.next;
            if (fastPointer != null) {
                fastPointer = fastPointer.next;
            }

            if (slowPointer == null || fastPointer == null) {
                return null;
            }

            else if (slowPointer == fastPointer) {
                System.out.println("breaking point");
                System.out.println(fastPointer.val);
                System.out.println(slowPointer.val);
                break;
            }

        }

        fastPointer = head;

        while (true) {
            System.out.print(slowPointer.val + ", " + fastPointer.val);
            if (fastPointer == slowPointer) {
                return fastPointer;
            }
            fastPointer = fastPointer.next;
            slowPointer = slowPointer.next;
        }

    }
}
