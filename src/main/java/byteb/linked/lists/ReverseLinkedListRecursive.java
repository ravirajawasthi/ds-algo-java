package byteb.linked.lists;

public class ReverseLinkedListRecursive {
    public static void main(String[] args) {
        var llPointer = new ListNode(1);
        var head = llPointer;
        for (int i = 2 ; i < 10000 ; i++) {
            llPointer.next = new ListNode(i);
            llPointer = llPointer.next;
        }
        llPointer = reverseList(head);
        while (llPointer != null) {
            System.out.print(llPointer.val + " -> ");
            llPointer = llPointer.next;
        }
        System.out.println("null");
    }


    public static ListNode reverseList(ListNode head) {
        return reverseListRecursive(head, null);
    }

    public static ListNode reverseListRecursive(ListNode curr, ListNode previous) {
        if (curr.next==null){
            curr.next = previous;
            return curr;
        }
        var next = curr.next.next;
        curr.next.next = curr;

        var bucket = curr.next;

        curr.next = previous;

        return reverseListRecursive(next, bucket);
    }

    public static ListNode reverseListIterative(ListNode head) {
        ListNode prev = null;
        ListNode curr = head;
        ListNode next;
        ListNode bucket;

        while (curr != null) {
            if (curr.next == null) {
                curr.next = prev;
                return curr;
            }

            //We store the next to next because we are going to lose reference to it
            next = curr.next.next;
            //Make the next element of current to point to current
            curr.next.next = curr;


            //Immediate next whose reference we need to change
            bucket = curr.next;

            //Make current to point to previous element
            curr.next = prev;

            //moving to next iteration
            curr = next;
            prev = bucket;
        }

        return prev;
    }


}
