package byteb.linked.lists;

import java.util.Scanner;

public class RemoveFromListEnd {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int headToRemoveFromEnd = sc.nextInt();
        int size = sc.nextInt();
        ListNode head = new ListNode(sc.nextInt());
        ListNode currNode = head;

        for (int i = 1 ; i < size ; i++) {
            currNode.next = new ListNode(sc.nextInt());
            currNode = currNode.next;
        }

        removeNthFromEnd(head, headToRemoveFromEnd);
        var llPointer = head;
        while (llPointer != null) {
            System.out.println("llPointer.val = " + llPointer.val);
            llPointer = llPointer.next;
        }

    }


    public static ListNode removeNthFromEnd(ListNode head, int n) {
        n--;//because array start from 1; :)
        ListNode llPointer = head;

        int fastPointerPos = 0;


        while (llPointer.next != null) {
            llPointer = llPointer.next;
            fastPointerPos++;
        }

        if (fastPointerPos == 0) {
            return null;
        }

        int toDeleteFromFront = fastPointerPos - n;

        llPointer = head.next;
        ListNode prevLLPointer = head;
        int currPointerPos = 1;
        if (toDeleteFromFront == 0) {
            return head.next;
        }
        while (currPointerPos != toDeleteFromFront) {
            prevLLPointer = llPointer;
            llPointer = llPointer.next;
            currPointerPos++;
        }
        prevLLPointer.next = llPointer.next;

        return head;


    }

}


