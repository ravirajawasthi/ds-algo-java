package byteb.linked.lists;

import java.util.Scanner;

//https://leetcode.com/problems/palindrome-linked-list
public class PalindromeLinkedList {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String num = sc.nextLine();

        ListNode head = null;
        ListNode llPointer = null;

        for (char digit : num.toCharArray()) {

            if (llPointer == null) {
                llPointer = new ListNode();
                head = llPointer;
            } else {
                llPointer.next = new ListNode();
                llPointer = llPointer.next;
            }
            llPointer.val = digit - '0';

        }

        isPalindrome(head);


    }

    public static boolean isPalindrome(ListNode head) {
        ListNode slowPointer = head;
        ListNode fastPointer = head;


        while (fastPointer.next != null) {
            slowPointer = slowPointer.next;
            fastPointer = fastPointer.next;
            if (fastPointer.next != null) {
                fastPointer = fastPointer.next;
            }
        }

        var llPointer = head;

        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();

        while (slowPointer != null) {
            sb1.append(llPointer.val);
            sb2.append(slowPointer.val);
            llPointer = llPointer.next;
            slowPointer = slowPointer.next;

        }

        return ( sb1.reverse().toString() ).contentEquals(sb2);

    }

}
