package byteb.linked.lists;

import java.util.Scanner;

//https://leetcode.com/problems/partition-list/
public class PartitionList {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int toRemove = sc.nextInt();
        int[] arr = new int[ sc.nextInt() ];
        ListNode head = new ListNode(sc.nextInt());
        ListNode currNode = head;
        for (int i = 0 ; i < arr.length - 1 ; i++) {
            currNode.next = new ListNode(sc.nextInt());
            currNode = currNode.next;
        }


        System.out.println("partition(head, toRemove) = " + partition(head, toRemove));

        while (head != null) {
            System.out.println(head.val + " -> ");
            head = head.next;
        }

    }

    public static ListNode partition(ListNode head, int x) {
        if (head==null){
            return null;
        }
        ListNode ans = null;
        ListNode largerHead = null;
        ListNode smallerHead = null;
        ListNode largerPointer = null;

        var llPointer = head;

        while (llPointer!=null){

            if (llPointer.val < x){
                if (smallerHead == null){
                    smallerHead = new ListNode();
                    smallerHead.val = llPointer.val;
                    ans = smallerHead;
                }else{
                    smallerHead.next = new ListNode();
                    smallerHead.next.val = llPointer.val;
                    smallerHead = smallerHead.next;
                }


            }else{
                if (largerPointer == null){
                    largerPointer = new ListNode();
                    largerPointer.val = llPointer.val;
                    largerHead = largerPointer;
                }else{
                    largerPointer.next = new ListNode();
                    largerPointer.next.val = llPointer.val;
                    largerPointer = largerPointer.next;
                }



            }

            llPointer = llPointer.next;
        }

        if (smallerHead!=null) {
            smallerHead.next = largerHead;
        }else{
            ans = largerHead;
        }

        return ans;

    }
}
