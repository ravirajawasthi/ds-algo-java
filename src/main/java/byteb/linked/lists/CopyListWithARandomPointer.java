package byteb.linked.lists;

import java.util.HashMap;

//https://leetcode.com/problems/copy-list-with-random-pointer/

public class CopyListWithARandomPointer {
    public static void main(String[] args) {
        Integer[][] aa = new Integer[][]
                { { 7, null }, { 13, 0 }, { 11, 4 }, { 10, 2 }, { 1, 0 } };

        HashMap<Integer, Node> hm = new HashMap<>();

        Node head = null;
        Node llPointer = null;

        for (Integer[] a : aa) {
            int n = a[ 0 ];
            var node = new Node(n);
            hm.put(n, node);

            if (head == null) {
                head = node;
                llPointer = node;
            } else {
                llPointer.next = node;
                llPointer = llPointer.next;

            }
        }

        llPointer = head;
        int index = 0;
        while (llPointer != null) {
            llPointer.random = hm.get(aa[ index ][ 1 ]);
            llPointer = llPointer.next;
            index++;
        }

        head = copyRandomList(head);

        while (head != null) {
            System.out.print(head.val + " -> ");
            head = head.next;
        }

        System.out.println("(null)");


    }

    public static class Node {
        int val;
        Node next;
        Node random;

        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }

    public static Node copyRandomList(Node head) {
        /*
         * Cannot change the original linked list

         * Approaches
         * 1) HashMap
         * - Create a duplicate ll and store the new and old node in a hashmap
         * - In second iteration for random pointer take use hashmap
         *
         * 2) Without extra space (New nodes in between the existing nodes)
         * - Insert the new nodes in between the existing nodes
         * - Add the random pointer.
         * - Revert the changes in original list
         */

        if (head == null) {
            return null;
        }

        var llPointer = head;
        Node bucket;
        Node next;
        Node copied;


        // - Insert the new nodes in between the existing nodes
        while (llPointer != null) {
            next = llPointer.next;

            //            bucket = new Node(llPointer.val); //New Node
            bucket = new Node(llPointer.val); //New Node

            //Inserting bucket in between of two nodes
            bucket.next = next;
            llPointer.next = bucket;

            llPointer = next;
        }

        copied = head.next;


        // - Get the random pointers in order
        llPointer = head;
        Node random;
        while (llPointer != null) {
            bucket = llPointer.next;
            random = llPointer.random;
            if (random == null) {
                bucket.random = null;
            } else {
                bucket.random = random.next; //Next because it's the real duplicated node
            }
            llPointer = llPointer.next.next; //Skipping 2 because, next immediate node is duplicate
        }

        //Restore the original list
        llPointer = head;
        while (llPointer != null) {
            bucket = llPointer.next;
            next = llPointer.next.next;
            if (bucket.next != null) {
                bucket.next = bucket.next.next;
            }
            llPointer.next = next;
            llPointer = llPointer.next;
        }


        return copied;

    }
}
