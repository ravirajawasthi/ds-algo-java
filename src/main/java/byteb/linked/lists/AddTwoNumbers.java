package byteb.linked.lists;

//https://leetcode.com/problems/add-two-numbers/description/

import java.util.Scanner;

public class AddTwoNumbers {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String n1 = sc.nextLine();

        String n2 = sc.nextLine();

        ListNode l1Head = null;
        ListNode l1 = null;

        ListNode l2Head = null;
        ListNode l2 = null;

        for (char digit : n1.toCharArray()) {
            if (l1 == null) {
                l1 = new ListNode();
                l1Head = l1;
            } else {
                l1.next = new ListNode();
                l1 = l1.next;
            }
            l1.val = digit - '0';

        }


        for (char digit : n2.toCharArray()) {
            if (l2 == null) {
                l2 = new ListNode();
                l2Head = l2;
            } else {
                l2.next = new ListNode();
                l2 = l2.next;
            }
            l2.val = digit - '0';
        }


        var llPointer = addTwoNumbers(l1Head, l2Head);


    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //Solving it the complicated way
        //Doing actual addition


        ListNode ans = null;
        ListNode head = null;
        int carry = 0;
        int n1;
        int n2;
        int bucket;
        while (l1 != null || l2 != null) {

            if (l1 == null) {
                n1 = 0;
            } else {
                n1 = l1.val;
                l1 = l1.next;
            }


            if (l2 == null) {
                n2 = 0;
            } else {
                n2 = l2.val;
                l2 = l2.next;
            }


            bucket = n1 + n2 + carry;

            if (bucket > 9) {
                carry = bucket / 10;
                bucket = bucket % 10;
            } else {
                carry = 0;
            }

            if (ans == null){
                ans = new ListNode();
                head = ans;
            }else{
                ans.next = new ListNode();
                ans = ans.next;
            }
            ans.val = bucket;
        }
        if (carry > 0){
            ans.next = new ListNode();
            ans = ans.next;
            ans.val = carry;
        }

        return head;
    }

}
