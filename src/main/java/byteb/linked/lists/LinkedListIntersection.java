package byteb.linked.lists;

//https://leetcode.com/problems/intersection-of-two-linked-lists/

public class LinkedListIntersection {
    public static void main(String[] args) {
    }

    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {


        var lengthA = 1;
        var lengthB = 1;

        var llPointerA = headA;
        var llPointerB = headB;

        while (llPointerA.next != null || llPointerB.next != null) {

            if (llPointerA.next != null) {
                llPointerA = llPointerA.next;
                lengthA++;
            }

            if (llPointerB.next != null) {
                llPointerB = llPointerB.next;
                lengthB++;
            }

        }

        if (llPointerB.val != llPointerA.val) {
            return null;
        }

        ListNode llLargePointer;
        ListNode llSmallPointer;

        if (Math.max(lengthA, lengthB) == lengthA) {
            llLargePointer = headA;
            llSmallPointer = headB;
        } else {
            llLargePointer = headB;
            llSmallPointer = headA;
        }

        var diff = Math.abs(lengthA - lengthB);

        while (diff > 0) {
            llLargePointer = llLargePointer.next;
            diff--;
        }
        System.out.print("largePointer stuck at : ");
        System.out.println(llLargePointer.val);

        System.out.print("SmallPointer stuck at : ");
        System.out.println(llSmallPointer.val);

        while (llSmallPointer != null && llLargePointer != null) {
            //Something should be return from here always
            if (llSmallPointer == llLargePointer) {
                return llSmallPointer;
            }
            llLargePointer = llLargePointer.next;
            llSmallPointer = llSmallPointer.next;
        }


        return null;

    }

}
