package byteb.linked.lists;

import java.util.Comparator;
import java.util.PriorityQueue;

//https://practice.geeksforgeeks.org/problems/flattening-a-linked-list/1?utm_source=youtube&utm_medium=collab_striver_ytdescription&utm_campaign=flattening-a-linked-list

public class FlatteningALinkedList {

    public static void main(String[] args) {
        var head = new Node(5);
        head.next = new Node(10);
        head.next.next = new Node(19);
        head.next.next.next = new Node(20);

        head.bottom = new Node(7);
        head.bottom.bottom = new Node(8);
        head.bottom.bottom.bottom = new Node(30);

        head.next.next.bottom = new Node(22);
        head.next.next.bottom.bottom = new Node(50);

        head = flatten(head);

        while (head != null) {
            System.out.print(head.data + " -> ");
            head = head.next;
        }
        System.out.println("(x)");

    }

    static class Node {
        int data;
        Node next;
        Node bottom;

        Node(int d) {
            data = d;
            next = null;
            bottom = null;
        }
    }

    static class CustomNodeComparator implements Comparator<Node> {

        @Override
        public int compare(Node o1, Node o2) {
            if (o1.data < o2.data) {
                return -1;
            } else if (o1.data > o2.data) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    static Node flatten(Node root) {
        Node head = null;
        Node llPointer = null;
        var pq = new PriorityQueue<>(new CustomNodeComparator());
        pq.add(root);

        while (!pq.isEmpty()) {
            var pop = pq.peek();
            pq.remove(pop);
            if (llPointer == null) {
                llPointer = new Node(pop.data);
                head = llPointer;
            } else {
                llPointer.next = new Node(pop.data);
                llPointer = llPointer.next;
            }
            if (pop.next != null) {
                pq.add(pop.next);
            }
            if (pop.bottom != null) {
                pq.add(pop.bottom);
            }


        }
        return head;

    }

}
