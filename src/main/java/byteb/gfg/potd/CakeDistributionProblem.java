package byteb.gfg.potd;

//https://practice.geeksforgeeks.org/problems/0a7c7f1089932257071f9fa076f25d353f91e0fd/1
public class CakeDistributionProblem {
    public static void main(String[] args) {
        System.out.println("maxSweetness(new int[]{6,3,2,8,7,5}, 6, 2) = " + maxSweetness(new int[]{ 6, 3, 2, 8, 7,
                5 }, 6, 2));
    }

    static int maxSweetness(int[] sweetness, int N, int K) {
        int totalSweetness = 0;
        int minVal = sweetness[ 0 ];
        for (int i : sweetness) {
            totalSweetness += i;
            minVal = Math.min(minVal, i);
        }

        int low = minVal;
        int high = totalSweetness;

        while (low <= high) {
            int midVal = ( low + high ) >>> 1;

            int cmp = checkIfSolExist(sweetness, totalSweetness, midVal, K + 1);

            if (cmp < 0)
                low = midVal + 1;
            else if (cmp > 0)
                high = midVal - 1;
            else
                return midVal; // key found
        }
        return -( low + 1 );  // key not found.

    }

    private static int checkIfSolExist(int[] sweetness, int totalSweetness, int midVal, int piecesRequired) {

        if (midVal * piecesRequired > totalSweetness) {
            return 1;
        }
        int piecesCreated = 0;
        int firstPieceCreatedSize = 0;
        int currSum = 0;
        for (int i : sweetness) {
            currSum += i;
            if (currSum >= midVal) {
                if (piecesCreated == 0){
                    firstPieceCreatedSize = currSum;
                }
                piecesCreated += 1;
                currSum = 0;
            }
        }
        if (piecesRequired < piecesCreated || firstPieceCreatedSize > midVal) {
            return -1;
        }
        else if (piecesCreated == piecesRequired) {
            return 0;
        }

        return 1;


    }


}
