package byteb.tle.level3.pointers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class IraAndFlamenco {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {

        public void solve(InputReader sc, PrintWriter out) {

            int t = sc.nextInt();
            while (t-- > 0) {
                long n = sc.nextLong();
                int m = sc.nextInt();

                TreeMap<Long, Long> map = new TreeMap<>();

                for (int i = 0; i < n; i++) {
                    long in = sc.nextLong();
                    map.put(in, map.getOrDefault(in, 0l) + 1);
                }

                var arr = map.keySet().toArray(new Long[map.keySet().size()]);

                n = arr.length;

                long ans = 0;
                long ncr = 1;

                int end = 0;

                while (end < n) {
                    ncr = (ncr * map.get(
                            arr[end])) % MODULO;

                    int start = end - m + 1;

                    if (start > 0) {
                        ncr = (ncr * modInverse(map.get(arr[start - 1]), MODULO)) % MODULO;                    }
                    if (start >= 0 && arr[end] - arr[start] + 1 == m) {
                        ans = (ans + ncr) % MODULO;
                    }
                    end++;
                }

                out.println(ans);

            }

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }
        public static long modPow(long x, long y, long MOD) {
            long result = 1;
            x = x % MOD;
            while (y > 0) {
                if ((y & 1) == 1) {
                    result = (result * x) % MOD;
                }
                y >>= 1;
                x = (x * x) % MOD;
            }
            return result;
        }

        // Function to compute modular inverse of x under modulo MOD
        public static long modInverse(long x, long MOD) {
            return modPow(x, MOD - 2, MOD);
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    static class MultiTreeSet {
        TreeMap<Integer, Integer> freqTreeMap = new TreeMap<>();
        int size;

        public MultiTreeSet() {
        }

        public MultiTreeSet(Collection<Integer> c) {
            for (Integer element : c)
                add(element);
        }

        public int size() {
            return size;
        }

        public void add(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                freqTreeMap.put(element, 1);
            else
                freqTreeMap.put(element, freq + 1);
            ++size;
        }

        public void remove(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq != null) {
                if (freq == 1)
                    freqTreeMap.remove(element);
                else
                    freqTreeMap.put(element, freq - 1);
                --size;
            }
        }

        public int get(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                return 0;
            return freq;
        }

        public boolean contains(Integer element) {
            return get(element) > 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public Integer first() {
            return freqTreeMap.firstKey();
        }

        public Integer last() {
            return freqTreeMap.lastKey();
        }

        public Integer pollLast() {
            var retVal = freqTreeMap.lastKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer pollFirst() {
            var retVal = freqTreeMap.firstKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer ceiling(Integer element) {
            return freqTreeMap.ceilingKey(element);
        }

        public Integer floor(Integer element) {
            return freqTreeMap.floorKey(element);
        }

        public Integer higher(Integer element) {
            return freqTreeMap.higherKey(element);
        }

        public Integer lower(Integer element) {
            return freqTreeMap.lowerKey(element);
        }

        @Override
        public String toString() {
            return "MultiTreeSet [freqTreeMap=" + freqTreeMap + ", size=" + size + "]";
        }
    }

}