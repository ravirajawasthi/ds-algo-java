package byteb.tle.level3.pointers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class SegmentOfSmallSet {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            int k = sc.nextInt();

            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
                arr[i] = sc.nextInt();

            TreeMap<Integer, Integer> map = new TreeMap<>();

            long segments = 0;

            int start = 0;
            int end = 0;
            while (end < n) {
                map.put(arr[end], map.getOrDefault(arr[end], 0) + 1);

                while (map.keySet().size() > k && start < end) {
                    int startEl = arr[start];
                    int val = map.get(startEl);
                    if (val - 1 == 0)
                        map.remove(startEl);
                    else
                        map.put(startEl, val - 1);
                    start++;
                }
                if (map.keySet().size() <= k) 
                    segments += (end - start) + 1;

                end++;

            }
            out.println(segments);
        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
