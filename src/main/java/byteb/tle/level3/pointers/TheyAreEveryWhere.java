package byteb.tle.level3.pointers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class TheyAreEveryWhere {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            int[] pokemons = new int[123];
            String line = sc.next();
            int unique = 0;

            for (char c : line.toCharArray()) {
                if (pokemons[c] == 0)
                    unique++;
                pokemons[c]++;
            }

            int start = 0;
            int end = 0;
            int ans = n;

            TreeMap<Character, Integer> map = new TreeMap<>();
            while (end < n) {
                char e = line.charAt(end);
                map.put(e, map.getOrDefault(e, 0) + 1);
                while (start < end && map.keySet().size() == unique) {
                    ans = Math.min(ans, end - start + 1);
                    char c = line.charAt(start);
                    int val = map.get(c);
                    if (val - 1 == 0) {
                        map.remove(c);
                    } else {
                        map.put(c, val - 1);
                    }
                    start++;
                }
                if (map.keySet().size() == unique)
                    ans = Math.min(ans, end - start + 1);

                end++;
            }
            out.println(ans);

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
