package byteb.tle.level3.pointers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class LoopedPlaylist {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {

        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            long p = sc.nextLong();

            int[] songs = new int[n];
            long sum = 0;

            for (int i = 0; i < n; i++) {
                int in = sc.nextInt();
                songs[i] = in;
                sum += in;
            }

            songs = Arrays.copyOf(songs, songs.length * 2);
            int pointer = 0;
            for (int i = songs.length / 2; i < songs.length; i++) {
                songs[i] = songs[pointer++];
            }
            int originalN = n;
            n = n * 2;

            int start = 0;
            int end = 0;

            long currpos = 0;
            long additionalAnswer;
            if (p % sum == 0) {
                out.printf("%d %d%n", 1, originalN * (p / sum));
                return;
            } else {
                additionalAnswer = p / sum;
                p = p % sum;
            }

            int startAns = 1;

            long ans = Long.MAX_VALUE;

            while (end < n) {
                currpos += songs[end];
                while (currpos > p && start < end && currpos - songs[start] >= p) {
                    currpos -= songs[start];
                    start++;
                }
                if (currpos >= p) {
                    long length = (long) end - start + 1;
                    if (length < ans && length <= n) {
                        ans = length;
                        startAns = (start) + 1;
                    }
                }
                end++;

            }
            out.printf("%d %d%n", startAns, (ans) + (additionalAnswer * originalN));

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    static class MultiTreeSet {
        TreeMap<Integer, Integer> freqTreeMap = new TreeMap<>();
        int size;

        public MultiTreeSet() {
        }

        public MultiTreeSet(Collection<Integer> c) {
            for (Integer element : c)
                add(element);
        }

        public int size() {
            return size;
        }

        public void add(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                freqTreeMap.put(element, 1);
            else
                freqTreeMap.put(element, freq + 1);
            ++size;
        }

        public void remove(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq != null) {
                if (freq == 1)
                    freqTreeMap.remove(element);
                else
                    freqTreeMap.put(element, freq - 1);
                --size;
            }
        }

        public int get(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                return 0;
            return freq;
        }

        public boolean contains(Integer element) {
            return get(element) > 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public Integer first() {
            return freqTreeMap.firstKey();
        }

        public Integer last() {
            return freqTreeMap.lastKey();
        }

        public Integer pollLast() {
            var retVal = freqTreeMap.lastKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer pollFirst() {
            var retVal = freqTreeMap.firstKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer ceiling(Integer element) {
            return freqTreeMap.ceilingKey(element);
        }

        public Integer floor(Integer element) {
            return freqTreeMap.floorKey(element);
        }

        public Integer higher(Integer element) {
            return freqTreeMap.higherKey(element);
        }

        public Integer lower(Integer element) {
            return freqTreeMap.lowerKey(element);
        }

        @Override
        public String toString() {
            return "MultiTreeSet [freqTreeMap=" + freqTreeMap + ", size=" + size + "]";
        }
    }

}
