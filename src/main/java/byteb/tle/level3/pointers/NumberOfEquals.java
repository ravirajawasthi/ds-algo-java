package byteb.tle.level3.pointers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class NumberOfEquals {
    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {

            int n, m;
            n = sc.nextInt();
            m = sc.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
            }
            int[] arr2 = new int[m];
            for (int j = 0; j < m; j++) {
                arr2[j] = sc.nextInt();
            }
            long[] tracker = new long[n];

            int equal = 0;
            int p1 = 0;
            int p2 = 0;

            while (p1 < n) {
                if (p1 > 0 && arr[p1] == arr[p1 - 1]) {
                    equal += tracker[p1 - 1];
                    tracker[p1] = tracker[p1 - 1];
                    p1++;
                    continue;
                }
                while (p2 < m && arr2[p2] < arr[p1])
                    p2++;

                if (p2 < m && arr2[p2] == arr[p1]) {
                    while (p2 < m && arr2[p2] == arr[p1]) {
                        tracker[p1]++;
                        equal++;
                        p2++;
                    }
                }
                p1++;
            }

            out.println(equal);

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}