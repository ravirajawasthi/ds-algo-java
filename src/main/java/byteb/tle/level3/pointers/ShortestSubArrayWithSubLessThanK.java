package byteb.tle.level3.pointers;

import java.util.ArrayDeque;

public class ShortestSubArrayWithSubLessThanK {
    public int shortestSubarray(int[] nums, long k) {
        long[] prefix = new long[nums.length];
        prefix[0] = nums[0];
        for (int i = 1; i < nums.length; i++)
            prefix[i] = prefix[i - 1] + nums[i];

        ArrayDeque<Integer> deq = new ArrayDeque<>();

        int end = 0;
        int ans = Integer.MAX_VALUE;
        while (end < nums.length) {
            long sum = prefix[end];
            if (sum >= k) // -> this condition is very important
                ans = Math.min(ans, end + 1);
            while (!deq.isEmpty() && sum - prefix[deq.peekLast()] >= k) {
                var removed = deq.removeLast();
                ans = Math.min(ans, end - removed);
            }
            while (!deq.isEmpty() && prefix[deq.peekFirst()] >= sum)
                deq.removeFirst();
            deq.addFirst(end);
            end++;
        }
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }
}
