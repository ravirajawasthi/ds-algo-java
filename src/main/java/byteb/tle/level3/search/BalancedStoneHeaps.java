package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class BalancedStoneHeaps {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {

            int t = sc.nextInt();
            while (t-- > 0) {

                int n = sc.nextInt();
                long[] heaps = new long[n];

                for (int i = 0; i < n; i++) {
                    heaps[i] = sc.nextLong();
                }

                long start = 1;
                long end = (long) 10e15;

                long ans = start;
                while (start <= end) {
                    long mid = start + (end - start) / 2;
                    if (possible(heaps, mid)) {
                        ans = mid;
                        start = mid + 1;
                    } else {
                        end = mid - 1;
                    }
                }
                out.println(ans);
            }

        }

        private boolean possible(long[] heapsOriginal, long mid) {
            var heaps = Arrays.copyOf(heapsOriginal, heapsOriginal.length);

            for (int i = heaps.length - 1; i > -1; i--) {
                if (heaps[i] < mid) {
                    return false;
                }

                if (i > 1) {
                    long d = Math.min(heapsOriginal[i] / 3, (heaps[i] - mid) / 3);
                    heaps[i - 1] += d;
                    heaps[i - 2] += 2 * d;
                }
            }

            return true;
        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
