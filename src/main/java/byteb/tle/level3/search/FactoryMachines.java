package byteb.tle.level3.search;

import java.io.*;
import java.util.StringTokenizer;

//https://cses.fi/problemset/task/1620
public class FactoryMachines {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int n = sc.nextInt();
            int products = sc.nextInt();
            int worstMachine = Integer.MIN_VALUE;
            int[] machines = new int[n];
            for (int i = 0; i < n; i++) {
                int num = sc.nextInt();
                worstMachine = Math.max(worstMachine, num);
                machines[i] = num;
            }
            long start = 1;
            long end = (long) worstMachine * products;
            long ans = -1;
            while (start <= end) {
                long mid = start + (end - start) / 2;
                if (possible(mid, machines, products)) {
                    ans = mid;
                    end = mid - 1;
                } else {
                    start = mid + 1;
                }
            }
            out.println(ans);
        }

        private boolean possible(long mid, int[] machines, int productsRequired) {
            long productsMade = 0;
            for (int i: machines){
                productsMade += mid / i;
                if (productsMade >= productsRequired) return true;
            }
            return false;
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
