package byteb.tle.level3.search;

import java.io.*;
import java.util.StringTokenizer;

public class Worm {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int n = sc.nextInt();
            int[] arr = new int[n];
            arr[0] = sc.nextInt();

            for (int i = 1; i < n; i++) {
                int input = sc.nextInt();
                arr[i] = arr[i - 1] + input;
            }

            int queries = sc.nextInt();

            for (int i = 0; i < queries; i++) {
                int query = sc.nextInt();
                int start = 0;
                int end = arr.length - 1;
                int ans = -1;
                while (start <= end) {
                    int mid = (start + end) / 2;
                    if (arr[mid] < query) {
                        start = mid + 1;
                    } else {
                        end = mid - 1;
                        ans = mid;

                    }
                }
                out.println(ans + 1);

            }

            // Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
