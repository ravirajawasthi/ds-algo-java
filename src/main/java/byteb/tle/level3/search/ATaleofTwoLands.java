package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class ATaleofTwoLands {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        // Solution Here
        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            int[] arr = new int[n];

            for (int i = 0; i < n; i++) {
                arr[i] = Math.abs(sc.nextInt());
            }

            Arrays.parallelSort(arr);

            int firstPosIndex = binarySearch(arr, 0);
            long count = 0;
            for (int i = firstPosIndex; i < arr.length; i++) {
                long limit = (long) arr[i] * 2;
                int limitIndex = binarySearch(arr, limit);
                if (limitIndex != -1 && limitIndex > i) {
                    count += limitIndex - i;
                }
            }
            out.println(count);

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    private static int binarySearch(int[] arr, long limit) {
        int start = 0;
        int end = arr.length - 1;
        while (start <= end) {
            int mid = (start) + (end - start) / 2;
            int val = arr[mid];
            if (val <= limit) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return end == -1 ? start : end;

    }

}
