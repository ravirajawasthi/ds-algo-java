package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class SumOfThreeValues {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {

        public void solve(InputReader sc, PrintWriter out) {
            int n = sc.nextInt();
            int target = sc.nextInt();
            ArrayList<int[]> nums = new ArrayList<>(n);

            for (int i = 0; i < n; i++) {
                nums.add(new int[] { sc.nextInt(), i });
            }

            nums.sort((e1, e2) -> e1[0] - e2[0]);

            for (int i = 0; i < nums.size() - 2; i++) {

                int start = i + 1;
                int end = nums.size() - 1;

                long leftOver = (long) target - nums.get(i)[0];

                while (start < end) {
                    long currSum = (long) nums.get(start)[0] + nums.get(end)[0];
                    if (currSum == leftOver) {
                        out.printf("%d %d %d%n", nums.get(i)[1] + 1, nums.get(start)[1] + 1,
                                nums.get(end)[1] + 1);
                        return;
                    } else if (currSum > leftOver)
                        end--;
                    else
                        start++;
                }

            }
            out.println("IMPOSSIBLE");

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
