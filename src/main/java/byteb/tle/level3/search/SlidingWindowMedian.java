package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class SlidingWindowMedian {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {

        private int[] arr;
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {
            int n = sc.nextInt();
            int k = sc.nextInt();
            arr = new int[n];
            for (int i = 0; i < n; i++)
                arr[i] = sc.nextInt();

            MultiTreeSet low = new MultiTreeSet();
            MultiTreeSet high = new MultiTreeSet();

            for (int i = 0; i < arr.length; i++) {

                low.add(i);
                high.add(low.pollLast());

                if ((!(low.size() == high.size()) && !(low.size() == high.size() + 1))) {
                    low.add(high.pollFirst());
                }

                if (i >= k) {
                    int toRemoveIndex = i - k;

                    if (low.contains(toRemoveIndex)) {
                        low.remove(toRemoveIndex);
                    } else {
                        high.remove(toRemoveIndex);
                    }
                    if (high.size() > low.size()) {
                        low.add(high.pollFirst());
                    } else if (low.size() - high.size() > 1) {
                        high.add(low.pollLast());
                    }

                }

                if (i >= k - 1) {

                    out.printf("%d ", arr[low.last()]);

                }

            }
            out.println();

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

        class MultiTreeSet {
            TreeMap<Integer, Integer> freqTreeMap = new TreeMap<>((i1, i2) -> arr[i1] - arr[i2]);
            int size;

            public MultiTreeSet() {
            }

            public MultiTreeSet(Collection<Integer> c) {
                for (Integer element : c)
                    add(element);
            }

            public int size() {
                return size;
            }

            public void add(Integer element) {
                Integer freq = freqTreeMap.get(element);
                if (freq == null)
                    freqTreeMap.put(element, 1);
                else
                    freqTreeMap.put(element, freq + 1);
                ++size;
            }

            public void remove(Integer element) {
                Integer freq = freqTreeMap.get(element);
                if (freq != null) {
                    if (freq == 1)
                        freqTreeMap.remove(element);
                    else
                        freqTreeMap.put(element, freq - 1);
                    --size;
                }
            }

            public int get(Integer element) {
                Integer freq = freqTreeMap.get(element);
                if (freq == null)
                    return 0;
                return freq;
            }

            public boolean contains(Integer element) {
                return get(element) > 0;
            }

            public boolean isEmpty() {
                return size == 0;
            }

            public Integer first() {
                return freqTreeMap.firstKey();
            }

            public Integer last() {
                return freqTreeMap.lastKey();
            }

            public Integer pollLast() {
                var retVal = freqTreeMap.lastKey();
                this.remove(retVal);
                return retVal;
            }

            public Integer pollFirst() {
                var retVal = freqTreeMap.firstKey();
                this.remove(retVal);
                return retVal;
            }

            public Integer ceiling(Integer element) {
                return freqTreeMap.ceilingKey(element);
            }

            public Integer floor(Integer element) {
                return freqTreeMap.floorKey(element);
            }

            public Integer higher(Integer element) {
                return freqTreeMap.higherKey(element);
            }

            public Integer lower(Integer element) {
                return freqTreeMap.lowerKey(element);
            }

            @Override
            public String toString() {
                return "MultiTreeSet [freqTreeMap=" + freqTreeMap + ", size=" + size + "]";
            }
        }

        static class Pair {
            public int x;
            public int y;

            public Pair(int x, int y) {
                this.x = x;
                this.y = y;
            }

            @Override
            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                Pair other = (Pair) obj;
                return x == other.x && y == other.y;
            }
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
