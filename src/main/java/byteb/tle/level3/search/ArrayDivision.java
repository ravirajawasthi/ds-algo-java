package byteb.tle.level3.search;

import java.io.*;
import java.util.StringTokenizer;

public class ArrayDivision {


    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            int k = sc.nextInt();

            int[] arr = new int[n];
            long start = Integer.MIN_VALUE;

            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
                start = Math.max(start, arr[i]);
            }

            long end = (long) 1e15;
            long ans = -1;

            while (start <= end) {
                long mid = start + (end - start) / 2;
                if (possible(mid, arr, k)) {
                    ans = mid;
                    end = mid - 1;
                } else {
                    start = mid + 1;
                }
            }
            out.println(ans);

        }

        private boolean possible(long mid, int[] arr, int k) {
            long currSum = 0;
            int divisions = 1;
            for (var n : arr) {
                if (currSum + n > mid){
                    divisions++;
                    currSum = 0;
                }
                currSum += n;
            }
            return divisions <= k;
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
