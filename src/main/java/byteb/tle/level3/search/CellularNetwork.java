package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class CellularNetwork {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {
            int n = sc.nextInt();
            int m = sc.nextInt();

            int[] cities = new int[n];
            for (int i = 0; i < n; i++)
                cities[i] = sc.nextInt();

            int[] towers = new int[m];
            for (int i = 0; i < m; i++)
                towers[i] = sc.nextInt();

            long start = 0;
            long end = (long) 1e17;

            long ans = -1;
            while (start <= end) {
                long mid = (start) + (end - start) / 2;
                if (possible(mid, towers, cities)) {
                    ans = mid;
                    end = mid - 1;
                } else {
                    start = mid + 1;
                }
            }
            out.println(ans);
        }

        private boolean possible(long mid, int[] towers, int[] cities) {
            int citiesIndex = 0;
            int towersIndex = 0;

            long coverUp = -1;
            long coverDown = -1;
            while (towersIndex < towers.length && citiesIndex < cities.length) {
                long tower = towers[towersIndex++];
                coverDown = tower - mid;
                coverUp = tower + mid;
                while (citiesIndex < cities.length && cities[citiesIndex] >= coverDown
                        && cities[citiesIndex] <= coverUp)
                    citiesIndex++;
            }

            return (citiesIndex >= cities.length);

        }

        private int lowerBound(int key, int[] arr) {
            int start = 0;
            int end = arr.length - 1;
            while (start <= end) {
                int mid = (start) + (end - start) / 2;
                if (arr[mid] > key) {
                    end = mid - 1;
                } else {
                    start = mid + 1;
                }
            }
            if (start == 0) {
                return -1;
            } else {
                return end;
            }
        }

        // Solution End

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
