package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class MultiplicationTable {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int n = sc.nextInt();
            long elements = (long) Math.pow(n, 2);
            long ans = -1;
            long start = 1;
            long end = elements;

            while (start <= end) {
                long mid = start + (end - start) / 2;
                var pos = possible(mid, n);
                if (pos >= elements/2 + 1){ //+1 is very important because in possible function it considers itself as greater than or equal to
                    ans = mid;
                    end = mid - 1;
                }else{
                    start = mid + 1;
                }
            }
            out.println(ans);

            // Solution End
        }

        private long possible(long mid, long n) {
            long bigger = 0;
            for (int i = 1; i <= Math.min(n, mid); i++){
                bigger += Math.min(n, mid / i);
            }    
            return bigger;
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
