package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class NastyaAndDoor {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                int k = sc.nextInt();

                int[] mountains = new int[n];
                for (int i = 0; i < n; i++) {
                    mountains[i] = sc.nextInt();
                }

                boolean[] peaks = new boolean[mountains.length];

                for (int i = 1; i < peaks.length - 1; i++) {
                    if (mountains[i] > mountains[i + 1] && mountains[i] > mountains[i - 1]) {
                        peaks[i] = true;
                    }
                }

                int best = 1;
                int l = 1;

                int pieces = 1;

                for (int i = 2; i < peaks.length; i++) {
                    if (peaks[i - 1]) {
                        pieces++;
                    }

                    int windowEnd = i - k + 1;
                    if (windowEnd > -1 && peaks[windowEnd]) {
                        pieces--;
                    }

                    if (windowEnd > -1 && pieces > best) {
                        best = pieces;
                        l = windowEnd + 1;
                    }
                }
                out.printf("%d %d%n", best, l);

            }

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
