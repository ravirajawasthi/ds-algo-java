package byteb.tle.level3.search;

class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {

        if (nums1.length > nums2.length){
            var temp = nums1;
            nums1 = nums2;
            nums2 = temp;
        }

        int elementsForMedian = (nums1.length + nums2.length + 1) / 2;

        int start = 0;
        int end = nums1.length - 1;
        while (true) {
            int mid = (start) + (end - start) / 2;

            int nums1LastElementLeft = mid - 1 < 0 ? Integer.MIN_VALUE : nums1[mid - 1];
            int nums1FirstElementRight = mid >= nums1.length ? Integer.MAX_VALUE : nums1[mid];

            // If i am taking mid elements mid elements nums1 then i need to take
            // (elementsForMedian - mid) from nums2

            int nums2FirstElementRight = elementsForMedian - mid >= nums2.length ? Integer.MAX_VALUE : nums2[elementsForMedian - mid ];
            int nums2LastElementLeft = elementsForMedian - mid - 1 < 0 ? Integer.MIN_VALUE: nums2[elementsForMedian - mid - 1];

            // nums1LastElementLeft <= nums2FirstElementRight else need to move left
            if (!(nums1LastElementLeft <= nums2FirstElementRight)) {
                end = mid - 1;
                continue;
            }

            // nums1FirstElementRight >= nums2LastElementLeft else we need to move right
            else if (!(nums1FirstElementRight >= nums2LastElementLeft)) {
                start = mid + 1;
            }

            // Median is of 2 elements
            else if ((nums2.length + nums1.length) % 2 == 0)
                return (Math.max(nums2LastElementLeft, nums1LastElementLeft) + Math.min(nums2FirstElementRight, nums1FirstElementRight)) / 2.0;

                // Median is of 1 element
            else
                return (double) Math.max(nums2LastElementLeft, nums1LastElementLeft);

        }

        // return -1;

    }
}
