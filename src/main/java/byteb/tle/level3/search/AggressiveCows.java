package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class AggressiveCows {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int s = sc.nextInt();
                int cows = sc.nextInt();
                int[] stalls = new int[s];
                for (int i = 0; i < s; i++) {
                    stalls[i] = sc.nextInt();
                }
                Arrays.sort(stalls);
                int start = 1;
                int end = (int) 1e9;
                int ans = -1;
                while (start <= end) {
                    int mid = (start) + (end - start) / 2;
                    if (possible(stalls, mid, cows)) {
                        start = mid + 1;
                        ans = mid;
                    } else {
                        end = mid - 1;
                    }
                }
                out.println(ans);
            }
        }

        private boolean possible(int[] stalls, int mid, int cows) {
            cows--;
            int index = 1;
            int lastPlaced = stalls[0];
            while (cows > 0 && index < stalls.length) {
                int stall = stalls[index++];
                if (stall - lastPlaced >= mid) {
                    lastPlaced = stall;
                    cows--;
                }
            }
            return cows == 0;
        }

        // Solution End

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
