package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class SetOrDecrease {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                long k = sc.nextLong();
                long[] arr = new long[n];
                long currSum = 0;
                for (int i = 0; i < n; i++) {
                    long in = sc.nextLong();
                    currSum += in;
                    arr[i] = in;
                }
                Arrays.sort(arr);
                long start = 0;
                long end = (long) 10e13;
                long ans = -1;
                while (start <= end) {
                    long mid = start + (end - start) / 2;
                    if (possible(mid, currSum, arr, k)) {
                        ans = mid;
                        end = mid - 1;
                    } else {
                        start = mid + 1;
                    }
                }
                out.println(ans);
            }

        }

        private boolean possible(long mid, long orignalSum, long[] arr, long target) {
            for (int i = 0; i <= Math.min(arr.length - 1, mid); i++) {

                if (i > 0) {
                    orignalSum -= arr[arr.length - i];
                }

                long typeOnes = mid - i;
                long a1 = arr[0] - typeOnes;
                long tempSum = orignalSum - arr[0] + (i + 1) * (a1);
                if (tempSum <= target)
                    return true;

            }
            return false;
        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
