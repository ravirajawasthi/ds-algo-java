package byteb.tle.level3.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class SageBirthdayHardVersion {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        // Solution Here

        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
                arr[i] = sc.nextInt();

            // Edge case
            if (n < 3) {
                out.println(0);
                for (int i : arr) {
                    out.printf("%d ", i);
                }
                out.println();
                return;
            }

            Arrays.sort(arr);

            int start = 1;
            int end = (n + 1) / 2;

            int ans = 0;
            int[] ansArr = arr;

            while (start <= end) {
                int mid = (start) + (end - start) / 2;
                var posArr = possible(mid, arr);
                if (posArr.length > 0) {
                    ans = mid;
                    ansArr = posArr;
                    start = mid + 1;
                } else {
                    end = mid - 1;
                }

            }

            out.println(ans);
            for (int i : ansArr)
                out.printf("%d ", i);
            out.println();

        }
        // Solution End

        private int[] possible(int mid, int[] arr) {
            int[] resArray = new int[arr.length];
            int smallPointer = 0;
            int bigPointer = arr.length - mid - 1; // This is key to the answer, we need to be extremely greedy
            if (2 * mid + 1 > arr.length)
                return new int[0];
            for (int i = 0; i <= mid * 2; i++) {
                if (i % 2 == 0) {
                    resArray[i] = arr[bigPointer++];
                } else {
                    resArray[i] = arr[smallPointer++];
                }
            }

            int resArrayPointer = mid + (mid + 1);
            for (int i = mid; i < arr.length - (mid + 1); i++) {
                resArray[resArrayPointer++] = arr[i];
            }

            int good = 0;
            for (int i = 1; i < arr.length; i += 2) {
                if (resArray[i - 1] > resArray[i] && (i != arr.length - 1 && resArray[i + 1] > resArray[i]))
                    good++;
                if (good >= mid)
                    return resArray;
            }
            return new int[0];
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
