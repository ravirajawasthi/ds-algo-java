package byteb.tle.level3.week2.binary.search;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class PythagoreanTriples {


    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            //Pre computation
            int limit = (int) 10e6;
            ArrayList<Long> ans = new ArrayList<>();
            for (int a = 3; a <= limit; a += 2) {//for a = 1, b = 0 and we need all a, b, c > 0
                //Only odd because for even a, b is decimal number
                //We will store c value because it is largest
                //With below formulas we can be sure they follow pythagorean triples property too
                long b = ((((long) a) * a) - 1) / 2;
                long c = b + 1;
                ans.add(c);
            }

            int t = sc.nextInt();
            for (int i = 0; i < t; i++) {
                int n = sc.nextInt();
                int start = 0;
                int end = ans.size();
                while (start <= end) {
                    int mid = start + (end - start) / 2;
                    long val = ans.get(mid);
                    if (val > n) {
                        end = mid - 1;
                    } else {
                        start = mid + 1;
                    }
                }
                out.println(start);
            }

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
