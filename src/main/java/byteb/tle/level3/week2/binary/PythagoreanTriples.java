package byteb.tle.level3.week2.binary;

public class PythagoreanTriples {
    public static void main(String[] args) {
        for (int i = 1; i < 1000; i++) {
            for (int j = 1; j < 1000; j++) {
                for (int k = 1; k < 1000; k++) {
                    if (i * i == j + k && i * i == j * j + k * k) {
                        System.out.format("a^2 = b + c, (a = %d, b = %d, c = %d)%n", i, j, k);
                    }
                }
            }
        }
    }
}
