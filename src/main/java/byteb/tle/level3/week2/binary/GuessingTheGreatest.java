package byteb.tle.level3.week2.binary;

import java.util.Scanner;

public class GuessingTheGreatest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int start = 1;
        int end = n;
        while (start <= end) {
            if (start == end) {
                break;
            }

            int mid = (start) + (end - start) / 2;
            int index = getIndex(sc, start, end);

            if (end - start == 1) {
                if (index == start)
                    start = end;
                break;
            }

            if (index <= mid) {
                if (getIndex(sc, start, mid) == index) {
                    end = mid;
                } else {
                    start = mid + 1;
                }
            } else {
                if (getIndex(sc, mid + 1, end) == index) {
                    start = mid;
                } else {
                    end = mid - 1;
                }
            }

        }

        System.out.printf("! %d%n", start);
        System.out.flush();
        sc.close();

    }

    private static int getIndex(Scanner sc, int start, int end) {
        if (end == start)
            return -1;
        System.out.printf("? %d %d%n", start, end);
        System.out.flush();
        int index = sc.nextInt();
        return index;
    }

}
// [5,1,4,2,3]

// [1, 2, 3]
// [98,1,2,3,4,5,6,99]