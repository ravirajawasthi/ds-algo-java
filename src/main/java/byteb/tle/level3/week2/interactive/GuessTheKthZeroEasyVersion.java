package byteb.tle.level3.week2.interactive;

import java.io.*;
import java.util.StringTokenizer;

public class GuessTheKthZeroEasyVersion {

    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    //Solution Here

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            int t = sc.nextInt(); //Always 1 in easy version of problem
            int k = sc.nextInt(); //forgot this to take as input....


            int start = 1;
            int end = n;
            int ans = Integer.MAX_VALUE;

            while (start <= end) {
                int mid = (start) + (end - start) / 2;
                int zeroes = mid - check(mid, sc);
                if (zeroes >= k) {
                    if (zeroes == k) ans = mid;
                    end = mid - 1;
                } else {
                    start = mid + 1;
                }
            }
            System.out.printf("! %d%n", ans == Integer.MAX_VALUE ? n : ans);
            System.out.flush();

        }

        private int check(int mid, InputReader sc) {
            System.out.printf("? %d %d%n", 1, mid);
            System.out.flush();
            return sc.nextInt();
        }

        //Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
