package byteb.tle.level3.week2.interactive;

import java.util.Scanner;

public class GuessingTheGreatest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int start = 1;
        int end = n + 1;
        while (end - start > 1) {
            int mid = (start) + (end - start) / 2;
            int index = getIndex(sc, start, end - 1);

            if (index < mid) {
                if (getIndex(sc, start, mid - 1) == index) {
                    end = mid;
                } else {
                    start = mid;
                }
            } else {
                if (getIndex(sc, mid, end - 1) == index) {
                    start = mid;
                } else {
                    end = mid;
                }
            }
        }

        System.out.printf("! %d%n", start);
        System.out.flush();
        sc.close();

    }

    private static int getIndex(Scanner sc, int start, int end) {
        if (end == start)
            return -1;
        System.out.printf("? %d %d%n", start, end);
        System.out.flush();
        int index = sc.nextInt();
        return index;
    }

    // BACKUP
    /*
     * 
     * public static void main(String[] args) {
     * Scanner sc = new Scanner(System.in);
     * int n = sc.nextInt();
     * 
     * int start = 1;
     * int end = n;
     * while (start <= end) {
     * 
     * int mid = (start) + (end - start) / 2;
     * int index = getIndex(sc, start, end);
     * 
     * if (index < mid) {
     * if (getIndex(sc, start, mid - 1) == index) {
     * end = mid - 1;
     * } else {
     * start = mid;
     * }
     * } else {
     * if (getIndex(sc, mid+1, end) == index) {
     * start = mid + 1;
     * } else {
     * end = mid;
     * }
     * }
     * 
     * }
     * 
     * System.out.printf("! %d%n", start);
     * System.out.flush();
     * sc.close();
     * 
     * }
     * 
     */

}
