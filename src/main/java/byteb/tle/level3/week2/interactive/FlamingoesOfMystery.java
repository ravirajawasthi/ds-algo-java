package byteb.tle.level3.week2.interactive;

import java.util.Scanner;

/**
 * FlamingoesOfMystery
 */
public class FlamingoesOfMystery {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] flamingoes = new int[n];

        int start = 1;
        int end = 3;

        // Getting first three

        System.out.println(String.format("? %d %d", start, start + 1));
        System.out.flush();
        int xPlusy = sc.nextInt();

        System.out.println(String.format("? %d %d", start + 1, start + 2));
        System.out.flush();
        int yPlusz = sc.nextInt();

        System.out.println(String.format("? %d %d", start, end));
        System.out.flush();
        int xPlusyPlusz = sc.nextInt();

        int z = xPlusyPlusz - xPlusy;
        int y = yPlusz - z;
        int x = xPlusy - y;
        flamingoes[start - 1] = x;
        flamingoes[start] = y;
        flamingoes[end - 1] = z;

        start = 3;
        end = 4;
        while (end <= n) {

            System.out.println(String.format("? %d %d", start, end));
            System.out.flush();
            xPlusy = sc.nextInt();
            x = flamingoes[start - 1];
            y = xPlusy - x;
            flamingoes[start] = y;

            end++;
            start++;

        }

        StringBuilder ans = new StringBuilder();
        ans.append("! ");
        for (int i : flamingoes)
            ans.append(i).append(" ");
        System.out.println(ans);

        sc.close();

    }
}