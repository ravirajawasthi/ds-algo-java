package byteb.tle.level3.week2.interactive;

import java.util.Scanner;

public class FixedPointGuessing {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();

            int start = 1;
            int end = n;

            while (start <= end) {
                int mid = start + (end - start) / 2;
                if (existLeft(start, mid, sc)) {
                    end = mid - 1;
                } else {
                    start = mid + 1;
                }
            }
            System.out.printf("! %d%n", start);
            System.out.flush();
        }
        sc.close();
    }

    private static boolean existLeft(int start, int mid, Scanner sc) {
        System.out.printf("? %d %d%n", start, mid);
        System.out.flush();
        int count = 0;
        for (int i = start; i <= mid; i++) {
            int in = sc.nextInt();
            if (in >= start && in <=mid)
                count++;
        }
        return count % 2 == 1;
    }

}
