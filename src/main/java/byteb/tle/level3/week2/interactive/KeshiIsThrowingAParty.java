package byteb.tle.level3.week2.interactive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class KeshiIsThrowingAParty {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                int[] rich = new int[n];
                int[] poor = new int[n];
                for (int i = 0; i < n; i++) {
                    rich[i] = sc.nextInt();
                    poor[i] = sc.nextInt();
                }
                int start = 1;
                int end = n;
                while (start <= end) {
                    int mid = start + (end - start) / 2;
                    if (valid(mid, rich, poor)) {
                        start = mid + 1;
                    } else {
                        end = mid - 1;
                    }
                }
                out.println(end == 0 ? start : end);

            }

        }

        private boolean valid(int mid, int[] rich, int[] poor) {
            int invited = 0;
            for (int i = 0; i < rich.length; i++) {
                if (poor[i] >= invited && rich[i] >= mid - invited -1) {
                    invited++;
                    if (invited >= mid)
                        return true;
                }
            }
            return false;
        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
