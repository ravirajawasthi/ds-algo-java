package byteb.tle.level3.week2.interactive;

import java.util.HashMap;
import java.util.Scanner;

public class LostNumbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        HashMap<Integer, int[]> mapping = new HashMap<>();
        mapping.put(32, new int[] { 4, 8 });
        mapping.put(60, new int[] { 4, 15 });
        mapping.put(64, new int[] { 4, 16 });
        mapping.put(92, new int[] { 4, 23 });
        mapping.put(168, new int[] { 4, 42 });
        mapping.put(120, new int[] { 8, 15 });
        mapping.put(128, new int[] { 8, 16 });
        mapping.put(184, new int[] { 8, 23 });
        mapping.put(336, new int[] { 8, 42 });
        mapping.put(240, new int[] { 15, 16 });
        mapping.put(345, new int[] { 15, 23 });
        mapping.put(630, new int[] { 15, 42 });
        mapping.put(368, new int[] { 16, 23 });
        mapping.put(672, new int[] { 16, 42 });
        mapping.put(966, new int[] { 23, 42 });
        int[] ans = new int[6];
        for(int i = 0;i< 5; i+=2){
            System.out.println(String.format("? %d %d" , i+1 , i+2));
            var arr = mapping.get(sc.nextInt());
            ans[i] = arr[0];
            ans[i+1] = arr[1];
        }
        StringBuilder sb = new StringBuilder();
        sb.append("! ");
        for (int i: ans){
            sb.append(i).append(" ");
        }
        System.out.println(sb);
        System.out.flush();
        sc.close();

    }
}
