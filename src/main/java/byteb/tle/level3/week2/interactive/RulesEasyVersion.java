package byteb.tle.level3.week2.interactive;

import java.util.Scanner;

public class RulesEasyVersion {

    private final static boolean TESTING = false;

    public static void main(String[] args) {

        int[] squares = new int[1000];
        for (int i = 1; i < 1000; i++)
            squares[i] = i * i;

        Scanner sc = new Scanner(System.in);

        int t = sc.nextInt();
        while (t-- > 0) {
            int start = 1;
            int end = 999;
            int ans = -1;
            while (start <= end) {
                int mid = (start) + (end - start) / 2;
                int area = interact(sc, mid, mid, start, end);
                if (squares[mid] == area) {
                    start = mid + 1;
                } else {
                    ans = mid;
                    end = mid - 1;
                }
            }
            System.out.printf("! %d%n", ans); // end is the last wrong ans System.out.flush();
        }

        sc.close();
    }

    private static int interact(Scanner sc, int a, int b, int start, int end) {
        if (TESTING) {
            System.out.printf("? %d %d | start = %d, end = %d | square = %d%n", a, b, start, end, a * b);

        } else {
            System.out.printf("? %d %d%n", a, b);
        }
        System.out.flush();
        return sc.nextInt();
    }
}
