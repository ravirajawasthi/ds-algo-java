package byteb.tle.level3.week2.interactive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

public class Interview {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                long[] arr = new long[n];
                arr[0] = sc.nextInt();
                for (int i = 1; i < n; i++) {
                    arr[i] = arr[i - 1] + sc.nextLong();
                }

                int start = 1;
                int end = n;
                while (start <= end) {
                    int mid = (start) + (end - start) / 2;
                    if (isLeftPartGood(start, mid, arr, sc)) {
                        start = mid + 1;
                    } else {
                        end = mid - 1;
                    }
                }
                System.out.println("! " + (start));
                System.out.flush();

            }

        }

        private boolean isLeftPartGood(int start, int mid, long[] arr, InputReader in) {
            int leftPart = ask(start, mid, in);
            long actualLeftPart = -1;
            if (start == 1) {
                actualLeftPart = arr[mid-1];
            } else {
                actualLeftPart = arr[mid-1] - arr[start - 2];
            }
            return actualLeftPart == leftPart;
        }

        private int ask(int start, int mid, InputReader in) {
            StringBuilder sb = new StringBuilder();
            sb.append("?").append(" ").append(mid-start+1).append(" ");
            IntStream.range(start, mid + 1).forEach(i -> sb.append(i).append(" "));
            System.out.println(sb);
            System.out.flush();
            return in.nextInt();
        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
