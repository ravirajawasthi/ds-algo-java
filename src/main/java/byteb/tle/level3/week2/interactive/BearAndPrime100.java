package byteb.tle.level3.week2.interactive;

import java.util.List;
import java.util.Scanner;

public class BearAndPrime100 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        var primes = List.of(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 4, 9, 25, 49);
        boolean prime = true;

        int divisors = 0;

        for (int p : primes) {
            System.out.println(p);
            System.out.flush();
            String input = sc.nextLine();
            if (input.equals("yes")) {
                divisors++;
                if (divisors > 1) {
                    prime = false;
                    break;
                }
            }
        }
        if (prime) {
            System.out.println("prime");
        } else {
            System.out.println("composite");
        }
    }

}
