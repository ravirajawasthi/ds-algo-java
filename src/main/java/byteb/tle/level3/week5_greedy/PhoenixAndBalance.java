package byteb.tle.level3.week5_greedy;

import java.util.Scanner;

public class PhoenixAndBalance {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0){
            int n = sc.nextInt();
            long bucket_a = 0;
            long bucket_b = 0;
            long currNum = 1;

            for (int i = 1; i < (n/2); i++ ){
                currNum *= 2;
                bucket_a += currNum;
            }
            for (int i = (n/2); i < n; i++){
                currNum *= 2;
                bucket_b += currNum;
            }
            currNum *= 2;
            bucket_a+= currNum;
            System.out.println(Math.abs(bucket_b - bucket_a));
        }
        sc.close();
    }
}

/* 
 * 2, 4, 8, 16, 32, 64
 * 
*/
