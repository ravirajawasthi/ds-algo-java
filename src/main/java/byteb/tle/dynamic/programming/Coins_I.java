package byteb.tle.dynamic.programming;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.TreeMap;

//https://atcoder.jp/contests/dp/tasks/dp_i
public class Coins_I {


    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    //Solution Here

    static class Task {


        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            double[] tails = new double[n];

            for (int i = 0; i < n; i++) {
                tails[i] = sc.nextDouble();
            }

            double[][] dp = new double[n + 1][n + 1];

            Arrays.fill(dp[0], 0d);

            for (int HEADS = 0; HEADS <= n; HEADS++) {
                for (int INDEX = 1; INDEX <= n; INDEX++) {

                    if (HEADS > INDEX) continue;

                    double h = tails[INDEX - 1];

                    //I get tails
                    //That means I need HEADS with INDEX - 1
                    //What happens with HEADS = 1 and INDEX = 1
                    double tail = 1 - h;
                    if (HEADS > INDEX - 1) {
                        tail = 0;
                    } else if (INDEX - 1 > 0) {
                        tail *= dp[HEADS][INDEX - 1];
                    }


                    //I get heads
                    //That means I need HEADS - 1 with INDEX - 1
                    //What happens with HEADS = 0 and WINDEX = 1
                    double head = h;
                    if (HEADS == 0) {
                        head = 0;
                    } else if (INDEX - 1 > 0) {
                        head *= dp[HEADS - 1][INDEX - 1];
                    }

                    dp[HEADS][INDEX] = tail + head;


                }


            }

            double sum = 0;
            for (int i = n / 2 + 1; i <= n; i++) {
                sum += dp[i][n];
            }

            out.println(sum);
        }

        //Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    static class MultiTreeSet {
        TreeMap<Integer, Integer> freqTreeMap = new TreeMap<>();
        int size;

        public MultiTreeSet() {
        }

        public MultiTreeSet(Collection<Integer> c) {
            for (Integer element : c)
                add(element);
        }

        public int size() {
            return size;
        }

        public void add(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                freqTreeMap.put(element, 1);
            else
                freqTreeMap.put(element, freq + 1);
            ++size;
        }

        public void remove(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq != null) {
                if (freq == 1)
                    freqTreeMap.remove(element);
                else
                    freqTreeMap.put(element, freq - 1);
                --size;
            }
        }

        public int get(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                return 0;
            return freq;
        }

        public boolean contains(Integer element) {
            return get(element) > 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public Integer first() {
            return freqTreeMap.firstKey();
        }

        public Integer last() {
            return freqTreeMap.lastKey();
        }

        public Integer pollLast() {
            var retVal = freqTreeMap.lastKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer pollFirst() {
            var retVal = freqTreeMap.firstKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer ceiling(Integer element) {
            return freqTreeMap.ceilingKey(element);
        }

        public Integer floor(Integer element) {
            return freqTreeMap.floorKey(element);
        }

        public Integer higher(Integer element) {
            return freqTreeMap.higherKey(element);
        }

        public Integer lower(Integer element) {
            return freqTreeMap.lowerKey(element);
        }

        @Override
        public String toString() {
            return "MultiTreeSet [freqTreeMap=" + freqTreeMap + ", size=" + size + "]";
        }
    }


}
