package byteb.tle.dynamic.programming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.TreeMap;

//https://codeforces.com/problemset/problem/1714/D

public class ColorWithOccurrences {

    private static final int MODULO = 1000000007;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    // Solution Here

    static class Task {

        public void solve(InputReader sc, PrintWriter out) {

            int t = sc.nextInt();
            while (t-- > 0) {
                String s = sc.next();
                int n = s.length();

                int k = sc.nextInt();

                String[] strings = new String[k];
                for (int i = 0; i < k; i++)
                    strings[i] = sc.next();

                int[][] dp = new int[n + 1][n + 1];

                for (var a : dp)
                    Arrays.fill(a, Integer.MAX_VALUE);

                dp[n][0] = 0;

                for (int i = n - 1; i > -1; i--) {
                    for (int j = 0; j < n; j++) {
                        int dont = Integer.MAX_VALUE;
                        if (i + j <= n && j != 0) {
                            dont = dp[i + 1][j - 1];
                        }

                        if (i + j == n) {
                            dp[i][j] = 0;
                            continue;
                        }

                        int match = Integer.MAX_VALUE;
                        for (String st : strings) {
                            if (i + st.length() <= n && s.substring(i, i + st.length()).equals(st)) {
                                int dpCall = dp[i + 1][Math.max(j - 1, st.length() - 1)];
                                if (dpCall != Integer.MAX_VALUE)
                                    dpCall++;
                                match = Math.min(match, dpCall);
                            }
                        }
                        dp[i][j] = Math.min(dont, match);

                    }

                }
                out.println(dp[0][0] == Integer.MAX_VALUE ? -1 : dp[0][0]);

            }

        }

        // Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    static class MultiTreeSet {
        TreeMap<Integer, Integer> freqTreeMap = new TreeMap<>();
        int size;

        public MultiTreeSet() {
        }

        public MultiTreeSet(Collection<Integer> c) {
            for (Integer element : c)
                add(element);
        }

        public int size() {
            return size;
        }

        public void add(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                freqTreeMap.put(element, 1);
            else
                freqTreeMap.put(element, freq + 1);
            ++size;
        }

        public void remove(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq != null) {
                if (freq == 1)
                    freqTreeMap.remove(element);
                else
                    freqTreeMap.put(element, freq - 1);
                --size;
            }
        }

        public int get(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                return 0;
            return freq;
        }

        public boolean contains(Integer element) {
            return get(element) > 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public Integer first() {
            return freqTreeMap.firstKey();
        }

        public Integer last() {
            return freqTreeMap.lastKey();
        }

        public Integer pollLast() {
            var retVal = freqTreeMap.lastKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer pollFirst() {
            var retVal = freqTreeMap.firstKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer ceiling(Integer element) {
            return freqTreeMap.ceilingKey(element);
        }

        public Integer floor(Integer element) {
            return freqTreeMap.floorKey(element);
        }

        public Integer higher(Integer element) {
            return freqTreeMap.higherKey(element);
        }

        public Integer lower(Integer element) {
            return freqTreeMap.lowerKey(element);
        }

        @Override
        public String toString() {
            return "MultiTreeSet [freqTreeMap=" + freqTreeMap + ", size=" + size + "]";
        }
    }

}
