package byteb.tle.dynamic.programming;

import java.io.*;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class ArraysDescription {

    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    //Solution Here

    static class Task {


        public void solve(InputReader sc, PrintWriter out) {

            int n = sc.nextInt();
            int m = sc.nextInt();
            int[] arr = new int[n];

            for (int i = 0; i < n; i++) arr[i] = sc.nextInt();

            long[][] dp = new long[m + 1][n + 1];
            if (arr[n - 1] == 0) {
                for (int j = 0; j <= m; j++) dp[j][n - 1] = 1;
            } else {
                dp[arr[n - 1]][n - 1] = 1;
            }

            for (int i = n - 2; i > -1; i--) {
                if (arr[i] == 0) {
                    for (int j = 1; j <= m; j++) {
                        dp[j][i] = ((j + 1 <= m ? dp[j + 1][i + 1] : 0) + dp[j][i + 1] + (j - 1 >= 1 ? dp[j - 1][i + 1] : 0)) % MODULO;
                    }
                } else {
                    dp[arr[i]][i] = (dp[arr[i]][i + 1] + (arr[i] - 1 >= 1 ? dp[arr[i] - 1][i + 1] : 0) + (arr[i] + 1 <= m ? dp[arr[i] + 1][i + 1] : 0)) % MODULO;
                }
            }

            if (arr[0] == 0) {
                long sum = 0;
                for (int i = 1; i <= m; i++) {
                    sum = (sum + dp[i][0]) % MODULO;
                }
                out.println(sum);
            } else {
                out.println(dp[arr[0]][0]);
            }


        }


        //Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    static class MultiTreeSet {
        TreeMap<Integer, Integer> freqTreeMap = new TreeMap<>();
        int size;

        public MultiTreeSet() {
        }

        public MultiTreeSet(Collection<Integer> c) {
            for (Integer element : c)
                add(element);
        }

        public int size() {
            return size;
        }

        public void add(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                freqTreeMap.put(element, 1);
            else
                freqTreeMap.put(element, freq + 1);
            ++size;
        }

        public void remove(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq != null) {
                if (freq == 1)
                    freqTreeMap.remove(element);
                else
                    freqTreeMap.put(element, freq - 1);
                --size;
            }
        }

        public int get(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                return 0;
            return freq;
        }

        public boolean contains(Integer element) {
            return get(element) > 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public Integer first() {
            return freqTreeMap.firstKey();
        }

        public Integer last() {
            return freqTreeMap.lastKey();
        }

        public Integer pollLast() {
            var retVal = freqTreeMap.lastKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer pollFirst() {
            var retVal = freqTreeMap.firstKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer ceiling(Integer element) {
            return freqTreeMap.ceilingKey(element);
        }

        public Integer floor(Integer element) {
            return freqTreeMap.floorKey(element);
        }

        public Integer higher(Integer element) {
            return freqTreeMap.higherKey(element);
        }

        public Integer lower(Integer element) {
            return freqTreeMap.lowerKey(element);
        }

        @Override
        public String toString() {
            return "MultiTreeSet [freqTreeMap=" + freqTreeMap + ", size=" + size + "]";
        }
    }


}
