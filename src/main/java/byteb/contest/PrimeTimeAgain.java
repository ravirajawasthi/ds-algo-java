package byteb.contest;

import java.util.Scanner;

public class PrimeTimeAgain {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int d = sc.nextInt();
        int p = sc.nextInt();
        int ans = 0;
        int maxval = d / p;
        for (int i = 0 ; i < maxval ; i++) {
            if (checkIfPrime(i)) {
                boolean allGood = true;
                for (int j = 1 ; j < p ; j++) {
                    if (!checkIfPrime(i + (d/p) * j)) {
                        allGood = false;
                        break;
                    }
                }
                if (allGood){
                    ans++;
                }
            }
        }
        System.out.println(ans);


    }

    static boolean checkIfPrime(int number) {
        if (number < 2)
            return false;
        for (int i = 2 ; i < number ; i++)
            if (number % i == 0)
                return false;
        return true;
    }


}
