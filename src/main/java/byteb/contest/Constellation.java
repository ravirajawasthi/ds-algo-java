package byteb.contest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class Constellation {
    static final char[][] A = new char[][]{
            { '.', '*', '.' },
            { '*', '*', '*' },
            { '*', '.', '*' }
    };


    static final char[][] E = new char[][]{
            { '*', '*', '*' },
            { '*', '*', '*' },
            { '*', '*', '*' }
    };


    static final char[][] U = new char[][]{
            { '*', '.', '*' },
            { '*', '.', '*' },
            { '*', '*', '*' }
    };

    static final char[][] I = new char[][]{
            { '*', '*', '*' },
            { '.', '*', '.' },
            { '*', '*', '*' }
    };

    static final char[][] O = new char[][]{
            { '*', '*', '*' },
            { '*', '.', '*' },
            { '*', '*', '*' }
    };

    static final char[][][] vowels = new char[][][]{ A, E, I, O, U };
    static final List<Character> vowelsChar = Arrays.asList('A', 'E', 'I', 'O', 'U');

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        HashSet<Integer> galaxyEnd = new HashSet<>();
        char[][] galaxy = new char[ 3 ][ n ];
        sc.nextLine();
        for (int line = 0 ; line < 3 ; line++) {
            String input = sc.nextLine();
            for (int i = 0 ; i < n ; i++) {
                galaxy[ line ][ i ] = input.charAt(i);
            }
        }

        for (int i = 0 ; i < n ; i++) {
            if (galaxy[ 0 ][ i ] == '#') {
                galaxyEnd.add(i);
            }
        }
        System.out.println(new String(galaxy[2]));
        StringBuilder res = new StringBuilder();
        int i = 0;
        while (i < n) {
            if (galaxyEnd.contains(i)) {
                res.append("#");
                i++;
            } else {
                int ans = checkAllVowelsStartingFromThisIndex(galaxy, i);
                if (ans == -1) {
                    i++;
                } else {
                    i += 3;
                    res.append(vowelsChar.get(ans));
                }

            }
        }
        System.out.println(res);
    }


    private static int checkAllVowelsStartingFromThisIndex(char[][] galaxy, int startIndex) {
        for (int index = 0 ; index < 5 ; index++) {
            if (checkVowel(galaxy, startIndex, vowels[index])) {
                return index;
            }
        }
        return -1;
    }

    private static boolean checkVowel(char[][] galaxy, int startIndex, char[][] vowel) {
        for (int i = 0 ; i < 3 ; i++) {
            for (int j = 0 ; j < 3 ; j++) {
                if (galaxy[ i ][ startIndex + j ] != vowel[ i ][ j ]) {
                    return false;
                }
            }
        }
        return true;
    }


}
