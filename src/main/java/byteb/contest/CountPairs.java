package byteb.contest;

import java.util.Arrays;
import java.util.Scanner;

public class CountPairs {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int x = sc.nextInt();

        int[] arr = new int[ n ];


        for (int i = 0 ; i < n ; i++) {
            int num = sc.nextInt();
            arr[ i ] = num;
        }
        if (x == 0) {
            System.out.println(n);
            return;
        }

        Arrays.parallelSort(arr);

        int count = 0;

        for (int i = 0 ; i < n ; i++) {
            int num = arr[ i ];
            if (i == 0 && ( arr[ 1 ] <= ( num + x ) && arr[ 1 ] >= ( num - x ) )) {
                count++;
            } else if (i == ( n - 1 ) && ( arr[ n - 2 ] <= ( num + x ) && arr[ n - 2 ] >= ( num - x ) )) {
                count++;
            } else if (( i != 0 && i != ( n - 1 ) ) &&
                    ( ( ( arr[ i - 1 ] <= ( num + x ) ) && ( arr[ i - 1 ] >= ( num - x ) ) ) ||
                            ( ( arr[ i + 1 ] <= ( num + x ) ) && ( arr[ i + 1 ] >= ( num - x ) ) ) )) {
                count++;

            }

        }
        System.out.println(count);


    }

}
