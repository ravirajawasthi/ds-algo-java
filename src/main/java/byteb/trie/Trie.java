package byteb.trie;
//https://leetcode.com/problems/implement-trie-prefix-tree/description/

class Trie {

    TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode currentNode = root;
        int wordIndex = 0;
        while (wordIndex < word.length()) {
            char c = word.charAt(wordIndex);
            if (currentNode.getAlphabet(c - 'a') == null) {
                currentNode.setAlphabet(c - 'a', new TrieNode());
            }
            currentNode = currentNode.getAlphabet(c - 'a');
            wordIndex++;
        }
        currentNode.setWordEndsHere(true);
    }

    public boolean search(String word) {
        TrieNode currNode = root;
        int currWordIndex = 0;
        while (currNode != null && currWordIndex < word.length()) {
            currNode = currNode.getAlphabet(word.charAt(currWordIndex) - 'a');
            currWordIndex++;
        }

        return currNode != null && currNode.getWordsEndsHere();
    }

    public boolean startsWith(String prefix) {

        TrieNode currNode = root;
        int currWordIndex = 0;
        while (currNode != null && currWordIndex < prefix.length()) {
            currNode = currNode.getAlphabet(prefix.charAt(currWordIndex) - 'a');
            currWordIndex++;
        }

        return currWordIndex == prefix.length() && currNode != null;

    }

    private static class TrieNode {
        private TrieNode[] alphabets;
        private boolean wordEndsHere;

        TrieNode() {
            alphabets = new TrieNode[26];
            wordEndsHere = false;
        }

        void setWordEndsHere(boolean flag) {
            this.wordEndsHere = flag;
        }

        boolean getWordsEndsHere() {
            return this.wordEndsHere;
        }

        TrieNode getAlphabet(int index) {
            return alphabets[index];
        }

        void setAlphabet(int index, TrieNode node) {
            this.alphabets[index] = node;
        }

    }

}
