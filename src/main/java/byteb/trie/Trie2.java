package byteb.trie;

class Trie2 {
    TrieNode root;

    public Trie2() {
        root = new TrieNode();
    }

    public void insert(String word) {
        int wordIndex = 0;
        TrieNode currNode = root;

        while (wordIndex < word.length()) {
            char c = word.charAt(wordIndex++);

            if (currNode.getAlphabet(c - 'a') == null)
                currNode.setAlphabet(c - 'a', new TrieNode());

            currNode = currNode.getAlphabet(c - 'a');
            currNode.incrementPrefixCount();
        }
        currNode.incrementEndsHere();

    }

    public int countWordsEqualTo(String word) {
        TrieNode currNode = getTrieNode(word);
        if (currNode == null) return 0;
        return currNode.getEndsHere();
    }

    public int countWordsStartingWith(String prefix) {
        TrieNode currNode = getTrieNode(prefix);
        if (currNode == null) return 0;
        return currNode.getPrefixCount();
    }

    private TrieNode getTrieNode(String prefix) {
        int wordIndex = 0;
        TrieNode currNode = root;
        while (wordIndex < prefix.length() && currNode != null) {
            char c = prefix.charAt(wordIndex++);
            currNode = currNode.getAlphabet(c - 'a');
        }
        return currNode;
    }

    public void erase(String word) {
        int wordIndex = 0;
        TrieNode currNode = root;
        while (wordIndex < word.length() && currNode != null) {
            char c = word.charAt(wordIndex++);
            currNode = currNode.getAlphabet(c - 'a');
            currNode.decrementPrefixCount();
        }

        if (currNode != null) currNode.decrementEndsHere();
    }

    private static class TrieNode {
        private final TrieNode[] alphabets;
        private int endsHere;
        private int prefixCount;

        TrieNode() {
            alphabets = new TrieNode[26];
            endsHere = 0;
            prefixCount = 0;
        }

        TrieNode getAlphabet(int index) {
            return alphabets[index];
        }

        void setAlphabet(int index, TrieNode node) {
            alphabets[index] = node;
        }


        public void incrementEndsHere() {
            this.endsHere++;
        }

        public void incrementPrefixCount() {
            this.prefixCount++;
        }

        public void decrementPrefixCount() {
            this.prefixCount--;
        }

        public void decrementEndsHere() {
            this.endsHere--;
        }

        public int getEndsHere() {
            return endsHere;
        }

        public int getPrefixCount() {
            return prefixCount;
        }
    }
}
