package byteb.trees;

import byteb.utils.Utils;

import java.util.*;

public class BoundaryTraversalCodingNinjas {
    public static void main(String[] args) {

        int[] tree = Utils.createArrayFromString("1 2 3 4 5 6 7 -1 -1 -1 -1 -1 -1 -1 -1");
        TreeNode root = createABtFromArray(tree);
        System.out.println(traverseBoundary(root));
    }

    private static class TreeNode {
        int data;
        TreeNode left;
        TreeNode right;

        TreeNode(int data) {
            this.data = data;
            this.left = null;
            this.right = null;
        }

    }

    private static class StackData {
        TreeNode node;
        int level;

        public StackData(TreeNode node, int level) {
            this.node = node;
            this.level = level;
        }
    }

    public static ArrayList<Integer> traverseBoundary(TreeNode root) {


        Deque<StackData> stack = new LinkedList<>();


        //Left tree
        HashSet<Integer> leftView = new HashSet<>();
        ArrayList<Integer> leftTraversal = new ArrayList<>();
        stack.push(new StackData(root.left, 1));

        while (!stack.isEmpty()) {
            StackData element = stack.pop();
            TreeNode node = element.node;
            int level = element.level;

            if (node.right != null) {
                stack.push(new StackData(node.right, level + 1));
            }
            if (node.left != null) {
                stack.push(new StackData(node.left, level + 1));
            }

            if (!leftView.contains(level)) {
                leftView.add(level);
                leftTraversal.add(node.data);
            } else if (node.left == null && node.right == null) {
                leftTraversal.add(node.data);
            }

        }

        stack.clear();

        //Right tree
        HashSet<Integer> rightView = new HashSet<>();
        ArrayList<Integer> rightTraversal = new ArrayList<>();
        stack.push(new StackData(root.right, 1));
        while (!stack.isEmpty()) {
            StackData element = stack.pop();
            TreeNode node = element.node;
            int level = element.level;

            if (node.left != null) {
                stack.push(new StackData(node.left, level + 1));
            }
            if (node.right != null) {
                stack.push(new StackData(node.right, level + 1));
            }


            if (!rightView.contains(level)) {
                rightView.add(level);
                rightTraversal.add(node.data);
            } else if (node.left == null && node.right == null) {
                rightTraversal.add(node.data);
            }

        }
        Collections.reverse(rightTraversal);


        leftTraversal.add(0, root.data);
        leftTraversal.addAll(rightTraversal);

        return leftTraversal;
    }

    private static TreeNode createABtFromArray(int[] elements) {
        int currIndex = 0;
        TreeNode root = new TreeNode(elements[currIndex++]);
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            TreeNode bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new TreeNode(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new TreeNode(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }

}
