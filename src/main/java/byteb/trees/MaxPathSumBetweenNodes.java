package byteb.trees;

import java.util.ArrayDeque;
import java.util.Queue;

public class MaxPathSumBetweenNodes {
    public static void main(String[] args) {
        TreeNode root = createABtFromArray(new int[]{5, 6, 2, 4, 3, -1, -1, 9, 7, -1, -1, -1, -1, -1, -1});
        System.out.println("findMaxSumPath(root) = " + findMaxSumPath(root));
    }

    static class TreeNode {
        int data;
        TreeNode left;
        TreeNode right;

        TreeNode(int data) {
            this.data = data;
            this.left = null;
            this.right = null;
        }

    }

    private static long maxPathSum = 0;


    private static long maxPathSum(TreeNode node) {

        long leftTree = node.left != null ? Math.max(maxPathSum(node.left), 0) : 0;
        long rightTree = node.right != null ? Math.max(maxPathSum(node.right), 0) : 0;

        long currSum = node.data + leftTree + rightTree;

        maxPathSum = Math.max(currSum, maxPathSum);

        return Math.max(node.data + leftTree, node.data + rightTree);
    }

    public static long findMaxSumPath(TreeNode root) {
        if (root.left == null || root.right == null) {
            return -1L;
        }

        maxPathSum = 0;
        maxPathSum(root);
        return maxPathSum;
    }


    private static TreeNode createABtFromArray(int[] elements) {
        int currIndex = 0;
        TreeNode root = new TreeNode(elements[currIndex++]);
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            TreeNode bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new TreeNode(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new TreeNode(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }
}
