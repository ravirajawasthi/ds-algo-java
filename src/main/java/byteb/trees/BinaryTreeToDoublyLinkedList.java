package byteb.trees;

import byteb.trees.model.BinaryTreeNode;

//https://www.codingninjas.com/studio/problems/convert-a-given-binary-tree-to-doubly-linked-list_8230744?challengeSlug=striver-sde-challenge

public class BinaryTreeToDoublyLinkedList {
    //TODO this is not complete

    public static BinaryTreeNode<Integer> BTtoDLL(BinaryTreeNode<Integer> root) {
        BinaryTreeNode<Integer> ansRoot = new BinaryTreeNode<>(-1);
        BinaryTreeNode<Integer> ans = ansRoot;

        BinaryTreeNode<Integer> curr = root;

        while (curr != null) {
            if (curr.left == null) {
                //both inorder and preorder
                ans.right = curr;
                curr.left = ans;
                ans = ans.right;

                curr = curr.right;

            } else {
                BinaryTreeNode<Integer> rightMost = curr.left;
                while (rightMost.right != null && rightMost.right != curr) {
                    rightMost = rightMost.right;
                }

                if (rightMost.right == null) {
                    rightMost.right = curr;
                    curr = curr.left;
                } else {
                    rightMost.right = null;

                    ans.right = curr;
                    curr.left = ans;
                    ans = ans.right;


                    curr = curr.right;
                }
            }
        }


        if (ansRoot.right != null) {
            ansRoot.right.left = null;
        }
        return ansRoot.right;
    }

}
