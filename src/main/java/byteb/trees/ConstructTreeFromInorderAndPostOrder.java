package byteb.trees;

import java.util.*;

public class ConstructTreeFromInorderAndPostOrder {
    public static void main(String[] args) {
        int[] inorder = new int[]{9, 3, 15, 20, 7};
        int[] postorder = new int[]{9, 15, 7, 20, 3};

        TreeNode newTree = buildTree(inorder, postorder);
        System.out.println("newTreeheight = " + height);


    }

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    static int height = 0;

    public static TreeNode buildTree(int[] inorder, int[] postorder) {
        return build(inorder, postorder, 0);

    }

    private static TreeNode build(int[] inorder, int[] postorder, int currHeight) {
        if (inorder.length == 0) {
            return null;
        }

        height = Math.max(height, currHeight);

        if (inorder.length == 1) {
            return new TreeNode(inorder[0]);
        }

        TreeNode root = new TreeNode(postorder[postorder.length - 1]);
        int rootIndex = -1;
        for (int i = 0; i < inorder.length; i++) {
            if (root.val == inorder[i]) {
                rootIndex = i;
            }
        }


        int[] leftTreeInOrder;
        HashSet<Integer> leftTreeInOrderHashSet = new HashSet<>();

        if (rootIndex == 0) {
            leftTreeInOrder = new int[0];
        } else {
            leftTreeInOrder = new int[rootIndex];
            System.arraycopy(inorder, 0, leftTreeInOrder, 0, rootIndex);
        }

        Arrays.stream(leftTreeInOrder).forEach(leftTreeInOrderHashSet::add);

        int[] rightTreeInOrder;
        HashSet<Integer> rightTreeInOrderHashSet = new HashSet<>();
        if (rootIndex == inorder.length - 1) {
            rightTreeInOrder = new int[0];
        } else {
            rightTreeInOrder = new int[inorder.length - rootIndex - 1];
            System.arraycopy(inorder, rootIndex + 1, rightTreeInOrder, 0, inorder.length - rootIndex - 1);
        }

        Arrays.stream(rightTreeInOrder).forEach(rightTreeInOrderHashSet::add);


        int[] leftTreePostOrder = new int[leftTreeInOrder.length];
        int[] rightTreePostOrder = new int[rightTreeInOrder.length];

        int leftTreePostOrderIndex = 0;
        int rightTreePostOrderIndex = 0;


        for (int num : postorder) {
            if (leftTreeInOrderHashSet.contains(num))
                leftTreePostOrder[leftTreePostOrderIndex++] = num;

            if (rightTreeInOrderHashSet.contains(num))
                rightTreePostOrder[rightTreePostOrderIndex++] = num;
        }


        root.left = build(leftTreeInOrder, leftTreePostOrder, currHeight + 1);
        root.right = build(rightTreeInOrder, rightTreePostOrder, currHeight + 1);

        return root;


    }

}
