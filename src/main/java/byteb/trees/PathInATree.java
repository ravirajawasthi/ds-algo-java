package byteb.trees;

import java.util.*;

public class PathInATree {
    public static void main(String[] args) {
        int[] tree = new int[]{1, 4, -1, 10, 5, 2, 9, 3, -1, -1, 8, -1, 7, 6, -1, -1, -1, -1, -1, -1, -1};
        TreeNode root = createABtFromArray(tree);
        System.out.println("pathInATree(root, 7) = " + pathInATree(root, 1));
    }

    static class TreeNode {
        int data;
        TreeNode left, right;

        public TreeNode(int item) {
            data = item;
            left = right = null;
        }
    }

    public static ArrayList<Integer> pathInATree(TreeNode root, int x) {
        if (root == null) {
            return null;
        }

        if (root.data == x) {
            return new ArrayList<>(Collections.singletonList(x));
        }
        HashMap<TreeNode, ArrayList<Integer>> map = new HashMap<>();
        map.put(root, new ArrayList<>(Collections.singletonList(root.data)));

        Queue<TreeNode> queue = new LinkedList<>();

        queue.add(root);


        while (!queue.isEmpty()) {
            TreeNode element = queue.remove();
            if (element.left != null) {
                ArrayList<Integer> path = new ArrayList<>(map.get(element));
                path.add(element.left.data);
                if (element.left.data == x) {
                    return path;
                }
                map.put(element.left, path);
                queue.add(element.left);
            }

            if (element.right != null) {
                ArrayList<Integer> path = new ArrayList<>(map.get(element));
                path.add(element.right.data);
                if (element.right.data == x) {
                    return path;
                }
                map.put(element.right, path);
                queue.add(element.right);
            }

        }
        return null;

    }

    private static TreeNode createABtFromArray(int[] elements) {
        int currIndex = 0;
        TreeNode root = new TreeNode(elements[currIndex++]);
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            TreeNode bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new TreeNode(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new TreeNode(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }
}
