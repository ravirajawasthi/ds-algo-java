package byteb.trees;

import byteb.trees.model.BinaryTreeNode;
import byteb.trees.model.TreeNode;

import java.util.ArrayList;
import java.util.List;


//https://leetcode.com/problems/binary-tree-inorder-traversal/

public class MorrisTraversal {


    public static List<Integer> inorder(TreeNode root) {
        //left root right
        ArrayList<Integer> inorderTraversal = new ArrayList<>();

        TreeNode curr = root;

        while (curr != null) {

            if (curr.left == null) {
                inorderTraversal.add(curr.val);
                curr = curr.right;
            } else {
                var rightMost = curr.left;
                while (rightMost.right != null && rightMost.right != curr) {
                    rightMost = rightMost.right;
                }

                if (rightMost.right == null) {
                    rightMost.right = curr;
                    curr = curr.left;
                } else {
                    inorderTraversal.add(curr.val);
                    rightMost.right = null;
                    curr = curr.right;
                }

            }
        }

        return inorderTraversal;
    }

    public static List<Integer> inorder(BinaryTreeNode<Integer> root) {
        //left root right
        ArrayList<Integer> inorderTraversal = new ArrayList<>();

        BinaryTreeNode<Integer> curr = root;

        while (curr != null) {

            if (curr.left == null) {
                inorderTraversal.add(curr.data);
                curr = curr.right;
            } else {
                var rightMost = curr.left;
                while (rightMost.right != null && rightMost.right != curr) {
                    rightMost = rightMost.right;
                }

                if (rightMost.right == null) {
                    rightMost.right = curr;
                    curr = curr.left;
                } else {
                    inorderTraversal.add(curr.data);
                    rightMost.right = null;
                    curr = curr.right;
                }

            }
        }

        return inorderTraversal;
    }

    public static List<Integer> preorder(TreeNode root) {
        //root left right
        ArrayList<Integer> preorder = new ArrayList<>();
        TreeNode curr = root;
        while (curr != null) {
            if (curr.left == null) {
                preorder.add(curr.val);
                curr = curr.right;
            } else {
                var rightMostNode = curr.left;
                while (rightMostNode.right != null && rightMostNode.right != curr) {
                    rightMostNode = rightMostNode.right;
                }

                if (rightMostNode.right == null){
                    rightMostNode.right = curr;
                    preorder.add(curr.val);
                    curr = curr.left;
                }else{
                    rightMostNode.right = null;
                    curr = curr.right;
                }
            }

        }

        return preorder;

    }
}