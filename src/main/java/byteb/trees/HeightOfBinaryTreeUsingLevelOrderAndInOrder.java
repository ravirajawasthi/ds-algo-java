package byteb.trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.IntStream;

public class HeightOfBinaryTreeUsingLevelOrderAndInOrder {
    //TODO need to optimise. dont know how
    public static void main(String[] args) {
        int[] inorder = new int[]{7, 4, 2, 1, 5, 3, 6};
        int[] levelorder = new int[]{1, 2, 3, 4, 5, 6, 7};

        System.out.println("heightOfTheTree(inorder, levelorder, 0) = " + heightOfTheTree(inorder, levelorder, 0));
    }

    static HashMap<Integer, Integer> inorderMap = new HashMap<>();
    static int[] inorder1;


    public static int heightOfTheTree(int[] inorder, int[] levelOrder, int N) {
        inorder1 = inorder;
        ArrayList<Integer> levelOrderLL = new ArrayList<>();
        IntStream.range(0, inorder.length).forEach(index -> {
            levelOrderLL.add(levelOrder[index]);
            inorderMap.put(inorder[index], index);
        });
        return buildTree(levelOrderLL, 0, inorder.length - 1) - 1;
    }

    private static int buildTree(ArrayList<Integer> levelOrder, int ios, int ioe) {
        if (levelOrder.size() < 2) {
            return levelOrder.size();
        }

        int root = levelOrder.get(0);
        int rootIndex = inorderMap.get(root);

        HashSet<Integer> leftInorderSet = new HashSet<>();
        IntStream.range(ios, rootIndex).forEach(i -> leftInorderSet.add(inorder1[i]));


        ArrayList<Integer> leftTreeLevelOrder = new ArrayList<>();
        ArrayList<Integer> rightTreeLevelOrder = new ArrayList<>();


        levelOrder.forEach(n -> {
            if (leftInorderSet.contains(n)) {
                leftTreeLevelOrder.add(n);
            } else if (n != root) {
                rightTreeLevelOrder.add(n);
            }
        });


        int left = buildTree(leftTreeLevelOrder, ios, rootIndex - 1);

        int right = buildTree(rightTreeLevelOrder, rootIndex + 1, ioe);

        return 1 + Math.max(left, right);


    }


}
