package byteb.trees;

import java.util.ArrayDeque;
import java.util.Queue;

public class LCABinaryTree {

    public static void main(String[] args) {
        TreeNode<Integer> root = createABtFromArray(new int[]{1, 2, 3, 4, 7, -1, -1, -1, -1, -1, -1});
        System.out.println("LCA(root, 4, 7) = " + LCA(root, 4, 7));
    }

    static class TreeNode<T> {

        T data;
        TreeNode<T> left;
        TreeNode<T> right;

        TreeNode(T data) {
            this.data = data;
            left = null;
            right = null;
        }
    }

    private static Integer LCA(TreeNode<Integer> node, int p, int q) {
        if (node.data == p || node.data == q) {
            return node.data;
        }

        Integer leftTree = node.left != null ? LCA(node.left, p, q) : null;
        Integer rightTree = node.right != null ? LCA(node.right, p, q) : null;

        if (leftTree != null && rightTree != null) {
            return node.data;
        } else if (leftTree == null && rightTree == null) {
            return null;
        }

        return leftTree == null ? rightTree : leftTree;

    }


    public static int lowestCommonAncestor(TreeNode<Integer> root, int x, int y) {
        return LCA(root, x, y);
    }

    private static TreeNode<Integer> createABtFromArray(int[] elements) {
        int currIndex = 0;
        TreeNode<Integer> root = new TreeNode<>(elements[currIndex++]);
        Queue<TreeNode<Integer>> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            TreeNode<Integer> bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new TreeNode<>(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new TreeNode<>(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }


}
