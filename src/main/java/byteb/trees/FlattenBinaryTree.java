package byteb.trees;


//https://leetcode.com/problems/flatten-binary-tree-to-linked-list/

import byteb.trees.model.TreeNode;

public class FlattenBinaryTree {

    public static void flatten(TreeNode root) {
        TreeNode curr = root;
        while (curr != null) {
            if (curr.left == null) {
                curr = curr.right;
            } else {
                var rightMost = curr.left;
                while (rightMost.right != null && rightMost.right != curr) {
                    rightMost = rightMost.right;
                }
                if (rightMost.right == null) {
                    rightMost.right = curr;
                    curr = curr.left;
                } else {
                    //this is gonna be overridden
                    TreeNode currRight = curr.right;
                    //whole left branch to right
                    curr.right = curr.left;
                    //the end of the branch connected with replaced right
                    rightMost.right = currRight;
                    //removing left, because flattening
                    curr.left = null;
                    //continuing morris traversal
                    curr = currRight;
                }
            }
        }
    }
}
