package byteb.trees;

public class HeightOfBinaryTree {

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public int maxDepth(TreeNode root) {
        return heightOfBinaryTree(root);
    }

    private int heightOfBinaryTree(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return Math.max(heightOfBinaryTree(node.left) + 1, heightOfBinaryTree(node.right) + 1);
    }

}
