package byteb.trees;

public class VerifyPreOrderInBinarySearchTree {
    private int[] preorder;


    public boolean verifyPreorder(int[] preorder) {
        this.preorder = preorder;

        return recursive(Integer.MAX_VALUE, Integer.MIN_VALUE, 0, preorder.length - 1);

    }

    private boolean recursive(int upperLimit, int lowerLimit, int start, int end) {

        if (start == end) {
            return preorder[start] > lowerLimit && preorder[start] < upperLimit;
        }
        int root = preorder[start];

        if (root > upperLimit || root < lowerLimit) return false;

        int leftStart = -1, leftEnd = -1, rightStart = -1, rightEnd = -1;
        leftStart = start + 1;
        for (int i = start + 1; i <= end; i++) {
            if (preorder[i] < root) {
                leftEnd = i;
            } else {
                rightStart = i;
                rightEnd = end;
                break;
            }
        }

        boolean left = true;
        boolean right = true;
        if (leftEnd != -1) {
            left = recursive(root, lowerLimit, leftStart, leftEnd);
        }
        if (rightEnd != -1) {
            right = recursive(upperLimit, root, rightStart, rightEnd);
        }


        return left && right;

    }

    public static void main(String[] args) {
        int[] nums = new int[8000];
        int c = 0;
        for (int i = 8000; i > 0; i--) nums[c++] = i;
        var obj = new VerifyPreOrderInBinarySearchTree();

        var nums2 = new int[]{5, 2, 6, 1, 3};
        System.out.println(obj.verifyPreorder(nums2));
    }
}
