package byteb.trees;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.TreeMap;

public class BottomViewBinaryTree {

    public static void main(String[] args) {

        BinaryTreeNode root = createABtFromArray(new int[]{20, 8, 22, 5, 3, -1, 25, -1, -1, 10, 14, -1, -1, -1, -1, -1, -1});
        System.out.println(bottomView(root));

    }


    static class BinaryTreeNode {
        int val;
        BinaryTreeNode left;
        BinaryTreeNode right;

        BinaryTreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }
    }


    static class Pair<L, R> {

        Pair(L left, R right) {
            this.node = left;
            this.delta = right;
        }

        public L node;
        public R delta;
    }

    public static ArrayList<Integer> bottomView(BinaryTreeNode root) {
        if (root == null) {
            return new ArrayList<>();
        }
        TreeMap<Integer, Integer> bottomView = new TreeMap<>();
        Queue<Pair<BinaryTreeNode, Integer>> queue = new ArrayDeque<>();
        queue.add(new Pair<>(root, 0));


        if (root.left != null) {
            queue.add(new Pair<>(root.left, 1));
        }

        if (root.right != null) {
            queue.add(new Pair<>(root.right, -1));
        }


        Pair<BinaryTreeNode, Integer> bucket;
        while (!queue.isEmpty()) {
            bucket = queue.remove();


            bottomView.put(bucket.delta, bucket.node.val);


            if (bucket.node.left != null) {
                queue.add(new Pair<>(bucket.node.left, bucket.delta + 1));
            }

            if (bucket.node.right != null) {
                queue.add(new Pair<>(bucket.node.right, bucket.delta - 1));
            }

        }
        return new ArrayList<>(bottomView.descendingMap().values());

    }

    private static BinaryTreeNode createABtFromArray(int[] elements) {
        int currIndex = 0;
        BinaryTreeNode root = new BinaryTreeNode(elements[currIndex++]);
        Queue<BinaryTreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            BinaryTreeNode bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new BinaryTreeNode(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new BinaryTreeNode(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }
}
