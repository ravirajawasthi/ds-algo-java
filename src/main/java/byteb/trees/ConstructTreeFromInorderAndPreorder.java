package byteb.trees;

import java.util.*;
import java.util.stream.IntStream;

public class ConstructTreeFromInorderAndPreorder {
    public static void main(String[] args) {
        int[] inorderArray = new int[]{1, 2, 4, 7, 3};
        int[] preorderArray = new int[]{4, 2, 7, 1, 3};

        ArrayList<Integer> inorder = new ArrayList<>();
        ArrayList<Integer> preorder = new ArrayList<>();
        Arrays.stream(inorderArray).forEach(inorder::add);
        Arrays.stream(preorderArray).forEach(preorder::add);

//        TreeNode<Integer> root = buildBinaryTree(preorder, inorder);
        TreeNode<Integer> root = buildBinaryTreeIterative(preorder, inorder);
        System.out.println("root = " + root);

    }

    private static class TreeNode<T> {

        T data;
        TreeNode<T> left;
        TreeNode<T> right;

        TreeNode(T data) {
            this.data = data;
            left = null;
            right = null;
        }
    }

    private static class StackElement {
        public int prs;
        public int pre;
        public int ios;
        public int ioe;
        public boolean left;
        public TreeNode<Integer> previous;

        public StackElement(int prs, int pre, int ios, int ioe, boolean left, TreeNode<Integer> previous) {
            this.prs = prs;
            this.pre = pre;
            this.ios = ios;
            this.ioe = ioe;
            this.left = left;
            this.previous = previous;
        }
    }


    public static TreeNode<Integer> buildBinaryTree(ArrayList<Integer> inorder, ArrayList<Integer> preorder) {
        HashMap<Integer, Integer> inorderMap = new HashMap<>();
        for (int i = 0; i < inorder.size(); i++) {
            inorderMap.put(inorder.get(i), i);
        }
        return build(inorder, preorder, inorderMap, 0, preorder.size() - 1, 0, inorder.size() - 1);
    }

    private static TreeNode<Integer> build(ArrayList<Integer> inorder, ArrayList<Integer> preorder, HashMap<Integer, Integer> inorderMap, int prs, int pre, int ios, int ioe) {
        if (prs == pre) {
            return new TreeNode<>(preorder.get(prs));
        }
        if (prs > pre) {
            return null;
        }
        int rootIndex = inorderMap.get(preorder.get(prs));
        TreeNode<Integer> root = new TreeNode<>(preorder.get(prs));

        root.left = build(inorder, preorder, inorderMap, prs + 1, prs + rootIndex - ios, ios, rootIndex - 1);
        root.right = build(inorder, preorder, inorderMap, prs + rootIndex - ios + 1, pre, rootIndex + 1, ioe);

        return root;
    }

    public static TreeNode<Integer> buildBinaryTreeIterative(ArrayList<Integer> inorder, ArrayList<Integer> preorder) {

        HashMap<Integer, Integer> inorderMap = new HashMap<>();
        IntStream.range(0, inorder.size() - 1).forEach(i -> inorderMap.put(inorder.get(i), i));

        Deque<StackElement> stack = new LinkedList<>();

        int rt = inorderMap.get(preorder.get(0));
        TreeNode<Integer> root = new TreeNode<>(preorder.get(0));
        stack.push(new StackElement(1, rt, 0, rt - 1, true, root));
        stack.push(new StackElement(rt + 1, preorder.size() - 1, rt + 1, inorder.size() - 1, false, root));

        while (!stack.isEmpty()) {
            StackElement stackElement = stack.pop();

            if (stackElement.prs == stackElement.pre) {
                if (stackElement.left) {
                    stackElement.previous.left = new TreeNode<>(preorder.get(stackElement.prs));
                } else {
                    stackElement.previous.right = new TreeNode<>(preorder.get(stackElement.prs));
                }
                continue;
            }

            if (stackElement.prs > stackElement.pre) {
                continue;
            }

            TreeNode<Integer> newNode = new TreeNode<>(preorder.get(stackElement.prs));
            int rootIndex = inorderMap.get(preorder.get(stackElement.prs));

            if (stackElement.left) {
                stackElement.previous.left = newNode;
            } else {
                stackElement.previous.right = newNode;
            }

            stack.push(new StackElement(stackElement.prs + 1, stackElement.prs + rootIndex - stackElement.ios, stackElement.ios, rootIndex - 1, true, newNode));
            stack.push(new StackElement(stackElement.prs + rootIndex - stackElement.ios + 1, stackElement.pre, rootIndex + 1, stackElement.ioe, false, newNode));

        }
        return root;
    }

}
