package byteb.trees;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.TreeMap;
import java.util.stream.Collectors;

class TopViewBinaryTree {

    public static void main(String[] args) {

        BinaryTreeNode root = createABtFromArray(new int[]{2, 7, 5, 2, 6, -1, 9, -1, -1, 5, 11, 4, -1, -1, -1, -1, -1, -1, -1});
        System.out.println(getTopView(root));

    }

    static class BinaryTreeNode {
        int val;
        BinaryTreeNode left;
        BinaryTreeNode right;

        BinaryTreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }
    }

    static class Pair<L, R> {

        Pair(L left, R right) {
            this.node = left;
            this.delta = right;
        }

        public L node;
        public R delta;
    }

    public static ArrayList<Integer> getTopView(BinaryTreeNode root) {
        TreeMap<Integer, BinaryTreeNode> topView = new TreeMap<>();
        Queue<Pair<BinaryTreeNode, Integer>> queue = new ArrayDeque<>();
        queue.add(new Pair<>(root, 0));

        Pair<BinaryTreeNode, Integer> bucket;
        while (!queue.isEmpty()) {
            bucket = queue.remove();

            if (!topView.containsKey(bucket.delta)) {
                topView.put(bucket.delta, bucket.node);
            }

            if (bucket.node.right != null) {
                queue.add(new Pair<>(bucket.node.right, bucket.delta - 1));
            }

            if (bucket.node.left != null) {
                queue.add(new Pair<>(bucket.node.left, bucket.delta + 1));
            }

        }
        return topView.descendingMap().values().stream().map(node -> node.val).collect(Collectors
                .toCollection(ArrayList::new));

    }

    private static BinaryTreeNode createABtFromArray(int[] elements) {
        int currIndex = 0;
        BinaryTreeNode root = new BinaryTreeNode(elements[currIndex++]);
        Queue<BinaryTreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            BinaryTreeNode bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new BinaryTreeNode(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new BinaryTreeNode(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }


}
