package byteb.trees;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.stream.IntStream;

public class ConstructBinaryTreeFromInorderAndPostOrder {
    public static void main(String[] args) {
        int[] inorder = new int[]{9, 3, 15, 20, 7};
        int[] postorder = new int[]{9, 15, 7, 20, 3};
//        TreeNode<Integer> root = getTreeFromPostorderAndInorder(postorder, inorder);
        TreeNode<Integer> root = getTreeFromPostorderAndInorderIterative(postorder, inorder);
        System.out.println("root = " + root);
    }

    static class TreeNode<T> {

        T data;
        TreeNode<T> left;
        TreeNode<T> right;

        TreeNode(T data) {
            this.data = data;
            left = null;
            right = null;
        }
    }

    private static class StackElement {
        public int pos;
        public int poe;
        public int ios;
        public int ioe;
        public boolean left;
        public TreeNode<Integer> previous;

        public StackElement(int pos, int poe, int ios, int ioe, boolean left, TreeNode<Integer> previous) {
            this.pos = pos;
            this.poe = poe;
            this.ios = ios;
            this.ioe = ioe;
            this.left = left;
            this.previous = previous;
        }
    }


    public static TreeNode<Integer> getTreeFromPostorderAndInorder(int[] postOrder, int[] inOrder) {
        HashMap<Integer, Integer> inorderMap = new HashMap<>();
        IntStream.range(0, inOrder.length).forEach(i -> inorderMap.put(inOrder[i], i));

        return buildTree(postOrder, inOrder, inorderMap, 0, postOrder.length - 1, 0, inOrder.length - 1);

    }

    private static TreeNode<Integer> buildTree(int[] postorder, int[] inorder, HashMap<Integer, Integer> inorderMap, int pos, int poe, int ios, int ioe) {
        if (pos > poe) {
            return null;
        }
        if (pos == poe) {
            return new TreeNode<>(postorder[pos]);
        }

        int rootIndex = inorderMap.get(postorder[poe]);
        TreeNode<Integer> root = new TreeNode<>(postorder[poe]);

        root.left = buildTree(postorder, inorder, inorderMap, pos, pos + rootIndex - ios - 1, ios, rootIndex - 1);
        root.right = buildTree(postorder, inorder, inorderMap, pos + rootIndex - ios, poe - 1, rootIndex + 1, ioe - 1);

        return root;

    }

    private static TreeNode<Integer> getTreeFromPostorderAndInorderIterative(int[] postOrder, int[] inOrder) {

        HashMap<Integer, Integer> inorderMap = new HashMap<>();
        IntStream.range(0, inOrder.length).forEach(i -> inorderMap.put(inOrder[i], i));

        Deque<StackElement> stack = new LinkedList<>();

        TreeNode<Integer> root = new TreeNode<>(postOrder[postOrder.length - 1]);

        int rt = inorderMap.get(root.data);

        stack.push(new StackElement(0, rt - 1, 0, rt - 1, true, root));
        stack.push(new StackElement(rt, postOrder.length - 2, rt + 1, inOrder.length - 1, false, root));

        while (!stack.isEmpty()) {
            StackElement stackElement = stack.pop();
            if (stackElement.pos > stackElement.poe) {
                continue;
            }
            if (stackElement.pos == stackElement.poe) {
                if (stackElement.left) {
                    stackElement.previous.left = new TreeNode<>(postOrder[stackElement.poe]);
                } else {
                    stackElement.previous.right = new TreeNode<>(postOrder[stackElement.poe]);
                }
                continue;
            }


            TreeNode<Integer> newNode = new TreeNode<>(postOrder[stackElement.poe]);

            if (stackElement.left) {
                stackElement.previous.left = newNode;
            } else {
                stackElement.previous.right = newNode;
            }

            int rootIndex = inorderMap.get(postOrder[stackElement.poe]);

            stack.push(new StackElement(stackElement.pos, stackElement.pos + rootIndex - stackElement.ios - 1, stackElement.ios, rootIndex - 1, true, newNode));
            stack.push(new StackElement(stackElement.pos + rootIndex - stackElement.ios, stackElement.poe - 1, rootIndex + 1, stackElement.ioe, false, newNode));

        }


        return root;

    }


}
