package byteb.trees;

import java.util.ArrayDeque;
import java.util.Queue;

public class DiameterOfBinaryTree {


    public static void main(String[] args) {
        int[] tree = new int[]{15, 2, -1, 2, 8, 4, 14, -1, 2, 5, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        TreeNode<Integer> root = createABtFromArray(tree);
        System.out.println("diameterOfBinaryTree(root) = " + diameterOfBinaryTree(root));
    }


    static class TreeNode<T> {
        public T data;
        public TreeNode<T> left;
        public TreeNode<T> right;

        TreeNode(T data) {
            this.data = data;
            left = null;
            right = null;
        }
    }

    static int ans;

    public static int diameterOfBinaryTree(TreeNode<Integer> root) {
        if (root == null) {
            return -1;
        }
        ans = 0;
        maxHeight(root);
        return ans - 1;
    }

    private static int maxHeight(TreeNode<Integer> node) {
        if (node == null) {
            return 0;
        }
        int leftSubTree = maxHeight(node.left);
        int rightSubTree = maxHeight(node.right);

        ans = Math.max(leftSubTree + rightSubTree + 1, ans);

        return 1 + Math.max(leftSubTree, rightSubTree);

    }

    private static TreeNode<Integer> createABtFromArray(int[] elements) {
        int currIndex = 0;
        TreeNode<Integer> root = new TreeNode<>(elements[currIndex++]);
        Queue<TreeNode<Integer>> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            TreeNode<Integer> bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new TreeNode<>(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new TreeNode<>(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }

}
