package byteb.trees;

import byteb.trees.model.BinaryTreeNode;
import byteb.utils.Utils;

import static byteb.utils.Utils.createABtFromArrayBinaryTreeNode;

public class HeightBalancedBinaryTree {
    public static void main(String[] args) {
        int[] tree = Utils.createArrayFromString("1 2 3 4 -1 -1 -1 -1 5 -1 -1 ");
        BinaryTreeNode<Integer> root = createABtFromArrayBinaryTreeNode(tree);
        System.out.println("isBalancedBT(root) = " + isBalancedBT(root));

    }



    static private class Pair {
        public int height;
        public boolean isBalanced;

        public Pair(int height, boolean isBalanced) {
            this.height = height;
            this.isBalanced = isBalanced;
        }
    }

    public static boolean isBalancedBT(BinaryTreeNode<Integer> root) {
        return isBalanced(root).isBalanced;
    }

    private static Pair isBalanced(BinaryTreeNode<Integer> root) {
        Pair left;
        Pair right;
        if (root.left == null) {
            left = new Pair(0, true);
        } else {
            left = isBalanced(root.left);
        }

        if (root.right == null) {
            right = new Pair(0, true);
        } else {
            right = isBalanced(root.right);
        }


        if (!left.isBalanced || !right.isBalanced) {
            return new Pair(0, false);
        }

        if (Math.abs(left.height - right.height) > 1) {
            return new Pair(0, false);
        } else {
            return new Pair(Math.max(left.height, right.height) + 1, true);
        }

    }



}
