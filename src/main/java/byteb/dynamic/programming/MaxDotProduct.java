package byteb.dynamic.programming;

import byteb.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class MaxDotProduct {
    public static void main(String[] args) {
//        System.out.println(maxDotProduct(new int[]{2, 1, -2, 5}, new int[]{3, 0, -6}));
//        System.out.println(maxDotProduct(new int[]{3, -2}, new int[]{2, -6, 7}));
//        System.out.println(maxDotProduct(new int[]{1, 1}, new int[]{-1, -1, -1}));
//        System.out.println(maxDotProduct(new int[]{-5, -1, -2}, new int[]{3, 3, 5, 5}));
        System.out.println(maxDotProduct(new int[]{-3, -8, 3, -10, 1, 3, 9}, new int[]{9, 2, 3, 7, -9, 1, -8, 5, -1, -1}));

//        System.out.println(maxDotProduct(new int[]{-3, 9,-1, 9}, new int[]{7, 2, 3, 9}));

    }

    public static int maxDotProduct(int[] nums1, int[] nums2) {
        int rows = nums2.length;
        int cols = nums1.length;

//        var num1List = new ArrayList<>(Arrays.stream(nums1).boxed().toList());
//        var num2List = new ArrayList<>(Arrays.stream(nums2).boxed().toList());
//
//        Collections.reverse(num1List);
//        Collections.reverse(num2List);
//
//        nums1 = num1List.stream().mapToInt(i -> i).toArray();
//        nums2 = num2List.stream().mapToInt(i -> i).toArray();

//        num2 -> rows
//        num1 -> cols
        int[][] dp = new int[rows][cols];

        dp[0][0] = nums1[0] * nums2[0];

//

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int pick = (nums2[i] * nums1[j]);
                int notPick;
                if (i > 0 && j > 0) {
                    pick += dp[i - 1][j - 1];
                    notPick = Math.max(dp[i][j - 1], dp[i - 1][j]);
                }else{
                    notPick = Math.min(0, pick);
                }

                dp[i][j] = Math.max(pick, notPick);

            }
        }

        return dp[rows - 1][cols - 1];
    }

}