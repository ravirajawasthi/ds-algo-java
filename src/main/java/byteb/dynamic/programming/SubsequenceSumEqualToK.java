package byteb.dynamic.programming;

import java.util.Arrays;

public class SubsequenceSumEqualToK {


    public static boolean subsetSumToK(int n, int k, int[] arr) {
        Boolean[][] dp = new Boolean[n + 1][k + 1];
        for (Boolean[] row : dp) {
            Arrays.fill(row, null);
        }
//        return subsetSumKRecursively(k, arr.length - 1, arr, dp);
        return subsetSumKDP(k, arr);
    }

    private static boolean subsetSumKRecursively(int k, int index, int[] arr, Boolean[][] dp) {
        if (index < 0) {
            return false;
        } else if (dp[index][k] != null) {
            return dp[index][k];
        } else if (arr[index] > k) {
            boolean res = subsetSumKRecursively(k, index - 1, arr, dp);
            dp[index][k] = res;
            return res;
        } else if (arr[index] == k) {
            return true;
        } else {
            boolean res = subsetSumKRecursively(k - arr[index], index - 1, arr, dp) || subsetSumKRecursively(k, index - 1, arr, dp);
            dp[index][k] = res;
            return res;
        }
    }

    private static boolean subsetSumKDP(int k, int[] arr) {
        int n = arr.length;
        boolean[][] dp = new boolean[k + 1][n];

        Arrays.fill(dp[0], true);

        for (int j = 1; j <= k; j++) {
            for (int index = 0; index < n; index++) {
                if (arr[index] > j) {
                    if (index == 0)
                        dp[j][index] = false;
                    else
                        dp[j][index] = dp[j][index - 1];
                } else if (arr[index] == j) {
                    dp[j][index] = true;
                } else {
                    if (index == 0) {
                        dp[j][index] = false;
                    } else
                        dp[j][index] = dp[j - arr[index]][index - 1] || dp[j][index - 1];
                }
            }
        }
        return dp[k][n - 1];
    }

}

