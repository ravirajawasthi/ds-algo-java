package byteb.dynamic.programming;

public class MinimumFallingPathSum {

    public static int minFallingPathSum(int[][] matrix) {
        int n = matrix.length;
        if (n == 1) {
            return matrix[0][0];
        }

        int[] prev = matrix[0];
        int[] curr = new int[prev.length];
        for (int j = 1; j < n - 1; j++) {
            int[] row = matrix[j];
            for (int i = 0; i < n; i++) {
                int prevIndex = i == 0 ? Integer.MAX_VALUE : prev[i - 1];
                int sameIndex = prev[i];
                int nextIndex = i == n - 1 ? Integer.MAX_VALUE : prev[i + 1];
                curr[i] = Math.min(nextIndex, Math.min(prevIndex, sameIndex)) + row[i];
            }
            prev = curr;
//            curr = new int[n];
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            int prevIndex = i == 0 ? Integer.MAX_VALUE : prev[i - 1];
            int sameIndex = prev[i];
            int nextIndex = i == n - 1 ? Integer.MAX_VALUE : prev[i + 1];
            min = Math.min(min, Math.min(nextIndex, Math.min(prevIndex, sameIndex)) + matrix[n - 1][i]);
        }
        return min;
    }

}
