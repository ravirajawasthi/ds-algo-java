package byteb.dynamic.programming;

public class CountSubSetWithSumK {

    public static int findWays(int num[], int tar) {
        return countSubsetWithSumK(num, tar);
    }

    private static int countSubsetWithSumK(int[] nums, int target) {
        boolean[][] dp = new boolean[nums.length][target + 1];
        if (nums[0] <= target)
            dp[0][nums[0]] = true;
        dp[0][0] = true;
        for (int j = 1; j < nums.length; j++) {
            int element = nums[j];
            for (int i = 1; i <= target; i++) {
                if (element > i) {
                    dp[j][i] = dp[j - 1][i];
                } else if (element == i) {
                    dp[j][i] = true;
                } else {
                    dp[j][i] = dp[j - 1][i] || dp[j - 1][i - element];
                }
            }
        }
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (dp[i][target]) count++;
        }
        return count;
    }
}
