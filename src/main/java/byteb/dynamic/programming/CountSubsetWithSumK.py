from typing import List


def findWays(arr: List[int], k: int) -> int:
    # dp = [[-1] * len(arr) for _ in range(0, k + 1)]
    # return findWaysRecursion(arr, k, 0, dp)
    return findWaysDP(arr, k)


def findWaysRecursion(
    arr: List[int], curr_sum: int, index: int, dp: List[List[int]]
) -> int:
    if dp[curr_sum][index] != -1:
        return dp[curr_sum][index]

    if curr_sum == 0:
        return 1

    if index == len(arr) - 1:
        if curr_sum == arr[index]:
            return 1
        else:
            return 0

    consider = 0
    if arr[index] <= curr_sum:
        consider = findWaysRecursion(arr, curr_sum - arr[index], index + 1, dp)
    not_consider = findWaysRecursion(arr, curr_sum, index + 1, dp)

    dp[curr_sum][index] = (consider + not_consider) % 1000000007

    return dp[curr_sum][index]


def findWaysDP(arr: List[int], k: int) -> int:
    prev = [0] * (k + 1)
    curr = [0] * (k + 1)
    prev[0] = curr[0] = 1

    for i in range(0, len(arr)):
        for target in range(1, k + 1):
            if arr[i] > target:
                curr[target] = prev[target]
            else:
                curr[target] = (prev[target] + prev[target - arr[i]]) % 1000000007
        prev = curr
        curr = [0] * (k + 1)
        curr[0] = 1
    return prev[k]


print(findWays([1, 4, 4, 5], 5))
