package byteb.dynamic.programming;


//https://www.codingninjas.com/studio/problems/house-robber-ii_839733?utm_source=striver&utm_medium=website&utm_campaign=a_zcoursetuf

public class HouseRobberII {

    public static long houseRobber(int[] valueInHouse) {

        if (valueInHouse.length == 1) {
            return valueInHouse[0];
        } else if (valueInHouse.length == 2) {
            return Math.max(valueInHouse[0], valueInHouse[1]);
        }

        //First house is robbed
        long p2 = valueInHouse[0];
        long p1 = Math.max(valueInHouse[0], valueInHouse[1]);
        long curr = p1;
        for (int i = 2; i < valueInHouse.length - 1; i++) {
            curr = Math.max(p1, p2 + valueInHouse[i]);
            p2 = p1;
            p1 = curr;
        }

        long firstHouseIsIncluded = curr;


        p2 = valueInHouse[1];
        p1 = Math.max(valueInHouse[2], valueInHouse[1]);
        curr = p1;
        for (int i = 3; i < valueInHouse.length; i++) {
            curr = Math.max(p1, p2 + valueInHouse[i]);
            p2 = p1;
            p1 = curr;
        }

        long firstHouseIsRemoved = curr;

        return Math.max(firstHouseIsRemoved, firstHouseIsIncluded);
    }

    public static int rob(int[] valueInHouse) {

        if (valueInHouse.length == 1) {
            return valueInHouse[0];
        } else if (valueInHouse.length == 2) {
            return Math.max(valueInHouse[0], valueInHouse[1]);
        }

        //First house is robbed
        int p2 = valueInHouse[0];
        int p1 = Math.max(valueInHouse[0], valueInHouse[1]);
        int curr = p1;
        for (int i = 2; i < valueInHouse.length - 1; i++) {
            curr = Math.max(p1, p2 + valueInHouse[i]);
            p2 = p1;
            p1 = curr;
        }

        int firstHouseIsIncluded = curr;


        p2 = valueInHouse[1];
        p1 = Math.max(valueInHouse[2], valueInHouse[1]);
        curr = p1;
        for (int i = 3; i < valueInHouse.length; i++) {
            curr = Math.max(p1, p2 + valueInHouse[i]);
            p2 = p1;
            p1 = curr;
        }

        int firstHouseIsRemoved = curr;

        return Math.max(firstHouseIsRemoved, firstHouseIsIncluded);
    }


}
