package byteb.dynamic.programming;

public class DecodeWays {

    private static boolean valid = true;

    public static int numDecodings(String s) {
//        valid = true;
//        int res = numDecodingsRecursive(s, 0);
//        if (!valid) {
//            return 0;
//        }
//        return res;

//        return decodeWaysDP(s);
        return decodeWaysDPOptimised(s);

    }

    private static int numDecodingsRecursive(String s, int index) {
        if (index >= s.length()) {
            return 0;
        }
        if (index == s.length() - 1) {
            return 1;
        }

        if (s.charAt(index) == '0') {
            valid = false;
            return 0;
        }

        int num = Integer.parseInt(s.substring(index, index + 2));

        if (s.charAt(index + 1) == '0') {
            if (num > 20) {
                valid = false;
                return 0;
            } else {
                return numDecodingsRecursive(s, index + 2);
            }
        } else if (num < 27) {
            return 1 + numDecodingsRecursive(s, index + 1) + numDecodingsRecursive(s, index + 2);
        } else {
            return numDecodingsRecursive(s, index + 1);
        }


    }

    private static int decodeWaysDP(String s) {
        int[] dp = new int[s.length()];
        if (s.charAt(0) == '0') {
            return 0;
        }
        dp[0] = 1;

        for (int i = 1; i < s.length(); i++) {
            String currChar = s.substring(i, i + 1);
            String prevChar = s.substring(i - 1, i);

            if (currChar.equals("0")) {
                if (prevChar.equals("1") || prevChar.equals("2")) {
                    if (i - 2 > -1)
                        dp[i] = dp[i - 2];
                    else {
                        dp[i] = 1;
                    }
                } else {
                    dp[i] = 0;
                    return 0;
                }
            } else if (prevChar.equals("0")) {
                dp[i] = dp[i - 1];
            } else {
                int num = Integer.parseInt(prevChar + currChar);
                dp[i] = dp[i - 1];
                if (num <= 26) {
                    if (i - 2 > -1) {
                        dp[i] += dp[i - 2];
                    } else {
                        dp[i] += 1;
                    }
                }
            }

        }

        return dp[dp.length - 1];
    }

    private static int decodeWaysDPOptimised(String s) {
        if (s.charAt(0) == '0') {
            return 0;
        }
        int p2 = 1;
        int p1 = s.charAt(0) == '0' ? 0 : 1;
        int curr = p1;

        for (int i = 2; i <= s.length(); i++) {
            char currChar = s.charAt(i - 1);
            char prevChar = s.charAt(i - 2);

            if (currChar == '0') {
                if (prevChar > '2' || prevChar == '0') {
                    return 0;
                } else {
                    curr = p2;
                }
            } else if (prevChar == '0') {
                curr = p1;
            } else {
                if (prevChar < '2' || prevChar < '3' && currChar < '7') {
                    curr = p1 + p2;
                } else {
                    curr = p1;
                }
            }
            p2 = p1;
            p1 = curr;
        }

        return curr;
    }


}
