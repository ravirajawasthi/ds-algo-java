package byteb.dynamic.programming;

public class DominoTrimino {

    private final int MODULO = 1000000007;

    private int n;

    private Integer[][][] dp;

    public int numTilings(int n) {
        return numTilingsRecursive(n);
    }

    private int numTilingsRecursive(int n) {
        this.n = n;
        this.dp = new Integer[n+1][2][2];
        return numTilingsHelper(0, true, false);
    }

    private int numTilingsHelper(int n, boolean square, boolean topEmpty) {
        /*
        n -> index
        prev -> 0 : domino, 1 : trimino
        square -> i have a perfect square or not
        topEmpty -> square == false, then only this is useful, topEmpty or bottom empty if not perfect square
         */
        if (n == this.n && square)
            return 1;
        if (n >= this.n) return 0;

        if (dp[n][square ? 0 : 1][topEmpty ? 0 : 1] != null) return dp[n][square ? 0 : 1][topEmpty ? 0 : 1];

        if (square) {
            return dp[n][0][topEmpty ? 0 : 1] =
                    (numTilingsHelper(n + 1, true, false) + // Placed a vertical domino to maintain square
                            numTilingsHelper(n + 1, false, true) + // Placed a L shaped trimino
                            numTilingsHelper(n + 1, false, false) + // Placed a |` shaped trimono
                            numTilingsHelper(n + 2, true, false))  //Placed two horizontal domino =
                            % MODULO;
        } else {
            if (topEmpty) {
                return dp[n][1][0] =
                        (numTilingsHelper(n + 1, false, false) + //Placed a horizontal domino
                                numTilingsHelper(n + 2, true, false)) //Placed symmetrical trimono `|
                                % MODULO;
            } else {

                return dp[n][1][1] =
                        (numTilingsHelper(n + 2, true, true) + //placed a symmetrical _| trimono
                                numTilingsHelper(n + 1, false, true) //placed a horizontal domino
                        ) % MODULO;
            }
        }
    }

}
