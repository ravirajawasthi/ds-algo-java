package byteb.dynamic.programming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LargestDivisibleSubset {

    private int[] nums;
    int globalBestAns = 0;
    List<Integer> ans;
    HashMap<String, List<Integer>> dp = new HashMap<>();

    public List<Integer> largestDivisibleSubset(int[] nums) {
        Arrays.parallelSort(nums);

        //Recursive
//        this.nums = nums;
//        recursive(1, 0);
//        return ans;



        //Bottom Up
        return bottomUp(nums);
    }

    private List<Integer> recursive(int prev, int i) {
        if (i >= nums.length)
            return new ArrayList<>();
        // Can i include number at i
        String key = prev + "#" + i;
        if (dp.containsKey(key))
            return dp.get(key);
        List<Integer> nonInclude;
        List<Integer> include = null;

        if (nums[i] % prev == 0) {
            include = new ArrayList<>(recursive(nums[i], i + 1));
            include.addFirst(nums[i]);
        }
        nonInclude = new ArrayList<>(recursive(prev, i + 1));

        var localAns = include != null && include.size() > nonInclude.size() ? include : nonInclude;
        if (localAns.size() > globalBestAns) {
            globalBestAns = localAns.size();
            var a = new ArrayList<>(localAns);
            ans = a;
            dp.put(key, a);
        } else {
            dp.put(key, new ArrayList<>(localAns));
        }
        return localAns;
    }

    //Better bottom solution

    private List bottomUp(int[] nums){
        int globalBestAnsIndex = -1;
        int globalBestLength = 0;

        List[] dp = new List[nums.length];
        for (int i = 0; i < nums.length; i++) dp[i] = List.of(nums[i]);


        for (int i = nums.length - 1; i > -1; i--){
            int prev = nums[i];
            int bestSeenIndex = -1;
            int bestSeenLength = 0;
            for (int j = i+1; j < nums.length; j++){
                if (nums[j] % prev == 0){
                    if (dp[j].size() + 1 > bestSeenLength){
                        bestSeenLength = dp[j].size() + 1;
                        bestSeenIndex = j;
                    }
                }
            }
            var dpArray = bestSeenIndex == -1 ? new ArrayList<>() : new ArrayList(dp[bestSeenIndex]);
            dpArray.add(nums[i]);
            if (dpArray.size() > globalBestLength){
                globalBestLength = dpArray.size();
                globalBestAnsIndex = i;
            }
            dp[i] = dpArray;
        }
        return dp[globalBestAnsIndex].reversed();



    }


}
