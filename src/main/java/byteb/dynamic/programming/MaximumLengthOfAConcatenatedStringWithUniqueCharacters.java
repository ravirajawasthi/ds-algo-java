package byteb.dynamic.programming;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

class MaximumLengthOfAConcatenatedStringWithUniqueCharacters {

    private HashMap<CacheKey, Integer> memo;

    private int ans = 0;
    private List<String> list;

    public int maxLength(List<String> arr) {
        //Iterative
//        return maxLengthIterative(arr);

        //Recursive
        return maxLengthRecursive(arr);
    }

    private int maxLengthIterative(List<String> arr) {
        //Wrong
        HashSet[] dp = new HashSet[arr.size() + 1];
        dp[0] = new HashSet<Character>();
        int ans = 0;
        for (int i = 1; i <= arr.size(); i++) {
            boolean canThisBeUsedWithPrevious = true;
            for (char c : arr.get(i - 1).toCharArray()) {
                if (dp[i - 1].contains(c)) {
                    canThisBeUsedWithPrevious = false;
                    break;
                }

            }
            if (canThisBeUsedWithPrevious) {
                var prevSet = dp[i - 1];
                for (char c : arr.get(i - 1).toCharArray()) prevSet.add(c);
                dp[i] = dp[i - 1];
            } else {
                if (dp[i - 1].size() > arr.get(i - 1).length()) {
                    dp[i] = dp[i - 1];
                } else {
                    dp[i] = new HashSet();
                    for (char c : arr.get(i - 1).toCharArray()) dp[i].add(c);
                }
            }
            ans = Math.max(ans, dp[i].size());

        }
        return ans;
    }

    private int maxLengthRecursive(List<String> arr) {
        this.list = arr;
        this.ans = 0;
        this.memo = new HashMap<>();
        return maxLengthHelper(0, new int[26]);
    }

    private int maxLengthHelper(int index, int[] prevMap) {
        if (index == list.size()) {
            int size = 0;
            for (int c : prevMap) size += c;
            return size;
        }
        var key = new CacheKey(index, prevMap);
        if (memo.containsKey(key)) return memo.get(key);

        var currMap = new int[26];
        for (char c: list.get(index).toCharArray()){
            if (currMap[c - 'a'] == 1){
                return Math.max(maxLengthHelper(index + 1, new int[26]), maxLengthHelper(index+1, prevMap));
            }
            currMap[c - 'a'] = 1;
        }

        boolean compatibleWithPreviousMap = true;
        for (char c : list.get(index).toCharArray()) {
            if (prevMap[c - 'a'] != 0) {
                compatibleWithPreviousMap = false;
                break;
            }
        }

        int forwardWithPreviousMap = 0;
        if (compatibleWithPreviousMap) {
            var newMap = Arrays.copyOf(prevMap, prevMap.length);
            for (char c : list.get(index).toCharArray()) {
                newMap[c - 'a'] = 1;
            }
            forwardWithPreviousMap = maxLengthHelper(index + 1, newMap);
        }


        var newMap = new int[26];
        for (char c : list.get(index).toCharArray()) {
            newMap[c - 'a'] = 1;
        }
        int forwardAsItIs = maxLengthHelper(index + 1, newMap);

        int forwardPrevMap = maxLengthHelper(index + 1, Arrays.copyOf(prevMap, prevMap.length));

        memo.put(key, Math.max(forwardWithPreviousMap, Math.max(forwardAsItIs, forwardPrevMap)));
        return memo.get(key);
    }

    private class CacheKey{
        private int key;
        private int[] map;
        public CacheKey(int index, int[] cacheKey){
            this.key = index;
            this.map = cacheKey;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CacheKey cacheKey = (CacheKey) o;

            if (key != cacheKey.key) return false;
            return Arrays.equals(map, cacheKey.map);
        }

        @Override
        public int hashCode() {
            int result = key;
            result = 31 * result + Arrays.hashCode(map);
            return result;
        }
    }

}