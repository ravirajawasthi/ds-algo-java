package byteb.dynamic.programming;

public class CoinChange2 {

    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;

        for (int coin : coins) {
            for (int j = 1; j <= amount; j++) {
                if (coin > j) continue;
                dp[j] += dp[j - coin];
            }
        }
        return dp[amount];
    }

}
