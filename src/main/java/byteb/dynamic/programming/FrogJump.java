package byteb.dynamic.programming;

import java.util.HashMap;

//https://www.codingninjas.com/studio/problems/frog-jump_3621012?utm_source=striver&utm_medium=website&utm_campaign=a_zcoursetuf
public class FrogJump {
    public static int frogJump(int n, int[] heights) {
//        HashMap<Integer, Integer> memoization = new HashMap<>();
//        return frogJumpHashMap(n - 1, heights, memoization);

//        return frogJumpRecursive(n, heights);

//        return frogJumpDynamicProgrammingWithFullArray(n, heights);
        return frogJumpDynamicProgrammingWithSpaceOptimised(n, heights);
    }


    private static int frogJumpRecursive(int n, int[] heights) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return Math.abs(heights[0] - heights[1]);
        }
        int prev = frogJumpRecursive(n - 1, heights) + Math.abs(heights[n - 1] - heights[n]);
        int prev2 = frogJumpRecursive(n - 2, heights) + Math.abs(heights[n - 2] - heights[n]);

        return Math.min(prev2, prev);
    }

    private static int frogJumpHashMap(int n, int[] heights, HashMap<Integer, Integer> map) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return Math.abs(heights[0] - heights[1]);
        }
        if (map.containsKey(n)) {
            return map.get(n);
        }

        int prev = frogJumpHashMap(n - 1, heights, map) + Math.abs(heights[n - 1] - heights[n]);
        int prev2 = frogJumpHashMap(n - 2, heights, map) + Math.abs(heights[n - 2] - heights[n]);

        int ans = Math.min(prev2, prev);
        map.put(n, ans);
        return ans;
    }

    private static int frogJumpDynamicProgrammingWithFullArray(int n, int[] heights) {
        int[] dpArray = new int[n];
        dpArray[0] = 0;
        dpArray[1] = Math.abs(heights[0] - heights[1]);

        for (int i = 2; i < n; i++) {
            int curr = heights[i];
            int p1 = heights[i - 1];
            int p2 = heights[i - 2];

            dpArray[i] = Math.min(dpArray[i - 1] + Math.abs(curr - p1), dpArray[i - 2] + Math.abs(curr - p2));

        }

        return dpArray[n - 1];
    }

    private static int frogJumpDynamicProgrammingWithSpaceOptimised(int n, int[] heights) {

        int dp2 = 0;
        int dp1 = Math.abs(heights[0] - heights[1]);
        int dpCurr = 0;

        for (int i = 2; i < n; i++) {
            int curr = heights[i];
            int p1 = heights[i - 1];
            int p2 = heights[i - 2];

            dpCurr = Math.min(dp1 + Math.abs(curr - p1), dp2 + Math.abs(curr - p2));
            dp2 = dp1;
            dp1 = dpCurr;

        }

        return dpCurr;
    }
}