package byteb.dynamic.programming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Vacations {

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int n = sc.nextInt();

            int[][] dp = new int[n + 1][3];
            // 0 -> gym
            // 1-> contest
            // 2-> rest
            int activity;

            for (int i = 1; i <= n; i++) {
                activity = sc.nextInt();

                switch (activity) {
                    case (0): {
                        // I can only rest today
                        dp[i][0] = dp[i][1] = dp[i][2] = 1
                                + Math.min(dp[i - 1][2], Math.min(dp[i - 1][0], dp[i - 1][1]));
                        break;
                    }
                    case (1): {
                        // I can only give contest today
                        dp[i][0] = Integer.MAX_VALUE;
                        dp[i][1] = Math.min(dp[i - 1][0], dp[i - 1][2]);
                        dp[i][2] = 1 + Math.min(dp[i - 1][0], dp[i - 1][1]);
                        break;

                    }
                    case (2): {
                        // I can only do gym today
                        dp[i][0] = Math.min(dp[i - 1][1], dp[i - 1][2]);
                        dp[i][1] = Integer.MAX_VALUE;
                        dp[i][2] = 1 + Math.min(dp[i - 1][0], dp[i - 1][1]);
                        break;
                    }
                    case (3): {
                        // I can do anything today
                        dp[i][0] = Math.min(dp[i - 1][2], dp[i - 1][1]);
                        dp[i][1] = Math.min(dp[i - 1][0], dp[i - 1][2]);
                        dp[i][2] = 1 + Math.min(dp[i - 1][0], Math.min(dp[i - 1][2], dp[i - 1][1]));
                        break;
                    }
                }

            }

            out.print(Math.min(dp[n][0], Math.min(dp[n][1], dp[n][2])));

            // Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
