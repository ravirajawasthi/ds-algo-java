package byteb.dynamic.programming;

public class BestTimeToBuyAndSellStock4 {

    private int[] prices;
    private int maxTransactions;

    Integer[][][] dpRecursive;

    public int maxProfit(int k, int[] prices) {
        //Recursive
        return maxProfitTopDown(k, prices);

        //Bottom Up
        //return maxProfitBottomUp(prices, k);

    }

    private int maxProfitTopDown(int k, int[] prices) {
        // Recursive
        this.prices = prices;
        this.maxTransactions = k;
        dpRecursive = new Integer[prices.length][maxTransactions + 1][2];

        return maxProfitRecursiveHelper(0, 0, Mode.BUY);

    }

    private int maxProfitRecursiveHelper(int stockIndex, int transactions, Mode mode) {

        if (stockIndex >= prices.length)
            return 0;
        if (transactions >= maxTransactions) {
                return 0;
        }

        if (dpRecursive[stockIndex][transactions][mode == Mode.BUY ? 0: 1] != null)
            return dpRecursive[stockIndex][transactions][mode == Mode.BUY ? 0: 1];

        if (mode == Mode.BUY) {
            // Either I buy the stock now
            int buy = maxProfitRecursiveHelper(stockIndex + 1, transactions, Mode.SELL) + -1 * prices[stockIndex];

            // Either I don't do anything today
            int dontDoAnything = maxProfitRecursiveHelper(stockIndex + 1, transactions, Mode.BUY);

            return dpRecursive[stockIndex][transactions][0] = Math.max(buy, dontDoAnything);
        } else {
            // I Sell the stock today
            int sellToday = prices[stockIndex]
                    + maxProfitRecursiveHelper(stockIndex + 1,
                            transactions + 1, Mode.BUY);

            // I keep holding the stock
            int keepHolding = maxProfitRecursiveHelper(stockIndex + 1, transactions, Mode.SELL);
            // Not memorizing the sell solution because based on buying price the sell for
            // same index will be different, so better to memoize once each transaction is
            // over
            return dpRecursive[stockIndex][transactions][1] = Math.max(sellToday, keepHolding);
        }

    }

    private int maxProfitBottomUp(int[] prices, int k) {
        //TODO need to figure out how to memoize with 3 variables
        int[][][] dp = new int[k + 1][prices.length + 1][2];

        // 0 -> Buy
        // 1 -> Sell
        for (int i = k-1; i > -1; i--){
            for (int j = prices.length - 2; j > -1; j--){
                //I buy this stock today?
                int buy = -1 * prices[j] + dp[i][j+1][0];

                //I don't buy anything today
                int notBuy = dp[i][j+1][0];

                dp[i][j][0] = Math.max(buy, notBuy);


                //I sell the stock today
                int sell = prices[j] + dp[i+1][j+1][1];

                //I don't sell anything today
                int notSell = dp[i][j+1][1];

                dp[i][j][1] = Math.max(sell, notSell);

            }
        }

        return dp[0][0][0];
    }


    private int maxProfitBottomUpLessMemoization(int[] prices, int k) {
        int[][] dp = new int[k + 1][prices.length + 1];

        for (int i = k-1; i > -1; i--){
            for (int j = prices.length - 2; j > -1; j--){

                //I buy this stock now?
                int buy = -1 * prices[j];
                for (int l = j+1; l < prices.length; l++){
                    //Don't want to check just the current profit of this transaction, but also the memorised condition
                    if (prices[l] - prices[j] + dp[i+1][l + 1] > buy){
                        buy = prices[l] - prices[j] + dp[i+1][l + 1];
                    }
                }
                int notBuy = dp[i][j+1];

                dp[i][j] = Math.max(buy, notBuy);

            }
        }

        return dp[0][0];
    }

    private enum Mode {
        BUY, SELL
    }
}
