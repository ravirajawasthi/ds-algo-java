package byteb.dynamic.programming;

import java.util.Arrays;

public class CoinChange {
    private int[] coins;
    private int[][] memo;

    public int coinChange(int[] coins, int amount) {
        if (amount == 0) return 0;
        //Top Down Recursive (Worst case takes 7.7 seconds with memoization)
        //return coinChangeRecursive(coins, amount);

        //Bottom Up (Bad)
        //int ans = coinChangeBottomUp(coins, amount);

        //Bottom Up (Even worse)
        //int ans = coinChangeBottomUp1D(coins, amount);

        //Bottom Up (text book)
        int ans = coinChangeBottomUpBest(coins, amount);
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    private int coinChangeBottomUpBest(int[] coins, int amount) {
        int[] dp = new int[amount + 1];
        for (int i = 1; i <= amount; i++) dp[i] = Integer.MAX_VALUE;

        for (int i = 1; i <= amount; i++) {
            for (int coin : coins) {
                if (coin <= i) {
                    /*
                    * We dont have to check how many coins we could potentially use
                    * Because we know we have figured best number of coins for all amounts less than i
                    */
                    int coinUse = dp[i - coin];
                    if (coinUse != Integer.MAX_VALUE) coinUse += 1;
                    dp[i] = Math.min(coinUse, dp[i]);
                }
            }
        }
        return dp[amount];
    }

    private int coinChangeBottomUp1D(int[] coins, int amount) {
        int[] dp = new int[amount + 1];
        for (int i = 1; i < dp.length; i++) dp[i] = Integer.MAX_VALUE;

        for (int i = 0; i < coins.length; i++) {
            int coinValue = coins[i];
            for (int a = 1; a <= amount; a++) {
                int potential = a / coinValue;
                for (int coin = 0; coin <= potential; coin++) {
                    int coinUse = dp[a - coinValue * coin] == Integer.MAX_VALUE ? Integer.MAX_VALUE : dp[a - coinValue * coin] + coin;
                    int coinNotUse = dp[a];
                    dp[a] = Math.min(coinNotUse, coinUse);
                }
            }
        }
        return dp[amount];

    }

    private int coinChangeRecursive(int[] coins, int amount) {
        this.coins = coins;
        this.memo = new int[coins.length][amount + 1];
        for (var arr : memo) Arrays.fill(arr, Integer.MAX_VALUE);
        int ans = coinChangeRecursiveHelper(0, amount);
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    private int coinChangeBottomUp(int[] coins, int amount) {
        int[][] dp = new int[coins.length][amount + 1];
        for (var arr : dp) for (int i = 1; i < arr.length; i++) arr[i] = Integer.MAX_VALUE;
        for (int i = amount; i > 0; i--) {
            dp[coins.length - 1][i] = i % coins[coins.length - 1] == 0 ? i / coins[coins.length - 1] : Integer.MAX_VALUE;
        }


        for (int i = coins.length - 2; i > 0; i--) {
            for (int j = amount; j > -1; j--) {

                int ans = Integer.MAX_VALUE;
                int dpCall;

                int coinNotUsed = dp[i + 1][j];
                int potential = j / coins[i];


                for (int coin = 1; coin <= potential; coin++) {
                    int deduction = j - coin * coins[i];
                    dpCall = dp[i + 1][deduction];
                    if (dpCall != Integer.MAX_VALUE) dpCall += coin;
                    ans = Math.min(ans, dpCall);
                }

                dp[i][j] = Math.min(ans, coinNotUsed);

            }


        }


        return dp[0][amount];

    }

    private int coinChangeRecursiveHelper(int coinIndex, int amountRemaining) {
        if (amountRemaining == 0) {
            return 0;
        }
        if (amountRemaining < 0 || coinIndex >= coins.length) {
            return Integer.MAX_VALUE;
        }


        if (memo[coinIndex][amountRemaining] != Integer.MAX_VALUE) {
            return memo[coinIndex][amountRemaining];
        }

        int min = Integer.MAX_VALUE;
        int timesDivisible = amountRemaining / coins[coinIndex];


        for (int i = 0; i <= timesDivisible; i++) {
            int dpCall = coinChangeRecursiveHelper(coinIndex + 1, amountRemaining - (coins[coinIndex] * i));
            if (dpCall != Integer.MAX_VALUE) dpCall += i;
            min = Math.min(min, dpCall);
        }
        return memo[coinIndex][amountRemaining] = min;

    }
}
