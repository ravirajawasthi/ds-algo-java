package byteb.dynamic.programming;

import java.util.Arrays;

public class MinCostForTickets {

    public static int mincostTickets(int[] days, int[] costs) {
//        int[] dp = new int[days.length];
//        return minCostRecursive(days, costs, 0, dp);
        return minCostTicketsDP(days, costs);
    }

    private static int minCostRecursive(int[] days, int[] costs, int index, int[] dp) {

        //Base case
        if (index >= days.length) {
            return 0;
        }

        if (dp[index] != 0) {
            return dp[index];
        }

        //Taking 1 day pass
        int oneDay = costs[0] + minCostRecursive(days, costs, index + 1, dp);

        //Taking 7 days pass
        int sevenDay = costs[1];
        int newIndex = index;
        while (newIndex < days.length && days[newIndex] < days[index] + 7) {
            newIndex++;
        }
        sevenDay += minCostRecursive(days, costs, newIndex, dp);

        //Taking 30 day pass
        int thirtyDay = costs[2];
        newIndex = index;
        while (newIndex < days.length && days[newIndex] < days[index] + 30) {
            newIndex++;
        }
        thirtyDay += minCostRecursive(days, costs, newIndex, dp);

        return dp[index] = Math.min(oneDay, Math.min(thirtyDay, sevenDay));
    }

    private static int minCostTicketsDP(int[] days, int[] costs) {
        int n = days.length;
        int[] dp = new int[n];

        dp[n - 1] = Math.min(costs[2], Math.min(costs[0], costs[1]));
        for (int i = n - 2; i > -1; i--) {
            int oneDay = costs[0] + dp[i + 1];

            //Seven day
            int sevenDay = costs[1];
            int newIndex = i;
            while (newIndex < n && days[newIndex] < days[i] + 7) {
                newIndex++;
            }
            if (newIndex != n) {
                sevenDay += dp[newIndex];
            }

            //thirty Day
            int thiryDay = costs[2];

            while (newIndex < n && days[newIndex] < days[i] + 30) {
                newIndex++;
            }
            if (newIndex < n && days[newIndex] < days[i] + 30) {
                thiryDay += dp[newIndex];
            }
            if (newIndex != n) {
                thiryDay += dp[newIndex];
            }

            dp[i] = Math.min(oneDay, Math.min(sevenDay, thiryDay));


        }
        return dp[0];

    }

}
