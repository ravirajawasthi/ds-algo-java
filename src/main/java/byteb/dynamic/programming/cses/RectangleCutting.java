package byteb.dynamic.programming.cses;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class RectangleCutting {

    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }
    //Solution Here

    static class Task {


        public void solve(InputReader sc, PrintWriter out) {

            int a = sc.nextInt();
            int b = sc.nextInt();

            int[][] dp = new int[a + 1][b + 1];

            for (var arr : dp) Arrays.fill(arr, Integer.MAX_VALUE);

            for (int i = 0; i <= Math.min(a, b); i++) {
                dp[i][i] = 0;
            }
            for (int i = 1; i <= a; i++) {
                for (int j = 1; j <= b; j++) {
                    //Divide i....
                    for (int k = i - 1; k > 0; k--) {
                        int dpCall_left = dp[k][j];
                        int dpCall_right = dp[i - k][j];
                        if (dpCall_right != Integer.MAX_VALUE && dpCall_left != Integer.MAX_VALUE) {
                            dp[i][j] = Math.min(1 + dpCall_left + dpCall_right, dp[i][j]);
                        }
                    }

                    for (int k = j - 1; k > 0; k--) {
                        int dpCall_left = dp[i][k];
                        int dpCall_right = dp[i][j - k];
                        if (dpCall_right != Integer.MAX_VALUE && dpCall_left != Integer.MAX_VALUE) {
                            dp[i][j] = Math.min(1 + dpCall_left + dpCall_right, dp[i][j]);
                        }
                    }
                }
            }

            out.println(dp[a][b]);


        }


        //Solution End
        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    static class MultiTreeSet {
        TreeMap<Integer, Integer> freqTreeMap = new TreeMap<>();
        int size;

        public MultiTreeSet() {
        }

        public MultiTreeSet(Collection<Integer> c) {
            for (Integer element : c)
                add(element);
        }

        public int size() {
            return size;
        }

        public void add(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                freqTreeMap.put(element, 1);
            else
                freqTreeMap.put(element, freq + 1);
            ++size;
        }

        public void remove(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq != null) {
                if (freq == 1)
                    freqTreeMap.remove(element);
                else
                    freqTreeMap.put(element, freq - 1);
                --size;
            }
        }

        public int get(Integer element) {
            Integer freq = freqTreeMap.get(element);
            if (freq == null)
                return 0;
            return freq;
        }

        public boolean contains(Integer element) {
            return get(element) > 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public Integer first() {
            return freqTreeMap.firstKey();
        }

        public Integer last() {
            return freqTreeMap.lastKey();
        }

        public Integer pollLast() {
            var retVal = freqTreeMap.lastKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer pollFirst() {
            var retVal = freqTreeMap.firstKey();
            this.remove(retVal);
            return retVal;
        }

        public Integer ceiling(Integer element) {
            return freqTreeMap.ceilingKey(element);
        }

        public Integer floor(Integer element) {
            return freqTreeMap.floorKey(element);
        }

        public Integer higher(Integer element) {
            return freqTreeMap.higherKey(element);
        }

        public Integer lower(Integer element) {
            return freqTreeMap.lowerKey(element);
        }

        @Override
        public String toString() {
            return "MultiTreeSet [freqTreeMap=" + freqTreeMap + ", size=" + size + "]";
        }
    }


}
