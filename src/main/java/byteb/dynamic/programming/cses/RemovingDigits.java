package byteb.dynamic.programming.cses;

import java.io.*;
import java.util.StringTokenizer;

public class RemovingDigits {


    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {

        Integer[] dp;

        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int n = sc.nextInt();

            //Recursive
//            dp = new Integer[n + 1];
//            out.println(recursiveHelper(n));
            //Recursive is not possible because of the huugee dept of recursive tree

            //Solution End
            out.println(iterative(n));
        }

        private int iterative(int n) {
            if (n < 10) return 1;
            if (n == 10) return 2;
            int[] dp = new int[n + 1];
            for (int i = 1; i < 10; i++) dp[i] = 1;
            dp[10] = 2;
            int ans;
            for (int i = 11; i <= n; i++) {
                ans = Integer.MAX_VALUE;
                for (char c : Integer.toString(i).toCharArray()) {
                    int d = c - '0';
                    if (d == 0) continue;
                    ans = Math.min(ans, dp[i - d] + 1);
                }
                dp[i] = ans;
            }
            return dp[n];
        }

        private int recursiveHelper(Integer n) {
            if (n == 0) return 0;
            if (n < 10) return 1;

            if (dp[n] != null) return dp[n];
            int ans = Integer.MAX_VALUE;
            for (char c : n.toString().toCharArray()) {
                if (c == '0') continue;
                int d = c - '0';
                int dpCall = recursiveHelper(n - d);
                if (dpCall != Integer.MAX_VALUE) dpCall++;
                ans = Math.min(dpCall, ans);
            }
            return dp[n] = ans;
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
