package byteb.dynamic.programming.cses;

import java.util.*;

// "static void main" must be defined in a public class.
public class RemovalGame {
    private static Integer[][] map;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];

        map = new Integer[n][n];

        for (int i = 0; i < n ;i++){
            arr[i] = sc.nextInt();
        }

        System.out.println(f(arr, 0, arr.length - 1));

    }

    private static int f(int[] arr, int l, int r){
        if (l >= r){
            return 0;
        }

        if (map[l][r] != null) return map[l][r];

        //Pick left
        int left = arr[l];
        if (l + 1 != r){
            if (arr[l+1] > arr[r]){
                left = arr[l] + f(arr, l+2, r);
            }else if (arr[l+1] < arr[r]){
                left = arr[l] + f(arr, l+1, r-1);
            }else{
                left = arr[l] + Math.max(arr[l] + f(arr, l+2, r), arr[l] + f(arr, l+1, r-1));
            }
        }

        //Pick Right
        int right = arr[r];
        if (r - 1 != l){
            if (arr[l] > arr[r-1]){
                right = arr[r] + f(arr, l+1, r-1);
            }else if (arr[l] < arr[r-1]){
                right = arr[r] + f(arr, l, r-2);
            }else{
                right = arr[r] + Math.max(arr[l] + f(arr, l+1, r-1), arr[l] + f(arr, l, r-2));
            }
        }


        return map[l][r] = Math.max(left, right);

    }
}