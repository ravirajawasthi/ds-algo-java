package byteb.dynamic.programming.cses;

import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

public class MinimizingCoins {

    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int coins = sc.nextInt();
            int target = sc.nextInt();

            int[] values = new int[coins];
            for (int i = 0; i < coins; i++) values[i] = sc.nextInt();

            int[] dp = new int[target + 1];
            Arrays.fill(dp, Integer.MAX_VALUE);
            dp[0] = 0;
            for (int i = 1; i <= target; i++) {
                for (int coin : values) {
                    if (coin > i) continue;
                    int dpCall = dp[i - coin];
                    if (dpCall != Integer.MAX_VALUE) dpCall += 1;
                    dp[i] = Math.min(dp[i], dpCall);
                }
            }
            out.println(dp[target] == Integer.MAX_VALUE ? -1: dp[target]);

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
