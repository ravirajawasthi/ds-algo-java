package byteb.dynamic.programming.cses;

import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class ArrayDescription {


    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int n = sc.nextInt();
            int m = sc.nextInt();

            int[] arr = new int[n];
            for (int i = 0; i < n; i++) arr[i] = sc.nextInt();

            int[][] dp = new int[n + 1][m + 1];
            for (int i = 0; i <= m; i++) dp[n][i] = 1;

            //Edge case
            boolean foundViolation = false;

            int prev = arr[0];
            for (int i = 0; i < n; i++) {
                int curr = arr[i];
                if (Math.abs(curr - prev) > 1 && curr != 0 && prev != 0) {
                    foundViolation = true;
                    break;
                }

                prev = curr;
            }
            if (foundViolation) {
                out.println(0);
                return;
            }


            for (int i = arr.length - 1; i > -1; i--) {
                if (arr[i] != 0) {
                    if (i == n - 1) dp[i][arr[i]] = 1;
                    else if (arr[i + 1] != 0) dp[i][arr[i]] = dp[i + 1][arr[i + 1]];
                    else
                        for (int j = Math.max(1, arr[i] - 1); j <= Math.min(m, arr[i] + 1); j++)
                            dp[i][arr[i]] = (
                                    dp[i][arr[i]] + dp[i + 1][j]
                            ) % MODULO;
                } else {
                    if (i == n - 1) for (int j = 1; j <= m; j++) dp[i][j] = 1;
                    else
                        for (int j = 1; j <= m; j++) {
                            for (int k = Math.max(1, j - 1); k <= Math.min(m, j + 1); k++) {
                                dp[i][j] = (
                                        dp[i][j] + dp[i + 1][k]
                                ) % MODULO;
                            }
                        }


                }
            }


            int ans = 0;
            if (arr[0] == 0)
                for (int i = 1; i <= m; i++) ans = (ans + dp[0][i]) % MODULO;
            else
                ans = dp[0][arr[0]];
            out.println(ans);

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
