package byteb.dynamic.programming;

import java.util.ArrayList;

public class LongestIncreasingSubsequence {
    public int lengthOfLIS(int[] nums) {
        //Bad graph solution of exponential time complexity

//        int[] dp = new int[nums.length];
//        Arrays.fill(dp, Integer.MAX_VALUE);
//
//        HashSet<Integer> potentialTreeRoots = new HashSet<>();
//        for (int i = 0; i < nums.length; i++) potentialTreeRoots.add(i);
//
//        ArrayList[] graph = new ArrayList[nums.length];
//        for (int i = 0; i < graph.length; i++) graph[i] = new ArrayList<Integer>();
//
//        for (int i = 0; i < nums.length; i++) {
//            int currNum = nums[i];
//            for (int j = 0; j < i; j++) {
//                int num = nums[j];
//                if (num < currNum) {
//                    graph[j].add(i);
//                    potentialTreeRoots.remove(i);
//                }
//            }
//        }
//        int ans = 0;
//        for (var node : potentialTreeRoots) {
//            if (dp[node] == Integer.MAX_VALUE) {
//                dp[node] = maxTreeHeight(node, graph);
//            }
//            ans = Math.max(ans, dp[node]);
//
//        }
//
//        return ans;

        return lisBottomUp(nums);


    }

    private int lisBottomUp(int[] nums) {
        int globalBest = Integer.MIN_VALUE;
        int[] dp = new int[nums.length];
        dp[0] = 1;
        for (int i = 1; i < nums.length; i++) {
            int num = nums[i];
            int notConsider = dp[i - 1];
            int currAnswer = Integer.MIN_VALUE;
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    currAnswer = Math.max(dp[j], currAnswer);
                }
            }

            dp[i] = Math.max(1, currAnswer + 1);
            globalBest = Math.max(dp[i], globalBest);
        }
        return globalBest;
    }

    private int maxTreeHeight(int root, ArrayList[] tree) {
        int max = 1;
        for (var node : tree[root])
            max = Math.max(max, 1 + maxTreeHeight((int) node, tree));
        return max;
    }
}
