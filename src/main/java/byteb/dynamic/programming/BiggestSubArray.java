package byteb.dynamic.programming;

import java.util.stream.Stream;

public class BiggestSubArray {

    private final int MIN = -100000;

    int[] arr1;
    int[] arr2;

    Integer[][] dp;

    public int findLength(int[] nums1, int[] nums2) {
        return findLengthRecursive(nums1, nums2);
    }

    private int findLengthRecursive(int[] num1, int[] num2) {
        this.arr1 = num1;
        this.arr2 = num2;
        dp = new Integer[num1.length][num2.length];
        return findLengthRecursiveHelper(0, 0);
    }

    private int findLengthRecursiveHelper(int index1, int index2) {
        if (index1 >= arr1.length || index2 >= arr2.length) {
            return 0;
        }

        if (dp[index1][index2] != null) return dp[index1][index2];

        int same = MIN;
        int skipLeft = MIN;
        int skipRight = MIN;
        int skipBoth = MIN;
        int i1 = index1;
        int i2 = index2;
        if (arr1[index1] == arr2[index2]) {
            int length = 1;
            index1++;
            index2++;
            while (index1 < arr1.length && index2 < arr2.length) {
                if (arr1[index1++] == arr2[index2++]) {
                    length++;
                } else {
                    break;
                }
            }
            same = length;
        }
        skipLeft = findLengthRecursiveHelper(i1 + 1, i2);
        skipBoth = findLengthRecursiveHelper(i1 + 1, i2 + 1);
        skipRight = findLengthRecursiveHelper(i1, i2 + 1);

        return dp[i1][i2] = Stream.of(same, skipLeft, skipRight, skipBoth).max(Integer::compareTo).get();
    }
}