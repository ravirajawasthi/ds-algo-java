package byteb.dynamic.programming;

public class MinCostOfStairs {

    //State reduction
    public static int minCostClimbingStairs(int[] cost) {

        if (cost.length < 2) {
            return 0;
        }

        int second = 0;
        int first = 0;
        int third = Integer.MAX_VALUE;

        for (int i = 2; i <= cost.length; i++) {
            third = Math.min(first + cost[i - 2], second + cost[i - 1]);
            first = second;
            second = third;
        }

        return third;
    }
}
