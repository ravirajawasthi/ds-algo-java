package byteb.dynamic.programming;

import java.util.TreeMap;

public class DeleteAndEarn {
    public static void main(String[] args) {
//        System.out.println("deleteAndEarn(new int[]{3,4,2}) = " + deleteAndEarn(new int[]{3, 4, 2}));
        System.out.println("deleteAndEarn(new int[]{8, 10, 4, 9, 1, 3, 5, 9, 4, 10}) = " + deleteAndEarn(new int[]{8, 10, 4, 9, 1, 3, 5, 9, 4, 10}));
    }

    public static int deleteAndEarn(int[] nums) {
        TreeMap<Integer, Integer> map = new TreeMap<>();

        for (int i : nums) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }

        var keySet = map.navigableKeySet();
        int[] dp = new int[keySet.size() + 1];


        int i = 1;
        for (int n : keySet) {
            int prevKey = n - 1;
            int pick;
            int notPick = dp[i - 1];
            if (map.containsKey(prevKey)) {
                pick = map.get(n) * n + dp[i - 2];
            } else {
                pick = map.get(n) * n + dp[i - 1];
            }
            dp[i++] = Math.max(notPick, pick);
        }

        return dp[keySet.size()];

    }

}
