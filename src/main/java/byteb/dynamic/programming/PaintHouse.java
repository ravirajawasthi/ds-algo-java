package byteb.dynamic.programming;

public class PaintHouse {
    public int minCost(int[][] costs) {
        return minCostDp(costs);
    }

    private int minCostDp(int[][] cost) {
        int n = cost.length;
        int h = cost[0].length;
        int[][] dp = new int[n][h];

        for (int i = 0; i < h; i++) {
            dp[n - 1][i] = cost[n - 1][i];
        }

        for (int i = n - 2; i > -1; i--) {
            for (int j = 0; j < h; j++) {
                int houseCost = cost[i][j];
                int otherHouseCost = Integer.MAX_VALUE;
                for (int k = 0; k < h; k++) {
                    if (j == k) continue;
                    otherHouseCost = Math.min(otherHouseCost, dp[i + 1][k]);
                }
                houseCost += otherHouseCost;
                dp[i][j] = houseCost;
            }
        }
        int ans = Integer.MAX_VALUE;
        for (int i = 0; i < h; i++) {
            ans = Math.min(ans, dp[0][i]);
        }
        return ans;
    }
}
