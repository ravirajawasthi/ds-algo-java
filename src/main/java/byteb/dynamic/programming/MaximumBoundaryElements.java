package byteb.dynamic.programming;

import java.io.IOException;
import java.time.Instant;

public class MaximumBoundaryElements {
    public long numberOfSubarrays(int[] nums) {

        int[][] dp = new int[nums.length][nums.length];
        int ans = 0;

        for (int i = 0; i < nums.length; i++) {
            ans++;
            dp[i][i] = nums[i];
        }

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int element = nums[j];
                dp[i][j] = Math.max(dp[i][j - 1], element);
                if (dp[i][i] == dp[i][j] && nums[j] == dp[i][i]) {
                    ans++;
                }
            }
        }

        return ans;


    }

    public static void main(String[] args) throws IOException {

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        String input = new String(classloader.getResourceAsStream("MaximumBoundaryElements.input").readAllBytes());

        var splitNums = input.split(",");
        var nums = new int[splitNums.length];

        for (int i = 0; i < splitNums.length; i++) {
            nums[i] = Integer.parseInt(splitNums[i]);
        }
        var now = Instant.now().toEpochMilli();
        var obj = new MaximumBoundaryElements();
        System.out.printf("Ans is = %s%n", obj.numberOfSubarrays(nums));
        var end = Instant.now().toEpochMilli();
        System.out.printf("length = %s%n", nums.length);
        System.out.printf("%s miliseconds", end - now + 1);

    }
}
