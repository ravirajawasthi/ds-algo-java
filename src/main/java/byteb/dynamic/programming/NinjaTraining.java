package byteb.dynamic.programming;

import java.util.Arrays;

public class NinjaTraining {

    public static int ninjaTraining(int n, int[][] points) {
        int[] dpArr2 = points[0];
        int[] dpArr = new int[3];

        for (int i = 1; i < n; i++) {
            dpArr[0] = Math.max(dpArr2[1], dpArr2[2]) + points[i][0];
            dpArr[1] = Math.max(dpArr2[0], dpArr2[2]) + points[i][1];
            dpArr[2] = Math.max(dpArr2[1], dpArr2[0]) + points[i][2];
            dpArr2 = Arrays.copyOf(dpArr, dpArr.length);
        }

        return Arrays.stream(dpArr).max().getAsInt();

    }

}
