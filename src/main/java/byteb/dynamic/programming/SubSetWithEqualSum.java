package byteb.dynamic.programming;

import java.util.Arrays;


//https://leetcode.com/problems/partition-equal-subset-sum/
//Code nations is working, leetcode has negative numbers
public class SubSetWithEqualSum {

    public static boolean canPartition(int[] nums) {
        int targetSum = Arrays.stream(nums).parallel().sum();
        if (targetSum % 2 != 0) {
            return false;
        } else {
//        return canPartitionRecursive(nums.length - 1, nums, 0, 0, 0, 0);
//            return subsetSumKDP(targetSum / 2, nums);
            return subsetSumWithTarget(nums, targetSum / 2);
        }
    }

    private static boolean canPartitionRecursive(int index, int[] nums, int set1Size, int set2Size, int set1Sum, int set2Sum) {
        //Either set is not empty and both sum are equal. both sum can be zero.
        if (set1Size + set2Size == nums.length && set1Sum == set2Sum) {
            return true;
        } else if (index < 0) {
            return false;
        } else {
            var addedToSubSet1 = canPartitionRecursive(index - 1, nums, set1Size + 1, set2Size, set1Sum + nums[index], set2Sum);
            var addedToSubSet2 = canPartitionRecursive(index - 1, nums, set1Size, set2Size + 1, set1Sum, set2Sum + nums[index]);
            return addedToSubSet1 || addedToSubSet2;
        }
    }


    private static boolean subsetSumWithTarget(int[] nums, int target) {
        boolean[] prev = new boolean[target + 1];
        boolean[] curr = new boolean[target + 1];
        curr[0] = prev[0] = true;
        for (int element : nums) {
            for (int j = 1; j <= target; j++) {
                if (element > j) {
                    curr[j] = prev[j];
                } else if (element == j) {
                    curr[j] = true;
                } else {
                    curr[j] = prev[j - element] || prev[j];
                }
            }
            prev = curr;
            curr = new boolean[target + 1];
            curr[0] = true;
        }

        return prev[target];


    }


}
