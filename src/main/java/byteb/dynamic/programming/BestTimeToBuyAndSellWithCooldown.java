package byteb.dynamic.programming;

public class BestTimeToBuyAndSellWithCooldown {
    private int[] prices;
    private Integer[][] dpRecursive;

    public int maxProfit(int[] prices) {
        //Recursive
        // return maxProfitRecursive(prices);


        //Bottom up
        return maxProfitBottomUp(prices);

    }

    private int maxProfitRecursive(int[] prices) {
        this.prices = prices;
        dpRecursive = new Integer[prices.length][2];
        return maxProfitRecursiveHelper(0, 0);
    }

    private int maxProfitRecursiveHelper(int index, int mode) {
        if (index >= prices.length) {
            return 0;
        }

        if (dpRecursive[index][mode] != null)
            return dpRecursive[index][mode];

        if (mode == 0) {
            // Buy Today
            int buyToday = -1 * this.prices[index] + maxProfitRecursiveHelper(index + 1, 1);
            int notBuyToday = maxProfitRecursiveHelper(index + 1, 0);
            return dpRecursive[index][mode] = Math.max(buyToday, notBuyToday);
        } else {
            int sellToday = prices[index] + maxProfitRecursiveHelper(index + 2, 0);
            int holdToday = maxProfitRecursiveHelper(index + 1, 1);
            return dpRecursive[index][mode] = Math.max(sellToday, holdToday);
        }
    }

    private int maxProfitBottomUp(int[] prices){
        int[][] dp = new int[prices.length+1][2];
        for (int i = prices.length-1; i > -1; i--){
            //Buying
            int buy = -1 * prices[i] + dp[i+1][1];
            int notBuy = dp[i+1][0];
            dp[i][0] = Math.max(buy, notBuy);

            //Selling
            int sellNow = prices[i] + (i == prices.length -1 ? 0 : dp[i+2][0]); //Just for first loop iteration
            int notSell = dp[i+1][1];
            dp[i][1] = Math.max(sellNow, notSell);
        }
        return dp[0][0];
    }

}
