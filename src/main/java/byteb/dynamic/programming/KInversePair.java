package byteb.dynamic.programming;

import java.util.HashMap;

public class KInversePair {

    private final int MODULO = ((int) Math.pow(10, 9)) + 7;

    private HashMap<CacheKey, Integer> dp;

    public int kInversePairs(int n, int k) {
        return kInversePairsRecursive(n, k);
    }

    private int kInversePairsRecursive(int n, int k) {
        //Recursive
        //dp = new HashMap<>();
        //return recursiveHelper(n, k);

        //Iterative
        return KInversePairIterative(n, k);
    }


    private int recursiveHelper(int n, int k) {
        if (k == 0 && n == 1) {
            return 1;
        }

        if (n <= 1 || k < 0) {
            return 0;
        }

        if (k == 0) return 1;

        var key = new CacheKey(n, k);

        if (dp.containsKey(key)) return dp.get(key);

        int count = 0;

        for (int i = 0; i < n; i++) {
            //Place the ith element here
            count = (count + recursiveHelper(n - 1, k - (n - 1 - i))) % MODULO;
        }
        dp.put(key, count);
        return count;
    }


    private class CacheKey {
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CacheKey cacheKey = (CacheKey) o;

            if (list != cacheKey.list) return false;
            return k == cacheKey.k;
        }

        @Override
        public int hashCode() {
            int result = list;
            result = 31 * result + k;
            return result;
        }

        private final int list;
        private final int k;


        public CacheKey(int list, int k) {
            this.list = list;
            this.k = k;
        }
    }


    private int KInversePairIterative(int n, int k) {
        int[][] dp = new int[n + 1][k + 1];

        for (int i = 0; i <= n; i++) {
            dp[i][0] = 1;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= k; j++) {
                for (int l = Math.max(1, i-j); l <= i; l++) {
                    int creatingPairs = i - l;
                    if (creatingPairs > j) continue;
                    dp[i][j] = (dp[i][j] + dp[i - 1][j - creatingPairs]) % MODULO;
                }
            }
        }

        return dp[n][k];
    }

    /*
    2 inverse pairs
    1 2 3
    3 2 1 | k = 2
    3 1 2 | k = 2

    Basically each number wants to know how many bigger elements are there than it


    */

}