package byteb.dynamic.programming;

import java.util.HashMap;

public class MaxScoreFromPerformingMultiplicationOperations {

    int[] nums;
    int[] multipliers;

    HashMap<String, Integer> memo;

    public int maximumScore(int[] nums, int[] multipliers) {
        this.nums = nums;
        this.multipliers = multipliers;
        this.memo = new HashMap<>();
        return maxScore(nums, multipliers);
//        return maxScore(0, nums.length - 1, 0);
    }

    private int maxScore(int startArray, int endArray, int multiplierIndex) {
        String memoKey = startArray + ":" + endArray + ":" + multiplierIndex;
        if (memo.containsKey(memoKey)) {
            return memo.get(memoKey);
        }
        if (startArray > endArray || multiplierIndex >= multipliers.length) {
            return 0;
        }
        if (endArray - startArray + 1 < multipliers.length - multiplierIndex) {
            //No point in computing further because we don't have enough numbers to use all remaining multipliers
            return 0;
        }


        int pick;

        int pickLastIndex = multipliers[multiplierIndex] * nums[endArray];
        int pickFirstIndex = multipliers[multiplierIndex] * nums[startArray];

        if (pickFirstIndex >= pickLastIndex) {
            pick = pickFirstIndex;
            startArray++;
        } else {
            pick = pickLastIndex;
            endArray--;
        }
        pick += maxScore(startArray, endArray, multiplierIndex + 1);

        memo.put(memoKey, pick);
        return memo.get(memoKey);
    }

    public int maxScore(int[] nums, int[] multipliers) {
        int[][] dp = new int[multipliers.length + 1][multipliers.length + 1];
        //DP[i][j] : I operations with j elements from left. Element from right to pick = j - i

        for (int i = 1; i <= multipliers.length; i++) {
            for (int j = i; j <= multipliers.length; j++) {

                int pickLeftElement = multipliers[j - 1] * nums[i - 1] + dp[i - 1][j - 1];
                int pickRightElement = multipliers[j - 1] * nums[nums.length - i] + dp[i][j - 1];

                dp[i][j] = Math.max(pickLeftElement, pickRightElement);

            }
        }

        return dp[multipliers.length][multipliers.length];


    }


}
