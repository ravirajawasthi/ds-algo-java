package byteb.dynamic.programming;

import java.util.Arrays;

public class PaintHouse3 {

    private int n;

    private int target;
    private int colors;

    private int[] houses;

    private int[][] costs;

    /*
        houses = [0,0,0,0,0]
        cost of painting houses = [
            [1,10],
            [10,1],
            [10,1],
            [1,10],
            [5,1]
        ],
        number of houses = 5,
        colors = 2,
        maximal group target = 3
     */


    public int minCost(int[] houses, int[][] cost, int m, int n, int target) {
        //Recursive
        // return minCostRecursive(houses, cost, m, n, target);

        //Iterative
        return minCostIterativeDp(houses, cost, m, n, target);
    }


    private int minCostRecursive(int[] houses, int[][] cost, int m, int n, int target) {
        this.n = m;
        this.colors = n;
        this.costs = cost;
        this.houses = houses;
        this.target = target;
        return minCostHelper(0, 0, 0);
    }


    private int minCostHelper(int houseIndex, int maximalGroups, int previousGroupColor) {

        if (houseIndex == n && maximalGroups == target) return 0;

        if (houseIndex >= n || maximalGroups > target) {
            return Integer.MAX_VALUE;
        }


        if (houses[houseIndex] != 0) {
            //Current house is already painted
            int partOfAMaximalGroup = Integer.MAX_VALUE;
            if (houses[houseIndex] == previousGroupColor) {
                //1) it could be part of a maximal group
                partOfAMaximalGroup = Math.min(partOfAMaximalGroup,
                        minCostHelper(houseIndex + 1, maximalGroups, previousGroupColor));
            } else {
                //2) It could be its own maximal group
                partOfAMaximalGroup = Math.min(partOfAMaximalGroup,
                        minCostHelper(houseIndex + 1, maximalGroups + 1, houses[houseIndex]));
            }
            return partOfAMaximalGroup;
        } else {
            //I can paint this house
            int paintNewHouse = Integer.MAX_VALUE;
            for (int i = 1; i <= colors; i++) {
                int dpCall;
                if (i == previousGroupColor) {
                    dpCall = minCostHelper(houseIndex + 1, maximalGroups, i);
                } else {
                    dpCall = minCostHelper(houseIndex + 1, maximalGroups + 1, i);
                }
                dpCall += dpCall != Integer.MAX_VALUE ? costs[houseIndex][i - 1] : 0;
                paintNewHouse = Math.min(dpCall, paintNewHouse);
            }

            return paintNewHouse;
        }

    }


    private int minCostIterativeDp(int[] houses, int[][] costs, int m, int colors, int target) {
        //Init
        int[][][] dp = new int[m + 1][target + 1][colors + 1];
        for (int[][] arr : dp) for (int[] inArr : arr) Arrays.fill(inArr, Integer.MAX_VALUE);


        if (houses[m - 1] != 0) {
            for (int i = 1; i <= colors; i++) {
                if (i == houses[m - 1]) continue;
                dp[m - 1][target - 1][i] = 0;
            }
        } else {
            for (int i = 1; i <= colors; i++) {
                dp[m - 1][target - 1][i] = costs[m - 1][i - 1];
            }
        }


        for (int houseIndex = m - 2; houseIndex > -1; houseIndex--) {
            for (int grouping = 0; grouping < target; grouping++) {
                for (int color = 1; color <= colors; color++) {
                    if (houses[houseIndex] == 0) {
                        //House can be painted
                        int otherThanPrevious = Integer.MAX_VALUE;
                        for (int c = 1; c <= colors; c++) {
                            if (c == color) continue;
                            int dpCall = dp[houseIndex + 1][grouping + 1][c];
                            if (dpCall != Integer.MAX_VALUE) dpCall += costs[houseIndex][c - 1];
                            otherThanPrevious = Math.min(otherThanPrevious, dpCall);
                        }
                        int sameAsPrevious = dp[houseIndex + 1][grouping][color];
                        if (sameAsPrevious != Integer.MAX_VALUE) sameAsPrevious += costs[houseIndex][color - 1];
                        dp[houseIndex][grouping][color] = Math.min(sameAsPrevious, otherThanPrevious);

                    } else {
                        //It can't be painted
                        int paintedColor = houses[houseIndex];
                        if (paintedColor == color) {
                            dp[houseIndex][grouping][color] = dp[houseIndex + 1][grouping][color];
                        } else {
                            dp[houseIndex][grouping][color] = dp[houseIndex + 1][grouping + 1][paintedColor];
                        }

                    }
                }
            }
        }
        int min = Integer.MAX_VALUE;
        for (int i : dp[0][0]) min = Math.min(min, i);
        return min == Integer.MAX_VALUE ? -1 : min;


    }
}


