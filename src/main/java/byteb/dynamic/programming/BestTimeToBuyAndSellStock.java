package byteb.dynamic.programming;

public class BestTimeToBuyAndSellStock {

    public int maxProfit(int[] prices) {
        int ans = 0;
        int minBuyValue = prices[0];
        for (int i = 1; i < prices.length; i++){
            ans = Math.max(ans, prices[i] - minBuyValue);
            minBuyValue = Math.min(minBuyValue, prices[i]);
        }
        return ans;
    }

}
