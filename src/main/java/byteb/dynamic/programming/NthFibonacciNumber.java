package byteb.dynamic.programming;

import java.util.Scanner;

public class NthFibonacciNumber {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n == 0) {
            System.out.println(0);
        } else if (n == 1) {
            System.out.println(1);
        } else {
            long p2 = 0;
            long p1 = 1;
            long curr = 1;
            for (int i = 2; i <= n; i++) {
                curr = p1 + p2;
                p2 = p1;
                p1 = curr;
            }
            System.out.println(curr);
        }
    }

}
