package byteb.dynamic.programming;

import java.util.Arrays;

public class MinimumScheduleJobDifficulty {

    private int[] jobDifficulty;
    private int[][] dp;

    public int minDifficulty(int[] jobDifficulty, int days) {

//
//        if (jobDifficulty.length < days) return -1;
//        this.jobDifficulty = jobDifficulty;
//        return minDifficultyRecursive(jobDifficulty, days);
        return minDifficultyDp(jobDifficulty, days);

    }

    private int minDifficultyDp(int[] jobDifficulty, int days) {
        int[][] dp = new int[days + 1][jobDifficulty.length];
        for (var arr : dp)
            Arrays.fill(arr, Integer.MAX_VALUE);

        int maxJob = Integer.MIN_VALUE;
        for (int i = jobDifficulty.length - 1; i >= days - 1; i--) {
            maxJob = Math.max(maxJob, jobDifficulty[i]);
            dp[days][i] = maxJob;
        }

        for (int i = days - 1; i > 0; i--) {

            for (int j = (jobDifficulty.length - (days - i) - 1); j >= (i - 1); j--) {
                maxJob = Integer.MIN_VALUE;
                int ans = Integer.MAX_VALUE;
                for (int jobIndex = j + 1; jobIndex <= (jobDifficulty.length - (days - i)); jobIndex++) {
                    maxJob = Math.max(jobDifficulty[jobIndex - 1], maxJob);
                    ans = Math.min(ans, dp[i + 1][jobIndex] == Integer.MAX_VALUE ? Integer.MAX_VALUE : dp[i + 1][jobIndex] + maxJob);
                }
                dp[i][j] = ans;
            }
        }
        return dp[1][0];


    }


    private int minDifficultyRecursive(int[] jobDifficulty, int d) {
        this.dp = new int[jobDifficulty.length][d + 1];
        for (var arr : dp) Arrays.fill(arr, Integer.MAX_VALUE);

        return minDifficultyRecursiveHelper(0, d);

    }


    private int minDifficultyRecursiveHelper(int jobIndex, int day) {
        // jobIndex -> job index
        // j -> day

        if (jobIndex >= jobDifficulty.length) {
            return 0;
        }

        if (day < 1) return Integer.MAX_VALUE;

        if (dp[jobIndex][day] != Integer.MAX_VALUE) return dp[jobIndex][day];

        int max = Integer.MIN_VALUE;
        int ans = Integer.MAX_VALUE;
        for (int i = jobIndex; i < jobDifficulty.length - day + 1; i++) {
            max = Math.max(jobDifficulty[i], max);
            int dpCall = minDifficultyRecursiveHelper(i + 1, day - 1);
            if (dpCall != Integer.MAX_VALUE) {
                dpCall += max;
            }
            ans = Math.min(dpCall, ans);
        }

        return dp[jobIndex][day] = ans;

    }


}
