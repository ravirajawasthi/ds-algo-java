package byteb.dynamic.programming;

import java.util.HashSet;
import java.util.List;

public class WordBreak {

    HashSet<String> set;

    Boolean[][] dp;

    public boolean wordBreak(String s, List<String> wordDict) {
        set = new HashSet<>();
        set.addAll(wordDict);

        //Recursive
        //dp = new Boolean[s.length()][s.length()];
        //return workBreakRecursive(s, 0, 0);

        //Bottom up
        return workBreakBottomUp(s);
    }

    private boolean workBreakRecursive(String s, int startIndex, int endIndex) {
        if (startIndex >= s.length())
            return true;
        if (endIndex >= s.length())
            return false;

        if (dp[startIndex][endIndex] != null) return dp[startIndex][endIndex];


        String word = s.substring(startIndex, endIndex + 1);

        //Matching current word
        boolean matching = set.contains(word);
        if (matching) matching = workBreakRecursive(s, endIndex + 1, endIndex + 1);

        //Start, endIndex + 1
        boolean biggerWord = workBreakRecursive(s, startIndex, endIndex + 1);

        return dp[startIndex][endIndex] = matching || biggerWord;


    }

    private boolean workBreakBottomUp(String s) {
        boolean[] dp = new boolean[s.length() + 1];
        for (int i = 0; i < s.length(); i++) {
            for (int j = 0; j <= i; j++) {
                String currWord = s.substring(j, i + 1);
                if (set.contains(currWord)) {
                    if (j == 0) {
                        dp[i] = true;
                    } else {
                        dp[i] = dp[i] | dp[j - 1];
                    }
                }

            }
        }
        return dp[s.length() - 1];

    }

}
