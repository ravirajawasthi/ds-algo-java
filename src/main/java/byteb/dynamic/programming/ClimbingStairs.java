package byteb.dynamic.programming;

public class ClimbingStairs {

    public static int climbStairs(int n) {
        int p2 = 1;
        int p1 = 2;
        if (n == 1) {
            return p2;
        } else if (n == 2) {
            return p1;
        } else {
            int curr = p1 + p2;
            p2 = p1;
            p1 = curr;
            for (int i = 4; i <= n; i++) {
                curr = p1 + p2;
                p2 = p1;
                p1 = curr;
            }
            return curr;
        }
    }




}
