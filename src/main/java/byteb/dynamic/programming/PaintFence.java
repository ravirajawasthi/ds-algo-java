package byteb.dynamic.programming;

public class PaintFence {

    private int k;
    private int n;


    public int numWays(int n, int k) {
        //Recursive
//        if (k > n) return (int) Math.pow(n, k);
//        return numWaysRecursive(n, k);

        //Iterative
        return numsWaysBottomUp(n, k);
    }

    private int numWaysRecursive(int n, int k){
        this.k = k;
        this.n = n;
        return this.numsWaysHelper("", 0);
    }

    private int numsWaysHelper(String last2, int d){
        if (d == n) return 1;

        int sum = 0;
        if (last2.length() < 2){
            for (int i = 0; i < k; i++){
                sum += numsWaysHelper(last2 + i, d+1);
            }
        }else{
            for (int i = 0; i < k; i++){
                if (!(last2.charAt(0) == last2.charAt(1) && last2.charAt(1) - '0' == i)){
                    sum += numsWaysHelper(last2.substring(last2.length() - 1)+ i, d+1);
                }

            }
        }
        return sum;
    }

    private int numsWaysBottomUp(int n, int k){
        int[] dp = new int[n];
        dp[0] = k;
        dp[1] = 2*k;

        for (int i = 2; i < n; i++){
            dp[i] = (k-1) * (dp[i-1] + dp[i-2]);
        }

        return dp[n-1];


    }
}
