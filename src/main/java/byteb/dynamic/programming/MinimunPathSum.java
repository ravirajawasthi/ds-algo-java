package byteb.dynamic.programming;

public class MinimunPathSum {
    public static int minPathSum(int[][] grid) {
        int rows = grid.length;
        int cols = grid[0].length;
        int[][] ansGrid = new int[rows][cols];


        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {

                if (i == 0 && j == 0) {
                    ansGrid[i][j] = grid[i][j];
                }else {
                    int fromUp = i > 0 ? ansGrid[i - 1][j] : Integer.MAX_VALUE;
                    int fromLeft = j > 0 ? ansGrid[i][j - 1] : Integer.MAX_VALUE;
                    ansGrid[i][j] = Math.min(fromUp, fromLeft) + grid[i][j];
                }
            }
        }
        return ansGrid[rows - 1][cols - 1];
    }
}
