package byteb.dynamic.programming;

public class CherryPickup2 {

    public static int cherryPickup(int[][] grid) {
//        int r = grid.length;
//        int c = grid[0].length;
//        int[][][] dp = new int[grid.length][c][c];
//        for (int[][] row : dp) {
//            for (int[] actualRow : row) {
//                Arrays.fill(actualRow, -1);
//            }
//        }
//        return cherriesPickup(0, 0, grid[0].length - 1, grid, dp);

        return cherriesPickupDP(grid);
    }


    public static int cherriesPickup(int row, int col1, int col2, int[][] grid, int[][][] dp) {
        if (col1 < 0 || col1 >= grid[0].length || col2 < 0 || col2 >= grid[0].length) {
            return 0;
        }
        if (dp[row][col1][col2] != -1) {
            return dp[row][col1][col2];
        }
        if (row == grid.length - 1) {
            if (col1 == col2) {
                return grid[row][col1];
            } else {
                return grid[row][col1] + grid[row][col2];
            }
        }

        int maxPath = Integer.MIN_VALUE;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                int maxCherries = cherriesPickup(row + 1, col1 + i, col2 + j, grid, dp);
                if (col1 == col2) {
                    maxPath = Math.max(maxPath, maxCherries + grid[row][col1]);
                } else {
                    maxPath = Math.max(maxPath, maxCherries + grid[row][col1] + grid[row][col2]);
                }
            }
        }
        dp[row][col1][col2] = maxPath;
        return maxPath;

    }

    private static int cherriesPickupDP(int[][] grid) {
        int rows = grid.length;
        int columns = grid[0].length;

        int[][] dp = new int[columns][columns];

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < columns; j++) {
                if (i != j) {
                    dp[i][j] = grid[rows - 1][i] + grid[rows - 1][j];
                } else {
                    dp[i][j] = grid[rows - 1][j];
                }
            }
        }

        int[][] curr = new int[columns][columns];

        for (int i = rows - 2; i >= 0; i--) {

            for (int col1 = 0; col1 < columns; col1++) {

                for (int col2 = 0; col2 < columns; col2++) {

                    int maxCherries = 0;

                    for (int c1 = col1 - 1; c1 <= col1 + 1; c1++) {
                        for (int c2 = col2 - 1; c2 <= col2 + 1; c2++) {
                            if (!(c1 < 0 || c1 >= columns || c2 < 0 || c2 >= columns)) {
                                maxCherries = Math.max(maxCherries, dp[c1][c2]);
                            }
                        }
                    }
                    if (col1 == col2) {
                        curr[col1][col2] = maxCherries + grid[i][col1];
                    } else {
                        curr[col1][col2] = maxCherries + grid[i][col1] + grid[i][col2];
                    }
                }
            }

            dp = curr;
            curr = new int[columns][columns];

        }
        //VIMP
        //Why 0, columns -1?
        return dp[0][columns - 1];

    }

}
