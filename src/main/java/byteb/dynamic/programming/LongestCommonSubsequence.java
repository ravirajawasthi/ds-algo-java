package byteb.dynamic.programming;

import java.util.HashMap;

public class LongestCommonSubsequence {
    public int longestCommonSubsequence(String text1, String text2) {
        return subSequenceRecursive(text1, text2);
    }


    private int subSequenceRecursive(String text1, String text2) {
        HashMap<String, Integer> memo = new HashMap<>();
        return subSequenceRecursiveHelper(text1, text2, memo, 0, 0);
    }

    private int subSequenceRecursiveHelper(String text1, String text2, HashMap<String, Integer> memo, int t1, int t2) {
        String key = t1 + ":" + t2;
        if (memo.containsKey(key)) return memo.get(key);

        if (t1 >= text1.length() || t2 >= text2.length()) return 0;

        int consider = 0;
        if (text1.charAt(t1) == text2.charAt(t2)) consider = 1;
        consider += subSequenceRecursiveHelper(text1, text2, memo, t1 + 1, t2 + 1);

        int notConsider = max(subSequenceRecursiveHelper(text1, text2, memo, t1, t2 + 1),
                subSequenceRecursiveHelper(text1, text2, memo, t1 + 1, t2));

        memo.put(key, max(consider, notConsider));

        return memo.get(key);
    }

    public int longestCommonSubsequenceIterative(String text1, String text2) {
        // 1) Get dp table of correct dimensions
        int[][] dp = new int[text1.length() + 1][text2.length() + 1];

        // 2) Get the base cases right
        // (Already 0)

        // 3) Iterate from base cases towards answer (Most difficult)
        for (int i = text1.length() - 1; i > -1; i--) {
            for (int j = text2.length() - 1; j > -1; j--) {
                // 4) The recursive calls from top down solution will be problems that are already solved

                int consider = text1.charAt(i) == text2.charAt(j) ? 1 : 0;
                consider += dp[i + 1][j + 1];

                int notConsider = Math.max(dp[i + 1][j], dp[i][j + 1]);

                dp[i][j] = Math.max(consider, notConsider);
            }
        }

        return dp[0][0];

    }


    private int max(int... args) {
        int max = Integer.MIN_VALUE;
        for (int n : args) max = Math.max(max, n);
        return max;
    }

}
