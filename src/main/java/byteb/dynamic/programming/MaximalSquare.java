package byteb.dynamic.programming;

public class MaximalSquare {

    public int maximalSquare(char[][] matrix) {

        int[][] dp = new int[matrix.length][matrix[0].length];

        int ans = 0;

        dp[0][0] = matrix[0][0] == '1' ? 1 : 0;

        ans = dp[0][0];

        for (int i = 1; i < matrix.length; i++) {
            dp[i][0] = matrix[i][0] == '1' ? 1 : dp[i - 1][0];
            ans = Math.max(dp[i][0], ans);
        }

        for (int i = 1; i < matrix[0].length; i++) {
            dp[0][i] = matrix[0][i] == '1' ? 1 : dp[0][i - 1];
            ans = Math.max(dp[0][i], ans);
        }


        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix[0].length; j++) {
                if (matrix[i][j] == '1') dp[i][j] = 1;
                int consider = dp[i][j];
                consider += matrix[i - 1][j] == '1' && matrix[i - 1][j - 1] == '1' && matrix[i][j - 1] == '1' && matrix[i][j] == '1' ? min(dp[i - 1][j], dp[i - 1][j - 1], dp[i][j - 1]) : 0;

                dp[i][j] = consider;
                ans = Math.max(ans, consider);
            }
        }
        return (int) Math.pow(ans, 2);
    }

    private int min(int... args) {
        int ans = Integer.MAX_VALUE;
        for (int i : args) ans = Math.min(ans, i);
        return ans;
    }

    private int max(int... args) {
        int ans = Integer.MIN_VALUE;
        for (int i : args) ans = Math.max(ans, i);
        return ans;
    }


}
