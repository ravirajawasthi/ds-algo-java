package byteb.dynamic.programming;

//https://www.codingninjas.com/studio/problems/minimal-cost_8180930?utm_source=striver&utm_medium=website&utm_campaign=a_zcoursetuf

public class FrogWithKJump {

    public static int minimizeCost(int n, int k, int[] height) {
        //Submited, Works!
        int[] dpArr = new int[n];

        dpArr[0] = 0;

        for (int i = 1; i < n; i++) {
            int startIndex = Math.max(i - k, 0);
            int min = Integer.MAX_VALUE;
            for (int j = startIndex; j < i; j++) {
                min = Math.min(min, Math.abs(height[j] - height[i]) + dpArr[j]);
            }
            dpArr[i] = min;
        }

        return dpArr[n - 1];

    }




}
