package byteb.dynamic.programming;

import java.util.HashMap;

public class CountVowelPermutation {
    private int maxLength;
    private final int MODULO = 1000000007;

    Long[][] dp;

    HashMap<Character, Integer> map;

    public int countVowelPermutation(int n) {
        return countVowelPermutationRecursive(n);
    }

    private int countVowelPermutationRecursive(int n) {
        this.maxLength = n;
        dp = new Long[n + 1][5];
        map = new HashMap<>();
        int sum = 0;

        int j = 0;
        for (char i : new char[]{'a', 'e', 'i', 'o', 'u'}) {
            map.put(i, j++);
        }

        for (char i : map.keySet()) {
            sum = (sum + (int) (countVowelPermutationRecursiveHelper(1, i) % MODULO)) % MODULO;
        }

        return sum;
    }

    private long countVowelPermutationRecursiveHelper(int index, char prev) {
        if (index == this.maxLength) return 1;

        int key = map.get(prev);

        if (dp[index][key] != null) {
            return dp[index][key];
        }

        switch (prev) {
            case 'a' -> {
                return dp[index][key] = countVowelPermutationRecursiveHelper(index + 1, 'e') % MODULO;
            }
            case 'e' -> {
                return dp[index][key] = (countVowelPermutationRecursiveHelper(index + 1, 'a') + countVowelPermutationRecursiveHelper(index + 1, 'i')) % MODULO;
            }

            case 'i' -> {
                return dp[index][key] = (countVowelPermutationRecursiveHelper(index + 1, 'a') +
                        countVowelPermutationRecursiveHelper(index + 1, 'e') +
                        countVowelPermutationRecursiveHelper(index + 1, 'o') +
                        countVowelPermutationRecursiveHelper(index + 1, 'u')) % MODULO;
            }

            case 'o' -> {
                return dp[index][key] = (countVowelPermutationRecursiveHelper(index + 1, 'i') +
                        countVowelPermutationRecursiveHelper(index + 1, 'u')) % MODULO;
            }

            case 'u' -> {
                return dp[index][key] = (countVowelPermutationRecursiveHelper(index + 1, 'a')) % MODULO;
            }
        }

        //Code will never reach here
        return 0;
    }


    private int countVowelPermutationBottomUp(int n) {
        char[] charMap = new char[]{'a', 'e', 'i', 'o', 'u'};
        HashMap<Character, Integer> intCharMap = new HashMap<>();

        for (int i = 0; i < charMap.length; i++) intCharMap.put(charMap[i], i);

        long[][] dp = new long[n + 1][charMap.length];
        for (int i = 0; i <= charMap.length; i++) dp[n][i] = 1;

        for (int i = n - 1; i > -1; i--) {
            for (int j = 0; j < charMap.length; j++) {
                char c = charMap[j];
                switch (c) {
                    case 'a' -> dp[i][j] = dp[i + 1][map.get('e')];

                    case 'e' -> dp[i][j] = (dp[i + 1][map.get('a')] + dp[i + 1][map.get('i')]) % MODULO;

                    case 'i' ->
                            dp[i][j] = (dp[i + 1][map.get('a')] + dp[i + 1][map.get('e')] + dp[i + 1][map.get('o')] + dp[i + 1][map.get('u')]) % MODULO;

                    case 'o' -> dp[i][j] = (dp[i + 1][map.get('i')] + dp[i + 1][map.get('u')]) % MODULO;

                    case 'u' -> dp[i][j] = dp[i + 1][map.get('a')];


                }
            }
        }
        long ans = 0;
        for (int i = 0; i < charMap.length; i++) ans = (ans + dp[0][i]) % MODULO;
        return (int) ans;

    }


}
