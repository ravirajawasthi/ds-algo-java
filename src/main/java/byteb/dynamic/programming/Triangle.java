package byteb.dynamic.programming;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Triangle {
    private static class Pair {
        public int left;
        public int right;

        public Pair(int left, int right) {
            this.left = left;
            this.right = right;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (left != pair.left) return false;
            return right == pair.right;
        }

        //This is improved performance a lot!!
        @Override
        public int hashCode() {
            int result = left;
            result = 52 * result + right;
            return result;
        }
    }

    public static int minimumTotal(List<List<Integer>> triangle) {
//        HashMap<Pair, Integer> map = new HashMap<>();
//        return minimumTotalRecursive(0, 0, triangle, map);
//        return minimumTotalTopDownDP(triangle);
        return minimumTotalBottomUpDP(triangle);
    }

    private static int minimumTotalRecursive(int row, int index, List<List<Integer>> triangle, HashMap<Pair, Integer> dp) {
        var key = new Pair(row, index);
        if (dp.containsKey(key)) {
            return dp.get(key);
        }
        if (row == triangle.size() - 1) {
            return triangle.get(row).get(index);
        } else {
            int i = minimumTotalRecursive(row + 1, index, triangle, dp);
            int i1 = minimumTotalRecursive(row + 1, index + 1, triangle, dp);
            int res = Math.min(i, i1) + triangle.get(row).get(index);
            dp.put(key, res);
            return res;
        }
    }

    private static int minimumTotalTopDownDP(List<List<Integer>> triangle) {
        if (triangle.size() == 1) {
            return triangle.get(0).get(0);
        }
        List<Integer> dp2 = triangle.get(0);
        List<Integer> dp = new ArrayList<>(triangle.get(1).size());
        for (int i = 1; i < triangle.size(); i++) {
            var row = triangle.get(i);
            dp.add(dp2.get(0) + row.get(0));
            for (int j = 1; j < row.size() - 1; j++) {
                int sameIndex = dp2.get(j);
                int beforeIndex = dp2.get(j - 1);
                dp.add(Math.min(sameIndex, beforeIndex) + row.get(j));
            }
            dp.add(dp2.get(dp2.size() - 1) + row.get(row.size() - 1));
            dp2 = dp;
            dp = new ArrayList<>(dp.size() + 1);
        }
        return dp2.stream().min(Integer::compareTo).get();
    }

    private static int minimumTotalBottomUpDP(List<List<Integer>> triangle) {
        int n = triangle.size();

        if (n == 1) {
            return triangle.get(0).get(0);
        }

        var dp = triangle.get(n - 1).stream().mapToInt(i -> i).toArray();

        for (int i = n - 2; i > -1; i--) {
            var row = triangle.get(i);

            for (int j = 0; j <= i; j++) {
                int sameIndex = dp[j];
                //Bottom to up in triangle
                int nextIndex = dp[j + 1];
                dp[j] = Math.min(sameIndex, nextIndex) + row.get(j);
            }

        }

        return dp[0];

    }


}
