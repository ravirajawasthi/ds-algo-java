package byteb.dynamic.programming;

//https://leetcode.com/problems/out-of-boundary-paths

public class OutOfBoundaryPaths {

    private final int MODULO = 1000000007;

    private int m;
    private int n;

    private int maxMoves;

    Integer[][][] dp;


    public int findPaths(int m, int n, int maxMove, int startRow, int startColumn) {
        //Recursive Solution
        return findPathRecursive(m, n, maxMove, startRow, startColumn);
    }

    private int findPathRecursive(int m, int n, int maxMove, int startRow, int startColumn) {
        this.m = m;
        this.n = n;
        this.maxMoves = maxMove;
        this.dp = new Integer[m + 1][n + 1][maxMove + 1];
        return recursiveHelper(startRow, startColumn, 0);

    }

    private int recursiveHelper(int x, int y, int moveCount) {
        if (x >= m || x < 0 || y >= n || y < 0) {
            return 1;
        }

        if (moveCount >= maxMoves) {
            return 0;
        }

        if (dp[x][y][maxMoves] != null) return dp[x][y][maxMoves];

        int moveUp = recursiveHelper(x - 1, y, moveCount + 1);
        int moveDown = recursiveHelper(x + 1, y, moveCount + 1);
        int left = recursiveHelper(x, y - 1, moveCount + 1);
        int right = recursiveHelper(x, y + 1, moveCount + 1);

        return dp[x][y][maxMoves] = (((moveUp + moveDown) % MODULO) + ((left + right) % MODULO)) % MODULO;


    }

}
