package byteb.dynamic.programming;

public class BestTimeToBuyAndSellStockWithTransactionFee {
    public int maxProfit(int[] prices, int fee) {
        return maxProfitDp(prices, fee);
    }

    private int maxProfitDp(int[] prices, int fee) {
        int[][] dp = new int[prices.length][2];
        dp[prices.length - 1][0] = 0; //No point in buying the stock at the last day
        dp[prices.length - 1][1] = prices[prices.length - 1];

        for (int i = prices.length - 2; i > -1; i--) {
            //Buy
            //I buy the stock today
            int buyStock = -1 * (prices[i] + fee) + dp[i + 1][1];
            int dontButStock = dp[i + 1][0];
            dp[i][0] = Math.max(buyStock, dontButStock);


            //Sell
            //I sell the stock today
            int sellToday = prices[i] + dp[i + 1][0];
            int dontSellToday = dp[i + 1][1];
            dp[i][1] = Math.max(sellToday, dontSellToday);
        }

        return dp[0][0];
    }
}
