package byteb.dynamic.programming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

//https://codeforces.com/problemset/problem/1380/C

public class CreateTeams {

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                int x = sc.nextInt();

                int[] skills = new int[n];
                for (int i = 0; i < n; i++)
                    skills[i] = sc.nextInt();

                Arrays.parallelSort(skills);

                int[] dp = new int[n + 1];

                dp[n - 1] = skills[n - 1] >= x ? 1 : 0;
                dp[n] = 0;

                for (int i = n - 2; i > -1; i--) {
                    int pick;
                    int extraProgrammersNeeded;
                    if (skills[i] >= x) {
                        pick = 1 + dp[i + 1];
                    } else {
                        extraProgrammersNeeded = (int) Math.ceil((double) x / skills[i]) - 1;
                        if (i + extraProgrammersNeeded > n - 1) {
                            pick = 0;
                        } else {
                            pick = 1 + dp[i + extraProgrammersNeeded + 1]; //+1 because till extraProgrammersNeeded will be part of my team and will get the answer from +1
                        }
                    }

                    int notPick = dp[i + 1];
                    dp[i] = Math.max(pick, notPick);
                }

                out.println(dp[0]);
            }

            // Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
