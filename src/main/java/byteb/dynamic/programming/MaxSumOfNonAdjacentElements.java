package byteb.dynamic.programming;

import java.util.List;

public class MaxSumOfNonAdjacentElements {


    public static int maximumNonAdjacentSum(List<Integer> nums) {

        if (nums.size() == 1) {
            return nums.get(0);
        } else if (nums.size() == 2) {
            return Math.max(nums.get(0), nums.get(1));
        }

        int[] dpArr = new int[nums.size()];
        dpArr[0] = nums.get(0);
        dpArr[1] = Math.max(nums.get(0), nums.get(1));

        for (int i = 2; i < nums.size(); i++) {
            int max = Integer.MIN_VALUE;
            for (int j = 0; j < i - 1; j++) {
                max = Math.max(max, dpArr[j] + nums.get(i));
            }
            dpArr[i] = Math.max(max, dpArr[i - 1]);
        }

        return dpArr[dpArr.length - 1];
    }

}
