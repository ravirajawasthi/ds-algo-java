package byteb.dynamic.programming;

public class UniquePaths2 {

    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int rows = obstacleGrid.length;
        int cols = obstacleGrid[0].length;
        if (obstacleGrid[0][0] == 1) {
            return 0;
        }
        var ansGrid = new int[rows][cols];
        ansGrid[0][0] = 1;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (obstacleGrid[i][j] != 1) {
                    if (i > 0 && obstacleGrid[i - 1][j] != 1) {
                        ansGrid[i][j] += ansGrid[i - 1][j];
                    }
                    if (j > 0 && obstacleGrid[i][j - 1] != 1) {
                        ansGrid[i][j] += ansGrid[i][j - 1];
                    }
                }

            }
        }
        return ansGrid[rows - 1][cols - 1];

    }
}
