package byteb.dynamic.programming;

public class SubsetSumEqualK {

    public static boolean subsetSumToK(int n, int k, int[] arr) {
        return subsetSumEqualToKDP(n, k, arr);
    }

    private static boolean subsetSumEqualToKDP(int n, int k, int[] arr) {
        boolean[] prev = new boolean[k + 1];
        boolean[] curr = new boolean[k + 1];

        prev[0] = curr[0] = true;
        for (int i = 0; i < n; i++) {
            for (int target = 1; target <= k; target++) {
                if (arr[i] > target) {
                    //Not pick
                    curr[target] = prev[target];
                } else {
                    //Not pick || pick
                    curr[target] = prev[target] || prev[target - arr[i]];
                }
            }
            prev = curr;
            curr = new boolean[k + 1];
        }

        return prev[k];

    }

    public static void main(String[] args) {
        System.out.println(subsetSumToK(4, 4, new int[]{1, 1, 1, 1}));
    }



}
