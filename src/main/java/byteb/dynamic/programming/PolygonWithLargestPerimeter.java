package byteb.dynamic.programming;

import java.util.Arrays;

public class PolygonWithLargestPerimeter {


    public long largestPerimeter(int[] nums) {
        return bottomUp(nums);
    }
    // public long largestPerimeter(int[] nums) {
    //     int bestPerimeter = -1;

    //     Arrays.parallelSort(nums);

    //     int[] presum = new int[nums.length];
    //     presum[0] = nums[0];
    //     for (int i = 1; i < nums.length; i++)
    //         presum[i] = presum[i - 1] + nums[i];

    //     int largest = nums.length - 1;
    //     int smallest = nums.length - 3;

    //     while (smallest > -1 && largest > 1) {
    //         // Checking Polygon condition
    //         int currSumOfSmallerSides;
    //         if (smallest == 0) {
    //             currSumOfSmallerSides = presum[largest] - nums[largest];
    //         } else {
    //             currSumOfSmallerSides = presum[largest - 1] - nums[smallest - 1];
    //         }
    //         if (currSumOfSmallerSides > nums[largest]) {
    //             if (currSumOfSmallerSides + nums[largest] > bestPerimeter) {
    //                 bestPerimeter = currSumOfSmallerSides + nums[largest];
    //             }
    //             smallest--;
    //         } else {
    //             largest--;
    //             if (largest - smallest < 2)
    //                 smallest--;
    //         }
    //     }

    //     return bestPerimeter;

    // }

    private long bottomUp(int[] nums) {
        Arrays.parallelSort(nums);
        long[] curr = new long[nums.length];
        long[] prev = new long[nums.length];

        long[] presum = new long[nums.length];
        presum[0] = nums[0];
        for (int i = 1; i < nums.length; i++)
            presum[i] = presum[i - 1] + nums[i];

        for (int largest = 2; largest < nums.length; largest++) {
            for (int smallest = 0; smallest < largest - 1; smallest++) {

                // Can this become a polygon
                boolean polygon = false;
                if (smallest == 0) {
                    polygon = presum[largest - 1] > nums[largest];
                } else {
                    polygon = presum[largest - 1] - presum[smallest - 1] > nums[largest];
                }

                long polygonPerimeter = 0;

                if (polygon) {
                    if (smallest == 0) {
                        polygonPerimeter = presum[largest];
                    } else {
                        polygonPerimeter = presum[largest] - presum[smallest - 1];
                    }
                }

                // Add 1 more line
                long largestPolygonPerimeter = 0;
                if (smallest != 0)
                    largestPolygonPerimeter = curr[smallest - 1];

                // Move largest 1 back
                long smallerLargestPerimeter = 0;
                if (largest - 1 - smallest > 1) {
                    smallerLargestPerimeter = prev[smallest];
                }

                curr[smallest] = Math.max(smallerLargestPerimeter,
                        Math.max(polygonPerimeter, largestPolygonPerimeter));

            }
            prev = curr;
            curr = new long[nums.length];
        }
        return prev[nums.length-3] == 0 ? -1 : prev[nums.length-3];
    }
}


