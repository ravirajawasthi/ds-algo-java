package byteb.dynamic.programming;

import java.util.Arrays;

public class MaximumSumCircularSubArray {

    public int maxSubarraySumCircular(int[] nums) {

        // Kadane's Algo
        return maxCircularSubArraySumIterative(nums);
    }

    private int maxCircularSubArraySumIterative(int[] nums) {
        int maxSumGlobal = nums[0];
        int minSumGlobal = nums[0];

        int arrSum = 0;

        int currMaxSum = 0;
        int currMinSum = 0;

        for (int i = 0; i < nums.length; i++) {
            int currNum = nums[i];
            // Overall array sum
            arrSum += currNum;

            // Kadane for max Subarray sum
            currMaxSum += currNum;
            maxSumGlobal = Math.max(maxSumGlobal, currMaxSum);
            if (currMaxSum < 0)
                currMaxSum = 0;

            // Kadane for min subarray sum
            currMinSum += currNum;
            minSumGlobal = Math.min(currMinSum, minSumGlobal);
            if (currMinSum > 0)
                currMinSum = 0;

        }

        return Math.max(maxSumGlobal, arrSum - minSumGlobal);

    }

}
