package byteb.dynamic.programming;

import java.util.HashMap;

public class MaxScoreFromPerformingMultiplicationOperations_OLD {

    int[] nums;
    int[] multipliers;

    HashMap<String, Integer> memo;

    public int maximumScore(int[] nums, int[] multipliers) {
        this.nums = nums;
        this.multipliers = multipliers;
        this.memo = new HashMap<>();
        return maxScore(nums, multipliers);
//        return maxScore(0, nums.length - 1, 0);
    }

    private int maxScore(int startArray, int endArray, int multiplierIndex) {
        String memoKey = startArray + ":" + endArray + ":" + multiplierIndex;
        if (memo.containsKey(memoKey)) {
            return memo.get(memoKey);
        }
        if (startArray > endArray || multiplierIndex >= multipliers.length) {
            return 0;
        }
        if (endArray - startArray + 1 < multipliers.length - multiplierIndex) {
            //No point in computing further because we don't have enough numbers to use all remaining multipliers
            return 0;
        }


        int pick;

        int pickLastIndex = multipliers[multiplierIndex] * nums[endArray];
        int pickFirstIndex = multipliers[multiplierIndex] * nums[startArray];

        if (pickFirstIndex >= pickLastIndex) {
            pick = pickFirstIndex;
            startArray++;
        } else {
            pick = pickLastIndex;
            endArray--;
        }
        pick += maxScore(startArray, endArray, multiplierIndex + 1);

        memo.put(memoKey, pick);
        return memo.get(memoKey);
    }

    private int maxScore(int[] nums, int[] multipliers) {
        int[][] dp = new int[nums.length + 1][multipliers.length + 1];
        //First column will be zero because : if you are using 0 multipliers then ans will be zero
        //First Row will be when we pick all the elements from the end of array


        for (int i = 1; i <= multipliers.length; i++) {
            dp[0][i] = multipliers[i - 1] * nums[multipliers.length - i];
        }

        for (int i = 1; i <= nums.length; i++) {
            for (int j = i; j <= multipliers.length; j++) {
                int rightIndex = nums.length - 1 - (j - (i));
                int pickLeftIndex = nums[i - 1] * multipliers[j - 1] + dp[i - 1][j - 1];
                int pickRightIndex = nums[rightIndex] * multipliers[j - 1] + dp[i][j - 1];
                dp[i][j] = Math.max(pickLeftIndex, pickRightIndex);
            }
        }

        return dp[nums.length][multipliers.length];


    }


}
