package byteb.code.chef;

//https://www.codechef.com/LP1TO201/problems/MAX_DIFF

import java.util.Scanner;

public class TheTwoDishes {
    public static void main(String[] args) {

        int t;
        Scanner sc = new Scanner(System.in);

        t = sc.nextInt();
        int s;
        int l;
        int ans;
        int minPossible;


        while (t-- > 0) { //post decrement return value first then performs the operation
            ans = 0;

            l = sc.nextInt();
            s = sc.nextInt();

            if (l > s) {
                minPossible = s;
            } else {
                minPossible = s - l;
            }

            for (int i = minPossible ; i <= Math.min(s, l); i++) {
                ans = Math.max(ans, Math.abs(i - ( s - i )));
            }


            System.out.println(ans);

        }
    }
}
