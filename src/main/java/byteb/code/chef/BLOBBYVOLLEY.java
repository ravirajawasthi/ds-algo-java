package byteb.code.chef;

import java.util.HashMap;
import java.util.Scanner;

public class BLOBBYVOLLEY {

    public static void main(String[] args) {
        int t;
        int n;
        String scores;
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();
        HashMap<Character, Integer> score = new HashMap<>();
        char server;

        while (t-- > 0) {
            n = sc.nextInt();
            sc.nextLine();
            scores = sc.nextLine();
            score.put('A', 0);
            score.put('B', 0);
            server = 'A';

            for (char W : scores.toCharArray()) {
                if (W == server) {
                    score.put(W, score.get(W) + 1);
                } else {
                    server = W;
                }
            }
            System.out.println(score.get('A') + " " + score.get('B'));


        }

    }

}
