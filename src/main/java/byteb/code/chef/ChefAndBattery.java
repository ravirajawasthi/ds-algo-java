package byteb.code.chef;

import java.util.Scanner;

//https://www.codechef.com/START86D/problems/FIFTYPE
public class ChefAndBattery {
    public static void main(String[] args) {
        int t;
        int charge;
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();

        while (t > 0) {
            charge = sc.nextInt();
            if (charge == 50) {
                System.out.println(0);
            } else if (charge > 50) {
                int diff = charge - 50;
                int rem = diff % 3;
                if (rem == 1) {
                    System.out.println(( diff / 3 ) + 2);
                } else if (rem == 2) {
                    System.out.println(( diff / 3 ) + 4);
                } else {
                    System.out.println(( diff / 3 ));
                }
            } else {
                int diff = 50 - charge;
                int rem = diff % 2;
                if (rem == 1) {
                    System.out.println(( diff / 2 ) + 3);
                } else {
                    System.out.println(( diff / 2 ));
                }
            }
            t--;
        }

    }
}
