package byteb.code.chef;

import java.util.Scanner;

//https://www.codechef.com/problems/APPENDOR
public class AppendForOR {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int t = sc.nextInt();
        int n;
        int x;
        int[] arr;
        while (t > 0) {
            n = sc.nextInt();
            x = sc.nextInt();
            arr = new int[ n ];
            for (int i = 0 ; i < n ; i++)
                arr[ i ] = sc.nextInt();
            System.out.println(appendOr(arr, n, x));
        }
    }

    public static int appendOr(int[] arr, int size, int x) {
        int or = 0;
        for (int i : arr) {
            or |= i; //OR of all elements in arr
        }
        if (( ( or ^ x ) | or ) == x) {
            return or ^ x;
        } else {
            return -1;
        }
    }


}
