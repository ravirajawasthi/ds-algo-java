package byteb.code.chef;

import java.util.Scanner;

//https://www.codechef.com/START86D/problems/MAXIMSCORE
public class MaximiseScore {
    public static void main(String[] args) {
        int t;
        int n;
        int[] arr;
        int bestAliceScore;
        int newScore;
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();
        while (t > 0) {
            n = sc.nextInt();
            arr = new int[ n ];
            for (int i = 0 ; i < n ; i++) {
                arr[ i ] = sc.nextInt();
            }

            bestAliceScore = Math.abs(arr[ 0 ] - arr[ 1 ]);

            for (int i = 1 ; i < n - 1 ; i++) {
                newScore = Math.max(Math.abs(arr[ i ] - arr[ i - 1 ]),
                                    Math.abs(arr[ i ] - arr[ i + 1 ]));
                if (newScore < bestAliceScore) {
                    bestAliceScore = newScore;
                }
            }

            newScore = Math.min(bestAliceScore, Math.abs(arr[ n - 1 ] - arr[ n - 2 ]));
            if (newScore < bestAliceScore) {
                bestAliceScore = newScore;
            }
            System.out.println(bestAliceScore);


            t--;
        }

    }
}
