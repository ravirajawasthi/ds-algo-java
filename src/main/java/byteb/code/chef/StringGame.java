package byteb.code.chef;

import java.util.Scanner;

//https://www.codechef.com/START86D/problems/STRAME

//WA
public class StringGame {

    private static final String RAMOS = "Ramos"; //[True]
    private static final String ZALTAN = "Zlatan"; //[False]

    public static void main(String[] args) {
        int t;
        int size;

        char[] stringList;
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();

        while (t > 0) {
            size = sc.nextInt();
            sc.nextLine();//to remove \n
            String binaryString = sc.nextLine();
            stringList = binaryString.toCharArray();
            int c0 = 0;
            int c1 = 0;
            for (char c : stringList) {
                if (c == '0') {
                    c0++;
                } else {
                    c1++;
                }
            }


            System.out.println(( Math.min(c0, c1) % 2 == 0 ) ? RAMOS : ZALTAN);

            t--;
        }

    }
}
