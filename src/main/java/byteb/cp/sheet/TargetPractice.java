package byteb.cp.sheet;

import java.io.*;
import java.util.StringTokenizer;

public class TargetPractice {


    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int score = 0;
                for (int i = 0; i < 10; i++) {
                    String input = sc.next().strip();
                    for (int j = 0; j < 10; j++) {
                        char c = input.charAt(j);
                        if (c == 'X') {
                            if (i > 3 && i < 6 && j > 3 && j < 6) {
                                score += 5;
                            } else if (i > 2 && i < 7 && j > 2 && j < 7) {
                                score += 4;
                            } else if (i > 1 && i < 8 && j > 1 && j < 8) {
                                score += 3;
                            } else if (i > 0 && i < 9 && j > 0 && j < 9) {
                                score += 2;
                            } else {
                                score += 1;
                            }
                        }
                    }
                }
                out.println(score);
            }

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
