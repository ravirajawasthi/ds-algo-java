package byteb.cp.sheet;

import java.io.*;
import java.util.StringTokenizer;
//https://codeforces.com/problemset/problem/1903/A

public class HalloumiBoxes {
    //if swaps allowed is more than 1 then bubble sort will always work
    //Else is swaps is 1 then we gotta check if array is already sorted


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here
            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                int k = sc.nextInt();
                boolean sorted = true;
                int[] arr = new int[n];
                for (int i = 0 ;i < n; i++) arr[i] = sc.nextInt();

                if (k == 1){
                    for (int i = 1; i < n; i++){
                        if (arr[i] < arr[i-1]) {
                            sorted = false;
                            break;
                        }
                    }
                }

                out.println(sorted ? "YES" : "NO");
            }
            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
