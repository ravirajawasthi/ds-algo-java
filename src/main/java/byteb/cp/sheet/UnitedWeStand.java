package byteb.cp.sheet;

import java.io.*;
import java.util.StringTokenizer;

public class UnitedWeStand {

    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                int[] arr = new int[n];
                boolean allSame = true;
                int smallest = Integer.MAX_VALUE;
                for (int i = 0; i < n; i++) {
                    arr[i] = sc.nextInt();
                    if (i > 0 && (arr[i] != arr[i - 1])) {
                        allSame = false;
                    }
                    smallest = Math.min(smallest, arr[i]);
                }
                if (allSame)
                    out.println(-1);
                else {
                    StringBuilder sb1 = new StringBuilder();
                    StringBuilder sb2 = new StringBuilder();
                    int b1 = 0;
                    int b2 = 0;
                    for (int i : arr) {
                        if (i == smallest) {
                            sb1.append(i).append(" ");
                            b1++;
                        } else {
                            sb2.append(i).append(" ");
                            b2++;
                        }
                    }
                    out.println(b1 + " " + b2);
                    out.println(sb1);
                    out.println(sb2);
                }

            }

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}