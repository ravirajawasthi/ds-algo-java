package byteb.cp.sheet;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class DoremyPaint3 {


    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                int[] arr = new int[n];
                for (int i = 0; i < n; i++) arr[i] = sc.nextInt();

                HashMap<Integer, Integer> map = new HashMap<>();
                for (int i : arr) {
                    map.put(i, map.getOrDefault(i, 0) + 1);
                }
                if (map.keySet().size() > 2) out.println("no");
                else if (map.keySet().size() == 1) out.println("yes");
                else {
                    List<Integer> keys = new ArrayList<>(map.keySet());
                    int keya = map.get(keys.get(0));
                    int keyb = map.get(keys.get(1));
                    if (Math.abs(keya - keyb) < 2 || keya == 2 || keyb == 2) {
                        out.println("yes");
                    } else {
                        out.println("no");
                    }
                }
            }

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
