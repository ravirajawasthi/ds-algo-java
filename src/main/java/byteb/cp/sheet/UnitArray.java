package byteb.cp.sheet;

import java.io.*;
import java.util.StringTokenizer;

public class UnitArray {

    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                int[] arr = new int[n];

                int oneCount = 0;
                int minusOneCount = 0;

                for (int i = 0; i < n; i++) {
                    arr[i] = sc.nextInt();
                    if (arr[i] == 1) oneCount++;
                    else minusOneCount++;
                }

                int operations = 0;

                if (oneCount < minusOneCount) {
                    int diff = minusOneCount - oneCount;
                    int ops = (int) Math.ceil((double) diff / 2.0);
                    minusOneCount -= ops;
                    oneCount += ops;
                    operations += ops;

                }

                if (minusOneCount % 2 != 0) {
                    operations++;
                    minusOneCount--;
                    oneCount ++;
                    if (minusOneCount > oneCount) operations++;
                }
                out.println(operations);

            }

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
