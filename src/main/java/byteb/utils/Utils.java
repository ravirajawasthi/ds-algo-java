package byteb.utils;

import byteb.trees.model.BinaryTreeNode;
import byteb.trees.model.TreeNode;

import java.util.*;
import java.util.concurrent.Callable;

public class Utils {

    private Utils() {
    }


    public static <T> T timeIt(String message, Callable<T> func) throws Exception {
        var timeNow = System.currentTimeMillis();
        T response = func.call();
        var timeTaken = System.currentTimeMillis() - timeNow;
        System.out.println("Time taken for " + message + " = " + timeTaken + "ms");
        return response;
    }

    public static BinaryTreeNode<Integer> createABtFromArrayBinaryTreeNode(int[] elements) {
        int currIndex = 0;
        BinaryTreeNode<Integer> root = new BinaryTreeNode<>(elements[currIndex++]);
        Queue<BinaryTreeNode<Integer>> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            BinaryTreeNode<Integer> bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new BinaryTreeNode<>(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new BinaryTreeNode<>(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;
    }

    public static BinaryTreeNode<Integer> createABtFromArrayBinaryTreeNode(String elements) {
        return createABtFromArrayBinaryTreeNode(createArrayFromString(elements));
    }

    public static TreeNode createABtFromArrayTreeNode(int[] elements) {
        int currIndex = 0;
        TreeNode root = new TreeNode(elements[currIndex++]);
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            TreeNode bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new TreeNode(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new TreeNode(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;
    }

    public static int[] createArrayFromString(String input) {
        ArrayList<Integer> list = new ArrayList<>();
        Scanner sc = new Scanner(input);
        while (sc.hasNextInt()) {
            list.add(sc.nextInt());
        }
        int[] ans = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            ans[i] = list.get(i);
        }
        return ans;
    }


    public static ArrayList<Integer> createListFromString(String s) {
        ArrayList<Integer> list = new ArrayList<>();
        Scanner sc = new Scanner(s);
        while (sc.hasNextInt()) {
            list.add(sc.nextInt());
        }
        return list;
    }


    /**
     * reverse array
     *
     * @param arr   integer array, with start and end both INCLUSIVE
     * @param start start index, inclusive
     * @param end   end index, inclusive
     */
    public static void reverseArray(int[] arr, int start, int end) {
        if (start >= end) {
            return;
        }

        while (end > start) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }


    }

    /**
     * reverse array
     *
     * @param arr   integer array, with start and end both INCLUSIVE
     * @param start start index, inclusive
     * @param end   end index, inclusive
     */
    public static <T> void reverseArray(T[] arr, int start, int end) {
        if (start >= end) {
            return;
        }

        while (end > start) {
            T temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }


    }

    public static int[][] twoDimensionalArrayFromString(String str) {

        List<List<Integer>> list = getArrayListFromString(str);

        int rows = list.size();
        int cols = list.get(0).size();
        var arr = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                arr[i][j] = list.get(i).get(j);
            }
        }

        return arr;
    }

    public static List<List<Integer>> getArrayListFromString(String str) {
        str = str.substring(1, str.length() - 1);

        List<List<Integer>> list = new ArrayList<>();

        int multiplier = 1;
        int currNumber = 0;
        ArrayList<Integer> currList = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == ',') {
                currList.add(currNumber);
                currNumber = 0;
                multiplier = 1;
            } else if (c == ']') {
                currList.add(currNumber);
                currNumber = 0;
                multiplier = 1;
                list.add(currList);
                currList = new ArrayList<>();
                i++;
            } else if (c == '[') {
                continue;
            } else {
                currNumber += Integer.parseInt(String.valueOf(c)) * multiplier;
                multiplier *= 10;
            }

        }
        return list;
    }
}
