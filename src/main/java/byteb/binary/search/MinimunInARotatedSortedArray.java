package byteb.binary.search;

public class MinimunInARotatedSortedArray {

    public static int findMin(int[] nums) {

        int start = 0;
        int end = nums.length - 1;
        int ans = Integer.MAX_VALUE;

        while (start <= end) {
            int mid = (start + end) / 2;
            int element = nums[mid];

            var leftPartSorted = nums[start] <= nums[mid];
            var rightPartSorted = nums[mid] <= nums[end];

            if (!leftPartSorted || (rightPartSorted)) {
                ans = Math.min(ans, element);
                end = mid - 1;
            } else {
                start = mid + 1;
            }

        }
        // Should never reach here
        return ans;
    }
}
