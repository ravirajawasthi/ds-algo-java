package byteb.binary.search;

import java.util.ArrayList;
import java.util.Optional;

public class AllocateBooks {


    public static int findPages(ArrayList<Integer> arr, int n, int m) {

        int max;
        Optional<Integer> maxOptional = arr.stream().parallel().max(Integer::compare);
        if (maxOptional.isEmpty() || m > n) {
            return -1;
        } else {
            max = maxOptional.get();
        }


        for (int i : arr) {
            max = Math.max(max, i);
        }

        int start = max;
        int end = start * (int) Math.ceil((double) n / m);

        while (start <= end) {
            int mid = (start + end) / 2;
            if (checkIfPagesArePossible(arr, mid, m)) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        return start;
    }

    private static boolean checkIfPagesArePossible(ArrayList<Integer> arr, int pages, int s) {

        int allocatedStudents = 1;
        int currAllotment = 0;
        for (int i : arr) {
            if (currAllotment + i > pages) {
                allocatedStudents++;
                currAllotment = i;
            } else {
                currAllotment += i;
            }
        }

        if (allocatedStudents == s) {
            return true;
        } else return allocatedStudents < s;
    }

}
