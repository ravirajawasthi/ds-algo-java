package byteb.binary.search;

//https://www.spoj.com/problems/AGGRCOW/

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class AGGRCOW {

    public static void main(String[] args) {
        String input = """
                5 3
                1
                2
                8
                4
                9""";
        int s;
        int c;
        int[] stalls;
        try (Scanner sc = new Scanner(input)) {
            s = sc.nextInt();
            c = sc.nextInt();

            stalls = new int[s];
            IntStream.range(0, s).forEach(i -> stalls[i] = sc.nextInt());
        }
        System.out.println("largestMinimumDistance(stalls, c, s) = " + largestMinimumDistance(stalls, c, s));


    }

    private static int largestMinimumDistance(int[] stalls, int c, int n) {
        Arrays.parallelSort(stalls);

        int start = 1;
        int end = stalls[n - 1] - stalls[0]; //at edge case end = 0; hence minimum distance is 0

        while (start <= end) {
            int mid = (start + end) / 2;
            if (canBePlacedWithDistance(stalls, mid, c)) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }

        }
        return end;

    }

    private static boolean canBePlacedWithDistance(int[] stalls, int distance, int c) {
        int prev = stalls[0];
        int placed = 1;

        for (int i = 1; i < stalls.length; i++) {
            int currDistance = stalls[i] - prev;
            if (currDistance >= distance) {
                placed++;
                prev = stalls[i];
            }
            if (placed == c) {
                return true;
            }
        }

        return false;
    }

}
