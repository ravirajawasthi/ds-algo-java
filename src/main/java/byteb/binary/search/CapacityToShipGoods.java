package byteb.binary.search;

//https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/

public class CapacityToShipGoods {

    public static int shipWithinDays(int[] weights, int days) {
        int start = Integer.MIN_VALUE;
        int end = 0;
        for (int i : weights) {
            start = Math.max(start, i);
            end += i;
        }
        int max = end;

        while (start <= end) {
            int mid = (start + end) / 2;
            if (daysTakenWithinLimit(weights, mid, days)) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        if (start == max + 1) {
            return max;
        }

        return start;


    }

    private static boolean daysTakenWithinLimit(int[] weights, int capacity, int days) {
        int dayLoadedCapacity = 0;
        int daysTaken = 0;
        for (int w : weights) {
            dayLoadedCapacity += w;
            if (dayLoadedCapacity == capacity) {
                dayLoadedCapacity = 0;
                daysTaken++;
            }
            if (dayLoadedCapacity > capacity) {
                dayLoadedCapacity = w;
                daysTaken++;
            }
        }
        if (dayLoadedCapacity != 0) daysTaken++;
        return daysTaken <= days;
    }

}
