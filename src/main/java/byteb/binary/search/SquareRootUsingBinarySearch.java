package byteb.binary.search;

class SquareRootUsingBinarySearch {
    public static long floorSqrt(long x) {
        long start = 1;
        long end = x / 2;
        long ans = start;
        while (start <= end) {
            long mid = (start + end) / 2;
            long element = mid * mid;
            if (element == x) {
                return mid;
            }
            if (element < x) {
                ans = mid;
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return ans;
    }
}
