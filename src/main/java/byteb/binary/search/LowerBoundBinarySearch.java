package byteb.binary.search;

class LowerBoundBinarySearch {

    static int findFloor(long[] arr, int n, long x) {
        int start = 0;
        int end = arr.length - 1;
        int ans = -1;
        while (start <= end) {
            int mid = (start + end) / 2;
            long element = arr[mid];
            if (element == x) {
                return mid;
            } else if (element < x) {
                ans = mid;
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return ans;
    }

}
