package byteb.binary.search;

import java.util.Arrays;

public class SmallestDivisorGivenAThreshold {

    public static int smallestDivisor(int[] nums, int threshold) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i : nums) {
            min = Math.min(min, i);
            max = Math.max(max, i);
        }

        int start = min;
        int end = max;

        int mid;

        while (start <= end) {
            mid = (start + end) / 2;
            if (isSumUnderThreshold(nums, mid, threshold)) {
                end = mid - 1;
            } else {
                start = mid + 1;

            }
        }

        return start;

    }

    private static boolean isSumUnderThreshold(int[] nums, int divisor, int threshold) {
        return Arrays.stream(nums).parallel().mapToDouble(i -> Math.ceil((double) i / divisor)).sum() <= threshold;
    }

}
