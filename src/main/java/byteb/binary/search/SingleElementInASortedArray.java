package byteb.binary.search;

class SingleElementInASortedArray {

    public static int singleNonDuplicate(int[] nums) {

        int start = 0;
        int end = nums.length - 1;

        int mid;
        int element;

        if (nums.length == 1) {
            return nums[0];
        }

        if (nums[0] != nums[1]) {
            return nums[0];
        }
        if (nums[end] != nums[end - 1]) {
            return nums[end];
        }

        while (start <= end) {
            mid = (start + end) / 2;
            element = nums[mid];
            if (nums[mid + 1] != element && nums[mid - 1] != element) {
                return element;
            }
            if (mid % 2 == 0) {
                if (nums[mid + 1] == element) {
                    start = mid + 1;
                } else {
                    end = mid - 1;
                }

            } else {
                if (nums[mid - 1] == element) {
                    start = mid + 1;
                } else {
                    end = mid - 1;
                }
            }

        }
        // should never reach here
        return -1;
    }
}