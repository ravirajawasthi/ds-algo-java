package byteb.binary.search;

class HowManyTimesArrayHasBeenRotated {

    public static int findKRotation(int[] arr, int n) {

        int start = 0;
        int end = n - 1;

        int mid;
        int element;

        int ans = Integer.MAX_VALUE;
        int ansIndex = -2;

        while (start <= end) {
            mid = (start + end) / 2;
            element = arr[mid];

            boolean leftSorted = arr[start] <= element;
            boolean rightSorted = element <= arr[end];

            if (!leftSorted || rightSorted) {
                if (element <= ans) {
                    ans = element;
                    ansIndex = mid;
                }
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        return ansIndex;
    }

}