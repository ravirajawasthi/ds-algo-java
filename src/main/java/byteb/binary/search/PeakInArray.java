package byteb.binary.search;

public class PeakInArray {

    public static int findPeakElement(int[] nums) {

        int n = nums.length;

        if (n == 1) {
            return 0;
        }

        if (nums[0] > nums[1]) {
            return 0;
        }

        if (nums[n - 1] > nums[n - 2]) {
            return n - 1;
        }

        //0 and n-1 have been checked
        int start = 1;
        int end = n - 2;

        int mid, element;

        while (start <= end) {

            mid = (start + end) / 2;
            element = nums[mid];

            if (element > nums[mid - 1] && element > nums[mid + 1]) {
                return mid;
            } else if (element <= nums[mid + 1]) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }


        }
        //should not reach here
        return -1;
    }

}
