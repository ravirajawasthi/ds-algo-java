package byteb.binary.search;

//https://www.codingninjas.com/studio/problems/implement-upper-bound_8165383

public class UpperBoundBinarySearch {

    public static int upperBound(int[] arr, int x, int n) {
        int start = 0;
        int end = n - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            int element = arr[mid];
            if (element == x) {
                return mid + 1;
            } else if (element < x) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return end + 1;
    }


}
