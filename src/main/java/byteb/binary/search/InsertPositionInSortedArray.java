package byteb.binary.search;

//https://leetcode.com/problems/search-insert-position/

public class InsertPositionInSortedArray {

    public static int searchInsert(int[] nums, int target) {
        int start = 0;
        int end = nums.length - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            int element = nums[mid];

            if (element == target) {
                return mid;
            } else if (element < target) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }

        return start;

    }

}
