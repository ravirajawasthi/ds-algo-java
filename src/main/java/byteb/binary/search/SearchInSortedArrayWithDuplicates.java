package byteb.binary.search;

//https://leetcode.com/problems/search-in-rotated-sorted-array-ii/
public class SearchInSortedArrayWithDuplicates {

    public static boolean search(int[] nums, int target) {
        int start = 0;
        int end = nums.length - 1;

        int mid, element;
        while (start <= end) {

            mid = (start + end) / 2;
            element = nums[mid];

            if (element == target) {
                return true;
            }

            if (nums[start] == nums[mid] && nums[mid] == nums[end]) {
                start++;
                end--;
                continue;
            }

            var leftPartSorted = nums[start] <= element;

            if (leftPartSorted) {
                if (nums[start] <= target && target < element) {
                    end = mid - 1;
                } else {
                    start = mid + 1;
                }
            } else {
                if (element < target && target <= nums[end]) {
                    start = mid + 1;
                } else {
                    end = mid - 1;
                }
            }


        }
        return false;
    }

}
