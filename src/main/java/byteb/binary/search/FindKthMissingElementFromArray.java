package byteb.binary.search;

public class FindKthMissingElementFromArray {

    public static int findKthPositive(int[] arr, int k) {

        //Checking if missing is before the array begins
        int elementsMissingTillStart = arr[0] - 1;
        if (elementsMissingTillStart >= k) {
            return k;
        }

        //Checking element missing is after array
        int elementMissingAfterTillArrayEnd = arr[arr.length - 1] - arr.length;
        if (k > elementMissingAfterTillArrayEnd) {
            int elementsToSkip = k - elementMissingAfterTillArrayEnd;
            return arr[arr.length - 1] + elementsToSkip;
        }

        int start = 0;
        int end = arr.length - 1;

        while (start <= end) {
            int mid = (start + end) / 2;
            int element = arr[mid];

            int missingElementsToLeft = element - (mid + 1);
            if (missingElementsToLeft >= k) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        int endElement = arr[end];

        int elementsMissingTillEnd = endElement - (end + 1);
        if (elementsMissingTillEnd == k) {
            return endElement - 1;
        }
        int elementsNeedToSkip = k - elementsMissingTillEnd;
        return endElement + elementsNeedToSkip;

    }

}
