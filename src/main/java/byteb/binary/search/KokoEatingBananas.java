package byteb.binary.search;

import java.util.Arrays;

public class KokoEatingBananas {
    public static int Solve(int N, int[] piles, int H) {
        long start = 1;
        long totalBananas = Arrays.stream(piles).parallel().max().getAsInt();
        long end = totalBananas;
        long ans = totalBananas;
        while (start <= end) {
            long mid = start + (end - start) / 2;
            long hoursTaken = howManyHoursForSpeedH(piles, mid);


            if (hoursTaken <= H) {
                ans = mid;
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        return (int) ans;
    }

    private static long howManyHoursForSpeedH(int[] arr, long h) {
        long ans = 0;
        for (int n : arr) {
            ans += Math.ceil((double) n / h);
        }
        return ans;
    }
}
