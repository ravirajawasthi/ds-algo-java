package byteb.binary.search;

//https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/

//Example 1:
//
//Input: nums = [5,7,7,8,8,10], target = 8
//Output: [3,4]
//
//Example 2:
//
//Input: nums = [5,7,7,8,8,10], target = 6
//Output: [-1,-1]
//
//Example 3:
//
//Input: nums = [], target = 0
//Output: [-1,-1]

public class FirstAndLastPositionInSortedArray {

    public static int[] searchRange(int[] nums, int target) {
        int start = 0;
        int end = nums.length - 1;
        int mid;
        int element;
        int lb = -1;
        int ub = -1;

        // lowerbound
        while (start <= end) {

            mid = (start + end) / 2;
            element = nums[mid];

            if (element < target) {

                start = mid + 1;

            } else {
                if (element == target) {
                    lb = mid;
                }
                end = mid - 1;
            }

        }

        if (lb == -1) {
            return new int[] {
                    -1, -1
            };
        }

        // upperbound
        start = 0;
        end = nums.length - 1;

        while (start <= end) {

            mid = (start + end) / 2;
            element = nums[mid];

            if (element > target) {
                end = mid - 1;
            } else {
                if (element == target) {
                    ub = mid;
                }
                start = mid + 1;
            }

        }

        return new int[] { lb, ub };

    }

}
