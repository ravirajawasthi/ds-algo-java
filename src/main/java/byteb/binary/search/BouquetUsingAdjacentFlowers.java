package byteb.binary.search;

//https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/

public class BouquetUsingAdjacentFlowers {

    public static int minDays(int[] bloomDay, int m, int k) {

        if (((long) m * k) > bloomDay.length) {
            return -1;
        }

        int start = Integer.MAX_VALUE;
        int end = Integer.MIN_VALUE;

        int element;

        for (int j : bloomDay) {
            element = j;
            start = Math.min(start, element);
            end = Math.max(end, element);
        }

        while (start <= end) {
            int mid = (start + end) / 2;
            var possible = mBouquetsPossible(bloomDay, mid, m, k);

            if (possible) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }

        }
        return start;

    }

    private static boolean mBouquetsPossible(int[] bloomDay, int day, int m, int bouquetSize) {
        int count = 0;
        int currCount = 0;
        for (int i : bloomDay) {
            if (day >= i) {
                currCount++;
            } else {
                currCount = 0;
            }

            if (currCount == bouquetSize) {
                count++;
                if (count == m) {
                    return true;
                }
                currCount = 0;
            }
        }
        return false;
    }

}
