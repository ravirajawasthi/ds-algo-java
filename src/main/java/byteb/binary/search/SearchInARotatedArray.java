package byteb.binary.search;

//https://leetcode.com/problems/search-in-rotated-sorted-array/


public class SearchInARotatedArray {

    public static int search(int[] nums, int target) {
        int start = 0, end = nums.length - 1;

        int mid, element;

        while (start <= end) {

            mid = (start + end) / 2;
            element = nums[mid];

            if (element == target) {
                return mid;
            }

            var leftPartSorted = nums[start] <= nums[mid];

            if (leftPartSorted) {
                if (nums[start] <= target && target < element) {
                    end = mid - 1;
                } else {
                    start = mid + 1;
                }
            } else {
                if (nums[mid] < target && target <= nums[end]) {
                    start = mid + 1;
                } else {
                    end = mid - 1;
                }
            }

        }
        return -1;

    }
}
