package byteb.binary.search;

//https://practice.geeksforgeeks.org/problems/number-of-occurrence2259/1?utm_source=youtube&utm_medium=collab_striver_ytdescription&utm_campaign=number-of-occurrence


public class NumberOfOccurrencesInSortedArray {
    static int count(int[] arr, int n, int target) {
        int start = 0;
        int end = n - 1;
        int lb = -1, ub = -1;
        int mid, element;
        while (start <= end) {
            mid = (start + end) / 2;
            element = arr[mid];

            if (element >= target) {
                if (element == target) {
                    lb = mid;
                }
                end = mid - 1;
            } else {

                start = mid + 1;
            }

        }

        if (lb == -1) {
            return 0;
        }

        start = 0;
        end = n - 1;
        while (start <= end) {

            mid = (start + end) / 2;
            element = arr[mid];

            if (element > target) {
                end = mid - 1;
            } else {
                if (target == element) {
                    ub = mid;
                }
                start = mid + 1;
            }

        }

        return ub - lb + 1;

    }

}
