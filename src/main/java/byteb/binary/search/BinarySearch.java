package byteb.binary.search;

//https://leetcode.com/problems/binary-search/

public class BinarySearch {

    public static int search(int[] nums, int target) {
        int start = 0;
        int end = nums.length - 1;
        while (end >= start) {
            int mid = (start + end) / 2;
            int element = nums[mid];
            if (nums[mid] == target) {
                return mid;
            } else if (element > target) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        return -1;
    }

    public static boolean search(int[][] matrix, int target) {

        int potentialRow = -1;
        int colSize = matrix[0].length;

        int start = 0;
        int end = matrix.length - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            boolean smaller = matrix[mid][0] <= target;
            boolean bigger = matrix[mid][colSize - 1] >= target;
            if (smaller && bigger) {
                potentialRow = mid;
                break;
            } else if (!smaller) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        if (potentialRow == -1) {
            return false;
        }

        start = 0;
        end = colSize - 1;

        while (start <= end) {
            int mid = (start + end) / 2;
            int element = matrix[potentialRow][mid];
            if (element == target) {
                return true;
            } else if (element > target) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        return false;
    }

}
