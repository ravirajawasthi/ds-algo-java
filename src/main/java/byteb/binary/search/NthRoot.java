package byteb.binary.search;

class NthRoot {
    public static int findNthRoot(int m, int n) {
        int start = 0;
        int end = n;
        int mid;
        int element;
        while (start <= end) {
            mid = (start + end) / 2;
            element = (int) Math.pow(mid, m);

            if (element == n) {
                return mid;
            }
            if (element < n) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }

        }
        //No answer found
        return -1;
    }
}