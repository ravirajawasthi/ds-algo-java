package byteb.binary.search.cses;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class MultiplicationTable {

    private static final int MODULO = 1000000007;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();


            ArrayList<Integer> arr = new ArrayList<>();
            int counter = 0;
            for (int i = 0; i < t; i++) {
                for (int j = i; j < t; j++) {
                    if (counter == (t * t) / 2){
                        return;
                    }
                        if (i == j) counter += 1;
                        else counter += 2;
                    int num = (i + 1) * (j + 1);
                    int index = binarySearch(arr, num);
                    if (index == -1)
                        arr.add(index, num);
                }
            }
            out.println(arr.get(arr.size() / 2));


            //Solution End
        }

        private int binarySearch(ArrayList<Integer> arr, int key) {
            int low = 0;
            int high = arr.size() - 1;
            while (low <= high) {
                int mid = (low + high) / 2;
                if (arr.get(mid) > key) {
                    low = mid + 1;
                } else if (arr.get(mid) < key) {
                    high = mid - 1;
                } else {
                    return mid;
                }
            }
            return high;
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
