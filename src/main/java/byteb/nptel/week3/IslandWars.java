package byteb.nptel.week3;

import java.util.Arrays;
import java.util.Scanner;

//https://atcoder.jp/contests/abc103/tasks/abc103_d
public class IslandWars {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int islands = sc.nextInt();
        int requests = sc.nextInt();

        Pair[] reqArr = new Pair[requests];

        for (int i = 0; i < requests; i++) {
            reqArr[i] = new Pair(sc.nextInt(), sc.nextInt());
        }

        Arrays.sort(reqArr);


        int bridges = 1;
        int currBreakPoint = reqArr[0].right;
        for (int i = 1; i < requests; i++) {
            Pair currReq = reqArr[i];
            if (currReq.left >= currBreakPoint) {
                bridges++;
                currBreakPoint = currReq.right;
            }
        }
        System.out.println(bridges);

        sc.close();
    }

    private static class Pair implements Comparable<Pair> {
        public int left;
        public int right;

        public Pair(int left, int right) {
            this.left = left;
            this.right = right;
        }

        @Override
        public int compareTo(Pair o) {
            //Ascending order on right coordinate
            return Integer.compare(this.right, o.right);
        }
    }

}
