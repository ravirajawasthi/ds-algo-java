package byteb.nptel.week3;

import java.util.HashMap;
import java.util.Scanner;
import java.util.stream.IntStream;

class PowerOf2 {

    private static final String[] POWER_OF_2 = new String[]{"1", "2", "4", "8", "16", "32", "64", "128", "256", "512",
            "1024", "2048", "4096", "8192", "16384", "32768", "65536", "131072", "262144", "524288", "1048576",
            "2097152"};

    public static void main(String[] args) {
        final HashMap<String, Integer> dp = new HashMap<>();
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        sc.nextLine();
        while (t-- > 0) {
            System.out.println(convertPowerOf2(sc.nextLine()));
        }
        sc.close();
    }

    public static int createPowerOf2Recursive(String n, int steps, int maxDepth) {
        if (steps >= maxDepth) {
            return maxDepth;
        }
        if (n.isEmpty()) {
            return Integer.MAX_VALUE;
        }
        if (checkIfPowerOf2(n) && n.charAt(0) != '0') {
            return steps;
        }

        //First Op
        int firstOperation = IntStream.range(0, n.length()).map(i ->
                createPowerOf2Recursive(n.substring(0, i) + n.substring(i + 1), steps + 1, maxDepth)
        ).min().getAsInt();


        //Second Op
        int secondOperation = IntStream.range(0, 10).map(c ->
                createPowerOf2Recursive(n + String.valueOf(c), steps + 1, maxDepth)
        ).min().getAsInt();

        return Math.min(firstOperation, secondOperation);


    }

    private static boolean checkIfPowerOf2(String n) {
        double num = Math.log(Integer.parseInt(n)) / Math.log(2);
        return num == (int) num;
    }

    public static int convertPowerOf2(String n) {
        int ans = Integer.MAX_VALUE;

        for (String e : POWER_OF_2) {
            ans = Math.min(ans, getDiffFromPowerOf2(n, e));
        }

        return ans;

    }

    private static int getDiffFromPowerOf2(String n, String pow) {


        int p_n = 0;
        int p_pow = 0;

        int changes = 0;

        while (p_n < n.length() && p_pow < pow.length()) {
            if (n.charAt(p_n) == pow.charAt(p_pow)) {
                p_n++;
                p_pow++;
            } else {
                p_n++;
                changes++;
            }
        }

        if (p_pow == pow.length() && p_n == n.length()) {
            return changes;
        } else {
            changes += Math.abs(p_n - n.length() + 1) + Math.abs(p_pow - pow.length() + 1);
        }

        return changes;


    }

}
