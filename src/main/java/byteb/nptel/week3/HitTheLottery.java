package byteb.nptel.week3;

import java.util.Scanner;

public class HitTheLottery {

    private static final int[] COINS = new int[]{100, 20, 10, 5, 1};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int currCoin = 0;
        int coinUsed = 0;
        while (n > 0) {
            int coin = COINS[currCoin++];
            if (coin <= n) {
                coinUsed += n / coin;
                n = n - (n / coin) * coin;
            }
        }
        System.out.println(coinUsed);


    }

}
