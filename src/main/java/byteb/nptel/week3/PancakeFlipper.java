package byteb.nptel.week3;

import java.util.BitSet;

public class PancakeFlipper {

    public static String oversizePancakeFlipper(String c, int n) {
        BitSet pancakes = new BitSet(n);
        for (int i = 0; i < c.length(); i++) {
            if (c.charAt(i) == '+')
                pancakes.set(i);
            else
                pancakes.clear(i);
        }

        int flips = 0;
        for (int i = 0; i < c.length() - n + 1; i++) {
            if (!pancakes.get(i)) {
                pancakes.flip(i, i + n);
                flips++;
            }
        }

        for (int i = c.length() - n + 1; i < c.length(); i++) {
            if (!pancakes.get(i)) {
                return "IMPOSSIBLE";
            }
        }
        return String.valueOf(flips);


    }

}
