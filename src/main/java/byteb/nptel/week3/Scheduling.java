package byteb.nptel.week3;

import java.util.*;

public class Scheduling {
    //WA TODO
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> res = new ArrayList<>();

        int m = sc.nextInt();
        int g = sc.nextInt();
        int d = sc.nextInt();

        int[] mav = new int[m];
        int[] go = new int[g];

        IntermediarySortingRepresentation[] arr = new IntermediarySortingRepresentation[m + g];

        for (int i = 0; i < m; i++) {
            mav[i] = sc.nextInt();
            arr[i] = new IntermediarySortingRepresentation(mav[i], i, 'M');
        }
        for (int i = 0; i < g; i++) {
            go[i] = sc.nextInt();
            arr[m + i] = new IntermediarySortingRepresentation(go[i], i, 'G');
        }

        Arrays.parallelSort(arr);


        int currGIndex = 0;
        int currMIndex = 0;
        int arrIndex = 0;

        while (d > 0 && arrIndex < arr.length) {

            if (d == (m - (currMIndex)) + (g - (currGIndex))) {

                while (currMIndex < m && currGIndex < g) {
                    if (mav[currMIndex] >= go[currGIndex]) {
                        res.add(mav[currMIndex]);
                        currMIndex++;
                    } else {
                        res.add(go[currGIndex]);
                        currGIndex++;
                    }
                }
                while (currMIndex < m) {
                    res.add(mav[currMIndex++]);
                }

                while (currGIndex < g) {
                    res.add(go[currGIndex++]);
                }

                break;


            }

            IntermediarySortingRepresentation el = arr[arrIndex++];
            if (el.originArray == 'M' && el.index >= currMIndex) {
                res.add(el.element);
                currMIndex = el.index + 1;
                d--;
            } else if (el.originArray == 'G' && el.index >= currGIndex) {
                res.add(el.element);
                currGIndex = el.index + 1;
                d--;
            }
        }

        for (int e : res) {
            System.out.print(e + " ");
        }


    }

    private static class IntermediarySortingRepresentation implements Comparable<IntermediarySortingRepresentation> {
        public int element;
        public int index;
        public char originArray;

        @Override
        public String toString() {
            return "{" +
                    "element=" + element +
                    ", index=" + index +
                    ", originArray=" + originArray +
                    '}';
        }

        public IntermediarySortingRepresentation(int element, int index, char originArray) {
            this.element = element;
            this.index = index;
            this.originArray = originArray;
        }

        @Override
        public int compareTo(IntermediarySortingRepresentation o) {
            //o.element and this.element are swapped to sort array in descending order
            return Integer.compare(o.element, this.element);
        }

    }
}
