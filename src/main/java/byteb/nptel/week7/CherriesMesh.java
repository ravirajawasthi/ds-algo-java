package byteb.nptel.week7;

import java.io.*;
import java.util.StringTokenizer;

public class CherriesMesh {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        CherriesMeshTask solver = new CherriesMeshTask();
        solver.solve(in, out);
        out.close();
    }

    public static class CherriesMeshTask {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            int c = 0;
            while (c++ < t) {

                int n = sc.nextInt();
                int m = sc.nextInt();

                DSU dsu = new DSU(n);

                int blackStrands = 0;
                int redStrands = 0;

                for (int i = 0; i < m; i++) {
                    int u = sc.nextInt();
                    int v = sc.nextInt();
                    if (dsu.findParent(u) != dsu.findParent(v)) {
                        dsu.union(u, v);
                        ++blackStrands;
                    }
                }
                redStrands = n - 1 - blackStrands;


                out.println("Case #" + c + ": " + (blackStrands + redStrands * 2));
            }


        }


        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

        public static class DSU {

            private final DSUNode[] dsuNodes;

            DSU(int size) {
                dsuNodes = new DSUNode[size + 1];
                for (int i = 0; i <= size; i++) dsuNodes[i] = new DSUNode(i, i);
            }


            public void union(int u, int v) {
                var uParent = findParent(u);
                var vParent = findParent(v);
                if (uParent != vParent)
                    dsuNodes[uParent].parent = vParent;

            }

            public int findParent(int u) {
                if (u == dsuNodes[u].parent) {
                    return u;
                }
                int curr = u;

                while (curr != dsuNodes[curr].parent) {
                    curr = dsuNodes[curr].parent;
                }
                int foundParent = curr;

                curr = u;
                int temp;
                while (curr != foundParent) {
                    temp = dsuNodes[curr].parent;
                    dsuNodes[curr].parent = foundParent;
                    curr = temp;
                }

                return foundParent;
            }

            private static class DSUNode {
                public int element;
                public int parent;

                public DSUNode(int element, int parent) {
                    this.element = element;
                    this.parent = parent;
                }


            }

        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
