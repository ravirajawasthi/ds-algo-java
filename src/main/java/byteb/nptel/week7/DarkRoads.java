package byteb.nptel.week7;

import java.io.*;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

public class DarkRoads {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here
            while (true) {
                int n = sc.nextInt();
                int roads = sc.nextInt();

                if (n == 0 && roads == 0) {
                    return;
                }

                HashSet[] adj = new HashSet[n];
                IntStream.range(0, n).forEach(i -> adj[i] = new HashSet<Edge>());

                long currCost = 0;
                for (int i = 0; i < roads; i++) {
                    int u = sc.nextInt();
                    int v = sc.nextInt();
                    int w = sc.nextInt();
                    adj[u].add(new Edge(v, w));
                    adj[v].add(new Edge(u, w));
                    currCost += w;
                }


                long newCost = 0;
                PriorityQueue<Edge> pq = new PriorityQueue<>();
                boolean[] visited = new boolean[n];

                pq.add(new Edge(0, 0));

                while (!pq.isEmpty()) {
                    Edge currElement = pq.remove();
                    if (visited[currElement.v]) continue;
                    visited[currElement.v] = true;
                    newCost += currElement.w;
                    for (Object e : adj[currElement.v]) {
                        Edge edge = (Edge) e;
                        if (!visited[edge.v]) {
                            pq.add(edge);
                        }
                    }
                }

                out.println(currCost - newCost);

            }
            //Solution End
        }

        static class Edge implements Comparable<Edge> {
            int v;
            int w;

            public Edge(int v, int w) {
                this.v = v;
                this.w = w;
            }

            @Override
            public int compareTo(Edge o) {
                return Integer.compare(this.w, o.w);
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                Edge edge = (Edge) o;

                if (v != edge.v) return false;
                return w == edge.w;
            }

            @Override
            public int hashCode() {
                int result = v;
                result = 31 * result + w;
                return result;
            }
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
