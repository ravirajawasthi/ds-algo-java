package byteb.nptel.week7;

import java.io.*;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class Assignment1 {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int v = sc.nextInt();
            int e = sc.nextInt();

            ArrayList<ArrayList<Pair>> graph = new ArrayList<>();
            for (int i = 0; i < v; i++) {
                graph.add(new ArrayList<>());
            }

            for (int i = 0; i < e; i++) {
                int x = sc.nextInt();
                int y = sc.nextInt();
                int w = sc.nextInt();

                graph.get(x).add(new Pair(y, w));
                graph.get(y).add(new Pair(x, w));
            }

            boolean[] visited = new boolean[v];

            int mostWeightedConnectedComponent = 0;

            for (int i = 0; i < v; i++) {

                int cost = 0;

                if (visited[i]) continue;

                PriorityQueue<Pair> pq = new PriorityQueue<>();
                pq.add(new Pair(i, 0));
                while (!pq.isEmpty()) {
                    Pair currElement = pq.remove();
                    if (visited[currElement.u]) continue;
                    visited[currElement.u] = true;
                    cost += currElement.w;
                    for (Pair edge : graph.get(currElement.u)) {
                        if (!visited[edge.u]) {
                            pq.add(edge);
                        }
                    }
                }
                mostWeightedConnectedComponent = Math.max(mostWeightedConnectedComponent, cost);

            }
            out.print(mostWeightedConnectedComponent);


            //Solution End
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }

    private static class Pair implements Comparable<Pair> {
        int u;
        int w;

        public Pair(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Pair o) {
            return Integer.compare(this.w, o.w);
        }
    }


}
