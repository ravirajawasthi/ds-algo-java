package byteb.nptel.week7;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.StringTokenizer;

//Why UVA Submission was failing
//* DSU Implementation was wrong
//* Additional speedup to stop Kruskal missing

public class Transportation {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            for (int c = 1; c <= t; c++) {

                int n = sc.nextInt();
                int threshold = sc.nextInt();


                Coord[] coords = new Coord[n];

                for (int i = 0; i < n; i++) {
                    coords[i] = new Coord(sc.nextInt(), sc.nextInt());
                }

                ArrayList<Edge> edgeList = new ArrayList<>();

                for (int i = 0; i < n; i++) {
                    for (int j = i + 1; j < n; j++) {


                        edgeList.add(new Edge(i, j, getPythagorasDistance(coords[i], coords[j])));


                    }
                }

                edgeList.sort(Comparator.comparingDouble(e -> e.w));


                DSU dsu = new DSU(n);

                double roadsCount = 0;
                double railRoadCount = 0;
                int stateCount = 1;
                int connected = 1;
                for (Edge edge : edgeList) {


                    if (dsu.findParent(edge.u) != dsu.findParent(edge.v)) {
                        dsu.union(edge.u, edge.v);
                        if (edge.w <= threshold) {
                            roadsCount += edge.w;
                        } else {
                            stateCount++;
                            railRoadCount += edge.w;
                        }
                        connected++;
                        if (connected == n)
                            break;
                    }

                }
                out.print("Case #" + c + ": ");
                out.println(stateCount + " " + Math.round(roadsCount) + " " + Math.round(railRoadCount));


            }

            //Solution End
        }


        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }


        private Double getPythagorasDistance(Coord x, Coord y) {
            return Math.sqrt(Math.pow(x.x - y.x, 2) + Math.pow(x.y - y.y, 2));
        }

    }


    public static class DSU {

        private final DSUNode[] dsuNodes;

        DSU(int size) {
            dsuNodes = new DSUNode[size + 1];
            for (int i = 0; i <= size; i++) dsuNodes[i] = new DSUNode(i, i);
        }


        public void union(int u, int v) {
            int uParent = findParent(u);
            int vParent = findParent(v);
            if (uParent != vParent)
                dsuNodes[uParent].parent = vParent;

        }

        public int findParent(int u) {
            if (u == dsuNodes[u].parent) {
                return u;
            }
            int curr = u;

            while (curr != dsuNodes[curr].parent) {
                curr = dsuNodes[curr].parent;
            }
            int foundParent = curr;

            curr = u;
            int temp;
            while (curr != foundParent) {
                temp = dsuNodes[curr].parent;
                dsuNodes[curr].parent = foundParent;
                curr = temp;
            }

            return foundParent;
        }

        private static class DSUNode {
            public int element;
            public int parent;

            public DSUNode(int element, int parent) {
                this.element = element;
                this.parent = parent;
            }


        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


    private static class Coord {
        public double x;
        public double y;

        public Coord(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    private static class Edge implements Comparable<Edge> {
        int u;
        int v;
        double w;

        public Edge(int u, int v, double w) {
            this.u = u;
            this.v = v;
            this.w = w;
        }

        @Override
        public int compareTo(Edge o) {
            return Double.compare(this.w, o.w);
        }
    }


}
