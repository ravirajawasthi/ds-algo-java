package byteb.nptel.week7;

//UVA 1013 Island Hopping

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class IslandHoppingUVA {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here


            int c = 0;
            while (true) {
                c++;
                int islands = sc.nextInt();
                if (islands == 0) {
                    return;
                }

                ArrayList<ArrayList<Edge>> adj = new ArrayList<>();
                for (int i = 0; i <= islands; i++) {
                    adj.add(new ArrayList<>());
                }

                Islands[] islandArr = new Islands[islands + 1];
                for (int i = 1; i <= islands; i++) {
                    islandArr[i] = new Islands(sc.nextInt(), sc.nextInt(), sc.nextInt());
                }

                for (int i = 1; i <= islands; i++) {
                    for (int j = 1; j <= islands; j++) {
                        if (i == j) continue;
                        adj.get(i).add(new Edge(j, pythagorasDistance(islandArr[i], islandArr[j])));
                    }
                }


                boolean[] visited = new boolean[islands + 1];
                double[] acquisitionTime = new double[islands + 1];
                PriorityQueue<PQEdge> pq = new PriorityQueue<>();

                pq.add(new PQEdge(1, 0, islandArr[1].population));

                while (!pq.isEmpty()) {
                    PQEdge currElement = pq.remove();
                    if (visited[currElement.v]) continue;
                    visited[currElement.v] = true;
                    acquisitionTime[currElement.v] = currElement.currWeight;
                    for (Edge e : adj.get(currElement.v)) {
                        if (!visited[e.v]) {
                            pq.add(new PQEdge(e.v, e.w, Math.max(currElement.w, e.w)));
                        }
                    }

                }

                double num = 0;
                double denom = 0;
                for (int i = 1; i <= islands; i++) {
                    double time = acquisitionTime[i];
                    denom += islandArr[i].population;
                    num += (time * islandArr[i].population);
                }

                DecimalFormat df = new DecimalFormat("#.##");
                df.setMinimumFractionDigits(2);
                df.setMaximumFractionDigits(2);

                out.println("Island Group: " + c + " Average " + df.format(num / denom));


            }

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

        public static double pythagorasDistance(Islands i1, Islands i2) {
            return Math.sqrt(Math.pow(i1.x - i2.x, 2) + Math.pow(i1.y - i2.y, 2));
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    private static class Islands {
        int x;
        int y;
        int population;

        public Islands(int x, int y, int population) {
            this.x = x;
            this.y = y;
            this.population = population;
        }
    }

    private static class Edge {
        int v;
        double w;


        public Edge(int v, double w) {
            this.v = v;
            this.w = w;
        }
    }

    private static class PQEdge implements Comparable<PQEdge> {
        int v;
        double w;
        double currWeight;


        public PQEdge(int v, double w, double currWeight) {
            this.v = v;
            this.w = w;
            this.currWeight = currWeight;
        }

        @Override
        public int compareTo(PQEdge o) {
            return Double.compare(this.w, o.w);
        }
    }


}
