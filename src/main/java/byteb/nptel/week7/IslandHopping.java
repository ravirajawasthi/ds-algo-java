package byteb.nptel.week7;

import java.io.*;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

//https://open.kattis.com/problems/islandhopping
public class IslandHopping {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {

                int n = sc.nextInt();
                Coord[] coords = new Coord[n + 1];
                boolean[] visited = new boolean[n + 1];

                for (int i = 1; i <= n; i++) {
                    coords[i] = new Coord(sc.nextDouble(), sc.nextDouble());
                }


                double[][] mat = new double[n + 1][n + 1];

                for (int i = 1; i <= n; i++) {
                    for (int j = 1; j <= n; j++) {
                        if (i == j) continue;
                        mat[i][j] = getManhattanDistance(coords[i], coords[j]);
                    }
                }

                PriorityQueue<Pair> pq = new PriorityQueue<>();
                pq.add(new Pair(1, 0));

                double mstCost = 0d;

                while (!pq.isEmpty()) {
                    Pair curr = pq.remove();
                    int u = curr.x;
                    if (visited[u]) {
                        continue;
                    }
                    visited[u] = true;
                    mstCost += curr.w;
                    for (int i = 1; i <= n; i++) {
                        if (i == u) continue;
                        if (!visited[i]) {
                            pq.add(new Pair(i, mat[u][i]));
                        }
                    }
                }

                out.println(mstCost);


            }


            //Solution End
        }

        private Double getManhattanDistance(Coord x, Coord y) {
            return Math.sqrt(Math.pow(x.x - y.x, 2) + Math.pow(x.y - y.y, 2));
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    private static class Coord {
        public double x;
        public double y;

        public Coord(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }


    private static class Pair implements Comparable<Pair> {
        public int x;
        public double w;

        public Pair(int x, double w) {
            this.x = x;
            this.w = w;
        }

        @Override
        public int compareTo(Pair o) {
            return Double.compare(this.w, o.w);
        }
    }


}
