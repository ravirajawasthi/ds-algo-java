package byteb.nptel.week7;

import java.io.*;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class Assignment2 {



    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int n = sc.nextInt();
            int e = sc.nextInt();
            int c = sc.nextInt() * 2;

            ArrayList<ArrayList<Pair>> graph = new ArrayList<>(n+1);
            for (int i  = 0; i <= n; i++){
                graph.add(new ArrayList<>());
            }

            for (int i = 0; i < e ; i++){
                int u = sc.nextInt();
                int v = sc.nextInt();
                int w= sc.nextInt();
                graph.get(u).add(new Pair(v, w));
                graph.get(v).add(new Pair(u, w));
            }

            long cost = 0;

            boolean[] visited = new boolean[n+1];
            PriorityQueue<Pair> pq = new PriorityQueue<>();
            pq.add(new Pair(1, 0));
            while(!pq.isEmpty()){
                Pair currElement = pq.remove();
                if (visited[currElement.u]) continue;
                visited[currElement.u] = true;
                cost += currElement.w ;
                for (Pair edge: graph.get(currElement.u)){
                    if (!visited[edge.u]){
                        pq.add(edge);
                    }
                }
            }

            out.print(cost * c);


            //Solution End
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }

    private static class Pair implements Comparable<Pair> {
        int u;
        int w;

        public Pair(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Pair o) {
            return Integer.compare(this.w, o.w);
        }
    }







}
