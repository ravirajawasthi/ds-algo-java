package byteb.nptel.week7;

import java.io.*;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class BLINNET {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {

                int cities = sc.nextInt();

                ArrayList[] adj = new ArrayList[cities + 1];
                for (int i = 1; i <= cities; i++) {
                    adj[i] = new ArrayList<Integer>();
                }


                for (int i = 1; i <= cities; i++) {
                    String cityName = sc.next();
                    int neighbours = sc.nextInt();
                    for (int j = 0; j < neighbours; j++) {
                        adj[i].add(new Edge(sc.nextInt(), sc.nextInt()));
                    }
                }

                int linesToBeBuilt = 0;

                PriorityQueue<Edge> pq = new PriorityQueue<>();
                boolean[] visited = new boolean[cities + 1];

                pq.add(new Edge(1, 0));


                while (!pq.isEmpty()) {
                    Edge currElement = pq.remove();
                    int u = currElement.v;
                    if (visited[u]) continue;
                    visited[u] = true;
                    linesToBeBuilt += currElement.w;
                    for (Object edge : adj[u]) {
                        Edge e = (Edge) edge;
                        if (!visited[e.v]) {
                            pq.add(new Edge(e.v, e.w));
                        }

                    }
                }

                out.println(linesToBeBuilt);


            }

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    static class Edge implements Comparable<Edge> {
        int v;
        int w;

        public Edge(int v, int w) {
            this.v = v;
            this.w = w;
        }

        @Override
        public int compareTo(Edge o) {
            return Integer.compare(this.w, o.w);
        }
    }


}
