package byteb.nptel.week7;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.StringTokenizer;

public class Heirarchy {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int employees = sc.nextInt();
            int[] qualifications = new int[employees + 1];

            for (int i = 1; i <= employees; i++) {
                qualifications[i] = sc.nextInt();
            }

            DSU dsu = new DSU(employees + 1);

            int application = sc.nextInt();

            int connected = 0;

            int cost = 0;

            ArrayList<int[]> edgeList = new ArrayList<>();


            for (int i = 0; i < application; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                int c = sc.nextInt();

                if (qualifications[u] < qualifications[v]) continue;
                edgeList.add(new int[]{u, v, c});
            }

            edgeList.sort(Comparator.comparingInt(e -> e[2]));
            boolean[] supervised = new boolean[employees + 1];

            for (int[] edge : edgeList) {
                int u = edge[0];
                int v = edge[1];
                int c = edge[2];

                int uP = dsu.findParent(u);
                int vP = dsu.findParent(v);

                if (vP == uP || supervised[v]) continue;

                dsu.union(u, v);

                supervised[v] = true;

                cost += c;
                connected++;
                if (connected == employees - 1) {
                    break;
                }


            }
            if (connected != employees - 1) {
                out.println(-1);
            } else {
                out.println(cost);
            }


            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

    public static class DSU {

        private final DSUNode[] dsuNodes;

        DSU(int size) {
            dsuNodes = new DSUNode[size];
            for (int i = 0; i < size; i++) dsuNodes[i] = new DSUNode(i, i);
        }


        public void union(int u, int v) {
            var uParent = findParent(u);
            var vParent = findParent(v);
            if (uParent != vParent)
                dsuNodes[uParent].parent = vParent;

        }

        public int findParent(int u) {
            if (u == dsuNodes[u].parent) {
                return u;
            }
            int curr = u;

            while (curr != dsuNodes[curr].parent) {
                curr = dsuNodes[curr].parent;
            }
            int foundParent = curr;

            curr = u;
            int temp;
            while (curr != foundParent) {
                temp = dsuNodes[curr].parent;
                dsuNodes[curr].parent = foundParent;
                curr = temp;
            }

            return foundParent;
        }

        private static class DSUNode {
            public int element;
            public int parent;

            public DSUNode(int element, int parent) {
                this.element = element;
                this.parent = parent;
            }


        }

    }


}
