package byteb.nptel.week10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

class Assignment2 {

    // Same as coin change problem

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int n = sc.nextInt();
            int x = sc.nextInt();

            int[] weights = new int[n];
            for (int i = 0; i < n; i++)
                weights[i] = sc.nextInt();

            int[] dp = new int[x + 1];
            for (int i = 1; i <= x; i++) {
                int currAns = Integer.MAX_VALUE;
                for (int w : weights) {
                    if (w > i)
                        continue;
                    currAns = Math.min(dp[i - w], currAns);
                }
                dp[i] = currAns == Integer.MAX_VALUE ? currAns : currAns + 1;
            }

            out.print(dp[x] == Integer.MAX_VALUE ? -1 : dp[x]);

            // Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}