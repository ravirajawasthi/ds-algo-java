package byteb.nptel.week10;

import java.io.*;
import java.util.StringTokenizer;

public class Assignment1 {

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int coinsRequired = sc.nextInt();

                int[] dp = new int[coinsRequired + 1];

                for (int i = 1; i <= coinsRequired; i++) {
                    // One Coin scenario
                    int oneCoin = 1 + dp[i - 1];

                    // Double Coin Scenario
                    int doubleCoin;

                    if (i % 2 == 0)
                        doubleCoin = dp[dp[(i - 2) / 2]] + 7;
                    else
                        doubleCoin = dp[dp[(i - 1) / 2]] + 6;

                    dp[i] = Math.min(doubleCoin, oneCoin);
                }

                out.println(dp[coinsRequired]);

            }

            // Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
