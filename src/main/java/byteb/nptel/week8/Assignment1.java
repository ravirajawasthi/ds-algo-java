package byteb.nptel.week8;

import java.io.*;
import java.util.*;

public class Assignment1 {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int n = sc.nextInt();
            int e = sc.nextInt();

            ArrayList<HashMap<Integer, Integer>> graph = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                graph.add(new HashMap<>());
            }

            for (int i = 0; i < e; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                int w = sc.nextInt();
                graph.get(u).put(v, w);
                graph.get(v).put(u, 0);
            }

            long maxFlow = 0;

            while (true) {
                int[] parent = new int[n];
                boolean[] visited = new boolean[n];

                for (int i = 0; i < n; i++) parent[i] = i;

                Queue<Integer> queue = new ArrayDeque<>();

                queue.add(0);
                while (!queue.isEmpty()) {
                    int currElement = queue.remove();
                    if (visited[currElement]) continue;
                    visited[currElement] = true;
                    for (int neighbour : graph.get(currElement).keySet()) {
                        if (!visited[neighbour] && graph.get(currElement).get(neighbour) > 0) {
                            parent[neighbour] = currElement;
                            if (neighbour == n - 1) {
                                queue.clear();
                                break;
                            }
                            queue.add(neighbour);
                        }
                    }
                }
                if (parent[n - 1] == n - 1) {
                    out.print(maxFlow);
                    break;
                } else {
                    ArrayList<Integer> chain = new ArrayList<>();
                    int path = n - 1;
                    chain.add(path);
                    int bottleNeck = Integer.MAX_VALUE;
                    while (parent[path] != path) {
                        chain.add(parent[path]);
                        bottleNeck = Math.min(graph.get(parent[path]).get(path), bottleNeck);
                        path = parent[path];

                    }
                    maxFlow += bottleNeck;
                    for (int i = chain.size() - 1; i > 0; i--) {
                        int v = chain.get(i - 1);
                        int u = chain.get(i);
                        int currCapacity = graph.get(u).get(v);
                        int backwardEdge = graph.get(v).get(u);
                        currCapacity -= bottleNeck;
                        backwardEdge += bottleNeck;
                        graph.get(u).put(v, currCapacity);
                        graph.get(v).put(u, backwardEdge);
                    }

                }
            }


            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
