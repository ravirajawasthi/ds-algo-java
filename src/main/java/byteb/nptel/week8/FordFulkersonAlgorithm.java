package byteb.nptel.week8;

import java.io.*;
import java.util.*;

//https://onlinejudge.org/index.php?option=onlinejudge&Itemid=8&page=show_problem&problem=761
public class FordFulkersonAlgorithm {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int ca = 0;
            while (true) {
                ca++;
                int n = sc.nextInt();
                if (n == 0) return;

                int s = sc.nextInt();
                int t = sc.nextInt();
                int c = sc.nextInt();

                ArrayList<HashMap<Integer, Integer>> graph = new ArrayList<>();
                for (int i = 0; i <= n; i++) {
                    graph.add(new HashMap<>());
                }

                //Graph Init
                for (int i = 0; i < c; i++) {
                    int u = sc.nextInt();
                    int v = sc.nextInt();
                    int w = sc.nextInt();
                    graph.get(u).put(v, graph.get(u).getOrDefault(v, 0) + w);
                    graph.get(v).put(u, graph.get(v).getOrDefault(u, 0) + w);
                }

                long flow = 0;

                while (true) {
                    int[] parent = new int[n + 1];
                    boolean[] visited = new boolean[n + 1];
                    parent[s] = -1;
                    for (int i = 0; i <= n; i++) parent[i] = i;

                    Queue<Integer> queue = new ArrayDeque<>();
                    queue.add(s);
                    while (!queue.isEmpty()) {
                        int currElement = queue.remove();
                        if (visited[currElement]) continue;
                        visited[currElement] = true;
                        for (int neighbours : graph.get(currElement).keySet()) {
                            if (!visited[neighbours] && graph.get(currElement).get(neighbours) > 0) {
                                parent[neighbours] = currElement;
                                queue.add(neighbours);
                            }
                        }
                    }
                    //No Path from source to vertex
                    if (parent[t] == t) {
                        out.println("Network " + ca);
                        out.println("The bandwidth is " + flow + ".");
                        out.println();
                        break;
                    } else {
                        int minValuePath = Integer.MAX_VALUE;
                        ArrayList<Integer> chain = new ArrayList<>();
                        chain.add(t);
                        int path = t;
                        while (path != parent[path]) {
                            minValuePath = Math.min(graph.get(parent[path]).get(path), minValuePath);
                            chain.add(parent[path]);
                            path = parent[path];
                        }

                        for (int i = chain.size() - 1; i > 0; i--) {
                            int u = chain.get(i);
                            int v = chain.get(i - 1);
                            int currCapacity = graph.get(u).get(v);
                            int backFlow = graph.get(v).get(u);
                            currCapacity -= minValuePath;
                            backFlow += minValuePath;
                            graph.get(u).put(v, currCapacity);
                            graph.get(v).put(u, backFlow);
                        }
                        flow += minValuePath;
                    }
                }


            }

//            BFS -> Keep a parent array and while find the path find the smallest path weight in graph

            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
