package byteb.nptel.week8;

import java.io.*;
import java.util.*;

//https://codeforces.com/problemset/problem/489/B
public class BersuBall {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int bois = sc.nextInt();
            int[] dancingSkillsBois = new int[bois + 1];
            for (int i = 1; i <= bois; i++) {
                dancingSkillsBois[i] = sc.nextInt();
            }

            int girls = sc.nextInt();
            int[] dancingSkillsGirls = new int[girls + 1];
            for (int i = 1; i <= girls; i++) {
                dancingSkillsGirls[i] = sc.nextInt();
            }

            int source = 0;
            int sink = bois + girls + 1;

            ArrayList<HashMap<Integer, Integer>> graph = new ArrayList<>();

            for (int i = 0; i <= bois + girls; i++) graph.add(new HashMap<>());
            graph.add(new HashMap<>()); //<--- Sink
            //0 is source
            // bois + girls + 1 is sink
            for (int i = 1; i <= bois; i++) {
                for (int j = 1; j <= girls; j++) {
                    if (Math.abs(dancingSkillsGirls[j] - dancingSkillsBois[i]) <= 1) {
                        graph.get(i).put(bois + j, 1);
                        graph.get(bois + j).put(i, 0);
                    }
                    if (i == 1) {
                        graph.get(bois + j).put(sink, 1);
                        graph.get(sink).put(bois + j, 0);
                    }
                }
                graph.get(source).put(i, 1);
                graph.get(i).put(source, 0);

            }


            //Hinting garbage collector to do some cleanup
            dancingSkillsBois = null;
            dancingSkillsGirls = null;


            long maxFlow = 0;

            while (true) {
                boolean[] visited = new boolean[sink + 1];
                int[] parent = new int[sink + 1];
                for (int i = 0; i <= sink; i++) parent[i] = i;

                Queue<Integer> queue = new ArrayDeque<>();
                queue.add(source);

                while (!queue.isEmpty()) {
                    int currElement = queue.remove();
                    if (visited[currElement]) continue;
                    visited[currElement] = true;
                    for (int neighbour : graph.get(currElement).keySet()) {
                        if (!visited[neighbour] && graph.get(currElement).get(neighbour) > 0) {
                            parent[neighbour] = currElement;
                            if (neighbour == sink) {
                                queue.clear();
                                break;
                            }
                            queue.add(neighbour);
                        }
                    }
                }

                if (parent[sink] == sink) {
                    out.println(maxFlow);
                    break; // No Path found and we can terminate the algorithm
                } else {
                    int path = sink;
                    ArrayList<Integer> chain = new ArrayList<>();
                    chain.add(sink);
                    int bottleNeck = Integer.MAX_VALUE;
                    while (path != parent[path]) {
                        bottleNeck = Math.min(graph.get(parent[path]).get(path), bottleNeck);
                        chain.add(parent[path]);
                        path = parent[path];
                    }
                    maxFlow += bottleNeck;
                    for (int element = chain.size() - 1; element > 0; element--) {
                        int u = chain.get(element - 1);
                        int v = chain.get(element);
                        graph.get(v).put(u, 0);
                        graph.get(u).put(v, 1);
                    }
                }

            }


            //Solution End
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
