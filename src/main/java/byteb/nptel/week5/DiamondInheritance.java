package byteb.nptel.week5;

import java.io.*;
import java.util.*;

public class DiamondInheritance {
    public static void main(String[] args) throws FileNotFoundException {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        DiamondInheritanceTask solver = new DiamondInheritanceTask();
        solver.solve(in, out);
        out.close();

    }

    public static class DiamondInheritanceTask {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {

                int n = sc.nextInt();
                HashMap<Integer, HashSet<Integer>> adj = new HashMap<>();
                for (int i = 1; i <= n; i++) {
                    adj.put(i, new HashSet<>());
                }


                for (int i = 1; i <= n; i++) {
                    int edges = sc.nextInt();

                    for (int j = 0; j < edges; j++) {
                        int u = sc.nextInt();
                        adj.get(i).add(u);
                    }
                }
                boolean noDiamondInheritance = true;

                for (int startPoint = 1; startPoint <= n && noDiamondInheritance; startPoint++) {
                    boolean[] visited = new boolean[n + 1];

                    Queue<Integer> queue = new LinkedList<>();
                    queue.add(startPoint);
                    while (!queue.isEmpty() && noDiamondInheritance) {
                        int currElement = queue.remove();
                        if (visited[currElement]){
                            noDiamondInheritance = false;
                            break;
                        }
                        visited[currElement] = true;
                        for (int edge : adj.getOrDefault(currElement, new HashSet<>())) {
                            if (!visited[edge]) {
                                queue.add(edge);
                            }else{
                                noDiamondInheritance = false;
                                break;
                            }
                        }
                    }
                }
                if (noDiamondInheritance)
                    out.println("No");
                else {
                    out.println("Yes");
                }


            }

            //Solution End
        }
    }


    public static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }

}
