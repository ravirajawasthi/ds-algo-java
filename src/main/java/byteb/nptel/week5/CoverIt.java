package byteb.nptel.week5;

import java.io.*;
import java.util.*;

//https://codeforces.com/problemset/problem/1176/E

public class CoverIt {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader sc = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);

        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            int e = sc.nextInt();

            HashMap<Integer, HashSet<Integer>> aj = new HashMap<>();
            for (int i = 1; i <= n; i++) {
                aj.put(i, new HashSet<>());
            }

            for (int i = 0; i < e; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();

                aj.get(u).add(v);
                aj.get(v).add(u);
            }

            int[] color = new int[n + 1];
            Arrays.fill(color, -1);

            color[1] = 0;
            int rColor = 1;
            int bColor = 0;

            Queue<Integer> queue = new LinkedList<>();
            queue.add(1);

            while (!queue.isEmpty()) {
                int currElement = queue.remove();
                for (int edge : aj.get(currElement)) {
                    if (color[edge] == -1) {
                        int c = 1 - color[currElement];
                        if (c == 0) {
                            rColor++;
                        } else {
                            bColor++;
                        }
                        color[edge] = c;
                        queue.add(edge);
                    }
                }
            }
            int toSearch = Math.max(rColor, bColor) == rColor ? 1 : 0;
            out.println(Math.min(rColor, bColor));
            for (int i = 1; i <= n; i++) {
                if (color[i] == toSearch) {
                    out.print(i + " ");
                }
            }
            out.println();

        }
        out.close();


    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

    }
}
