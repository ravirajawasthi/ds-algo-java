package byteb.nptel.week5;

//https://codeforces.com/problemset/problem/862/B

import java.util.*;

public class MahmoudAndEhabAndTheBipartiteness {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        HashMap<Integer, ArrayList<Integer>> aj = new HashMap<>();
        for (int i = 1; i <= n; i++) {
            aj.put(i, new ArrayList<>());
        }


        int u, v;
        for (int i = 0; i < n - 1; i++) {
            u = sc.nextInt();
            v = sc.nextInt();

            aj.get(u).add(v);
            aj.get(v).add(u);
        }

        sc.close();

        long setUCount = 0;
        long setVCount = 0;


        boolean[] visited = new boolean[n + 1];

        Queue<Pair> queue = new ArrayDeque<>();
        queue.add(new Pair(1, false));

        while (!queue.isEmpty()) {
            Pair currElement = queue.remove();
            visited[currElement.element] = true;
            if (currElement.set) {
                setVCount++;
            } else {
                setUCount++;
            }
            for (int edge : aj.get(currElement.element)) {
                if (!visited[edge]) {
                    queue.add(new Pair(edge, !currElement.set));
                }
            }

        }

        System.out.println(((long) (setVCount * setUCount)) - (n - 1));


    }

    private static class Pair {
        public int element;
        public boolean set;


        public Pair(int element, boolean set) {
            this.element = element;
            this.set = set;
        }
    }

}

