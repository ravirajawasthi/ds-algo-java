package byteb.nptel.week5;

import java.util.*;

public class TreePathSum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        HashMap<Integer, ArrayList<Integer>> aj = new HashMap<>();

        int n = sc.nextInt();

        int[] nodeMapping = new int[n];
        for (int i = 0; i < n; i++) {
            nodeMapping[i] = sc.nextInt();
        }

        for (int i = 0; i < n; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            aj.put(i, new ArrayList<>(Arrays.asList(u, v)));
        }

        ArrayList<String> allPaths = dfs(nodeMapping, aj, 0);
        Long ans = 0L;
        for (String path : allPaths) {
            ans += Long.parseLong(path);
        }

        System.out.print(ans);


    }

    static ArrayList<String> dfs(int[] nodeMapping, HashMap<Integer, ArrayList<Integer>> aj, int n) {

        if (n == -1) {
            return new ArrayList<>();
        }

        int node = nodeMapping[n];

        ArrayList<Integer> children = aj.get(n);
        int left = children.get(0);
        int right = children.get(1);

        ArrayList<String> leftPath = dfs(nodeMapping, aj, left);

        ArrayList<String> rightPaths = dfs(nodeMapping, aj, right);

        if (leftPath.isEmpty() && rightPaths.isEmpty()) {
            return new ArrayList<>(List.of(String.valueOf(node)));
        } else if (leftPath.isEmpty()) {
            rightPaths.replaceAll(s -> node + s);
            return rightPaths;
        } else if (rightPaths.isEmpty()) {
            leftPath.replaceAll(s -> node + s);
            return leftPath;
        } else {
            leftPath.replaceAll(s -> node + s);
            rightPaths.replaceAll(s -> node + s);
            leftPath.addAll(rightPaths);
            return leftPath;
        }


    }


    private static class TreeNode {
        public int node;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int node, TreeNode left, TreeNode right) {
            this.node = node;
            this.left = left;
            this.right = right;
        }
    }


}
