package byteb.nptel.week5;

import java.util.*;

public class TelephoneSystem {
    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        int states = sc.nextInt();
        int connections = sc.nextInt();

        HashMap<Integer, HashSet<Integer>> aj = new HashMap<>();

        for (int i = 1; i <= states; i++) {
            aj.put(i, new HashSet<>());
        }


        for (int j = 0; j < connections; j++) {

            int u = sc.nextInt();
            int v = sc.nextInt();

            aj.get(u).add(v);
            aj.get(v).add(u);
        }

        int cc = 0;

        boolean[] visited = new boolean[states + 1];

        for (int state : aj.keySet()) {
            if (visited[state]) {
                continue;
            }

            cc++;

            ArrayDeque<Integer> stack = new ArrayDeque<>();

            stack.push(state);
            while (!stack.isEmpty()) {
                int u = stack.pop();
                visited[u] = true;
                for (int v : aj.get(u)) {
                    if (!visited[v]) {
                        stack.push(v);
                    }
                }

            }


        }
        System.out.println(cc);


    }


}
