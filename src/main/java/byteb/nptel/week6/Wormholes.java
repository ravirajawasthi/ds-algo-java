package byteb.nptel.week6;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

public class Wormholes {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            while (t-- > 0) {
                int n = sc.nextInt();
                int e = sc.nextInt();

                long[] dist = new long[n];
                Arrays.fill(dist, Long.MAX_VALUE);
                dist[0] = 0;

                ArrayList<Edge> edgeList = new ArrayList<>();
                IntStream.range(0, e).forEach(
                        i -> {
                            int u = sc.nextInt();
                            int v = sc.nextInt();
                            int w = sc.nextInt();
                            edgeList.add(new Edge(u, v, w));
                        }
                );

                for (int i = 0; i < n; i++) {
                    boolean flag = false;
                    for (Edge edge : edgeList) {
                        int u = edge.u;
                        int v = edge.v;
                        int w = edge.w;

                        if (dist[v] > dist[u] + w) {
                            dist[v] = dist[u] + w;
                            flag = true;
                        }
                    }
                    if (!flag) {
                        break;
                    }
                }

                boolean negativeCycle = false;


                for (Edge edge : edgeList) {
                    int u = edge.u;
                    int v = edge.v;
                    int w = edge.w;
                    if (dist[v] > dist[u] + w) {
                        negativeCycle = true;
                        break;
                    }
                }


                if (negativeCycle) {
                    out.println("possible");
                } else {
                    out.println("not possible");
                }

            }

            //Solution End
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }

    private static class Edge {
        public int v;
        public int u;
        public int w;


        public Edge(int u, int v, int w) {
            this.u = u;
            this.v = v;
            this.w = w;
        }
    }


}
