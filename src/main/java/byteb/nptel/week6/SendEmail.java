package byteb.nptel.week6;

import java.io.*;
import java.util.*;

public class SendEmail {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int t = sc.nextInt();
            for (int c = 1; c <= t; c++) {
                int n = sc.nextInt();
                int m = sc.nextInt();
                int source = sc.nextInt();
                int destination = sc.nextInt();

                HashMap<Integer, ArrayList<Pair>> adj = new HashMap<>();
                for (int i = 0; i < n; i++) {
                    adj.put(i, new ArrayList<>());
                }

                for (int i = 0; i < m; i++) {
                    //U -> V with weight w
                    int u = sc.nextInt();
                    int v = sc.nextInt();
                    int w = sc.nextInt();

                    adj.get(u).add(new Pair(v, w));
                    adj.get(v).add(new Pair(u, w));
                }


                int[] dist = new int[n];
                Arrays.fill(dist, Integer.MAX_VALUE);

                PriorityQueue<Pair> pq = new PriorityQueue<>();

                dist[source] = 0;
                pq.add(new Pair(source, 0));

                while (!pq.isEmpty()) {
                    int u = pq.remove().u;

                    for (Pair edge : adj.get(u)) {
                        int v = edge.u;
                        if (dist[v] > edge.weight + dist[u]) {
                            dist[v] = edge.weight + dist[u];
                            pq.add(new Pair(v, dist[v]));
                        }
                    }
                }
                out.print("Case #" + c + ": ");
                out.println(dist[destination] == Integer.MAX_VALUE ? "unreachable" : dist[destination]);
            }

            //Solution End
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }


    private static class Pair implements Comparable<Pair> {
        public int u;
        public int weight;

        public Pair(int u, int weight) {
            this.u = u;
            this.weight = weight;
        }

        @Override
        public int compareTo(Pair o) {
            return Integer.compare(this.weight, o.weight);
        }
    }


}
