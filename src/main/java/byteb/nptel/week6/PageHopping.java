package byteb.nptel.week6;

import java.io.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;

//https://onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=762
//APSP
//WA due to Decimal rounding off
public class PageHopping {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here
            int c = 1;

            while (true) {
                ArrayList<int[]> edgeList = new ArrayList<>();
                HashSet<Integer> vertices = new HashSet<>();

                long[][] dist = new long[101][101];
                for (long[] arr : dist) {
                    Arrays.fill(arr, Integer.MAX_VALUE);
                }

                while (true) {
                    int u = sc.nextInt();
                    int v = sc.nextInt();
                    if (u == 0 && v == 0) {
                        break;
                    }
                    vertices.add(u);
                    vertices.add(v);
                    dist[u][v] = 1;
                    edgeList.add(new int[]{u, v});
                }

                if (edgeList.isEmpty()) {
                    return;
                }

                for (int r : vertices) {
                    for (int u : vertices) {
                        if (r == u) continue;
                        for (int v : vertices) {
                            if (r == v) continue;
                            dist[u][v] = Math.min(dist[u][v], dist[u][r] + dist[r][v]);
                        }
                    }

                }
                long sum = 0;
                for (int u : vertices) {
                    for (int v : vertices) {
                        if (u != v)
                            sum += dist[u][v] > Integer.MAX_VALUE ? 0 : dist[u][v];
                    }
                }

                long n = (long) vertices.size() * (vertices.size() - 1);
                DecimalFormat df = new DecimalFormat("#.###");
                df.setRoundingMode(RoundingMode.CEILING);
                df.setMinimumFractionDigits(3);

                String ans = String.valueOf(df.format((double) sum / n));

                out.println("Case " + c + ": average length between pages = " + ans + " clicks");
                c++;


            }

            //Solution End
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }


}
