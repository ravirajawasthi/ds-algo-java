package byteb.nptel.week6;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

public class ProgrammingAssignment1 {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int nodes = sc.nextInt();
            int edges = sc.nextInt();

            ArrayList<ArrayList<Integer>> adj = new ArrayList<>(nodes + 1);
            IntStream.range(0, nodes + 1).forEach(i -> adj.add(new ArrayList<>()));

            IntStream.range(0, edges).forEach(edge -> {
                int u = sc.nextInt();
                int v = sc.nextInt();
                adj.get(u).add(v);
                adj.get(v).add(u);
            });


            PriorityQueue<Pair> priorityQueue = new PriorityQueue<>();
            int[] dist = new int[nodes + 1];
            boolean[] visited = new boolean[nodes + 1];
            Arrays.fill(dist, Integer.MAX_VALUE);

            dist[1] = 0;
            priorityQueue.add(new Pair(1, 1));


            while (!priorityQueue.isEmpty()) {
                int currElement = priorityQueue.remove().u;
                if (!visited[currElement]) {
                    out.print(currElement + " ");
                }
                visited[currElement] = true;

                for (int edge : adj.get(currElement)) {
                    if (dist[edge] > dist[currElement] + 1) {
                        dist[edge] = dist[currElement] + 1;
                        priorityQueue.add(new Pair(edge, edge));
                    }
                }
            }

            //Solution End
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }

    private static class Pair implements Comparable<Pair> {
        public int u;
        public int weight;

        public Pair(int u, int weight) {
            this.u = u;
            this.weight = weight;
        }

        @Override
        public int compareTo(Pair o) {
            //USING LONG IS SUPER DUPER IMPORTANT HERE
            //Looks like in case of overflow java has a lot of performance penalty
            //Long prevents overflow and gives Accepted in 1/3 time!!
            return Integer.compare(this.weight, o.weight);
        }
    }


}
