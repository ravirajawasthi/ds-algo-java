package byteb.nptel.week6;

import java.io.*;
import java.util.*;

public class Dijkstra {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int n = sc.nextInt();
            int e = sc.nextInt();


            HashMap<Integer, ArrayList<Pair>> adj = new HashMap<>();

            for (int i = 1; i <= n; i++) {
                adj.put(i, new ArrayList<>());
            }

            for (int i = 0; i < e; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                int w = sc.nextInt();

                adj.get(u).add(new Pair(v, w));
                adj.get(v).add(new Pair(u, w));
            }

            long[] dist = new long[n + 1];
            Arrays.fill(dist, Long.MAX_VALUE);
            dist[1] = 0;

            int[] path = new int[n + 1];
            path[1] = 1;

            PriorityQueue<Pair> pq = new PriorityQueue<>();
            pq.add(new Pair(1, 0));

            while (!pq.isEmpty()) {
                int u = pq.remove().u;
                for (Pair edge : adj.get(u)) {
                    int v = edge.u;
                    long w = edge.weight;
                    if (dist[v] > dist[u] + w) {
                        dist[v] = dist[u] + w;
                        path[v] = u;
                        pq.add(new Pair(v, dist[v]));
                    }
                }
            }

            if (path[n] == 0) {
                out.println(-1);
            } else {
                ArrayDeque<Integer> p = new ArrayDeque<>();
                int curr = n;
                while (path[curr] != curr) {
                    p.push(curr);
                    curr = path[curr];
                }
                p.push(curr);
                while (!p.isEmpty()) {
                    out.print(p.pop() + " ");
                }

            }


            //Solution End
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }

    private static class Pair implements Comparable<Pair> {
        public int u;
        public long weight;

        public Pair(int u, long weight) {
            this.u = u;
            this.weight = weight;
        }

        @Override
        public int compareTo(Pair o) {
            //USING LONG IS SUPER DUPER IMPORTANT HERE
            //Looks like in case of overflow java has a lot of performance penalty
            //Long prevents overflow and gives Accepted in 1/3 time!!
            return Long.compare(this.weight, o.weight);
        }
    }


}
