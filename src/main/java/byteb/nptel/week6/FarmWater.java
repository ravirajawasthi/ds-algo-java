package byteb.nptel.week6;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class FarmWater {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int n = sc.nextInt();
            int source = sc.nextInt();

            ArrayList<ArrayList<Pair>> adj = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                adj.add(new ArrayList<>());
            }

            for (int u = 0; u < n; u++) {
                int edges = sc.nextInt();
                while (edges-- > 0) {
                    int v = sc.nextInt();
                    int w = sc.nextInt();
                    adj.get(u).add(new Pair(v, w));
                }
            }

            int[] dist = new int[n];
            Arrays.fill(dist, Integer.MAX_VALUE);
            dist[source] = 0;

            PriorityQueue<Pair> pq = new PriorityQueue<>();
            pq.add(new Pair(source, 0));

            while (!pq.isEmpty()) {
                int u = pq.remove().u;

                for (Pair edge : adj.get(u)) {
                    if (dist[edge.u] > dist[u] + edge.weight) {
                        dist[edge.u] = dist[u] + edge.weight;
                        pq.add(new Pair(edge.u, dist[edge.u]));
                    }
                }
            }

            int ans = Integer.MIN_VALUE;
            for (int e : dist) {
                if (e != Integer.MAX_VALUE) {
                    ans = Math.max(ans, e);
                }else{
                    out.print(-1);
                    return;
                }
            }
            if (ans==0){
                out.print(-1);
            }else{
                out.print(ans);
            }

            //Solution End
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

    }

    private static class Pair implements Comparable<Pair> {
        public int u;
        public int weight;

        public Pair(int u, int weight) {
            this.u = u;
            this.weight = weight;
        }

        @Override
        public int compareTo(Pair o) {
            //USING LONG IS SUPER DUPER IMPORTANT HERE
            //Looks like in case of overflow java has a lot of performance penalty
            //Long prevents overflow and gives Accepted in 1/3 time!!
            return Integer.compare(this.weight, o.weight);
        }
    }


}
