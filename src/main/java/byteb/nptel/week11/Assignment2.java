package byteb.nptel.week11;

import java.io.*;
import java.util.HashMap;
import java.util.StringTokenizer;

public class Assignment2 {
    static int[] arr;
    static HashMap<Integer, Integer> memo;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            // Solution Here

            int n = sc.nextInt();
            arr = new int[n];
            memo = new HashMap<>();
            for (int i = 0; i < n; i++)
                arr[i] = sc.nextInt();

            out.print(1 + dp(1, arr[0] - 1));

            // Solution End
        }

        int dp(int index, int currFuel) {

            if (index == arr.length - 1)
                return 0;
            

            int notStop = Integer.MAX_VALUE;
            if (currFuel > 0)
                notStop = dp(index + 1, currFuel - 1);
            int stop = 1 + dp(index + 1, currFuel + arr[index]-1);


            return Math.min(stop, notStop);

        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }

}
