package byteb.nptel.week9;

//https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=2414

import java.io.*;
import java.util.*;

public class SAMIAM {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int r = sc.nextInt();
            int c = sc.nextInt();
            int enemies = sc.nextInt();
            if (r == 0 && c == 0 && enemies == 0) {
                return;
            }

            int source = 0;
            int sink = r + c + 1;

            ArrayList<HashMap<Integer, Integer>> graph = new ArrayList<>();
            for (int i = 0; i <= sink; i++) graph.add(new HashMap<>());


            for (int i = 1; i <= r; i++) {
                graph.get(source).put(i, 1);
                graph.get(i).put(source, 1);
            }

            for (int i = r + 1; i <= r + c; i++) {
                graph.get(i).put(sink, 1);
                graph.get(sink).put(i, 0);
            }

            for (int i = 0; i < enemies; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                graph.get(u).put(v, Integer.MAX_VALUE);
                graph.get(v).put(u, 0);
            }

            while (true) {
                Queue<Integer> queue = new ArrayDeque<>();
                queue.add(0);
                int[] parent = new int[sink + 1];
                for (int i = 0; i <= sink; i++) parent[i] = i;
                boolean[] visited = new boolean[sink + 1];
                while (!queue.isEmpty()) {
                    int u = queue.remove();
                    if (visited[u]) continue;
                    visited[u] = true;
                    for (int v : graph.get(u).keySet()) {
                        if (!visited[v] && graph.get(u).get(v) > 0) {
                            parent[v] = u;
                            if (v == sink) {
                                queue.clear();
                                break;
                            }
                            queue.add(v);
                        }
                    }
                }
                if (parent[sink] == sink) {
                    HashSet<Integer> reachable = reachable(graph);

                    for (int i = 1; i <= r; i++) {
                        if (!reachable.contains(i)) {
                            out.print("r" + i + " ");
                        }
                    }

                    for (int i = r + 1; i < sink; i++) {
                        if (reachable.contains(i)) {
                            out.print("c" + i + " ");
                        }
                    }

                    break;
                } else {
                    ArrayList<Integer> path = new ArrayList<>();
                    path.add(sink);
                    int curr = sink;
                    while (curr != parent[curr]) {
                        path.add(parent[curr]);
                        curr = parent[curr];
                    }
                    for (int i = path.size() - 1; i > 0; i--) {
                        int u = path.get(i);
                        int v = path.get(i - 1);
                        graph.get(u).put(v, graph.get(u).get(v) - 1);
                        graph.get(v).put(u, graph.get(v).get(u) + 1);
                    }
                }

            }

            //Solution End
        }

        HashSet<Integer> reachable(ArrayList<HashMap<Integer, Integer>> graph) {
            HashSet<Integer> reachable = new HashSet<>();
            Queue<Integer> queue = new ArrayDeque<>();
            queue.add(0);
            while (!queue.isEmpty()) {
                int currElement = queue.remove();
                if (reachable.contains(currElement)) continue;
                reachable.add(currElement);
                for (int neighbour : graph.get(currElement).keySet()) {
                    if (!reachable.contains(neighbour) && graph.get(currElement).get(neighbour) > 0) {
                        queue.add(neighbour);
                    }
                }
            }
            return reachable;

        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
