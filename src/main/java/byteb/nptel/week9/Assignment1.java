package byteb.nptel.week9;

import java.io.*;
import java.util.*;

public class Assignment1 {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int n = sc.nextInt();
            int m = sc.nextInt();
            int t = sc.nextInt();

            ArrayList<HashMap<Integer, Integer>> graph = new ArrayList<>();
            for (int i = 0; i <= n; i++) graph.add(new HashMap<>());

            int neighbourOf0 = sc.nextInt();

            for (int i = 0; i < neighbourOf0; i++) {
                int v = sc.nextInt();
                graph.get(0).put(v, 1);
                graph.get(v).put(0, 1);
            }

            for (int i = 0; i < m; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();

                graph.get(u).put(v, 1);
                graph.get(v).put(u, 1);
            }

            while (true) {
                boolean[] visited = new boolean[n + 1];
                int[] parent = new int[n + 1];
                for (int i = 0; i <= n; i++) parent[i] = i;
                Queue<Integer> queue = new ArrayDeque<>();
                queue.add(0);
                while (!queue.isEmpty()) {
                    int currElement = queue.remove();
                    if (visited[currElement]) continue;
                    visited[currElement] = true;
                    for (int neighbour : graph.get(currElement).keySet()) {
                        if (!visited[neighbour] && graph.get(currElement).get(neighbour) > 0) {
                            parent[neighbour] = currElement;
                            if (neighbour == t) {
                                queue.clear();
                                break;
                            }
                            queue.add(neighbour);
                        }
                    }
                }

                if (parent[t] == t) {
                    int java = 0;
                    HashSet<Integer> reachable = reachable(graph, 0);
                    for (int i : reachable) {
                        for (int neighbour : graph.get(i).keySet()) {
                            if (!reachable.contains(neighbour) && graph.get(i).get(neighbour) == 0) {
                                java++;
                            }
                        }
                    }
                    out.print(java);
                    break;
                } else {
                    ArrayList<Integer> path = new ArrayList<>();
                    path.add(t);
                    int curr = t;
                    while (parent[curr] != curr) {
                        path.add(parent[curr]);
                        curr = parent[curr];
                    }
                    //All edge weight are 1 so bottleneck will also be 1
                    for (int i = path.size() - 1; i > 0; i--) {
                        int v = path.get(i - 1);
                        int u = path.get(i);
                        graph.get(u).put(v, graph.get(u).get(v) - 1);
                        graph.get(v).put(u, graph.get(v).get(u) + 1);
                    }

                }

            }


            //Solution End
        }

        HashSet<Integer> reachable(ArrayList<HashMap<Integer, Integer>> graph, int source) {
            HashSet<Integer> reachable = new HashSet<>();
            Queue<Integer> queue = new ArrayDeque<>();
            queue.add(source);
            while (!queue.isEmpty()) {
                int currElement = queue.remove();
                if (reachable.contains(currElement)) continue;
                reachable.add(currElement);
                for (int neighbour : graph.get(currElement).keySet()) {
                    if (!reachable.contains(neighbour) && graph.get(currElement).get(neighbour) > 0) {
                        queue.add(neighbour);
                    }
                }
            }
            return reachable;

        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}
