package byteb.nptel.week9;

import java.io.*;
import java.util.*;

public class PoliceChase {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(in, out);
        out.close();
    }

    static class Task {
        public void solve(InputReader sc, PrintWriter out) {
            //Solution Here

            int n = sc.nextInt();
            int e = sc.nextInt();

            ArrayList<HashMap<Integer, Integer>> graph = new ArrayList<>();
            for (int i = 0; i <= n; i++) graph.add(new HashMap<>());

            for (int i = 0; i < e; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                graph.get(u).put(v, 1);
                graph.get(v).put(u, 1);
            }

            ArrayList<int[]> edgedToBeRemoved = new ArrayList<int[]>();
            while (true) {
                Queue<Integer> queue = new LinkedList<>();
                queue.add(1);

                int[] parent = new int[n + 1];
                boolean[] visited = new boolean[n + 1];
                visited[1] = true;
                for (int i = 1; i <= n; i++) parent[i] = i;

                while (!queue.isEmpty()) {
                    int currElement = queue.remove();
                    for (int neighbour : graph.get(currElement).keySet()) {
                        if (!visited[neighbour] && graph.get(currElement).get(neighbour) > 0) {
                            visited[neighbour] = true;
                            parent[neighbour] = currElement;
                            if (neighbour == n) { //We need the shortest path till neighbour
                                queue.clear();
                                break;
                            }
                            queue.add(neighbour);
                        }
                    }
                }

                if (parent[n] == n) {
                    HashSet<Integer> reachable = reachableFromSource(graph);
                    int count = 0;
                    ArrayList<int[]> STCut = new ArrayList<>();
                    for (int i = 1; i <= n; i++) {
                        for (int neighbour : graph.get(i).keySet()) {
                            if (reachable.contains(i) && !reachable.contains(neighbour)) {
                                count++;
                                STCut.add(new int[]{i, neighbour});
                            }
                        }
                    }
                    out.println(count);
                    for (int[] edge : STCut) {
                        out.println(edge[0] + " " + edge[1]);
                    }
                    break;
                } else {
                    ArrayList<Integer> path = new ArrayList<>();
                    int curr = n;
                    int bottleneck = Integer.MAX_VALUE;
                    path.add(curr);
                    while (curr != parent[curr]) {
                        path.add(parent[curr]);
                        if (graph.get(parent[curr]).get(curr) < bottleneck) {
                            bottleneck = graph.get(parent[curr]).get(curr);

                        }
                        curr = parent[curr];
                    }
                    for (int i = path.size() - 2; i > -1; i--) {
                        int v = path.get(i);
                        int u = path.get(i + 1);
                        graph.get(u).put(v, Math.max(0, graph.get(u).get(v) - 1)); //Reducing forward capacity
                        graph.get(v).put(u, graph.get(v).get(u) + 1); //Residual capacity in case we need to reverse flow
                    }

                }
            }


            //Solution End
        }

        HashSet<Integer> reachableFromSource(ArrayList<HashMap<Integer, Integer>> graph) {
            HashSet<Integer> reachable = new HashSet<>();
            Queue<Integer> queue = new LinkedList<>();
            queue.add(1);
            while (!queue.isEmpty()) {
                int currElement = queue.remove();
                if (reachable.contains(currElement)) continue;
                reachable.add(currElement);
                for (int neighbour : graph.get(currElement).keySet()) {
                    if (!reachable.contains(neighbour) && graph.get(currElement).get(neighbour) > 0) {
                        queue.add(neighbour);
                    }
                }
            }
            return reachable;
        }

        public static long lcm(long a, long b) {
            return Math.abs(a * b) / gcd(a, b);
        }

        public static long gcd(long a, long b) {
            while (b != 0) {
                long temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }

    }


}