package byteb.nptel.week4;

import java.util.Scanner;

public class DSUImpl {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int ops = sc.nextInt();

        var dsu = new DSU(n);

        sc.nextLine();
        int u, v;
        String[] operation;
        String operationString;
        for (int i = 0; i < ops; i++) {
            operation = sc.nextLine().split(" ");
            operationString = operation[0];
            u = Integer.parseInt(operation[1]);
            v = Integer.parseInt(operation[2]);
            if (operationString.equals("union")) {
                dsu.union(u, v);
            } else {
                System.out.println(dsu.findParent(u) == dsu.findParent(v) ? "YES" : "NO");
            }
        }

    }


    public static class DSU {

        private final DSUNode[] dsuNodes;

        DSU(int size) {
            dsuNodes = new DSUNode[size + 1];
            for (int i = 1; i <= size; i++) dsuNodes[i] = new DSUNode(i, i);
        }


        public void union(int u, int v) {
            var uParent = findParent(u);
            var vParent = findParent(v);
            if (uParent != vParent)
                dsuNodes[uParent].parent = vParent;

        }

        public int findParent(int u) {
            if (u == dsuNodes[u].parent) {
                return u;
            }
            int curr = u;

            while (curr != dsuNodes[curr].parent) {
                curr = dsuNodes[curr].parent;
            }
            int foundParent = curr;

            curr = u;
            int temp;
            while (curr != foundParent) {
                temp = dsuNodes[curr].parent;
                dsuNodes[curr].parent = foundParent;
                curr = temp;
            }

            return foundParent;
        }

        private static class DSUNode {
            public int element;
            public int parent;

            public DSUNode(int element, int parent) {
                this.element = element;
                this.parent = parent;
            }


        }

    }


}
