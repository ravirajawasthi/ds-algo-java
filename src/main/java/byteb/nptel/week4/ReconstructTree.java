package byteb.nptel.week4;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class ReconstructTree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        HashMap<Integer, Integer> map = new HashMap<>();
        HashSet<Integer> set = new HashSet<>();

        int[] queue = new int[n];

        for (int i = 0; i < n; i++) {
            int forward = sc.nextInt();
            int backward = sc.nextInt();
            if (forward == 0) {
                queue[1] = backward;
            } else {
                map.put(forward, backward);
                set.add(forward);
            }

        }


        for (int key : map.keySet()) {
            set.remove(map.get(key));
        }


        for (int element : set) {
            if (element != queue[1]) {
                queue[0] = element;
                break;
            }
        }
        int currIndex = 0;
        int currQueueIndex = 2;
        while (currQueueIndex < n) {
            queue[currQueueIndex++] = map.get(queue[currIndex]);
            currIndex++;
        }

        for (int i = 0; i < queue.length - 1; i++) {
            System.out.print(queue[i] + " ");
        }
        System.out.print(queue[queue.length - 1]);


    }


}
