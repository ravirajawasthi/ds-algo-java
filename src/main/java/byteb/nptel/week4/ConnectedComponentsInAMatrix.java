package byteb.nptel.week4;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class ConnectedComponentsInAMatrix {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int rows = sc.nextInt();
        int cols = sc.nextInt();

        int[][] mat = new int[rows][cols];
        boolean[][] visited = new boolean[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                mat[i][j] = sc.nextInt();
            }
        }

        long maxSumConnectedComponent = Long.MIN_VALUE;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (visited[i][j] || mat[i][j] == 0) {
                    continue;
                }

                Queue<Pair> queue = new LinkedList<>();
                queue.add(new Pair(i, j));
                long connectedComponentSum = 0;
                while (!queue.isEmpty()) {
                    Pair element = queue.remove();
                    int x = element.x;
                    int y = element.y;
                    if (visited[x][y]) {
                        continue;
                    }
                    visited[x][y] = true;
                    connectedComponentSum += mat[x][y];
                    if (x + 1 < rows && mat[x + 1][y] != 0 && !visited[x + 1][y]) {
                        queue.add(new Pair(x + 1, y));
                    }
                    if (x - 1 > -1 && mat[x - 1][y] != 0 && !visited[x - 1][y]) {
                        queue.add(new Pair(x - 1, y));
                    }
                    if (y + 1 < cols && mat[x][y + 1] != 0 && !visited[x][y + 1]) {
                        queue.add(new Pair(x, y + 1));
                    }
                    if (y - 1 > -1 && mat[x][y - 1] != 0 && !visited[x][y - 1]) {
                        queue.add(new Pair(x, y - 1));
                    }


                }

                maxSumConnectedComponent = Math.max(connectedComponentSum, maxSumConnectedComponent);

            }


        }
        System.out.println(maxSumConnectedComponent);


    }

    private static class Pair {
        public int x;
        public int y;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (x != pair.x) return false;
            return y == pair.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }
    }

}
