package byteb.nptel.week4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class DSUMinMaxImpl {
    public static void main(String[] args) throws IOException {
        FastReader s = new FastReader();
        int n = s.nextInt();
        int ops = s.nextInt();

        var dsu = new DSU(n);

        int u, v;
        String[] operation;
        String operationString;
        for (int i = 0; i < ops; i++) {
            operation = s.nextLine().split(" ");
            operationString = operation[0];
            u = Integer.parseInt(operation[1]);
            if (operationString.equals("union")) {
                v = Integer.parseInt(operation[2]);
                dsu.union(u, v);
            } else {
                System.out.println(dsu.getNodeInfo(dsu.findParent(u)));
            }
        }

    }


    public static class DSU {

        private final DSUNode[] dsuNodes;

        DSU(int size) {
            dsuNodes = new DSUNode[size + 1];
            for (int i = 1; i <= size; i++) dsuNodes[i] = new DSUNode(i);
        }


        public void union(int u, int v) {
            var uParent = findParent(u);
            var vParent = findParent(v);
            if (uParent != vParent) {
                dsuNodes[uParent].parent = vParent;
                var vParentNode = dsuNodes[vParent];
                vParentNode.size += dsuNodes[uParent].size;
                vParentNode.minElement = Math.min(vParentNode.minElement, dsuNodes[uParent].minElement);
                vParentNode.maxElement = Math.max(vParentNode.maxElement, dsuNodes[uParent].maxElement);
            }


        }

        public int findParent(int u) {
            if (u == dsuNodes[u].parent) {
                return u;
            }
            int curr = u;

            while (curr != dsuNodes[curr].parent) {
                curr = dsuNodes[curr].parent;
            }
            int foundParent = curr;

            curr = u;
            int temp;
            while (curr != foundParent) {
                temp = dsuNodes[curr].parent;
                dsuNodes[curr].parent = foundParent;
                curr = temp;
            }

            return foundParent;
        }

        public String getNodeInfo(int n) {
            if (n < 1 || n > this.dsuNodes.length - 1) {
                return null;
            } else {
                var node = dsuNodes[n];
                return String.format("%s %s %s", node.minElement, node.maxElement, node.size);
            }
        }

        private static class DSUNode {
            public int element;
            public int parent;
            public int size;
            public int minElement;
            public int maxElement;


            public DSUNode(int element) {
                this.element = element;
                this.parent = element;
                this.minElement = element;
                this.maxElement = element;
                this.size = 1;
            }


        }

    }

    static class FastReader {
        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(
                    new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        long nextLong() {
            return Long.parseLong(next());
        }

        double nextDouble() {
            return Double.parseDouble(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }


}

