package byteb.nptel.week2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class SortPartially {
    //WA TODO
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int p = sc.nextInt();

        int times = p * (n / p);
        int rem = n % p;

        int[] container = new int[p];
        int[] smallContainer = new int[rem];
        ArrayList<Integer> res = new ArrayList<>();

        int currCount = 0;

        for (int i = 0; i < times; i++) {
            container[currCount++] = sc.nextInt();
            if (currCount == p) {
                currCount = 0;
                Arrays.parallelSort(container);
                for (int e : container) {
                    res.add(e);
                }
            }
        }

        currCount = 0;
        for (int i = 0; i < rem; i++) {
            smallContainer[currCount++] = sc.nextInt();
        }

        Arrays.parallelSort(smallContainer);
        for (int e : smallContainer) {
            res.add(e);
        }

        for (int i = 0; i < res.size() - 1; i++) {
            System.out.print(res.get(i) + " ");
        }
        System.out.print(res.get(res.size() - 1));


    }


}
