package byteb.nptel.week2;

import java.util.Arrays;

public class Troublesort {

    public static String TroubleSort(int n, int[] arr) {
        int[] odd;
        int[] even;

        int oddSize = n / 2;
        int evenSize;

        if (n % 2 == 0) {
            evenSize = n / 2;
        } else {
            evenSize = (n + 1) / 2;
        }

        even = new int[evenSize];
        odd = new int[oddSize];


        int oddIndex = 0;
        for (int i = 1; i < n; i += 2) {
            odd[oddIndex++] = arr[i];
        }

        int evenIndex = 0;
        for (int i = 0; i < n; i += 2) {
            even[evenIndex++] = arr[i];
        }


        Arrays.parallelSort(even);
        Arrays.parallelSort(odd);

        evenIndex = 0;
        oddIndex = 0;

        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                arr[i] = even[evenIndex++];
            } else {
                arr[i] = odd[oddIndex++];
            }
        }

        for (int i = 0; i < n - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                return String.valueOf(i);
            }
        }
        return "OK";


    }

}
