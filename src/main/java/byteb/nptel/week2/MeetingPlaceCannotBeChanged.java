package byteb.nptel.week2;

import java.text.DecimalFormat;
import java.util.Scanner;

public class MeetingPlaceCannotBeChanged {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] speed = new int[n];
        int[] location = new int[n];

        double start = 0;
        double end = Double.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            location[i] = sc.nextInt();
            end = Math.max(end, location[i]);
        }

        for (int i = 0; i < n; i++) {
            speed[i] = sc.nextInt();
        }

        while (start <= end) {
            double mid = (start + end) / 2;

            if (meetingPossible(mid, location, speed)) {
                end = mid - 0.0000001;
            } else {
                start = mid + 0.0000001;
            }


        }
        DecimalFormat df = new DecimalFormat("#.######");
        System.out.println(df.format(end));


    }


    private static boolean meetingPossible(double time, int[] location, int[] speed) {
        int n = location.length;
        double right = location[0] + speed[0] * time;
        double left = location[0] - speed[0] * time;
        for (int i = 1; i < n; i++) {
            double rightTravel = location[i] + speed[i] * time;
            double leftTravel = location[i] - speed[i] * time;

            right = Math.min(right, rightTravel);
            left = Math.max(leftTravel, left);
        }

        return left <= right;
    }

}
