package byteb.nptel.week2;

import java.util.Scanner;

public class MagicShip {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long x = sc.nextInt();
        long y = sc.nextInt();

        long x1 = sc.nextInt();
        long y1 = sc.nextInt();

        int w = sc.nextInt();
        //Needed to remove next line after nextInt
        sc.nextLine();


        char[] weather = sc.nextLine().toCharArray();

        long[][] weatherTable = new long[w + 1][2];
        weatherTable[0] = new long[]{0, 0};

        for (int i = 0; i < w; i++) {
            switch (weather[i]) {
                case ('U') -> weatherTable[i + 1] = new long[]{weatherTable[i][0], weatherTable[i][1] + 1};
                case ('R') -> weatherTable[i + 1] = new long[]{weatherTable[i][0] + 1, weatherTable[i][1]};
                case ('D') -> weatherTable[i + 1] = new long[]{weatherTable[i][0], weatherTable[i][1] - 1};
                case ('L') -> weatherTable[i + 1] = new long[]{weatherTable[i][0] - 1, weatherTable[i][1]};
            }
        }


        //Starting and destination will always be different
        long start = 1;
        long end = (long) Math.pow(10,15);
        boolean ansFound = false;

        while (start <= end) {
            long mid = end - (end - start) / 2;

            if (ifYouCanReachWithinKDays(mid, x, y, x1, y1, weatherTable, w)) {
                ansFound = true;
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        if (!ansFound) {
            System.out.println(-1);
        } else {
            System.out.println(end + 1);
        }


    }


    private static boolean ifYouCanReachWithinKDays(long k, long x, long y, long x1, long y1, long[][] weatherTable, int w) {

        long weatherCycles = (k / w);
        int remainingWeatherCycles = (int) (k % w);

        x = x + weatherCycles * weatherTable[w][0] + weatherTable[remainingWeatherCycles][0];
        y = y + weatherCycles * weatherTable[w][1] + weatherTable[remainingWeatherCycles][1];

        //If after k days destination is within k Euclidean distance then we are good to go
        long eucledianDistance = getManhattanDistance(x, y, x1, y1);

        return eucledianDistance <= k;

    }

    private static long getManhattanDistance(long x, long y, long x1, long y1) {
        return Math.abs(x - x1) + Math.abs(y - y1);
    }


}
