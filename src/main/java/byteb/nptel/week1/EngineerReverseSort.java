package byteb.nptel.week1;

import byteb.utils.Utils;

public class EngineerReverseSort {

    public static String engineerReverseSort(int cost, int size) {
        if (cost > ((size + 2) * (size - 1) / 2) || cost < (size - 1)) {
            return "IMPOSSIBLE";
        }
        return engineerReverseSortRecursive(cost, size, 1);
    }

    private static String engineerReverseSortRecursive(int cost, int size, int min) {
        if (size == 1) {
            return String.valueOf(min);
        }
        //If the cost is already in good range for size - 1 recursive call
        if (cost - 1 >= size - 2 && cost - 1 <= (size + 1) * (size - 2) / 2) {
            var arr = engineerReverseSortRecursive(cost - 1, size - 1, min + 1);
            return min + " " + arr;
        } else {
            var needToShave = cost - (size + 1) * (size - 2) / 2;
            var arr = min + " " + engineerReverseSortRecursive(cost - needToShave, size - 1, min + 1);
            String[] sarr = arr.split(" ");
            Utils.reverseArray(sarr, 0, needToShave - 1);
            return String.join(" ", sarr);
        }
    }
}
