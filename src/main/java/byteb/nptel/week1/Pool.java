package byteb.nptel.week1;

import java.util.Arrays;
import java.util.Scanner;

public class Pool {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int rows = sc.nextInt();
        int cols = sc.nextInt();
        int steps = sc.nextInt();
        int x = sc.nextInt();

        int sizeOfArray = (rows + cols) * 2 - 4;

        steps = steps % sizeOfArray;

        int[][] mat = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                mat[i][j] = sc.nextInt();
            }
        }

        int[] arr = new int[sizeOfArray];

        matrixToArray(arr, mat);
        rotateArray(arr, steps);
        System.out.println(arrayToMatrix(arr, mat, x));
    }

    private static void matrixToArray(int[] arr, int[][] twoDArr) {
        int cols = twoDArr[0].length;
        int rows = twoDArr.length;
        int arrIndex = 0;

        //Top row
        for (int i = 0; i < cols - 1; i++) {
            arr[arrIndex++] = twoDArr[0][i];
        }

        //Right column
        for (int i = 0; i < rows - 1; i++) {
            arr[arrIndex++] = twoDArr[i][cols - 1];
        }

        //Bottom row
        for (int i = cols - 1; i > 0; i--) {
            arr[arrIndex++] = twoDArr[rows - 1][i];
        }

        //Left Column
        for (int i = rows - 1; i > 0; i--) {
            arr[arrIndex++] = twoDArr[i][0];
        }

    }

    private static StringBuilder arrayToMatrix(int[] arr, int[][] twoDArr, int x) {
        StringBuilder sb = new StringBuilder();

        int cols = twoDArr[0].length;
        int rows = twoDArr.length;
        int arrIndex = 0;

        //Top row
        for (int i = 0; i < cols - 1; i++) {
            twoDArr[0][i] = arr[arrIndex++];
            if (i % x == 0) {
                sb.append(arr[arrIndex - 1]);
                sb.append(" ");
            }
        }

        //Right column
        for (int i = 0; i < rows - 1; i++) {
            twoDArr[i][cols - 1] = arr[arrIndex++];
            if ((i + cols - 1) % x == 0) {
                sb.append(arr[arrIndex - 1]);
                sb.append(" ");
            }
        }

        //Bottom row
        for (int i = cols - 1; i > 0; i--) {
            twoDArr[rows - 1][i] = arr[arrIndex++];
            if ((i + rows - 1) % x == 0) {
                sb.append(arr[arrIndex - 1]);
                sb.append(" ");
            }
        }

        //Left Column
        for (int i = rows - 1; i > 0; i--) {
            twoDArr[i][0] = arr[arrIndex++];
            if ((i) % x == 0) {
                sb.append(arr[arrIndex - 1]);
                sb.append(" ");
            }
        }

        return sb;

    }

    private static void rotateArray(int[] arr, int r) {
        int[] temp = Arrays.copyOf(arr, arr.length);
        int arrIndex = r;
        for (int i = 0; i < arr.length; i++) {
            arr[arrIndex++] = temp[i];
            arrIndex = arrIndex % arr.length;
        }
    }

}
