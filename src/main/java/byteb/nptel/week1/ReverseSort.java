package byteb.nptel.week1;

import byteb.utils.Utils;

public class ReverseSort {

    public static int reverseSort(int[] arr) {
        int n = arr.length;
        int cost = 0;
        //Arr.length - 1, the last index is guaranteed to be sorted, as per nature of reverse sort algorithm
        for (int i = 0; i < n - 1; i++) {
            int min = arr[i];
            int minIndex = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < min) {
                    minIndex = j;
                    min = arr[j];
                }
            }
            cost += minIndex - i + 1;
            Utils.reverseArray(arr, i, minIndex);
        }
        return cost;
    }


}
