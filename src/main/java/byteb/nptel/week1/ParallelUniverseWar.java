package byteb.nptel.week1;

import java.util.Arrays;
import java.util.Scanner;

public class ParallelUniverseWar {
    private final static String Yudhisthira = "Yudhisthira";
    private final static String Duryodhana = "Duryodhana";
    private final static String Draw = "Draw";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int t = sc.nextInt();

        while (t-- > 0) {
            int n = sc.nextInt();
            int m = sc.nextInt();

            long[] yud = new long[n];
            long[] dur = new long[m];

            for (int i = 0; i < n; i++) yud[i] = sc.nextLong();
            for (int i = 0; i < m; i++) dur[i] = sc.nextLong();

            Arrays.sort(yud);
            Arrays.sort(dur);

            int y = 0;
            int d = 0;

            while (y < n && d < m) {
                if (yud[y] > dur[d]) {
                    yud[y] -= dur[d];
                    d++;
                } else if (yud[y] < dur[d]) {
                    dur[d] -= yud[y];
                    y++;
                } else {
                    y++;
                    d++;

                }

            }
            if (y == n && d == m) {
                System.out.println(Draw);
            } else if (y == n) {
                System.out.println(Duryodhana);
            } else {
                System.out.println(Yudhisthira);
            }

        }
    }
}
