package byteb.matrix;

import java.util.HashSet;
import java.util.List;

public class SubMatrixToTargetSum {

    private int[][] mat;
    private int[][] presum;
    private int target;
    private HashSet<List<Integer>> calculated;

    public int numSubmatrixSumTarget(int[][] matrix, int target) {
        //Recursive
        return sumRecursive(matrix, target);
    }

    private int sumRecursive(int[][] matrix, int target) {
        this.mat = matrix;
        this.target = target;
        calculated = new HashSet<>();
        this.presum = new int[matrix.length][matrix[0].length];
        presum[0][0] = mat[0][0];
        for (int i = 1; i < matrix[0].length; i++) {
            presum[0][i] = matrix[0][i] + presum[0][i - 1];
        }

        for (int i = 1; i < mat.length; i++) {
            presum[i][0] = matrix[i][0] + presum[i - 1][0];
        }


        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix[0].length; j++) {
                presum[i][j] = presum[i - 1][j] + presum[i][j - 1] - presum[i - 1][j - 1] + matrix[i][j];
            }
        }
        return recursiveHelper(0, 0, 0, 0, mat[0][0]);
    }

    private int recursiveHelper(int x1, int x2, int y1, int y2, int currSum) {

        //Trying to do a sliding window over the whole matrix
        if (x2 >= mat.length || x1 < -1 || y2 >= mat[0].length || y1 < -1 || x1 > x2 || y1 > y2) {
            return 0;
        }

        var key = List.of(x1, x2, y1, x2);
        if (calculated.contains(key)) {
            return 0;
        }

        int count = 0;
        if (currSum == target) {
            count++;
        }


        //Remove the top row
        int removeTopRow = recursiveHelper(x1 + 1, x2, y1, y2, getSubMatrixSum(x1 + 1, x2, y1, y2));

        //Add new bottom row
        int addBottomRow = 0;
        if (x2 + 1 != mat.length) {
            addBottomRow = recursiveHelper(x1, x2 + 1, y1, y2, getSubMatrixSum(x1, x2 + 1, y1, y2));
        }

        //Remove left column
        int removeLeftColumn = recursiveHelper(x1, x2, y1 + 1, y2, getSubMatrixSum(x1, x2, y1 + 1, y2));

        //Add right column
        int addRightColumn = 0;
        if (y2 + 1 != mat[0].length) {
            addRightColumn = recursiveHelper(x1, x2, y1, y2 + 1, getSubMatrixSum(x1, x2, y1, y2 + 1));
        }
        calculated.add(key);
        return count + removeTopRow + addBottomRow + removeLeftColumn + addRightColumn;

    }

    private int getSubMatrixSum(int x1, int x2, int y1, int y2) {
        int sum = presum[x2][y2];
        int colRemoveComponent = x1 == 0 ? 0 : presum[x1 - 1][y2];
        int rowRemoveComponent = y1 == 0 ? 0 : presum[x2][y1 - 1];
        int commonComponent = x1 == 0 || y1 == 0 ? 0 : presum[x1 - 1][y1 - 1];
        return sum + commonComponent - rowRemoveComponent - colRemoveComponent;
    }

}
