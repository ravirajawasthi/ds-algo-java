package byteb.strings.and.arrays;

//https://leetcode.com/problems/string-compression/

public class StringCompression {
    public static void main(String[] args) {
        System.out.println("compress(\"hi\".toCharArray()) = " + compress("aa".toCharArray()));
    }

    public static int compress(char[] chars) {
        char currCharacter;
        char previousCharacter = chars[ 0 ];
        StringBuilder res = new StringBuilder();
        int count = 1;
        for (int i = 1 ; i < chars.length ; i++) {
            currCharacter = chars[ i ];
            if (currCharacter != previousCharacter) {
                res.append(previousCharacter);
                if (count != 1) {
                    res.append(count);
                }
                count = 1;
            } else {
                count++;
            }
            previousCharacter = currCharacter;
        }
        res.append(previousCharacter);
        if (count != 1) res.append(count);
        if (res.length() <= chars.length){
            for (int i = 0; i < res.length(); i++){
                chars[i] = res.charAt(i);
            }
        }
        return res.length();
    }
}
