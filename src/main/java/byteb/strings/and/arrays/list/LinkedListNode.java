package byteb.strings.and.arrays.list;

public class LinkedListNode {
    private String key;

    public LinkedListNode(String key, String value, LinkedListNode previous, LinkedListNode next) {
        this.key = key;
        this.value = value;
        this.previous = previous;
        this.next = next;
    }

    private String value;

    private LinkedListNode previous;

    private LinkedListNode next;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LinkedListNode getPrevious() {
        return previous;
    }

    public void setPrevious(LinkedListNode previous) {
        this.previous = previous;
    }

    public LinkedListNode getNext() {
        return next;
    }

    public void setNext(LinkedListNode next) {
        this.next = next;
    }
}
