package byteb.strings.and.arrays.list;

public class LinkedListImpl {

    private LinkedListNode head;
    private LinkedListNode end;
    private int currentLength;

    public LinkedListImpl() {
        head = null;
        end = null;
        currentLength = 0;
    }

    public LinkedListNode add(String key, String value) {
        if (currentLength == 0 && head == null) {
            //LinkedList is empty
            head = new LinkedListNode(key, value, null, null);
            end = head;
        } else {
            var newNode = new LinkedListNode(key, value, end, null);
            end.setNext(newNode);
            end = newNode;
        }
        currentLength += 1;
        return end;
    }

    public LinkedListNode find(String data) {
        var currNode = head;
        LinkedListNode foundNode = null;

        while (currNode != null) {
            if (currNode.getKey().equals(data)) {
                foundNode = currNode;
                break;
            }
            currNode = currNode.getNext();
        }
        return foundNode;
    }

    public LinkedListNode get(int i) {
        if (i >= currentLength) {
            return null;
        }
        var nodeToReturn = head;
        var currIndex = 0;

        while (currIndex != i) {
            currIndex++;
            nodeToReturn = nodeToReturn.getNext();
        }
        return nodeToReturn;
    }

}



