package byteb.strings.and.arrays;// Java program for above approach

import java.util.ArrayList;

public class AllPossibleStringWithSpaces {
    private static ArrayList<String> spaceString(String str) {

        ArrayList<String> strs = new ArrayList<>();

        // Check if str.length() is 1
        if (str.length() == 1) {
            strs.add(str); return strs;
        }

        ArrayList<String> strsTemp = spaceString(str.substring(1));

        // Iterate over strsTemp
        for (String s : strsTemp) {
            strs.add(str.charAt(0) + s); strs.add(str.charAt(0) + " " + s);
        }

        // Return strs
        return strs;
    }

    // Driver Code
    public static void main(String[] args) {
        ArrayList<String> patterns;

        // Function Call
        patterns = spaceString("ABCD");

        // Print patterns
        for (String s : patterns) {
            System.out.println(s);
        }
    }
}
