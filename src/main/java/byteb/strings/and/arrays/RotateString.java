package byteb.strings.and.arrays;

//https://leetcode.com/problems/rotate-string/
public class RotateString {
    public static void main(String[] args) {
        System.out.println("rotateString(\"waterbottle\", \"erbottlewat\") = " + rotateString("bbbacddceeb",
                                                                                              "ceebbbbacdd"));
    }

    public static boolean rotateString(String s, String goal) {
        if (s.length() != goal.length()) return false;
        var res = s+s;
        return res.contains(goal);
    }
}
