package byteb.strings.and.arrays;

import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ContainsDuplicate {
    public static void main(String[] args) {
        System.out.println(containsDuplicate(new int[]{ 1, 2, 3, 1 }));
    }

    public static boolean containsDuplicate(int[] nums) {
        Set<Integer> set = ConcurrentHashMap.newKeySet();

        return Arrays.stream(nums).parallel().anyMatch(n -> {
            if (set.contains(n)) {
                return true;
            }
            set.add(n);
            return false;
        });


    }

}
