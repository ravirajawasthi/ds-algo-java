package byteb.strings.and.arrays;

//https://leetcode.com/problems/rotate-image/
public class RotateImage {
    public static void main(String[] args) {
        rotate(new char[][]{
                { 'a', 'b', 'c', 'd' },
                { 'e', 'f', 'g', 'h' },
                { 'i', 'j', 'k', 'l' },
                { 'm', 'n', 'o', 'p' }
        });
    }

    public static void rotate(char[][] matrix) {
        int rows = matrix.length;
        int columns = matrix[ 0 ].length;
        char bucket;

        for (int i = 0 ; i < rows ; i++) {
            for (int j = i ; j < columns ; j++) {
                bucket = matrix[ j ][ i ];
                matrix[ j ][ i ] = matrix[ i ][ j ];
                matrix[ i ][ j ] = bucket;

            }
        }

        for (int i = 0 ; i < rows ; i++) {
            for (int j = 0 ; j < columns / 2 ; j++) {
                bucket = matrix[ i ][ columns - 1 - j ];
                matrix[ i ][ columns - 1 - j ] = matrix[ i ][ j ];
                matrix[ i ][ j ] = bucket;
            }
        }
    }
}
