package byteb.strings.and.arrays;

import java.util.ArrayDeque;
import java.util.Deque;

//https://leetcode.com/problems/maximum-nesting-depth-of-the-parentheses

public class MaximumNestingDepthOfTheParentheses {

    private static final char OPENING = '(';
    private static final char CLOSING = ')';

    public static void main(String[] args) {
        System.out.println("maxDepth(\"(1+(2*3)+((8)/4))+1\") = " + maxDepth("(1+(2*3)+((8)/4))+1"));
    }

    private static int maxDepth(String s) {
        int ans = 0;
        Deque<Character> opening = new ArrayDeque<>();
        //We can maybe not use Deque, since we know the given string is a valid parenthesis string.
        for (char c : s.toCharArray()) {
            if (c == OPENING) {
                opening.push(c);
                ans = Math.max(opening.size(), ans);
            } else if (c == CLOSING) {
                opening.pop();
            }
        }
        return ans;
    }
}
