package byteb.strings.and.arrays;

import java.util.HashMap;
import java.util.Map;

public class RomanToInt {

    private static final HashMap<Character, Integer> ROMAN = new HashMap<>(Map.of('I', 1,
                                                                                  'V', 5,
                                                                                  'X', 10,
                                                                                  'L', 50,
                                                                                  'C', 100,
                                                                                  'D', 500,
                                                                                  'M', 1000));


    public static void main(String[] args) {
        System.out.println("romanToInt(\"DCXXI\") = " + romanToInt("DCXXI"));
    }

    public static int romanToInt(String s) {
        int ans = 0;
        char currChar;
        for (int i = 0 ; i < s.length() ; i++) {
            currChar = s.charAt(i);
            if (currChar == 'I' || currChar == 'X' || currChar == 'C') {
                if (i + 1 == s.length()) {
                    ans += ROMAN.get(currChar);
                } else if (s.charAt(i + 1) != currChar && ROMAN.get(s.charAt(i + 1)) > ROMAN.get(currChar)) {
                    ans += ROMAN.get(s.charAt(i + 1)) - ROMAN.get(currChar);
                    i++; // next char is already counted for
                } else {
                    ans += ROMAN.get(currChar);
                }
            } else {
                ans += ROMAN.get(currChar);
            }
        }
        return ans;
    }
}
