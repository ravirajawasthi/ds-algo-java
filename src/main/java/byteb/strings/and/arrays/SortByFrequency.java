package byteb.strings.and.arrays;

import java.util.Collections;

//https://leetcode.com/problems/sort-characters-by-frequency/
//https://practice.geeksforgeeks.org/problems/sorting-elements-of-an-array-by-frequency/0
public class SortByFrequency {
    protected static final char[] lookup = new char[]{ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9' };

    public static void main(String[] args) {
        System.out.println("frequencySort(\"2a554442f544asfasssffffasss\") = " + frequencySort("2a554442f544asfasssffffasss"));
    }

    public static String frequencySort(String s) {


        int[] alphabets = new int[ 62 ];
        for (char c : s.toCharArray()) {
            if (c > 96) {
                alphabets[ c - 71 ]++;
            } else if (c > 64) {
                alphabets[ c - 65 ]++;
            } else {
                alphabets[ c + 4 ]++;
            }
        }

        int filled = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ; i < 62 ; i++) {
            var curMax = 0;
            var currMaxIndex = -1;
            for (int j = 0 ; j < 62 ; j++) {
                if (alphabets[ j ] >= curMax) {
                    curMax = alphabets[ j ];
                    currMaxIndex = j;
                }

            }

            filled += curMax;
            sb.append(String.join("", Collections.nCopies(curMax, String.valueOf(lookup[ currMaxIndex ]))));
            if (filled == s.length()) {
                break;
            }
            alphabets[ currMaxIndex ] = -1;
        }
        return sb.toString();

    }

}
