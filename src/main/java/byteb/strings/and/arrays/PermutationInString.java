package byteb.strings.and.arrays;

import java.util.stream.IntStream;

class PermutationInString {
    public static void main(String[] args) {
        var s1 = "ab";
        var s2 = "eidboaoo";

        System.out.println("checkInclusion(s1, s2) = " + checkInclusion(s1, s2));

    }

    public static boolean checkInclusion(String s1, String s2) {

        if (s1.length() > s2.length()) {
            return false;
        }

        var s1FrequencyMap = customStringHashMap(s1);

        var notSame = false;
        //j<= s2.length because, in substring method j is not inclusive
        for (int i = 0, j = s1.length() ; j <= s2.length() ; i++, j++) {
            var s2Frequency = customStringHashMap(s2.substring(i, j));

            notSame = IntStream.range(0, 26).anyMatch(index -> s2Frequency[ index ] != s1FrequencyMap[ index ]);

            if (!notSame){
                return true;
            }
        }

        return false;
    }

    private static int[] customStringHashMap(String str) {
        int[] alphabetsFrequency = new int[ 26 ]; //Represents all alphabets
        for (char ch : str.toCharArray()) {
            //97 -> 'a', 122 -> 'z'
            alphabetsFrequency[ ch - 97 ] += 1;
        }

        return alphabetsFrequency;
    }
}