package byteb.strings.and.arrays;

import java.util.stream.IntStream;

//https://www.codingninjas.com/codestudio/problems/one-away_1171178

public class OneAway {
    public static void main(String[] args) {
        System.out.println("isOneAway(\"abc\", \"ac\") = " + isOneAway("abc", "ac"));
    }

    public static boolean isOneAway(String source, String target) {

        if (Math.abs(source.length() - target.length()) > 1) {
            return false;
        }

        // Padding with empty space to make comparision easy
        var bigger = Math.max(source.length(), target.length());
        String sourcePadded = source + repeat(bigger - source.length());
        String targetPadded = target + repeat(bigger - target.length());

        String edit = "";

        int diffFound = 0;

        for (int i = 0, j = 0; i < bigger && j < bigger; i++, j++) {
            if (sourcePadded.charAt(i) != targetPadded.charAt(j)) {
                diffFound++;
                if (diffFound > 1) {
                    return false;
                }

                // Delete is required
                if (source.length() > target.length()) {
                    edit = "delete";
                    j--;
                }
                // Replace is required
                else if (source.length() == target.length()) {
                    edit = "replace";
                }
                // Insert is required
                else {
                    edit = "insert";
                    i--;
                }
            }
        }

        System.out.println(edit.length() == 0 ? "same" : edit);
        return true;
    }

    private static String repeat(int frequency){
        StringBuilder sb = new StringBuilder(" ");
        IntStream.range(0, frequency).forEach(n -> sb.append(" "));
        return sb.toString();

    }

}