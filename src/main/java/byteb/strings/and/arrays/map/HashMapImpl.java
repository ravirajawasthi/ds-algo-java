package byteb.strings.and.arrays.map;

import byteb.strings.and.arrays.list.LinkedListImpl;

public class HashMapImpl {

    private int internalArrayLength;

    LinkedListImpl[] internalHashMapArray;

    public HashMapImpl(int initialSize) {
        internalArrayLength = initialSize;
        internalHashMapArray = new LinkedListImpl[ initialSize ];
    }

    public void put(String key, String value) {
        var index = getHashCode(key);
        if (internalHashMapArray[ index ] == null) {
            var head = new LinkedListImpl();
            head.add(key, value);
            internalHashMapArray[ index ] = head;
        } else {
            internalHashMapArray[ index ].add(key, value);
        }
    }

    public String get(String key) {
        var index = getHashCode(key);
        if (internalHashMapArray[ index ] != null) {
            var foundNode = internalHashMapArray[ index ].find(key);
            if (foundNode != null) {
                return foundNode.getValue();
            }
        }
        return null;
    }

    private int getHashCode(String key) {
        return Math.abs(key.hashCode()) % internalArrayLength;
    }

}
