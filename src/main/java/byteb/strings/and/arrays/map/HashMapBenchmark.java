package byteb.strings.and.arrays.map;

import byteb.utils.Utils;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.UUID;
import java.util.stream.IntStream;

public class HashMapBenchmark {

    private HashMapBenchmark() {
        //static methods, no need of constructor
    }

    private static final SecureRandom random = new SecureRandom();
    private static final int BENCHMARK_SIZE = 50_000;
    //Tested upto 1_000_000. even after collisions custom linked list works fine.

    public static void execute() throws Exception {
        String[] randomWords = new String[ BENCHMARK_SIZE ];
        int[] randomValues = new int[ BENCHMARK_SIZE ];

        var hashMap = new HashMap<>();
        var customHashMap = new HashMapImpl(10_000);

        IntStream.range(0, BENCHMARK_SIZE).forEach(num -> {
            randomWords[ num ] = UUID.randomUUID().toString(); //Because currently HashMap and LinkedList are both
            // not designed to work with same keys
            randomValues[ num ] = random.nextInt();
        });

        Utils.timeIt("Custom hashmap benchmark inserting values test", () -> {
            IntStream.range(0, BENCHMARK_SIZE).forEach(num -> customHashMap.put(randomWords[ num ],
                                                                                String.valueOf(randomValues[ num ])));
            return null;
        });

        Utils.timeIt("HashMap benchmark inserting values test", () -> {
            IntStream.range(0, BENCHMARK_SIZE).forEach(num -> hashMap.put(randomWords[ num ],
                                                                          String.valueOf(randomValues[ num ])));
            return null;
        });

        Utils.timeIt("Custom hashmap benchmark getting values test", () -> {
            IntStream.range(0, BENCHMARK_SIZE).forEach(num -> customHashMap.get(randomWords[ num ]));
            return null;
        });

        Utils.timeIt("HashMap benchmark get values test", () -> {
            IntStream.range(0, BENCHMARK_SIZE).forEach(num -> hashMap.get(randomWords[ num ]));
            return null;
        });


        var correctness = true;
        for (int i = 0 ; i < BENCHMARK_SIZE ; i++) {
            if (!( hashMap.get(randomWords[ i ]).equals(customHashMap.get(randomWords[ i ])) )) {
                correctness = false;
                break;
            }
        }
        if (correctness) System.out.println("Custom hashmap implementation is correct!");
        else System.out.println("Custom hashmap implementation is incorrect!");


    }

}
