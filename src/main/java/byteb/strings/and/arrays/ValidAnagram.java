package byteb.strings.and.arrays;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ValidAnagram {
    public static void main(String[] args) {

        var s1 = "rat";
        var s2 = "car";

        System.out.println("isAnagram(s1, s2) = " + isAnagram(s1, s2));

    }

    /*
     *Checks if t is an anagram of s
     */

    public static boolean isAnagram(String s, String t) {
        if (s.length() != t.length())
            return false;

        AtomicInteger distinctElements = new AtomicInteger(); //Represents distinct letters that are still unused

        var map = new ConcurrentHashMap<Character, Integer>();

        s.chars().parallel().forEach(n -> {
            Character currCharacter = (char) n;
            var currCharacterCount = map.getOrDefault(currCharacter, 0);
            if (currCharacterCount == 0) {
                map.put(currCharacter, 1);
                distinctElements.getAndIncrement();
            } else {
                map.put(currCharacter, currCharacterCount + 1);
            }
        });

        var notAnagram = t.chars().parallel().anyMatch(c -> {
            Character currCharacter = (char) c;
            var currCharacterCount = map.getOrDefault(currCharacter, 0);
            if (currCharacterCount == 0) {
                return true;
            } else {
                map.put(currCharacter, currCharacterCount - 1);
                if (currCharacterCount - 1 == 0) distinctElements.getAndDecrement();
            }
            return false;

        });


        return ( !notAnagram && distinctElements.get() == 0 );
    }
}
