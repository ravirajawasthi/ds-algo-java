package byteb.strings.and.arrays;

//https://leetcode.com/problems/set-matrix-zeroes/

import java.util.Arrays;
import java.util.HashSet;

public class MatrixZeros {
    public static void main(String[] args) {
        setZeroes(new int[][]{ { 0, 1, 2, 0 }, { 3, 4, 5, 2 }, { 1, 3, 1, 5 } });
    }

    public static void setZeroes(int[][] matrix) {


        var isRow = false;
        var isCol = false;

        for (int c = 0 ; c < matrix[ 0 ].length ; c++) {
            if (matrix[ 0 ][ c ] == 0) {
                isRow = true;
                break;
            }
        }

        for (int c = 0 ; c < matrix.length ; c++) {
            if (matrix[ c ][ 0 ] == 0) {
                isCol = true;
                break;
            }
        }

        for (int i = 1 ; i < matrix.length ; i++) {
            for (int j = 1 ; j < matrix[ 0 ].length ; j++) {
                if (matrix[ i ][ j ] == 0) {
                    matrix[ 0 ][ j ] = 0;
                    matrix[ i ][ 0 ] = 0;
                }
            }
        }

        //C = 1, because ignoring (0, 0) is very crucial

        for (int c = 1 ; c < matrix.length ; c++) {
            if (matrix[ c ][ 0 ] == 0) {
                Arrays.fill(matrix[ c ], 0);
            }
        }

        for (int c = 1 ; c < matrix[ 0 ].length ; c++) {
            if (matrix[ 0 ][ c ] == 0) {
                for (int i = 0 ; i < matrix.length ; i++) {
                    matrix[ i ][ c ] = 0;
                }
            }
        }

        if (isRow) {
            Arrays.fill(matrix[ 0 ], 0);
        }
        if (isCol) {
            for (int i = 0 ; i < matrix.length ; i++) {
                matrix[ i ][ 0 ] = 0;
            }
        }


    }

}
