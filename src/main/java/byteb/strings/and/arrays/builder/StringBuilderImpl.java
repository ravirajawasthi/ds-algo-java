package byteb.strings.and.arrays.builder;

import java.util.Arrays;

public class StringBuilderImpl {
    private static final int RESIZE_FACTOR = 2;
    private char[] internalMutableArray;
    private int lastWrittenInternalArrayIndex = -1;

    protected int currentInternalCapacity;

    public StringBuilderImpl(String string) {
        int initLength = Math.max(10, string.length() * RESIZE_FACTOR);
        internalMutableArray = new char[ initLength ];
        currentInternalCapacity = initLength;
        for (int i = 0 ; i < string.length() ; i++) {
            internalMutableArray[ i ] = string.charAt(i);

        }
        if (string.length() != 0) lastWrittenInternalArrayIndex += string.length() - 1;
    }

    public void append(String string) {
        if (string.length() + lastWrittenInternalArrayIndex >= currentInternalCapacity) {
            currentInternalCapacity = ( currentInternalCapacity + string.length() ) * RESIZE_FACTOR;
            internalMutableArray = Arrays.copyOf(internalMutableArray, currentInternalCapacity);
        }
        for (int i = lastWrittenInternalArrayIndex + 1, j = 0 ; j < string.length() ; i++, j++) {
            internalMutableArray[ i ] = string.charAt(j);
        }
        lastWrittenInternalArrayIndex += string.length();
    }

    @Override
    public String toString() {
        return new String(Arrays.copyOfRange(internalMutableArray, 0, lastWrittenInternalArrayIndex + 1));
    }

}
