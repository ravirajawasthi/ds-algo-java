package byteb.strings.and.arrays.builder;

import com.github.javafaker.Faker;
import byteb.utils.Utils;

import java.util.stream.IntStream;

public class StringBuilderBenchmark {

    private StringBuilderBenchmark() {
    }

    private static final Faker faker = new Faker();
    public static final int BENCHMARK_SIZE = 30_000;

    public static void execute() throws Exception {
        String[] lotsOfStringsToAppend = new String[ BENCHMARK_SIZE ];
        IntStream.range(0, BENCHMARK_SIZE).forEach(n -> lotsOfStringsToAppend[ n ] = faker.name().name());


        //n2 time complexity
        String stringConcatRes = Utils.timeIt("Timing General String append function", () -> {
            String result = "";
            for (int i = 0 ; i < BENCHMARK_SIZE ; i++) {
                result += lotsOfStringsToAppend[ i ];
            }
            return result;
        });

        String stringBuilderAppendResult = Utils.timeIt("Timing String Builder append function", () -> {
            StringBuilder result = new StringBuilder();
            for (int i = 0 ; i < BENCHMARK_SIZE ; i++) {
                result.append(lotsOfStringsToAppend[ i ]);
            }
            return result.toString();
        });


        String customStringBuilderResponse = Utils.timeIt("Timing Custom String Builder append function", () -> {
            StringBuilderImpl result = new StringBuilderImpl("");
            for (int i = 0 ; i < BENCHMARK_SIZE ; i++) {
                result.append(lotsOfStringsToAppend[ i ]);
            }
            return result.toString();
        });

        if (stringConcatRes.equals(stringBuilderAppendResult) && stringConcatRes.equals(customStringBuilderResponse))
            System.out.println("Custom String Builder response SUCCESS!!");
        else
            System.out.println("Custom String Builder response FAILURE!!");


    }

}
