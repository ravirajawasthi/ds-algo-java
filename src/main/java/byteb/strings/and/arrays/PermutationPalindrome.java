package byteb.strings.and.arrays;


//https://www.codingninjas.com/codestudio/problems/palindrome-permutation_1171180
public class PermutationPalindrome {

    public static void main(String[] args) {
        System.out.println("palindromeString(args) = " + palindromeString("acab"));
    }

    public static boolean palindromeString(String s) {
        var frequencyTable = new int[ 26 ];

        for (char character : s.toCharArray()) {
            frequencyTable[ character - 97 ]++;
        }

        var oneFreqCount = 0;

        for (int num : frequencyTable) {
            if (num % 2 != 0) {
                oneFreqCount++;
                if (oneFreqCount > 1){
                    return false;
                }
            }
        }
        return true;
    }

}
